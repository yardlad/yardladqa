﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class RatingController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /Rating/

        public ViewResult Index()
        {
            var ratings = db.Ratings.Include(r => r.Contractor).Include(r => r.UserProfile);
            return View(ratings.ToList());
        }

        //
        // GET: /Rating/Details/5

        public ViewResult Details(int id)
        {
            Rating rating = db.Ratings.Find(id);
            return View(rating);
        }

        //
        // GET: /Rating/Create

        public ActionResult Create()
        {
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name");
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName");
            return View();
        } 

        //
        // POST: /Rating/Create

        [HttpPost]
        public ActionResult Create(Rating rating)
        {
            if (ModelState.IsValid)
            {
                db.Ratings.Add(rating);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", rating.ContractorId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName", rating.UserId);
            return View(rating);
        }
        
        //
        // GET: /Rating/Edit/5
 
        public ActionResult Edit(int id)
        {
            Rating rating = db.Ratings.Find(id);
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", rating.ContractorId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName", rating.UserId);
            return View(rating);
        }

        //
        // POST: /Rating/Edit/5

        [HttpPost]
        public ActionResult Edit(Rating rating)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rating).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", rating.ContractorId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName", rating.UserId);
            return View(rating);
        }

        //
        // GET: /Rating/Delete/5
 
        public ActionResult Delete(int id)
        {
            Rating rating = db.Ratings.Find(id);
            return View(rating);
        }

        //
        // POST: /Rating/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Rating rating = db.Ratings.Find(id);
            db.Ratings.Remove(rating);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /Rating/Dispute/5

        public ActionResult Dispute(int id)
        {
            var rating = db.Ratings.Where(r => r.RatingId == id).Single();
            return View(rating);
        }

        //
        // POST: /Rating/Dispute/5
        
        [HttpPost, ActionName("Dispute")]
        public ActionResult DisputeConfirm(int id, string reasoning)
        {
            // look up the rating
            var rating = db.Ratings.Where(r => r.RatingId == id).Single();
            var message = "Thank you! Your message has been sent to a Yard Lad admin.";

            // send an email to the admin
            var adminEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

            var user = System.Web.Security.Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();

            var customer = System.Web.Security.Membership.GetUser(rating.UserId);
            string customerUsername = customer.UserName;
            int cUserId = (int)customer.ProviderUserKey;
            var cUserProfile = db.UserProfiles.Where(up => up.UserId == cUserId).Single();
            string mobileNumber = null;

            if (cUserProfile.Mobile != null)
	        {
		        mobileNumber = cUserProfile.Mobile;
	        }
            else
	        {
                mobileNumber = "[ not entered ]";
	        }

            if (db.Disputes.Where(d => d.RatingId == rating.RatingId).SingleOrDefault() == null)
            {
                // only send email if the dispute object doesn't exist - will be created after sending email
                IEmailSender sender = new GoDaddyEmailSender();
                string subject = "Rating Dispute";
                string body = null;

                body = string.Format(@"<div><p>New rating dispute from {0}!</p>
                        <div style='margin: 1em 0;'>
                            <div style='text-decoration: underline;'>Rating Details</div>
                            <p>
                                Rating ID: {1}
                                Value: {2}<br />
                                Comments: {3}<br />
                                Submitted by: {4} on {5}
                            </p>
                        </div>

                        <div style='margin: 1em 0;'>
                            <div style='text-decoration: underline;'>Dispute</div>
                            <p>
                                Username: {6}<br />
                                Name: {7}<br />
                                Reasoning: {8}
                            </p>
                        </div>
    
                        <div style='margin: 1em 0;'>
                            <div style='text-decoration: underline;'>Requested Service Information</div>
                            <p>
                                Requested Service Id: {9}<br />
                                Service: {10}<br />
                                Date Completed: {11}<br />
                                Customer Name: {12}<br />
                                Customer Username: {13}<br />
                                Phone: {14}<br />
                                Mobile: {15}<br />
                            </p>
                        </div>
    
                    </div>",
                    rating.Contractor.Name, rating.RatingId, rating.Value, rating.Comments, rating.UserProfile.FirstName + " " + rating.UserProfile.LastName,
                    rating.DateRated.ToShortDateString(), user.UserName, userProfile.FirstName + " " + userProfile.LastName, reasoning,
                    rating.RequestedServices.FirstOrDefault().RequestedServiceId, rating.RequestedServices.FirstOrDefault().ContractorService.Service.Name,
                    rating.RequestedServices.FirstOrDefault().DateCompleted, rating.UserProfile.FirstName + " " + rating.UserProfile.LastName,
                    customerUsername, rating.UserProfile.Phone, mobileNumber);
            
                try 
	            {	        
		            sender.SendEmail(subject, body, from: adminEmail, recipient: adminEmail);
	            }
	            catch (Exception)
	            {
	                message = "There was an error sending your dispute to the admin. If the error persists, please contact a Yard Lad admin";
	            }
            
                // create the dispute object - only if a dispute for the rating doesn't exist
                YardLad.Models.Domain.Dispute dispute = new Models.Domain.Dispute()
                {
                    RatingId = rating.RatingId,
                    Reasoning = reasoning,
                    ResolutionStatus = "unresolved"
                };

                db.Disputes.Add(dispute);
                db.SaveChanges();
            }

            // return the contractor to their service history with confirmation message
            TempData["message"] = message;
            return RedirectToAction("ServiceHistory", "Contractor", new { id = rating.ContractorId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}