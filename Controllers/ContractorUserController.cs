﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize(Roles = "admin")]
    public class ContractorUserController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /ContractorUser/

        public ViewResult Index(/*string mode = "", int contractorTypeId = 0*/)
        {
            // initialize default data source
            var contractorUsers = db.ContractorUsers.Include(cu => cu.Contractor).Include(cu => cu.my_aspnet_users.UserProfile)
                    .OrderBy(cu => cu.Contractor.IsApproved).ThenByDescending(cu => cu.DateCreated)
                    .ThenBy(cu => cu.Contractor.ContractorType.Type).ThenBy(cu => cu.Contractor.Name).ToList();

            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

            ViewBag.LastLoginDate = userProfile.LastLoginDate;

            return View(contractorUsers);
        }

        //
        // GET: /ContractorUser/Create

        public ActionResult Create()
        {
            // set default values
            Contractor newContractor = new Contractor()
            {
                ApprovedOn = null,
                IsApproved = false,
                IsActive = false, // default to inactive
                DateCreated = DateTime.Now.AddHours(2)
            };

            Address newAddress = new Address()
            {
                IsActive = true
            };

            RegisterViewModel newRegisterModel = new RegisterViewModel()
            {
                Address = newAddress,
                DateOfBirth = DateTime.Now.AddHours(2).AddYears(-18)
            };

            ContractorUserViewModel model = new ContractorUserViewModel()
            {
                Contractor = newContractor,
                UserInfo = newRegisterModel,
                DateCreated = DateTime.Now.AddHours(2)
            };

            ViewBag.ContractorType = new SelectList(db.ContractorTypes.OrderBy(ct => ct.Type), "ContractorTypeId", "Type");
            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name");
            return View(model);
        } 

        //
        // POST: /ContractorUser/Create

        [HttpPost]
        public ActionResult Create(ContractorUserViewModel model)
        {
            // variable to hold user info
            my_aspnet_users user;

            // checks to make sure that the length of the phone number input fields is correct
                if (model.UserInfo.Phone.Length != 14)
                ModelState.AddModelError("", "please enter a valid phone number");

            // checks to make sure that the length of the mobile number input fields is correct
            //I commented out this code because it is validating a nullable field. Everything contained
            //within the if statement != is the orginal code.
            //if(model.UserInfo.Mobile != null)
            //{
            //    if (model.UserInfo.Mobile.Length != 14)
            //    ModelState.AddModelError("", "please enter a valid mobile number");
            //}
            
            // checks to make sure the user is 18 years old
            if (model.UserInfo.DateOfBirth.Date > DateTime.Now.AddHours(2).Date.AddYears(-18))
            {
                ModelState.AddModelError("", "the user must be at least 18 years of age");
            }

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserInfo.Username, model.UserInfo.Password, model.UserInfo.Email, null, null, true, null, out createStatus);
                Roles.AddUserToRole(model.UserInfo.Username, "contractor"); // assign contractor role

                if (createStatus == MembershipCreateStatus.Success)
                {
                    // lookup the user we just created, and store the info in a local variable
                    user = db.my_aspnet_users.Where(u => u.name == model.UserInfo.Username).Single();

                    // create a new address
                    Address newAddress = new Address()
                    {
                        Line1 = model.UserInfo.Address.Line1,
                        Line2 = model.UserInfo.Address.Line2,
                        City = model.UserInfo.Address.City,
                        StateId = model.UserInfo.Address.StateId,
                        PostalCode = model.UserInfo.Address.PostalCode,
                        IsActive = true // ensure it is set to active
                    };

                    db.Addresses.Add(newAddress);

                    // create a new profile
                    UserProfile newProfile = new UserProfile()
                    {
                        UserId = user.id,
                        FirstName = model.UserInfo.FirstName,
                        LastName = model.UserInfo.LastName,
                        DateOfBirth = model.UserInfo.DateOfBirth,
                        Gender = model.UserInfo.Gender,
                        Phone = model.UserInfo.Phone,
                        Mobile = model.UserInfo.Mobile,
                        AcceptSMS = model.UserInfo.AcceptSMS,
                        IsActive = true,
                        Address = newAddress,
                        LastLoginDate = DateTime.Now.AddHours(2)
                    };

                    db.UserProfiles.Add(newProfile);
                    db.SaveChanges();
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }

                // add the contractor
                Contractor newContractor = model.Contractor;
                newContractor.IsActive = false; // set to inactive
                newContractor.DateCreated = DateTime.Now.AddHours(2);
                db.Contractors.Add(newContractor);

                // lookup the user (look up again -- check to see if this variable is null at runtime)
                user = db.my_aspnet_users.Where(u => u.name == model.UserInfo.Username).Single();

                ContractorUser newContractorUser = new ContractorUser()
                {
                    Contractor = newContractor,
                    UserId = user.id,
                    DateCreated = DateTime.Now.AddHours(2),
                    IsActive = true
                };

                // no need to do this because the user profile is associated with the user, not a contractor user
                // newContractorUser.my_aspnet_users.UserProfile = db.UserProfiles.Where(up => up.UserId == user.id).Single();

                db.ContractorUsers.Add(newContractorUser);
                db.SaveChanges();

                // add a tax record for the contractor, set a default tax value
                Tax tax = new Tax()
                {
                    AddressId = (int)db.UserProfiles.Where(up => up.UserId == newContractorUser.UserId).Single().AddressId,
                    ContractorId = newContractorUser.ContractorId,
                    Rate = 8.75M // hard coded default value
                };

                db.Taxes.Add(tax);
                db.SaveChanges();

                // add contractor availability
                ContractorAvailability availability = new ContractorAvailability()
                {
                    ContractorId = newContractor.ContractorId,
                    Monday = true,
                    Tuesday = true,
                    Wednesday = true,
                    Thursday = true,
                    Friday = true,
                    Saturday = true,
                    Sunday = true,
                    HasBeenSet = false,
                    UpdatedOn = DateTime.Now.AddHours(2)
                };

                db.ContractorAvailabilities.Add(availability);
                db.SaveChanges();

                return RedirectToAction("Index");  
            }

            ViewBag.ContractorType = new SelectList(db.ContractorTypes.OrderBy(ct => ct.Type), "ContractorTypeId", "Type");
            ViewBag.State = new SelectList(db.States, "StateId", "Name");
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}