﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    public class ContractorController : Controller
    {
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(ContractorController));
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /Contractor/Details/5

        [Authorize]
        public ViewResult Details(int id, string returnUrl)
        {
            Contractor model = db.Contractors.Include(c => c.ContractorType).Where(c => c.ContractorId == id).SingleOrDefault();

            if (returnUrl != null)
            {
                ViewBag.ReturnUrl = returnUrl;
            }

            return View(model);
        }

        //
        // GET: /Contractor/Edit/5

        [Authorize]
        public ActionResult Edit(int id)
        {
            Contractor model = db.Contractors.Include(c => c.ContractorType).Where(c => c.ContractorId == id).SingleOrDefault();

            ViewBag.ContractorType = new SelectList(db.ContractorTypes, "ContractorTypeId", "Type", model.ContractorTypeId);
            return View(model);
        }

        //
        // POST: /Contractor/Edit/5

        [HttpPost]
        public ActionResult Edit(Contractor model)
        {
            // if the contractor hasn't uploaded any qualifications
            if (model.QualificationId == null || model.Qualification.TaxId == null || model.Qualification.Source == null)
                ModelState.AddModelError("", "you cannot approve this contractor until they have uploaded qualifications");

            if (ModelState.IsValid)
            {
                // if the contractor had never been approved before, and the admin select approve, then update 
                if (model.IsApproved == true && model.ApprovedOn == null)
                {
                    model.ApprovedOn = DateTime.Now.AddHours(2);
                }

                YardLadDevSSEntities ctx = new YardLadDevSSEntities(); // created this because of object manager issues...

                // lookup another instance of the same contractor (pre edit)
                int previousContractorId = ctx.Contractors.Where(c => c.ContractorId == model.ContractorId).Single().ContractorTypeId;

                // if the contractor type was modified, make the approved field false and empty the approved on column null
                if (model.ContractorTypeId != previousContractorId)
                {
                    model.IsApproved = false;
                    model.ApprovedOn = null;

                    // set the new contractor type to the one selected
                    model.ContractorType = db.ContractorTypes.Where(ct => ct.ContractorTypeId == model.ContractorTypeId).Single();

                    // send email to contractor account notifying them of the changes?
                    IEmailSender sender = new GoDaddyEmailSender();
                    
                    // recipient email should be email registered under contractor user
                    // step 1) find user id
                    int userId = db.ContractorUsers.Where(cu => cu.ContractorId == model.ContractorId).Single().UserId;

                    // step 2) acquire necessary info for email
                    string contractorEmail = db.my_aspnet_membership.Where(u => u.userId == userId).Single().Email;
                    var contractorUserProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

                    // step 3) construct body
                    string body = "<p>Hello " + contractorUserProfile.FirstName + ",</p>"
                        + "<p>Your account type has been updated to " + model.ContractorType.Type + ".</p>"
                        + "<p>You will need to submit new qualifications to complete the process. If you did not request these changes, please contact an administrator at YardLad.</p>";

                    string adminEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").FirstOrDefault().Value;

                    // step 4) try to send email
                    try
                    {
                        // will need to change the from address to a yardlad email
                        sender.SendEmail("Account Change", body, from: adminEmail, recipient: contractorEmail);
                    }
                    catch (SmtpFailedRecipientException)
                    {
                        throw new Exception("The email notifying the contractor of changes did not send.");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                // if the contractor's approval status is set to false, clear the approved on field
                if (model.IsApproved == false)
                {
                    model.ApprovedOn = null;
                }

                ctx.Dispose(); // clear up the second context

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", "ContractorUser");
            }

            ViewBag.ContractorTypeId = new SelectList(db.ContractorTypes, "ContractorTypeId", "Type", model.ContractorTypeId);
            return View(model);
        }

        //
        // GET: /ServiceArea/5

        [Authorize]
        public ActionResult ServiceArea(int id) // contractorId
        {
            ServiceAreaEditViewModel model = new ServiceAreaEditViewModel()
            {
                ContractorId = id,
                Contractor = db.Contractors.Where(c => c.ContractorId == id).Single(),
                ServiceAreaId = null
            };

            // they haven't select a service area yet
            if (model.Contractor.ServiceAreaId == null)
            {
                ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name");
                ViewBag.ServiceArea = new SelectList(db.ServiceAreas, "ServiceAreaId", "Name");
                return View(model);
            }

            // they have selected a service area - pull in the values
            model.ServiceAreaId = model.Contractor.ServiceAreaId;
            model.StateId = model.Contractor.ServiceArea.StateId;

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name", model.StateId);
            ViewBag.ServiceArea = new SelectList(db.ServiceAreas.OrderBy(s => s.Name), "ServiceAreaId", "Name", model.ServiceAreaId);
            return View(model);
        }

        //
        // POST: /ServiceArea/5

        [HttpPost]
        public ActionResult ServiceArea(ServiceAreaEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                // lookup the contractor
                var contractor = db.Contractors.Where(c => c.ContractorId == model.ContractorId).Single();
                contractor.ServiceAreaId = model.ServiceAreaId; // change their service area

                // save changes
                db.Entry(contractor).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("MyAccount", "Account");
            }

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name");
            ViewBag.ServiceArea = new SelectList(db.ServiceAreas, "ServiceAreaId", "Name", model.ServiceAreaId);
            return View(model);
        }

        //
        // GET: /Contractor/ServiceHistory/5

        [Authorize]
        public ActionResult ServiceHistory(int id = 0)
        {
            if (TempData["message"] != null)
            {
                // TempData was being saved across browser session, switched from user to contractor account/service history, weird issue (debug later)
                if (TempData["message"].ToString() != "Thanks for rating! Would you like to take an anonymous survey about using Yard Lad Services? Your feedback is appreciated.")
                {
                    ViewBag.Message = TempData["message"];  
                }
                
            }

            if (id == 0)
            {
                // look up the user and find contractor id
                var user = Membership.GetUser();
                var userId = (int)user.ProviderUserKey;
                id = db.ContractorUsers.Where(cu => cu.UserId == userId).SingleOrDefault().ContractorId;
            }

            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            var requestedServices = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId)
                .Where(rs => rs.IsCompleted == false).ToList();
            var completedServices = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId)
                .Where(rs => rs.IsCompleted == true).ToList();

            var requestedServicesGrouped = requestedServices.GroupBy(x => x.DateRequestMade.Date).Distinct()
                .OrderByDescending(x => x.Key.Date).ToList();
            var completedServicesGrouped = completedServices.GroupBy(x => x.DateRequestMade.Date).Distinct()
                .OrderByDescending(x => x.Key.Date).ToList();

            ContractorServiceHistoryViewModel model = new ContractorServiceHistoryViewModel()
            {
                ContractorId = contractor.ContractorId,
                Contractor = contractor,
                RequestedServices = requestedServices,
                GroupedRequestedServices = requestedServicesGrouped,
                GroupedCompletedRequestedServices = completedServicesGrouped,
                IncompleteServices = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Where(rs => rs.IsCompleted != true).Count(),
                CompletedServices = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Where(rs => rs.IsCompleted == true).Count()
            };

            return View(model);
        }

        //
        // GET: /Contractor/Approve/5

        [Authorize]
        public ActionResult Approve(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            if (contractor.IsApproved == false)
            {
                contractor.IsApproved = true;
                contractor.ApprovedOn = DateTime.Now.AddHours(2);

                db.Entry(contractor).State = EntityState.Modified;
                db.SaveChanges();
            }

            // send email notifying the contractor that they've been approved

            return RedirectToAction("Details", new { id = id });
        }

        //
        // GET: /Contractor/Disapprove/5

        [Authorize]
        public ActionResult Disapprove(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            if (contractor.IsApproved == true)
            {
                contractor.IsApproved = false;
                contractor.ApprovedOn = null;

                db.Entry(contractor).State = EntityState.Modified;
                db.SaveChanges();
            }

            // send email notifying the contractor that they have not been approved

            return RedirectToAction("Details", new { id = id });
        }

        //
        // GET: /Contractor/MakePayment/5

        [Authorize]
        public ActionResult MakePayment(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            decimal price = 0.00M;

            if (contractor.ContractorType.Type.ToLower() == "freelance")
            {
                price = Decimal.Parse(db.SiteSettings.Where(ss => ss.Name == "Freelance Contractor Yearly Fee").Single().Value);
            }

            if (contractor.ContractorType.Type.ToLower() == "student")
            {
                price = Decimal.Parse(db.SiteSettings.Where(ss => ss.Name == "Student Contractor Yearly Fee").Single().Value);
            }

            if (contractor.ContractorType.Type.ToLower() == "business")
            {
                price = Decimal.Parse(db.SiteSettings.Where(ss => ss.Name == "Business Professional Yearly Fee").Single().Value);
            }

            ViewBag.ContractorType = contractor.ContractorType;
            ViewBag.Price = price;

            return View(contractor);
        }

        //
        // GET: /Contractor/CheckList/5

        [Authorize]
        public ActionResult CheckList(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            ChecklistViewModel model = new ChecklistViewModel();
            model.ContractorId = id;

            if (contractor.IsActive == false)
            {
                model.IsListed = false;
            }
            else
            {
                model.IsListed = true;
            }

            if (contractor.QualificationId == null) // they haven't submitted any qualifications
            {
                model.HasSubmittedQualifications = false;
            }
            else
            {
                model.HasSubmittedQualifications = true;
                model.QualificationId = (int)contractor.QualificationId;
            }

            if (contractor.IsApproved == true) // the contractor has submitted qualifications and is approved
            {
                model.HasSubmittedQualifications = true;
                model.IsQualified = true;
            }

            if (contractor.ContractorPayments.Count == 0) // they haven't made any payments
            {
                model.HasPaid = false;
                model.HasActiveSubscription = null;
            }

            if (contractor.ContractorPayments.Count > 0) // they have made payment(s) 
            {
                model.HasPaid = true;

                // check expiration status
                var contractorPayment = contractor.ContractorPayments.Where(cp => cp.ContractorId == model.ContractorId).OrderByDescending(cp => cp.DateSubmitted).FirstOrDefault();
                {
                    if (DateTime.Now.AddHours(2) <= contractorPayment.DateSubmitted.AddYears(1)) // they have an active subscription
                    {
                        model.HasActiveSubscription = true;
                    }
                    else // they have an expired subscription
                    {
                        model.HasActiveSubscription = false;
                    }
                }
            }
            
            // check to see if they have set a primary service area
            if (contractor.ServiceAreaId == 0 || contractor.ServiceAreaId == null)
            {
                model.HasSetPrimaryServiceArea = false;
            }
            else
            {
                model.HasSetPrimaryServiceArea = true;
            }

            // check to make sure that they have added at least one service to be offered
            if (contractor.ContractorServices.Count > 0)
            {
                model.HasAddedAService = true;
            }
            else
            {
                model.HasAddedAService = false;
            }

            // check to make sure that they have set their tax rate
            var tax = contractor.Taxes.Where(t => t.ContractorId == model.ContractorId).SingleOrDefault();

            if (tax.SetByContractor == true)
            {
                model.HasSetTax = true;
            }
            else
            {
                model.HasSetTax = false;
            }

            if (db.ContractorAvailabilities.Where(ca => ca.ContractorId == model.ContractorId).Single().HasBeenSet == false)
            {
                model.HasSetAvailability = false;
            }
            else
            {
                model.HasSetAvailability = true;
            }

            if (model.HasSubmittedQualifications == true && model.IsQualified == true && model.HasSetTax == true && 
                model.HasSetPrimaryServiceArea == true && model.HasPaid == true && model.HasActiveSubscription == true && model.IsListed == false)
            {
                model.ReadyToBeListed = true;   
            }

            return View(model);
        }

        //
        // GET: /Contractor/GetListed/5

        [Authorize]
        public ActionResult GetListed(int id)
        {
            bool success = true;
            List<string> errors = new List<string>();

            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            // make sure the contract has everything they need to get listed
            // make them active and return them to their account page

            // they need to have submitted their qualifications and need to have been approved by an admin
            if (contractor.QualificationId == null || contractor.IsApproved == false)
            {
                success = false;
                errors.Add("you need to make sure you have submitted your qualifications and have been approved by an admin");
            }

            if (DateTime.Now.AddHours(2) >= contractor.Qualification.DateSubmitted.AddYears(1))
            {
                success = false;
                errors.Add("please resubmit your qualifications");
            }

            // they need to have updated their tax information
            var tax = db.Taxes.Where(t => t.ContractorId == id).Single();
            if (tax.SetByContractor == false)
            {
                success = false;
                errors.Add("please set your tax rate information");
            }

            // they need to have added at least one service (contractor service)
            if (contractor.ContractorServices.Count == 0)
            {
                success = false;
                errors.Add("you need to have at least one service added before you can be listed");
            }

            // they have to have set their primary service area
            if (contractor.ServiceAreaId == null || contractor.ServiceAreaId == 0)
            {
                errors.Add("please select your primary service area");
                success = false;
            }

            // they need to have paid
            if (contractor.ContractorPayments.Count == 0)
            {
                errors.Add("please submit your first payment");
                success = false;
            }

            // they need to have an active subscription
            if (DateTime.Now.AddHours(2) > db.ContractorPayments.Where(cp => cp.ContractorId == id).OrderByDescending(cp => cp.DateSubmitted).FirstOrDefault().DateSubmitted.AddYears(1))
            {
                errors.Add("your subscription has expired, please submit another payment");
                success = false;
            }

            if (success == false)
            {
                TempData["Errors"] = errors;
                return RedirectToAction("MyAccount", "Account");   
            }

            if (success == true)
            {
                contractor.IsActive = true;
                db.Entry(contractor).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("MyAccount", "Account");
        }

        //
        // GET: /Contractor/ViewListing/5

        [Authorize]
        public ActionResult ViewListing(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            // instantiate a new request model
            ViewListingViewModel model = new ViewListingViewModel();

            model.ContractorId = contractor.ContractorId;
            model.Contractor = contractor;
            model.ContractorServices = db.ContractorServices.Where(cs => cs.ContractorId == model.ContractorId).ToList();
            model.ContractorServiceOptions = db.ContractorServiceOptions.Where(cso => cso.ContractorService.ContractorId == model.ContractorId).ToList();
            model.ContractorServiceOptionItems = db.ContractorServiceOptionItems.Where(csoi => csoi.ContractorServiceOption.ContractorService.ContractorId == model.ContractorId).ToList();
            model.Services = db.Services.ToList();
            model.ServiceAreas = db.ServiceAreas.ToList();
            model.TaxRate = (decimal)db.Taxes.Where(t => t.ContractorId == model.ContractorId).Single().Rate;

            // only show the services they have added (in the ddl)
            HashSet<int> serviceIds = new HashSet<int>(db.ContractorServices.Where(cs => cs.ContractorId == contractor.ContractorId).Select(x => x.ServiceId).ToList());
            model.Services = model.Services.FindAll(x => serviceIds.Contains(x.ServiceId));

            // limit the service area ddl to only show the ones that they have added
            HashSet<int> serviceAreaIds = new HashSet<int>(db.ContractorServiceAreas.Where(cs => cs.ContractorId == contractor.ContractorId).Select(x => x.ServiceAreaId).ToList());
            serviceAreaIds.Add((int)contractor.ServiceAreaId);
            model.ServiceAreas = model.ServiceAreas.FindAll(x => serviceAreaIds.Contains(x.ServiceAreaId));

            // limit the contractors state selection ddl to be the states that they have added service areas for
            HashSet<int> stateIds = new HashSet<int>(db.ContractorServiceAreas.Where(csa => csa.ContractorId == contractor.ContractorId).Select(x => x.ServiceArea.StateId).ToList());
            stateIds.Add((int)contractor.ServiceArea.StateId);
            var states = db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name).ToList().FindAll(x => stateIds.Contains(x.StateId));

            model.StateId = states.FirstOrDefault().StateId;
            model.ServiceAreaId = 0;
            model.ServiceId = model.Services.FirstOrDefault().ServiceId;

            ViewBag.StateId = new SelectList(states, "StateId", "Name", model.StateId); // state ddl
            ViewBag.ServiceAreaId = new SelectList(model.ServiceAreas, "ServiceAreaId", "Name", model.ServiceAreaId); // service area ddl
            ViewBag.ServiceId = new SelectList(model.Services, "ServiceId", "Name", model.ServiceId); // service ddl

            return View(model);
        }

        //
        // GET: /Contractor/AddUser/5

        [Authorize]
        public ActionResult AddUser(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            AddContractorUserViewModel model = new AddContractorUserViewModel()
            {
                ContractorId = id,
                Contractor = db.Contractors.Where(c => c.ContractorId == id).Single(),
                DateOfBirth = DateTime.Now.AddHours(2).AddYears(-18),
                Role = null
            };

            List<string> roles = new List<string>();
            roles.Add("primary admin");
            roles.Add("secondary admin");

            ViewBag.RoleId = new SelectList(roles);

            return View(model);
        }

        //
        // POST: /Contractor/AddUser/5

        [HttpPost]
        public ActionResult AddUser(AddContractorUserViewModel model)
        {
            // checks to make sure that the length of the phone number input fields is correct
            if (model.Phone.Length != 14)
                ModelState.AddModelError("", "please enter a valid phone number");

            if (model.Mobile.Length != 14)
                ModelState.AddModelError("", "please enter a valid mobile number");

            // checks to make sure the user is 18 years old
            if (model.DateOfBirth.Date > DateTime.Now.AddHours(2).Date.AddYears(-18))
            {
                ModelState.AddModelError("", "you must be at least 18 years of age");
            }

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.Username, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success) // if the new user was successfully created
                {
                    // find the user that was just created
                    var user = db.my_aspnet_users.Where(u => u.name == model.Username).Single();

                    // assign role
                    if (model.Role == "primary admin")
                    {
                        Roles.AddUserToRole(user.name, "contractor");
                    }
                    if (model.Role == "secondary admin")
                    {
                        Roles.AddUserToRole(user.name, "contractor2");
                    }

                    // create a new profile, associate values from the model
                    UserProfile newProfile = new UserProfile()
                    {
                        UserId = user.id, // pulled from the local variable
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        DateOfBirth = model.DateOfBirth,
                        Phone = model.Phone,
                        Mobile = model.Mobile,
                        AcceptSMS = model.AcceptSMS, // save this in db for SMS text service
                        Gender = model.Gender,
                        IsActive = true, // set default to true
                        AddressId = null, // set address to null, user will supply this later
                        LastLoginDate = DateTime.Now.AddHours(2)
                    };

                    // add and save the new profile
                    db.UserProfiles.Add(newProfile);
                    db.SaveChanges();

                    // add new contractor user
                    ContractorUser contractorUser = new ContractorUser()
                    {
                        ContractorId = model.ContractorId,
                        DateCreated = DateTime.Now.AddHours(2),
                        IsActive = true,
                        UserId = user.id,
                    };

                    db.ContractorUsers.Add(contractorUser);
                    db.SaveChanges();

                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    // send an email to the new contractor who just signed up
                    string subject = "Thank you for registering!";
                    string body = null;

                    body += "<p>Thank you for registering with Yard Lad!<br />You can now login with your account ";
                    body += "<a href=\"http://www.yardlad.com/Account/MyAccount\">here</a></p>";

                    // send email (will need to replace the emails when deploying)
                    // emailsender.SendEmail(subject, body, "ksexton@technopole.us", "ksexton@technopole.us");

                    string acceptSMS = model.AcceptSMS ? "yes" : "no";

                    // send an email to the admin notifying them about a new contractor registration
                    subject = "New User Registered";
                    body = string.Format("<p>New user registered!</p>" + "<p>Username: {0}<br />Email: {1}<br />"
                        + "Name: {2} {3}<br>Phone: {4}<br>Mobile: {5}<br>Date of Birth: {6:d}<br>Gender: {7}<br>Accepted SMS Text Service: {8}</p>",
                        model.Username, model.Email, model.FirstName, model.LastName,
                        model.Phone, model.Mobile, model.DateOfBirth, model.Gender, acceptSMS);

                    // send email (will need to replace the emails when deploying)
                    // emailsender.SendEmail(subject, body, "ksexton@technopole.us", "ksexton@technopole.us");

                    // else return them to their account overview
                    return RedirectToAction("MyAccount", "Account");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: Contractor/Availability/1

        [Authorize]
        public ActionResult Availability(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            var contractorAvailability = db.ContractorAvailabilities.Where(ca => ca.ContractorId == contractor.ContractorId).Single();

            return View(contractorAvailability);
        }

        //
        // POST: Contractor/Availability/1

        [HttpPost]
        public ActionResult Availability(ContractorAvailability model)
        {
            if (ModelState.IsValid)
            {
                model.UpdatedOn = DateTime.Now.AddHours(2);
                model.HasBeenSet = true;
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("MyAccount", "Account");
            }

            return View(model);
        }

        //
        // GET: Contractor/PayeeInfo/1

        [Authorize]
        public ActionResult PayeeInfo(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            var model = new PayeeCreateViewModel()
            {
                ContractorId = contractor.ContractorId,
            };

            List<string> depositVia = new List<string>();
            depositVia.Add("send a check");
            depositVia.Add("Electronic Funds Transfer (EFT)");
            depositVia.Add("use PayPal");

            ViewBag.DepositVia = new SelectList(depositVia);
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name), "StateId", "Name");

            return View(model);
        }

        //
        // POST: Contractor/PayeeInfo/1

        [HttpPost]
        public ActionResult PayeeInfo(PayeeCreateViewModel model)
        {
            bool hasErrors = false;

            // VALIDATION
            if (model.DepositVia == "send a check")
            {
                if (model.DaytimePhone == null && model.EveningPhone == null)
                {
                    ModelState.AddModelError("", "please make sure to leave a contact phone number");
                    hasErrors = true;
                }
            }

            if (hasErrors == false)
            {
                Payee payee = new Payee()
                {
                    ContractorId = model.ContractorId,
                    DepositVia = model.DepositVia,
                    PayTo = model.PayTo,
                    DaytimePhone = model.DaytimePhone,
                    EveningPhone = model.EveningPhone,
                    Fax = model.Fax,
                    AccountType = model.AccountType,
                    BusinessOrPersonal = model.BusinessOrPersonal,
                    NameOnAccount = model.NameOnAccount,
                    AccountNumber = model.AccountNumber,
                    RoutingNumber = model.RoutingNumber,
                    PayPalEmail = model.PayPalEmail,
                    IsActive = true // default to active
                };

                if (model.Line1 != null)
                {
                    Address address = new Address()
                    {
                        Line1 = model.Line1,
                        Line2 = model.Line2,
                        City = model.City,
                        StateId = model.StateId,
                        PostalCode = model.PostalCode,
                        IsActive = true
                    };

                    db.Addresses.Add(address);
                    payee.AddressId = address.AddressId;
                }

                db.Payees.Add(payee);
                db.SaveChanges();
                return RedirectToAction("MyAccount", "Account");
            }

            List<string> depositVia = new List<string>();
            depositVia.Add("send a check");
            depositVia.Add("Electronic Funds Transfer (EFT)");
            depositVia.Add("use PayPal");

            ViewBag.DepositVia = new SelectList(depositVia);
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name), "StateId", "Name", model.StateId);

            return View(model);
        }

        //
        // GET: /Contractor/EditPayeeInfo/5

        [Authorize]
        public ActionResult EditPayeeInfo(int id)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            var payeeInfo = db.Payees.Where(p => p.ContractorId == contractor.ContractorId).Single();

            var model = new PayeeEditViewModel()
            {
                PayeeId = payeeInfo.PayeeId,
                ContractorId = payeeInfo.ContractorId,
                DepositVia = payeeInfo.DepositVia,
                PayTo = payeeInfo.PayTo,
                DaytimePhone = payeeInfo.DaytimePhone,
                EveningPhone = payeeInfo.EveningPhone,
                Fax = payeeInfo.Fax,
                AccountType = payeeInfo.AccountType,
                BusinessOrPersonal = payeeInfo.BusinessOrPersonal,
                NameOnAccount = payeeInfo.NameOnAccount,
                AccountNumber = payeeInfo.AccountNumber,
                RoutingNumber = payeeInfo.RoutingNumber,
                PayPalEmail = payeeInfo.PayPalEmail
            };

            if (payeeInfo.AddressId != null)
            {
                model.AddressId = (int)payeeInfo.AddressId;
                model.Line1 = payeeInfo.Address.Line1;
                model.Line2 = payeeInfo.Address.Line2;
                model.City = payeeInfo.Address.City;
                model.StateId = payeeInfo.Address.StateId;
                model.PostalCode = payeeInfo.Address.PostalCode;
            }

            List<string> depositVia = new List<string>();
            depositVia.Add("send a check");
            depositVia.Add("Electronic Funds Transfer (EFT)");
            depositVia.Add("use PayPal");

            ViewBag.DepositVia = new SelectList(depositVia, model.DepositVia);
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name), "StateId", "Name", model.StateId);
            ViewBag.DepositViaSelection = model.DepositVia;

            return View(model);
        }

        //
        // POST: /Contractor/EditPayeeInfo/5

        [HttpPost]
        public ActionResult EditPayeeInfo(PayeeEditViewModel model)
        {
            bool hasErrors = false;

            // VALIDATION
            if (model.DepositVia == "send a check")
            {
                if (model.DaytimePhone == null && model.EveningPhone == null)
                {
                    ModelState.AddModelError("", "please make sure to leave a contact phone number");
                    hasErrors = true;
                }
            }

            if (hasErrors == false)
            {
                // look up current payee info
                var payeeInfo = db.Payees.Where(p => p.PayeeId == model.PayeeId).Single();

                payeeInfo.ContractorId = model.ContractorId;
                payeeInfo.DepositVia = model.DepositVia;
                payeeInfo.PayTo = model.PayTo;
                payeeInfo.DaytimePhone = model.DaytimePhone;
                payeeInfo.EveningPhone = model.EveningPhone;
                payeeInfo.Fax = model.Fax;
                payeeInfo.AccountType = model.AccountType;
                payeeInfo.BusinessOrPersonal = model.BusinessOrPersonal;
                payeeInfo.NameOnAccount = model.NameOnAccount;
                payeeInfo.AccountNumber = model.AccountNumber;
                payeeInfo.RoutingNumber = model.RoutingNumber;
                payeeInfo.PayPalEmail = model.PayPalEmail;
                payeeInfo.IsActive = true;

                // the payee info doesn't have an associated address
                // (the user didn't initially select pay via check, but they are switching to pay via check)
                if (model.AddressId == 0 && model.DepositVia == "send a check")
                {
                    // add a new address
                    Address address = new Address()
                    {
                        Line1 = model.Line1,
                        Line2 = model.Line2,
                        City = model.City,
                        StateId = model.StateId,
                        PostalCode = model.PostalCode,
                        IsActive = true
                    };

                    // associate new address with the payee info
                    db.Addresses.Add(address);
                    payeeInfo.AddressId = address.AddressId;
                }

                db.Entry(payeeInfo).State = EntityState.Modified; // indicate that the entity has changed

                // the payee info has an address associated (user first selected pay via check)
                if (model.AddressId != 0 && model.DepositVia == "send a check")
                {
                    var address = db.Addresses.Where(a => a.AddressId == model.AddressId).Single();
                    address.Line1 = model.Line1;
                    address.Line2 = model.Line2;
                    address.City = model.City;
                    address.StateId = model.StateId;
                    address.PostalCode = model.PostalCode;

                    db.Entry(address).State = EntityState.Modified;
                }
                
                db.SaveChanges();
                return RedirectToAction("MyAccount", "Account");
            }

            List<string> depositVia = new List<string>();
            depositVia.Add("send a check");
            depositVia.Add("Electronic Funds Transfer (EFT)");
            depositVia.Add("use PayPal");

            ViewBag.DepositVia = new SelectList(depositVia);
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name), "StateId", "Name", model.StateId);

            return View(model);
        }

        //
        // GET: /Contractor/SendReminder/5

        public JsonResult SendReminder(int id)
        {
            // get contractor/user info
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            var contractorUser = db.ContractorUsers.Where(cu => cu.ContractorId == contractor.ContractorId).Single();
            var userId = contractorUser.UserId;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

            // find email associated with account
            var email = Membership.GetUser(userId, false).Email;

            // setup email
            IEmailSender sender = new GoDaddyEmailSender();
            string subject = "Upload Qualifications";
            string body = null;
            string message = "message sent successfully"; // return message

            body = string.Format("<p>Hello {0},</p>"
                + "<p>You haven't submitted your qualifications yet!<br>You will need to do this before you can be listed in the system.</p>"
            + "<p>Click <a href=\"http://yardlad.design-develop.deploy.com/Account/MyAccount\" target=\"_blank\">here</a> to visit your account</p>", userProfile.FirstName );

            // the db contact email is pulled from the site settings
            var contactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

            try
            {
                // try to send the email (REPLACE LATER WITH EMAIL VARIABLE ABOVE (from contractors user profile)
                sender.SendEmail(subject, body, from: contactEmail, recipient: email);
            }
            catch (Exception ex)
            {
                _logger.Error("there was an error sending the qualification reminder email to contractor: " + contractor.Name, ex);
                message = "an error occurred, please try again later!";
            }

            // return confirmation or error message to admin (on same page)
            return this.Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetServiceAreasByState(int stateId, int contractorId)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == contractorId).Single();

            // limit the service area ddl to only show the ones that they have added
            HashSet<int> serviceAreaIds = new HashSet<int>(db.ContractorServiceAreas.Where(cs => cs.ContractorId == contractor.ContractorId).Select(x => x.ServiceAreaId).ToList());
            serviceAreaIds.Add((int)contractor.ServiceAreaId);

            var serviceAreas = db.ServiceAreas.Where(sa => sa.StateId == stateId).OrderBy(sa => sa.Name).ToList().FindAll(x => serviceAreaIds.Contains(x.ServiceAreaId))
                .Select(x => new
                {
                    ServiceAreaId = x.ServiceAreaId,
                    Name = x.Name
                }).ToList();

            return this.Json(serviceAreas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetServiceDescription(int serviceId, int contractorId)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == contractorId).Single();
            var contractorServices = db.ContractorServices.Where(cs => cs.ContractorId == contractorId).Where(cs => cs.ServiceId == serviceId).Single().Service.Description.ToString();
            var contractorServiceOptions = db.ContractorServiceOptions.Where(cso => cso.ContractorService.ServiceId == serviceId)
                .Where(cso => cso.ContractorService.ContractorId == contractorId).OrderBy(cso => cso.Order).ToList();

            return this.Json(contractorServices, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetServiceOptions(int serviceId, int contractorId)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == contractorId).Single();
            var contractorServices = db.ContractorServices.Where(cs => cs.ContractorId == contractorId).Where(cs => cs.ServiceId == serviceId).Single().Service.Description.ToString();
            var contractorServiceOptions = db.ContractorServiceOptions.Where(cso => cso.ContractorService.ServiceId == serviceId)
                .Where(cso => cso.ContractorService.ContractorId == contractorId).OrderBy(cso => cso.Order).ToList();

            var data = contractorServiceOptions.Select(x => new
            {
                id = x.ContractorServiceOptionId,
                contractorServiceId = x.ContractorServiceId,
                name = x.Name,
                items = x.ContractorServiceOptionItems.OrderBy(z => z.Order).Select(item => new
                {
                    id = item.ContractorServiceOptionItemId,
                    name = item.Name,
                    price = item.Price
                })
                
            });

            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBasePrice(int serviceId, int contractorId)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == contractorId).Single();
            var contractorServices = db.ContractorServices.Where(cs => cs.ContractorId == contractorId).Where(cs => cs.ServiceId == serviceId).Single();

            var data = contractorServices.BasePrice;

            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}