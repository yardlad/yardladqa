﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    [Authorize(Roles = "admin")]
    public class SiteSettingController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /SiteSetting/

        public ViewResult Index()
        {
            return View(db.SiteSettings.OrderBy(ss => ss.Name).ToList());
        }

        //
        // GET: /SiteSetting/Details/5

        public ViewResult Details(int id)
        {
            SiteSetting sitesetting = db.SiteSettings.Find(id);
            return View(sitesetting);
        }

        //
        // GET: /SiteSetting/Edit/5
 
        public ActionResult Edit(int id)
        {
            SiteSetting sitesetting = db.SiteSettings.Find(id);
            return View(sitesetting);
        }

        //
        // POST: /SiteSetting/Edit/5

        [HttpPost]
        public ActionResult Edit(SiteSetting sitesetting)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sitesetting).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sitesetting);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}