﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class AddressController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /Address/Create

        public ActionResult Create()
        {
            AddressCreateViewModel model = new AddressCreateViewModel()
            {
                Address = new Address()
                {
                    IsActive = true // default to active status
                }
            };
            
            // sent from account controller
            if (TempData["createAddressBeforePay"] != null)
            {
                // let the user know that they need to add an address to their profile, and that it is the service address on their account
                ViewBag.Message = "You must add your billing address to your profile, this will be the address used for service requests";
            }

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name");
            return View(model);
        }

        //
        // POST: /Address/Create

        [HttpPost]
        public ActionResult Create(AddressCreateViewModel model, string returnUrl, int userId = 0)
        {
            if (ModelState.IsValid)
            {
                model.Address.IsActive = true; // reinforce the active status of the new address
                db.Addresses.Add(model.Address); // add the address to the database

                if (returnUrl == "User/Details/" && userId != 0)
                {
                    // find current user's profile
                    var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

                    // if the profile has no address associated
                    if (userProfile.AddressId == null)
                    {
                        userProfile.AddressId = model.Address.AddressId; // associate the new address with their profile
                        db.Entry(userProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        return Redirect("~/" + returnUrl + userId); // redirect them back to their edit profile view
                    }
                    else
                    {
                        // update the address already associated with their profile
                        db.Entry(userProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        return Redirect("~/" + returnUrl + userId);
                    }
                }


                // if the user came from their profile (edit view)
                if (returnUrl == "UserProfile/Edit" && userId != 0)
                {
                    // find current user's profile
                    var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

                    // if the profile has no address associated
                    if (userProfile.AddressId == null)
                    {
                        userProfile.AddressId = model.Address.AddressId; // associate the new address with their profile
                        db.Entry(userProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        return Redirect("~/" + returnUrl + "/" + userId); // redirect them back to their edit profile view
                    }
                    else
                    {
                        // update the address already associated with their profile
                        db.Entry(userProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        return Redirect("~/" + returnUrl + "/" + userId);
                    }
                }

                if (returnUrl == "PayPal/ValidateCommand")
                {
                    // get the currently logged in user
                    var user = Membership.GetUser();

                    // find current user's profile
                    var userProfile = db.UserProfiles.Where(up => up.UserId == (int)user.ProviderUserKey).Single();

                    // if the profile has no address associated
                    if (userProfile.AddressId == null)
                    {
                        userProfile.AddressId = model.Address.AddressId; // associate the new address with their profile
                        db.Entry(userProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        return Redirect("~/" + returnUrl); // redirect them back to their edit profile view
                    }
                    else
                    {
                        // update the address already associated with their profile
                        db.Entry(userProfile).State = EntityState.Modified;
                        db.SaveChanges();
                        return Redirect("~/" + returnUrl);
                    }
                }

                // if we end up here, we are just adding an unassociated address to the database
                // db.SaveChanges(); // don't save changes...
                return RedirectToAction("Index");  
            }

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name", model.Address.StateId);
            return View(model);
        }
        
        //
        // GET: /Address/Edit/5
 
        public ActionResult Edit(int id, string returnUrl, int userId = 0)
        {
            AddressEditViewModel model = new AddressEditViewModel()
            {
                Address = db.Addresses.Where(a => a.AddressId == id).Single()
            };

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name", model.Address.StateId);
            return View(model);
        }

        //
        // POST: /Address/Edit/5

        [HttpPost]
        public ActionResult Edit(AddressEditViewModel model, string returnUrl, int userId = 0)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model.Address).State = EntityState.Modified;
                db.SaveChanges();

                // if the user came from their profile (edit view)
                if (returnUrl == "UserProfile/Edit" && userId != 0)
                {
                    // find current user profile
                    var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();
                    return Redirect("~/" + returnUrl + "/" + userId); // redirect them back to their edit profile view
                }

                if (returnUrl != null)
                {
                    return Redirect("~/" + returnUrl);
                }

                return RedirectToAction("Index");
            }

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name", model.Address.StateId);
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}