﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    [Authorize]
    public class UserProfileController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /UserProfile/Details/5

        public ViewResult Details(int id)
        {
            UserProfile model = db.UserProfiles.Where(up => up.UserId == id).Single();

            return View(model);
        }

        //
        // GET: /UserProfile/Edit/5

        public ActionResult Edit(int id)
        {
            var userProfile = db.UserProfiles.Where(up => up.UserId == id).Single();

            // if the user is not an admin
            if (!User.IsInRole("admin"))
            {
                // look up current user id
                var currentUserId = (int)System.Web.Security.Membership.GetUser().ProviderUserKey;

                // check to make sure that the user profile is attached to the currently logged in user
                if (userProfile.UserId == currentUserId)
                {
                    return View(userProfile);
                }
                else
                {
                    throw new Exception("you cannot access a user profile that is not your own");
                }
            }

            // the user is an admin, so let them view the profile
            return View(userProfile);
        }

        //
        // POST: /UserProfile/Edit/5

        [HttpPost]
        public ActionResult Edit(UserProfile model)
        {
            if (model.AddressId != null && db.Addresses.Any(a => a.AddressId == model.AddressId))
            {
                model.Address = db.Addresses.First(a => a.AddressId == model.AddressId);
            }
            //model.Address = db.Addresses.Where(a => a.AddressId == model.AddressId).Single();

            // check to verify that the phone number length is correct
            // (when using an input mask, this shouldn't be a problem)
            if (model.Phone.Length != 14)
            {
                ModelState.AddModelError("", "please enter a valid phone number");
            }

            if (model.Mobile != null && model.Mobile.Length != 14)
            {
                ModelState.AddModelError("", "please enter a valid mobile number");
            }

            // checks to make sure the user is 18 years old model.DateOfBirth.Value.Date
            //model.DateOfBirth.Date
            if (model.DateOfBirth > DateTime.Now.AddHours(2).Date.AddYears(-18))
            {
                ModelState.AddModelError("", "you must be at least 18 years of age");
            }

            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MyAccount", "Account");
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}