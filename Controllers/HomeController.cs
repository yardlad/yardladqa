﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.View;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models;
using System.Web.Security;

namespace YardLad.Controllers
{
    public class HomeController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: Home/

        public ActionResult Index()
        {
            ViewBag.Content = db.Contents.ToList(); // will be used in foreach loops in the view to display content by id
            ViewBag.FeaturedYardImageFilePath = db.Contents.Where(c => c.ContentId == 4).Single().Image.Source;
            ViewBag.YardOwner = db.Contents.Where(c => c.ContentId == 4).Single().YardOwner;
            ViewBag.Ratings = db.Ratings.Where(r => r.Value >= 3).OrderBy(r => r.DateRated).ThenBy(r => r.Value).Take(5).ToList();

            if (TempData["message"] != null)
            {
                ViewBag.Message = TempData["message"].ToString();
            }

            var currentExpirationDate = new DateTime(2014, 8, 20, 12, 0, 0);
            var currentDate = DateTime.Now;

            var difference = currentExpirationDate - currentDate;

            // tack on the extra remaining time to the new expiration date
            var expiresOn = (currentExpirationDate - difference).AddYears(1);

            return View();
        }

        //
        // POST: Home/

        [HttpPost] // when a user fills out and submits the contact form on the home page
        public ActionResult Index(ContactViewModel model)
        {
            if (model.Area.Length != 3 || model.Prefix.Length != 3 || model.Suffix.Length != 4)
            {
                ModelState.AddModelError("", "please make sure you have entered a valid phone number");
            }

            if (ModelState.IsValid)
            {
                model.Phone = "(" + model.Area + ") " + model.Prefix + "-" + model.Suffix;

                IEmailSender emailsender = new GoDaddyEmailSender(); // send the email via GoDaddy (implementing IEmailSender interface)
                string body = string.Format("<p>New contact form submission!</p>" + "<p>Name: {0}<br />Email: {1}<br />Phone: {2}<br />"
                    + "Message: {3}</p>",
                    model.Name, model.Email, model.Phone, model.Message);

                var contactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                // send email to contact@yardlad.com (will need to replace the emails when deploying)
                //emailsender.SendEmail(model.Subject, body, from: contactEmail, recipient: contactEmail);

                return RedirectToAction("Submitted", "Contact", model);
            }

            ViewBag.Content = db.Contents.ToList(); // will be used in foreach loops in the view to display content by id
            ViewBag.FeaturedYardImageFilePath = db.Contents.Where(c => c.ContentId == 4).Single().Image.Source;
            ViewBag.YardOwner = db.Contents.Where(c => c.ContentId == 4).Single().YardOwner;
            return View(model);
        }

        //
        // GET: Home/About

        public ActionResult About()
        {
            // single page, return db and use foreach to loop through and only show the about section
            // if this is all that will be display, could return the content with only the id for this section

            // about us content id = 1
            return View(db.Contents.ToList());
        }

        //
        // GET: Home/Terms

        public ActionResult Terms()
        {
            // single page, will probably need to make this editable content for the admin
            // terms and conditions content id = 7
            return View(db.Contents.ToList());
        }

        public PartialViewResult Footer()
        {
            ViewBag.YardLadAddress = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Address").Single().Value;
            ViewBag.YardLadPhoneNumber = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Phone Number").Single().Value;
            ViewBag.YardLadContactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

            return PartialView();
        }

        //
        // GET: Home/Navigation

        public PartialViewResult Navigation()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userId = (int)System.Web.Security.Membership.GetUser(false).ProviderUserKey;
                var user = System.Web.Security.Membership.GetUser(false);

                if (System.Web.Security.Roles.IsUserInRole(user.UserName, "contractor") || System.Web.Security.Roles.IsUserInRole(user.UserName, "contractor2"))
                {
                    var contractor = db.ContractorUsers.Where(cu => cu.UserId == userId).FirstOrDefault();
                    ViewBag.ContractorId = contractor.ContractorId;
                }

                if (db.RequestedServices.Where(rs => rs.UserId == userId).Count() > 0)
                {
                    ViewBag.HasRequestedServices = true;
                }
                else
                {
                    ViewBag.HasRequestedServices = false;
                }
            }

            return PartialView();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}