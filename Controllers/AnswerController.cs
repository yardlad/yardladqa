﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    public class AnswerController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /Answer/

        public ViewResult Index(int id /* question id */)
        {
            // return all the answers for the particular question
            var answers = db.Answers.Include(a => a.Question).Where(a => a.QuestionId == id).ToList();
            ViewBag.QuestionId = id;
            ViewBag.Question = db.Questions.Where(q => q.QuestionId == id).FirstOrDefault();

            return View(answers);
        }

        //
        // GET: /Answer/Create

        public ActionResult Create(int id)
        {
            ViewBag.Question = db.Questions.Where(q => q.QuestionId == id).FirstOrDefault();
            ViewBag.QuestionId = id;
            return View();
        } 

        //
        // POST: /Answer/Create

        [HttpPost]
        public ActionResult Create(Answer answer)
        {
            if (ModelState.IsValid)
            {
                answer.IsActive = true;
                db.Answers.Add(answer);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = answer.QuestionId });  
            }

            ViewBag.QuestionId = answer.QuestionId;

            return View(answer);
        }
        
        //
        // GET: /Answer/Edit/5
 
        public ActionResult Edit(int id)
        {
            Answer answer = db.Answers.Find(id);
            return View(answer);
        }

        //
        // POST: /Answer/Edit/5

        [HttpPost]
        public ActionResult Edit(Answer answer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(answer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = answer.QuestionId });
            }
            return View(answer);
        }

        //
        // GET: /Answer/Delete/5
 
        public ActionResult Delete(int id)
        {
            Answer answer = db.Answers.Find(id);
            return View(answer);
        }

        //
        // POST: /Answer/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Answer answer = db.Answers.Find(id);
            db.Answers.Remove(answer);
            db.SaveChanges();
            return RedirectToAction("Index", id = answer.QuestionId);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}