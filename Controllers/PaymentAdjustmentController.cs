﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;

namespace YardLad.Controllers
{
    [Authorize(Roles = "admin, contractor, contractor2")]
    public class PaymentAdjustmentController : Controller
    {
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(PaymentAdjustmentController));
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /PaymentAdjustment/

        [Authorize(Roles="admin")]
        public ViewResult Index()
        {
            var paymentadjustments = db.PaymentAdjustments.Include(p => p.RequestedService);
            return View(paymentadjustments.ToList());
        }

        //
        // GET: /PaymentAdjustment/Details/5

        public ViewResult Details(int id)
        {
            PaymentAdjustment paymentadjustment = db.PaymentAdjustments.Find(id);
            return View(paymentadjustment);
        }

        //
        // GET: /PaymentAdjustment/Create/5

        public ActionResult Create(int id /*requested service id*/)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

            PaymentAdjustment adjustment = new PaymentAdjustment()
            {
                RequestedServiceId = requestedService.RequestedServiceId,
                RequestedService = requestedService,
                IsCompleted = false
            };

            

            return View(adjustment);
        } 

        //
        // POST: /PaymentAdjustment/Create/

        [HttpPost]
        public ActionResult Create(PaymentAdjustment paymentadjustment, string returnUrl)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == paymentadjustment.RequestedServiceId).SingleOrDefault();
            paymentadjustment.RequestedService = requestedService;

            if (paymentadjustment.Amount == 0)
            {
                ModelState.AddModelError("", "please enter a positive or negative amount to adjust");
            }

            if (ModelState.IsValid)
            {
                db.PaymentAdjustments.Add(paymentadjustment);
                db.SaveChanges();

                if (paymentadjustment.Amount < 0)
                {
                    // inactivate the original payment if the user will be credited an amount
                    var payment = requestedService.Payments.OrderBy(p => p.DateSubmitted).FirstOrDefault();
                    payment.IsActive = false;
                    db.Entry(payment).State = EntityState.Modified;
                    db.SaveChanges();
                }

                var customer = db.my_aspnet_users.Where(u => u.id == paymentadjustment.RequestedService.UserId).SingleOrDefault();
                var customerEmail = Membership.GetUser(customer.id, false).Email;
                var contractor = db.Contractors.Where(c => c.ContractorId == paymentadjustment.RequestedService.Contractor.ContractorId).SingleOrDefault();
                var contractorUsers = db.ContractorUsers.Where(cu => cu.ContractorId == contractor.ContractorId).ToList();
                var yardLadEmail = db.SiteSettings.Where(ss => ss.Name.ToLower() == "yard lad contact email").SingleOrDefault().Value;

                IEmailSender sender = new GoDaddyEmailSender();
                string subject = "Payment Adjustment";
                string body = null;
                //string paypalLink = "<a href=\"http://yardlad.design-develop-deploy.com/PayPal/PaymentAdjustment/" + paymentadjustment.PaymentAdjustmentId + ">here</a>";

                // customer needs to pay an additional amount
                if (paymentadjustment.Amount > 0)
                {
                    body = String.Format("<p>Hi, {0}</p>"
                    + "<p>Your service request has been completed but requires a payment adjustment. You will need to login to your account and make a payment of " + paymentadjustment.Amount.ToString("c") + ".<br />"
                    + "You can make a payment by clicking <a href=\"http://yardlad.design-develop-deploy.com/PayPal/PaymentAdjustment/" + paymentadjustment.PaymentAdjustmentId + ">here</a>"
                    + " or by visiting your service history. After submitting the payment you may rate the contractor by visiting your service history page</p>"
                    + "Thanks<br />-The Yard Lad Team", customer.UserProfile.FirstName);
                }
                else
                {
                    // the customer is being credited an amount, will need to make another full payment (of new amount)
                    body = String.Format("<p>Hi, {0}</p>"
                    + "<p>Your service request has been completed but requires a payment adjustment. Your account will be credited the following amount: " + paymentadjustment.RequestedService.Payments.FirstOrDefault().Amount.ToString("c") + ".<br />"
                    + "Once you have received your refund, you will need to make the full reduced payment by clicking <a href=\"http://yardlad.design-develop-deploy.com/PayPal/PaymentAdjustment/" + paymentadjustment.PaymentAdjustmentId + ">here</a>" 
                    + " or by visiting your service history. You also can now rate the contractor by visiting your service history page</p>", customer.UserProfile.FirstName);

                    body += "Thanks<br />-The Yard Lad Team";
                }
                
                // send an email to the customer
                try
                {
                    sender.SendEmail(subject, body, yardLadEmail, customerEmail);
                }
                catch (Exception ex)
                {

                    _logger.Error("there was an error sending an email to the customer after a contractor submitted a payment adjustment", ex);
                }

                // send an email to the contractor
                try
                {
                    foreach (var contractorUser in contractorUsers)
                    {
                        var contractorEmail = Membership.GetUser(contractorUser.UserId, false).Email;
                        body = String.Format("<p>Hi {0},</p>"
                            + "<p>You have recently requested a payment adjustment for the following service: {1}<br />"
                            + "You can view the status by visiting your <a href=\"http://yardlad.design-develop-deploy.com/Contractor/ServiceHistory/" + contractor.ContractorId + ">service history</a> in your account</p>",
                            contractorUser.my_aspnet_users.UserProfile.FirstName, paymentadjustment.RequestedService.ContractorService.Service.Name);

                        body += "Thanks<br />-The Yard Lad Team";

                        sender.SendEmail(subject, body, yardLadEmail, contractorEmail);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("there was an error sending an email to the contractor after they submitted a payment adjustment", ex);
                }

                // send an email to the admin if the user needs to be refunded
                if (paymentadjustment.Amount < 0)
                {
                    subject = "Payment Adjustment - Refund";
                    body = "<p>A payment adjustment has been made for the following service. Please refund the user the full amount of the original payment for the service</p>";
                    body += "<h3>Requested Service</h3>"
                        + "<p>Requested Service ID: " + paymentadjustment.RequestedServiceId + "<br />"
                        + "Service: " + paymentadjustment.RequestedService.ContractorService.Service.Name + "<br />"
                        + "Contractor: " + paymentadjustment.RequestedService.Contractor.Name + "</p>"
                        + "<h3>Original Payment Information</h3>"
                        + "<p>Payment ID: " + paymentadjustment.RequestedService.Payments.FirstOrDefault().PaymentId + "<br />"
                        + "Transaction ID: " + paymentadjustment.RequestedService.Payments.FirstOrDefault().TransactionId + "<br />"
                        + "Amount: " + paymentadjustment.RequestedService.Payments.FirstOrDefault().Amount.ToString("c") + "</p>"
                        + "<h3>Customer Information</h3>"
                        + "<p>Username: " + Membership.GetUser(paymentadjustment.RequestedService.UserId, false).UserName + "<br />"
                        + "Name: " + paymentadjustment.RequestedService.UserProfile.FirstName + " " + paymentadjustment.RequestedService.UserProfile.LastName + "<br />"
                        + "Email: " + Membership.GetUser(paymentadjustment.RequestedService.UserId, false).Email + "<br />"
                        + "Phone: " + paymentadjustment.RequestedService.UserProfile.Phone + "</p>";
                }

                try
                {
                    sender.SendEmail(subject, body, yardLadEmail, yardLadEmail);
                }
                catch (Exception ex)
                {
                    _logger.Error("there was an error sending the email to the admin about a payment adjustment/refund", ex);
                }

                // mark the service as completed
                requestedService.IsCompleted = true;
                requestedService.DateCompleted = DateTime.Now.AddHours(2);
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();

                if (returnUrl != null)
                {
                    return Redirect("~/" + returnUrl);
                }

                return RedirectToAction("ServiceHistory", "Contractor");  
            }

            return View(paymentadjustment);
        }
        
        //
        // GET: /PaymentAdjustment/Edit/5
 
        public ActionResult Edit(int id)
        {
            PaymentAdjustment paymentadjustment = db.PaymentAdjustments.Find(id);
            ViewBag.RequestedServiceId = new SelectList(db.RequestedServices, "RequestedServiceId", "ContractorComment", paymentadjustment.RequestedServiceId);
            return View(paymentadjustment);
        }

        //
        // POST: /PaymentAdjustment/Edit/5

        [HttpPost]
        public ActionResult Edit(PaymentAdjustment paymentadjustment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(paymentadjustment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RequestedServiceId = new SelectList(db.RequestedServices, "RequestedServiceId", "ContractorComment", paymentadjustment.RequestedServiceId);
            return View(paymentadjustment);
        }

        //
        // GET: /PaymentAdjustment/Delete/5
 
        public ActionResult Delete(int id)
        {
            PaymentAdjustment paymentadjustment = db.PaymentAdjustments.Find(id);
            return View(paymentadjustment);
        }

        //
        // POST: /PaymentAdjustment/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            PaymentAdjustment paymentadjustment = db.PaymentAdjustments.Find(id);
            db.PaymentAdjustments.Remove(paymentadjustment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}