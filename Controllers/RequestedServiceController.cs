﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class RequestedServiceController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /RequestedService/

        public ViewResult Index()
        {
            var requestedservices = db.RequestedServices.Include(r => r.Contractor).Include(r => r.ContractorService).Include(r => r.UserProfile)
                .Where(rs => rs.IsActive == true);
            return View(requestedservices.ToList());
        }

        //
        // GET: /RequestedService/Details/5

        public ViewResult Details(int id)
        {
            RequestedService requestedservice = db.RequestedServices.Find(id);
            return View(requestedservice);
        }

        //
        // GET: /RequestedService/Create

        public ActionResult Create()
        {
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name");
            ViewBag.ContractorServiceId = new SelectList(db.ContractorServices, "ContractorServiceId", "ContractorServiceId");
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName");
            return View();
        } 

        //
        // POST: /RequestedService/Create

        [HttpPost]
        public ActionResult Create(RequestedService requestedservice)
        {
            if (ModelState.IsValid)
            {
                db.RequestedServices.Add(requestedservice);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", requestedservice.ContractorId);
            ViewBag.ContractorServiceId = new SelectList(db.ContractorServices, "ContractorServiceId", "ContractorServiceId", requestedservice.ContractorServiceId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName", requestedservice.UserId);
            return View(requestedservice);
        }
        
        //
        // GET: /RequestedService/Edit/5
 
        public ActionResult Edit(int id)
        {
            RequestedService requestedservice = db.RequestedServices.Find(id);
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", requestedservice.ContractorId);
            ViewBag.ContractorServiceId = new SelectList(db.ContractorServices, "ContractorServiceId", "ContractorServiceId", requestedservice.ContractorServiceId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName", requestedservice.UserId);
            return View(requestedservice);
        }

        //
        // POST: /RequestedService/Edit/5

        [HttpPost]
        public ActionResult Edit(RequestedService requestedservice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(requestedservice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", requestedservice.ContractorId);
            ViewBag.ContractorServiceId = new SelectList(db.ContractorServices, "ContractorServiceId", "ContractorServiceId", requestedservice.ContractorServiceId);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "FirstName", requestedservice.UserId);
            return View(requestedservice);
        }

        //
        // GET: /RequestedService/Delete/5
 
        public ActionResult Delete(int id)
        {
            RequestedService requestedservice = db.RequestedServices.Find(id);
            return View(requestedservice);
        }

        //
        // POST: /RequestedService/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            RequestedService requestedservice = db.RequestedServices.Find(id);
            db.RequestedServices.Remove(requestedservice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // POST: /RequestedService/SetDate/5

        [HttpPost]
        public ActionResult SetDate(int id, int contractorId, string startDate, string startTime)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).Single();

            if (requestedService.RequestedStartDate != null)
            {
                DateTime date = DateTime.Parse(startDate);
                DateTime time = DateTime.Parse(startTime);

                var dateTime = date.Add(time.TimeOfDay);

                requestedService.RequestedStartDate = dateTime;
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();
            }

            if (requestedService.RequestedStartDate == null)
            {
                DateTime date = DateTime.Parse(startDate);
                DateTime time = DateTime.Parse(startTime);

                var dateTime = date.Add(time.TimeOfDay);

                requestedService.RequestedStartDate = dateTime;
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("ServiceHistory", "Contractor", new { id = contractorId });
        }

        //
        // GET: /RequestedService/RemoveDate/5

        public ActionResult RemoveDate(int id, int contractorId)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).Single();

            requestedService.RequestedStartDate = null;
            db.Entry(requestedService).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("ServiceHistory", "Contractor", new { id = contractorId });
        }

        //
        // POST: /RequestedService/AddContractorComment/5

        public ActionResult AddContractorComment(string comment, int id = 0)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

            if (id != 0)
            {
                requestedService.ContractorComment = comment;
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("ServiceHistory", "Contractor", new { id = requestedService.ContractorId });
        }

        //
        // POST: /RequestedService/EditContractorComment/5

        public ActionResult EditContractorComment(string comment, int id = 0)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

            if (id != 0)
            {
                requestedService.ContractorComment = comment;
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("ServiceHistory", "Contractor", new { id = requestedService.ContractorId });
        }

        //
        // GET: /RequestedService/RemoveContractorComment/5

        public ActionResult RemoveContractorComment(string comment, int id = 0)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

            if (id != 0)
            {
                requestedService.ContractorComment = null;
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("ServiceHistory", "Contractor", new { id = requestedService.ContractorId });
        }

        //
        // POST: /RequestedService/Complete/5

        [HttpPost]
        public ActionResult Complete(string returnUrl, int id = 0)
        {
            if (id != 0)
            {
                var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).Single();
                requestedService.IsCompleted = true;
                requestedService.DateCompleted = DateTime.Now.AddHours(2);
                db.Entry(requestedService).State = EntityState.Modified;
                db.SaveChanges();

                // send email to the customer
                IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)
                var customer = db.my_aspnet_users.Where(u => u.id == requestedService.UserId).SingleOrDefault();
                string yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;
                string customerEmail = Membership.GetUser(customer.name, false).Email;

                string subject = "Service Completed";
                string body = null;

                try
                {
                    emailsender.SendEmail(subject, body, yardladEmail, customerEmail);
                }
                catch (Exception ex)
                {
                    // log?   
                }

            }

            if (returnUrl != null)
            {
                return Redirect("~/" + returnUrl);
            }
            else
            {
                return RedirectToAction("Account", "MyAccount");
            }
            
        }

        //
        // GET: /RequestedService/Rate/5

        public ActionResult Rate(int id)
        {
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).Single();
            AddRatingViewModel model = new AddRatingViewModel()
            {
                RequestedServiceId = requestedService.RequestedServiceId,
                //RequestedService = requestedService,
                UserId = requestedService.UserId,
                ContractorId = requestedService.ContractorId,
            };

            return View(model);
        }

        //
        // POST: /RequestedService/Rate/5

        [HttpPost]
        public ActionResult Rate(AddRatingViewModel model)
        {
            if (model.Value == 0)
            {
                ModelState.AddModelError("", "please select a rating");
            }

            if (ModelState.IsValid)
            {
                // look up the requested service
                var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == model.RequestedServiceId).Single();

                // add the new rating to the db
                Rating rating = new Rating()
                {
                    UserId = model.UserId,
                    ContractorId = model.ContractorId,
                    Value = model.Value,
                    Comments = model.Comments,
                    IsActive = true,
                    DateRated = DateTime.Now.AddHours(2),
                };

                db.Ratings.Add(rating);

                // add rating to requested service
                requestedService.RatingId = rating.RatingId;
                db.Entry(requestedService).State = EntityState.Modified;

                // save and return to account history
                db.SaveChanges();

                // send email to contractor admin/s notifying them that they have received a rating
                List<string> adminEmails = new List<string>();

                // get all users under the contractor
                var contractorUsers = db.ContractorUsers.Where(c => c.ContractorId == requestedService.ContractorId);

                foreach (var contractorUser in contractorUsers.ToList())
                {
                    bool isAdmin = System.Web.Security.Roles.IsUserInRole(contractorUser.my_aspnet_users.name, "contractor");

                    // if the contractor user is an admin
                    if (isAdmin == true)
                    {
                        string email = System.Web.Security.Membership.GetUser(contractorUser.UserId, false).Email;
                        adminEmails.Add(email);
                    }
                }

                IEmailSender sender = new GoDaddyEmailSender();
                string subject = "new rating";
                string body = null;

                body = String.Format("<p>You have received a rating for the following service:</p>"
                    + "<p>Service: {0}<br />Customer: {1}<br />Completed On: {2}</p>"
                    + "<p>Click <a href=\"http://www.yardlad.com/Rating/Details/{3}\">here</a> to view your rating</p>",
                    requestedService.ContractorService.Service.Name, requestedService.UserProfile.FirstName + " " + requestedService.UserProfile.LastName,
                    requestedService.DateCompleted.Value.ToShortDateString(), requestedService.RatingId);

                var from = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                foreach (var email in adminEmails)
                {
                    try
                    {
                        sender.SendEmail(subject, body, from, "ksexton@technopole.us");
                    }
                    catch (Exception)
                    {
                        TempData["error"] = "your email was not sent, please try again later.";
                    }
                }

                TempData["Message"] = "Thanks for rating! Would you like to take a survey about using Yard Lad Services? Your feedback is appreciated.";
                TempData["RequestedServiceId"] = requestedService.RequestedServiceId;
                return RedirectToAction("History", "Account");
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}