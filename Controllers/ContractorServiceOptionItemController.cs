﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class ContractorServiceOptionItemController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /ContractorServiceOptionItem/

        public ViewResult Index(int id)
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }

            // all contractor service option items under the contractor service option id (passed in)
            var contractorServiceOptionItems = db.ContractorServiceOptionItems
                .Where(csoi => csoi.ContractorServiceOptionId == id).ToList();

            // the contractor service option
            var contractorServiceOption = db.ContractorServiceOptions.Where(cso => cso.ContractorServiceOptionId == id).Single();

            // ddl of service option items under the service option
            var serviceOptionItems = db.ServiceOptionItems
                .Where(so => so.ServiceOption.ServiceId == contractorServiceOption.ContractorService.ServiceId)
                .Where(so => so.ServiceOption.Name == contractorServiceOption.Name).ToList();
                // since the records can't be looked up by id, use the service option name (this can only be changed by the admin)

            var availableServiceOptionItems = new List<ServiceOptionItem>(); // empty list to populate later

            // the contractor hasn't added all the available option items under the service
            if (serviceOptionItems.Count != contractorServiceOptionItems.Count)
            {
                // filter by name
                foreach (var serviceOptionItem in serviceOptionItems)
                {
                    try
                    {
                        var x = contractorServiceOptionItems.Where(csoi => csoi.Name == serviceOptionItem.Name).Single();
                    }
                    catch (InvalidOperationException)
                    {
                        availableServiceOptionItems.Add(serviceOptionItem);
                    }
                }
            }

            if (availableServiceOptionItems.Count > 0)
            {
                ViewBag.ServiceOptionItems = new SelectList(availableServiceOptionItems, "ServiceOptionItemId", "Name");
            }
            else
            {
                ViewBag.ServiceOptionItems = null;
            }

            ViewBag.ServiceId = contractorServiceOption.ContractorService.ServiceId;
            ViewBag.ContractorServiceId = contractorServiceOption.ContractorServiceId;
            ViewBag.ContractorServiceOption = contractorServiceOption;
            ViewBag.ContractorServiceOptionId = contractorServiceOption.ContractorServiceOptionId;
            ViewBag.ContractorId = contractorServiceOption.ContractorService.ContractorId;

            ViewBag.ContractorServiceBasePrice = contractorServiceOption.ContractorService.BasePrice;

            return View(contractorServiceOptionItems);
        }

        //
        // POST: /ContractorServiceOptionItem/Index/

        [HttpPost, ActionName("Index")]
        public ActionResult IndexAddOption(int id, string serviceOptionItems)
        {
            var serviceOptionItemId = int.Parse(serviceOptionItems);

            // add the contractor service option (copy admin values)
            var serviceOptionItem = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == serviceOptionItemId).Single();

            ContractorServiceOptionItem contractorServiceOptionItem = new ContractorServiceOptionItem()
            {
                ContractorServiceOptionId = id,
                Name = serviceOptionItem.Name,
                Order = serviceOptionItem.Order,
                Price = serviceOptionItem.Price,
                IsActive = serviceOptionItem.IsActive
            };

            db.ContractorServiceOptionItems.Add(contractorServiceOptionItem);
            db.SaveChanges();

            return RedirectToAction("Index", new { id = id });
        }

        public ActionResult RequestNew(int id, int contractorServiceId, int contractorServiceOptionId)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            var contractorServiceOption = db.ContractorServiceOptions.Where(cso => cso.ContractorServiceOptionId == contractorServiceOptionId).Single();

            ServiceOptionRequestViewModel model = new ServiceOptionRequestViewModel()
            {
                ServiceOption = new ServiceOptionRequest() { Name = contractorServiceOption.Name, Service = contractorServiceOption.ContractorService.Service },
                ServiceOptionItems = new List<ServiceOptionItemRequest>()
            };

            ViewBag.ContractorId = id;
            ViewBag.ContractorServiceId = contractorServiceId;
            return View(model);
        }

        [HttpPost]
        public ActionResult RequestNew(ServiceOptionRequestViewModel model, string contractorId)
        {
            // add all the service option items to be requested
            for (int i = 1; i <= model.ServiceOptionItems.Count; i++)
            {
                var serviceOptionItemRequest = new ServiceOptionItemRequest()
                {
                    ContractorId = int.Parse(contractorId),
                    ServiceOptionRequestId = null, // not associated with a service option request, this is separately requested
                    Order = i,
                    ServiceName = model.ServiceOption.Service.Name,
                    OptionName = model.ServiceOption.Name,
                    Name = model.ServiceOptionItems[i - 1].Name,
                    Price = model.ServiceOptionItems[i - 1].Price,
                    DateRequested = DateTime.Now.AddHours(2),
                    IsActive = true
                };

                db.ServiceOptionItemRequests.Add(serviceOptionItemRequest);
                db.SaveChanges();
            }

            TempData["Message"] = "Your request has been submitted! You will receive an email if the request is approved";
            return RedirectToAction("Index", "ContractorServiceOptionItem", new { id = Request["contractorServiceOptionId"] });
        }

        //
        // GET: /ContractorServiceOptionItem/Details/5

        public ViewResult Details(int id)
        {
            ContractorServiceOptionItem contractorServiceOptionItem = db.ContractorServiceOptionItems.Find(id);
            return View(contractorServiceOptionItem);
        }

        //
        // GET: /ContractorServiceOptionItem/Edit/5
 
        public ActionResult Edit(int id)
        {
            ContractorServiceOptionItem contractorserviceoptionitem = db.ContractorServiceOptionItems.Find(id);
            return View(contractorserviceoptionitem);
        }

        //
        // POST: /ContractorServiceOptionItem/Edit/5

        [HttpPost]
        public ActionResult Edit(ContractorServiceOptionItem contractorserviceoptionitem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contractorserviceoptionitem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = contractorserviceoptionitem.ContractorServiceOptionId });
            }

            return View(contractorserviceoptionitem);
        }

        public ActionResult Remove(int id)
        {
            ContractorServiceOptionItem contractorServiceOptionItem = db.ContractorServiceOptionItems.Where(cso => cso.ContractorServiceOptionItemId == id).Single();
            return View(contractorServiceOptionItem);
        }

        [HttpPost, ActionName("Remove")]
        public ActionResult RemoveConfirmed(ContractorServiceOptionItem model)
        {
            var contractorServiceOptionItem = db.ContractorServiceOptionItems.Where(cso => cso.ContractorServiceOptionItemId == model.ContractorServiceOptionItemId).Single();

            // remove the contractor service option item
            db.ContractorServiceOptionItems.Remove(contractorServiceOptionItem);
            db.SaveChanges();

            return RedirectToAction("Index", new { id = model.ContractorServiceOptionId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}