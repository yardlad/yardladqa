﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class ContractorServiceAreaController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /ContractorServiceArea/

        public ViewResult Index(int id)
        {
            var contractorServiceAreas = db.ContractorServiceAreas
                .Include(c => c.Contractor).Include(c => c.ServiceArea)
                .Where(csa => csa.ContractorId == id)
                .OrderBy(csa => csa.ServiceArea.State.Name)
                .ThenBy(csa => csa.ServiceArea.Name);

            ViewBag.ContractorId = id;

            return View(contractorServiceAreas.ToList());
        }

        //
        // GET: /ContractorServiceArea/Create

        public ActionResult Create(int id)
        {
            ContractorServiceArea model = new ContractorServiceArea()
            {
                ContractorId = id,
                Price = 0.00M,
            };

            var primaryServiceAreaId = db.Contractors.Where(c => c.ContractorId == id).Single().ServiceAreaId;

            ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name");
            ViewBag.ServiceAreaId = new SelectList(db.ServiceAreas.Where(sa => sa.ServiceAreaId != primaryServiceAreaId), "ServiceAreaId", "Name");
            return View(model);
        } 

        //
        // POST: /ContractorServiceArea/Create

        [HttpPost]
        public ActionResult Create(ContractorServiceArea model)
        {
            if (ModelState.IsValid)
            {
                db.ContractorServiceAreas.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index", new { id = model.ContractorId });  
            }

            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", model.ContractorId);
            ViewBag.ServiceAreaId = new SelectList(db.ServiceAreas, "ServiceAreaId", "Name", model.ServiceAreaId);
            return View(model);
        }
        
        //
        // GET: /ContractorServiceArea/Edit/5
 
        public ActionResult Edit(int id)
        {
            ContractorServiceArea contractorservicearea = db.ContractorServiceAreas.Find(id);
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", contractorservicearea.ContractorId);
            ViewBag.ServiceAreaId = new SelectList(db.ServiceAreas, "ServiceAreaId", "Name", contractorservicearea.ServiceAreaId);
            return View(contractorservicearea);
        }

        //
        // POST: /ContractorServiceArea/Edit/5

        [HttpPost]
        public ActionResult Edit(ContractorServiceArea contractorservicearea)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contractorservicearea).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = contractorservicearea.ContractorId });  
            }
            ViewBag.ContractorId = new SelectList(db.Contractors, "ContractorId", "Name", contractorservicearea.ContractorId);
            ViewBag.ServiceAreaId = new SelectList(db.ServiceAreas, "ServiceAreaId", "Name", contractorservicearea.ServiceAreaId);
            return View(contractorservicearea);
        }

        //
        // GET: /ContractorServiceArea/Delete/5
 
        public ActionResult Delete(int id)
        {
            ContractorServiceArea contractorservicearea = db.ContractorServiceAreas.Find(id);
            return View(contractorservicearea);
        }

        //
        // POST: /ContractorServiceArea/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            ContractorServiceArea contractorservicearea = db.ContractorServiceAreas.Find(id);
            db.ContractorServiceAreas.Remove(contractorservicearea);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = contractorservicearea.ContractorId });  
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}