﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class ContractorServiceOptionController : Controller
    {
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /ContractorServiceOption/

        public ViewResult Index(int id)
        {
            List<ContractorServiceOption> contractorServiceOptions = db.ContractorServiceOptions
                .Where(cso => cso.ContractorServiceId == id).OrderBy(cso => cso.Name).ToList();

            // look up the contractor
            var userId = (int)Membership.GetUser().ProviderUserKey;
            var contractorUser = db.ContractorUsers.Where(cu => cu.UserId == userId).Single();

            // look up the service
            var contractorService = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();

            // ddl of service options under the service
            var serviceOptions = db.ServiceOptions.Where(so => so.ServiceId == contractorService.ServiceId).ToList();
            var availableServiceOptions = new List<ServiceOption>();

            // the contractor hasn't added all the available options under the service
            if (contractorServiceOptions.Count != serviceOptions.Count)
            {
		        // filter by name
                foreach (var serviceOption in serviceOptions)
                {
                    try 
	                {	        
		                var x = contractorServiceOptions.Where(cso => cso.Name == serviceOption.Name).Single();
	                }
	                catch (InvalidOperationException)
	                {
		                availableServiceOptions.Add(serviceOption);
	                }
                }
	        }

            if (availableServiceOptions.Count > 0)
            {
                ViewBag.ServiceOptions = new SelectList(availableServiceOptions, "ServiceOptionId", "Name");
            }
            else
            {
                ViewBag.ServiceOptions = null;
            }

            ViewBag.ContractorId = contractorUser.ContractorId;
            ViewBag.ServiceId = contractorService.ServiceId;
            ViewBag.ContractorServiceId = contractorService.ContractorServiceId;

            return View(contractorServiceOptions);
        }

        [HttpPost, ActionName("Index")]
        public ActionResult IndexAddOption(int id, string serviceOptions)
        {
            var serviceOptionId = int.Parse(serviceOptions);

            // add the contractor service option (copy admin values)
            var serviceOption = db.ServiceOptions.Where(so => so.ServiceOptionId == serviceOptionId).Single();

            ContractorServiceOption contractorServiceOption = new ContractorServiceOption()
            {
                ContractorServiceId = id,
                Name = serviceOption.Name,
                Order = serviceOption.Order,
                IsActive = serviceOption.IsActive
            };

            db.ContractorServiceOptions.Add(contractorServiceOption);
            db.SaveChanges();

            // add the contractor service option items (copy admin values)
            foreach (var serviceOptionItem in db.ServiceOptionItems.Where(soi => soi.ServiceOptionId == serviceOption.ServiceOptionId))
            {
                ContractorServiceOptionItem contractorServiceOptionItem = new ContractorServiceOptionItem()
                {
                    ContractorServiceOptionId = contractorServiceOption.ContractorServiceOptionId,
                    Name = serviceOptionItem.Name,
                    Order = serviceOptionItem.Order,
                    Price = serviceOptionItem.Price,
                    IsActive = serviceOptionItem.IsActive
                };

                db.ContractorServiceOptionItems.Add(contractorServiceOptionItem);
            }

            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public ActionResult RequestNew(int id, int serviceId, int contractorServiceId)
        {
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();

            ServiceOptionRequest request = new ServiceOptionRequest()
            {
                ContractorId = contractor.ContractorId,
                IsActive = true,
                ServiceId = serviceId,
                Service = db.Services.Where(s => s.ServiceId == id).Single(),
                DateRequested = DateTime.Now.AddHours(2)
            };

            ServiceOptionRequestViewModel model = new ServiceOptionRequestViewModel()
            {
                ServiceOption = request,
                ServiceOptionItems = new List<ServiceOptionItemRequest>()
            };

            ViewBag.ContractorServiceId = contractorServiceId;
            return View(model);
        }

        [HttpPost]
        public ActionResult RequestNew(ServiceOptionRequestViewModel model)
        {
            if (ModelState.IsValid)
            {
                // look through the current requests under the contractor Id
                // check for duplicate names

                foreach (var serviceOptionRequest in db.ServiceOptionRequests)
                {
                    if (serviceOptionRequest.Name.ToLower() == model.ServiceOption.Name.ToLower())
                    {
                        ModelState.AddModelError("", "a request has already been made to add this item");
                        return View(model);
                    }
                }

                // add the service option requests
                ServiceOptionRequest newRequest = new ServiceOptionRequest()
                {
                    ContractorId = model.ServiceOption.ContractorId,
                    DateRequested = DateTime.Now.AddHours(2),
                    IsActive = true,
                    Name = model.ServiceOption.Name,
                    ServiceId = model.ServiceOption.ServiceId
                };

                db.ServiceOptionRequests.Add(newRequest);
                db.SaveChanges();

                // add all the service option items to be requested
                for (int i = 1; i <= model.ServiceOptionItems.Count; i++)
                {
                    var serviceOptionItemRequest = new ServiceOptionItemRequest()
                    {
                        ContractorId = model.ServiceOption.ContractorId,
                        DateRequested = DateTime.Now.AddHours(2),
                        Order = i,
                        Name = model.ServiceOptionItems[i - 1].Name,
                        Price = model.ServiceOptionItems[i - 1].Price,
                        ServiceOptionRequestId = newRequest.ServiceOptionRequestId,
                        IsActive = true
                    };

                    db.ServiceOptionItemRequests.Add(serviceOptionItemRequest);
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index", "ContractorService");
        }

        public ActionResult Remove(int id)
        {
            ContractorServiceOption contractorServiceOption = db.ContractorServiceOptions.Where(cso => cso.ContractorServiceOptionId == id).Single();

            return View(contractorServiceOption);
        }

        [HttpPost, ActionName("Remove")]
        public ActionResult RemoveConfirmed(ContractorServiceOption model)
        {
            var contractorServiceOption = db.ContractorServiceOptions.Where(cso => cso.ContractorServiceOptionId == model.ContractorServiceOptionId).Single();

            // remove all the contractor service option items under the contractor service option
            foreach (var contractorServiceOptionItem in db.ContractorServiceOptionItems.Where(csoi => csoi.ContractorServiceOptionId == contractorServiceOption.ContractorServiceOptionId))
            {
                db.ContractorServiceOptionItems.Remove(contractorServiceOptionItem);
            }

            // remove the contractor service option
            db.ContractorServiceOptions.Remove(contractorServiceOption);
            db.SaveChanges();

            return RedirectToAction("Index", new { id = model.ContractorServiceId });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}