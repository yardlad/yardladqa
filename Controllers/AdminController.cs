﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    public class AdminController : Controller
    {
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(AdminController));
        private YardLadDevSSEntities db = new YardLadDevSSEntities();

        //
        // GET: /Admin/ControlPanel

        [Authorize(Roles = "admin")]
        public ActionResult ControlPanel()
        {
            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

            ViewBag.NewContractors = db.Contractors.Where(c => c.DateCreated > userProfile.LastLoginDate).Count();
            ViewBag.NewUploadedQualifications = db.Qualifications.Where(q => q.DateSubmitted > userProfile.LastLoginDate).Count();
            ViewBag.NewServiceOptionRequests = db.ServiceOptionRequests.Where(sor => sor.DateRequested > userProfile.LastLoginDate).Count();
            ViewBag.NewServiceOptionItemRequests = db.ServiceOptionItemRequests.Where(soir => soir.DateRequested > userProfile.LastLoginDate).Count();

            return View();
        }

        //
        // GET: /Admin/Contact
        
        public ActionResult Contact(string returnUrl, string subject = "")
        {
            // get the current user
            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();
            string name = userProfile.FirstName + " " + userProfile.LastName;

            ContactAdminViewModel model = new ContactAdminViewModel()
            {
                Name = name,
                Email = user.Email,
            };

            if (subject != null && subject != "")
            {
                model.Subject = subject;
            }

            // check roles
            if (Roles.IsUserInRole("contractor") || Roles.IsUserInRole("contractor2"))
            {
                model.IsContractor = true;

                // look up the contractor information
                var contractorUser = db.ContractorUsers.Where(cu => cu.UserId == userId).Single();

                model.TypeOfBusiness = contractorUser.Contractor.ContractorType.Type;
                model.BusinessName = contractorUser.Contractor.Name;
                model.BusinessPhone = userProfile.Phone;
                model.Roles = Roles.GetRolesForUser(user.UserName);
            }

            if (returnUrl != null)
            {
                model.ReturnUrl = returnUrl;
            }

            return View(model);
        }

        //
        // POST: Admin/Contact

        [HttpPost]
        public ActionResult Contact(ContactAdminViewModel model)
        {
            // setup email
            IEmailSender sender = new GoDaddyEmailSender();
            string subject = null;

            if (model.Subject != null)
            {
                subject = model.Subject;
            }
            else
	        {
                subject = "No Subject";
	        }

            string body = null;

            if (model.IsContractor != true)
            {
                body = string.Format("<p>New contact form submission!</p>" + "<p>Name: {0}<br />Email: {1}<br />"
                + "Message: {2}</p>", model.Name, model.Email, model.Message);
            }
            else // is a contractor
            {
                string roles = null;

                if (model.Roles.Count() > 1)
                {
                    for (int i = 0; i < model.Roles.Length; i++)
                    {
                        roles += model.Roles[i] + ",";
                    }
                }
                else
                {
                    roles = model.Roles[0];
                }

                body = string.Format("<p>New contact form submission!</p>" + "<p>Name: {0}<br />Email: {1}<br />Contractor Type: {2}<br />"
                + "Business Name: {3}<br />Business Phone: {4}<br />Roles: {5}<br />Message: {6}</p>", 
                model.Name, model.Email, model.TypeOfBusiness, model.BusinessName, model.BusinessPhone, roles, model.Message);
            }

            // the db contact email is pulled from the site settings
            var contactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

            try
            {
                // try to send the email
                sender.SendEmail(model.Subject, body, from: contactEmail, recipient: contactEmail);
            }
            catch (Exception ex)
            {
                _logger.Error("there was an error sending the contact email to the admin from " + model.Name, ex);
            }

            // let the user know that their message was sent
            return View("Submitted", model);
        }

        public ActionResult Submitted()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
