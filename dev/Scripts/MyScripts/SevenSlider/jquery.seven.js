(function($)
{
	//seven slider
    //global variable
	var object;
	//Interface for seven slider
    $.fn.sevenslider = function(options){
	 	 object=new sevenslider({
			 handle:$(this),
			 option:options
		 });	
		return object;
   };

  //Main sevenSlider Class
  function sevenslider(arg)
  {
	    var handle;
		var option;
		//timerhandle for automation
		var a_timer;
		//tentative variable for timer
		var cp_temp;
		//tentative variable for mouse/touch drag
		var mp_temp,tp_temp,cr_temp;
		//variable for array
		var tbl;
		//flags
		var anim_flag,t_flag,a_flag;
		//mouse capture flag
		var mouseflag,o_flag;
		//current and next slide index
		var index,newindex;
		//original rate between width:height
		var rate;
		//Default Args
		var defaults={
			//slider width
			width:800,
			//slider height
			height:300,
			//bullet
			bullet: null,
			//carousel type
			carousel: null,
			//fullwidth
			fullwidth: false,
			//animation type
			animation: 0,
			//automation
			automation: false,
			//time iterval for automation
			autointerval: 5,
			//progress bar
			progress:true,
			//progress type
			progresstype:'linear',
			//circular type
			circular:false,
			//responsive
			responsive:false,
			//touch
			swipe:false,
			//keyboard
			keyboard:false,
			//skin type
			skin:'default',
			//lightbox
			lightbox:false,
			//custom event
			onanimstart:function(){return false;},
			//custom event
			onanimend: function(){return false;}
		};
	
	  //container Object
	  handle=arg.handle;
	  //option Values
	  option=$.extend({}, defaults, arg.option || {});
	  
	  //initialization for fullwidth
	  if(option.fullwidth==true)
	  {
			option.width=screen.width;
			option.carousel=false;
			//option.bullet=false;
			//option.skin='sharp';
			option.responsive=true;
			handle.css("margin-left",0).css("width",option.width).css("height",option.height).css("left",0).css("top",0);
	  }
	    
	  //initialization for progress type
	  if(option.automation==true)
	  {
		  if(seven_isIE()!=false&&seven_isIE()<9)
		  {
			  option.progresstype='linear';
		  }
		  seven_showprogressbar();
		  seven_play();
	  }

	  //initialization for skin
	  seven_skin_setting();
	  
	  //pre-initialization 
	  seven_init(option);
  
  //initialization 
  function seven_init(arg)
  {
	  //Variable initialization
	  anim_flag=t_flag=mouseflag=oflag=0;
	  //flag for automation
	  a_flag=(option.automation==true)?1:-1;
	  index=newindex=0;
	  cp_temp=0;
	  //functional initialization
	  handle.css("width",arg.width).css("height",arg.height).css("max-width",arg.width).css("max-height",arg.height);	  
	  //carousel viewport initialization
	  handle.find("#seven_hviewport").css("width",160*handle.find(".seven_slide").length);
	  handle.find(".seven_slide").each(function(i)
	  {
		  var temp=$(this).find("img").attr("data-src");
		  if(typeof(temp)!='undefined')
			  $(this).append("<img class='seven_play' src='img/skin/play.png' style='position:absolute;left:"+Math.ceil((arg.width-50)/2)+"px;top:"+Math.ceil((arg.height-50)/2)+"px;' />");
	  });
	  handle.find("#seven_viewport").prepend("<div id='seven_next_slide' class='seven_func_slide'></div>").prepend("<div id='seven_prev_slide' class='seven_func_slide'></div>");	
	  handle.find("#seven_prev_slide").html(handle.find(".seven_slide:nth-child(1)").html());	
	  
  	  if(arg.fullwidth==true)
	  {
		var width = (window.innerWidth > screen.width) ? window.innerWidth : screen.width;
		handle.css("max-width",4000).css("width",width).attr("max-width",width);
	  }
	  if(arg.responsive==true)
	  		seven_resize_screen();	  
  }
  //get imageSize
  function seven_get_imagesize(src)
  {
	  var arr=[];
	  var hiddenImg = src.clone().css('visibility', 'hidden').removeAttr('height').removeAttr('width').appendTo('body');
	  arr.width=hiddenImg.width();
	  arr.height=hiddenImg.height();
	  hiddenImg.remove();
	  return arr;
  }
  //resize the screen
  function seven_resize_screen()
  {
	  		var font_size=$(window).width()*3/100;
			var length=handle.find(".seven_slide").length;
			
			if(font_size>24)
				font_size=24;
			else if(font_size<10)
				font_size=10;
			//original rate
			if(!option.fullwidth)
				rate=parseInt(handle.css("max-width"))/parseInt(handle.css("max-height"));
			else
			{
				var width = handle.attr("max-width");
				rate=width/parseInt(handle.css("max-height"));
			}
			if($(window).width()>400&&$(window).width()<=parseInt(handle.css("max-width")))
			{
				option.width=(option.skin=='sharp'||option.skin=='clean')?Math.ceil($(window).width())-20:Math.ceil($(window).width())-40;
				if(option.fullwidth)
					option.width=$(window).width();
				option.height=Math.ceil(option.width/rate);
			}
			else if($(window).width()<400)
			{
				option.width=(option.fullwidth)?400:380;
				option.height=Math.ceil(option.width/rate);
			}
			else if(($(window).width()>parseInt(handle.css("max-width"))))
			{
				option.width=parseInt(handle.css("max-width"));
				option.height=Math.ceil(option.width/rate);
			}
			//In case carousel overflows the board
			if(160*length-Math.abs(parseInt(handle.find("#seven_hviewport").css("left")))<option.width&&160*length>option.width)
			{
				handle.find("#seven_hviewport").css("left",-160*length+option.width);
			}
			handle.css("width",option.width).css("height",option.height);
			handle.find(".seven_slide_title").css("font-size",font_size);
			handle.find(".seven_play").css("left",Math.ceil((option.width-50)/2)+"px").css("top",Math.ceil((option.height-50)/2)+"px");
			handle.find(".seven_func_slide").each(function(i)
			{
					var arr=[index+1,newindex+1];
					var temp=seven_get_imagesize(handle.find(".seven_slide:nth-child("+arr[i%2]+")").find("img"));
					var temp_rate=temp.width/temp.height;
					if(rate>temp_rate)
						$(this).find("img").css("height","").css("width",option.width);
					else 
						$(this).find("img").css("width","").css("height",option.height);
			});
  }
  //bullet initialization
  function seven_showbullet(arg)
  {
	  //add Bullet to the div
	  handle.append("<div class='seven_bullet_control'><div id='seven_bullet_viewport' class='seven_clearfix' align='center'><div id='seven_bullet_inner_viewport'></div></div></div>");
	  //seven_thumbnail
	  $("<div class='seven_bt_preview'><div class='seven_bt_container'></div></div>").insertAfter(handle.find("#seven_bullet_viewport"));
	  handle.find(".seven_slide").each(function(i)
	  {
		  //Bullet
		  if(i==0)
			  handle.find("#seven_bullet_inner_viewport").append("<div class='seven_circle active'></div>");
		  else
			  handle.find("#seven_bullet_inner_viewport").append("<div class='seven_circle'></div>");
		  //Thumbnail
		  handle.find(".seven_bt_container").append("<div class='seven_bt_slide'><img class='seven_preview_img' src='"+$(this).find("img").attr("src")+"'/></div>");
	  });
	  //adjust width of the button container
	  handle.find(".seven_bt_container").css("width",100*handle.find(".seven_slide").length);
	  //add > & || button
	  handle.find("#seven_bullet_inner_viewport").append("<div class='seven_a_play' ></div>");
	  if(option.automation==false)
		  handle.find(".seven_a_play").addClass("seven_a_pause");
	  
  }
  //show Carousels
  function seven_showcarousel(arg,identifier)
  {
	  var t_width,t_height;
	  var length=handle.find(".seven_slide").length;
      
 	  //horizontal carousel bar
	  identifier.append("<div class='seven_hcarousel'><div id='seven_hviewport'></div><a class='seven_carousel_nav cn_left'></a><a class='seven_carousel_nav cn_right'></a></div>");
	  handle.find(".seven_slide").each(function(i)
	  {
			  if(i==0)
			  	handle.find("#seven_hviewport").append("<div class='carousel active'><a class='seven_ci'><img src='"+$(this).find("img").attr("src")+"'/></a></div>"); 
			  else
			    handle.find("#seven_hviewport").append("<div class='carousel'><a class='seven_ci'><img src='"+$(this).find("img").attr("src")+"'/></a></div>"); 
	  });
  }
  //set up the skin
  function seven_skin_setting()
  {
	  switch(option.skin)
	  {
		  case 'default':
			  if(option.bullet==null)	 option.bullet=true;
			  if(option.carousel==null)	option.carousel=false;
		  break;
		  case 'round':
			  if(option.bullet==null)  option.bullet=true;
			  if(option.carousel==null) option.carousel=false;
		  break;
		  case 'sharp':
			  if(option.bullet==null)	option.bullet=true;
			  if(option.carousel==null)  option.carousel=true;
		  break;
		  case 'clean':
			  if(option.bullet==null)	option.bullet=true;
			  if(option.carousel==null) option.carousel=false;
		  break;
		  case 'square':
			  if(option.bullet==null) option.bullet=true;
			  if(option.carousel==null) option.carousel=true;
		  break;
	  }
      //initialization for bullet
	  if(option.bullet)
	  {
		    seven_showbullet(handle);
	  }
	   
	  //initialization for carousel
	  if(option.carousel==true)
	  {
		   seven_showcarousel(option.carousel,handle);
	  }
	  
	  if(option.bullet)  handle.find(".seven_hcarousel").addClass("seven_bullet");
	  handle.addClass("seven_"+option.skin);
  }
  //initialize the progress bar
  function seven_showprogressbar()
  {
	  	  switch(option.progresstype)
		  {
			  case 'linear':
			 		handle.find("#seven_viewport").prepend('<div id="lp_ct" class="progressbar"><div id="lprogress"></div></div>');
			  break;
			  case 'circle':
			  		handle.find("#seven_viewport").prepend('<div id="cprogress" class="progressbar"><input class="knob cprogress" data-thinkness=".2" data-skin="tron" data-fgcolor="#ddd" data-width="40" data-displayInput=false value="0"></div>');
					//initialize the circular bar
					handle.find(".knob").knob({
						 draw : function () {
							 // "tron" case
							 if(this.$.data('skin') == 'tron') {
								  var a = this.angle(this.cv)  // Angle
								 , sa = this.startAngle          // Previous start angle
								 , sat = this.startAngle         // Start angle
								 , ea                            // Previous end angle
								 , eat = sat + a                 // End angle
								 , r = 1;
								 this.g.lineWidth = this.lineWidth;
								 this.o.cursor
									   && (sat = eat - 0.3)
									   && (eat = eat + 0.3);
								 if (this.o.displayPrevious) {
								   ea = this.startAngle + this.angle(this.v);
								   this.o.cursor
								   && (sa = ea - 0.3)
								   && (ea = ea + 0.3);
								   this.g.beginPath();
											this.g.strokeStyle = this.pColor;
											this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
											this.g.stroke();
										}
										this.g.beginPath();
										this.g.strokeStyle = r ? this.o.fgColor : this.fgColor ;
										this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
										this.g.stroke();
										this.g.lineWidth = 2;
										this.g.beginPath();
										this.g.strokeStyle = this.o.fgColor;
										this.g.arc( this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
										this.g.stroke();
										return false;
							 }
						 }
					});
					 
			  break;
		  };
		  if(option.progress==false)
		  	handle.find(".progressbar").addClass("invisible");
  }
  //linear automate function
  function seven_linear_automate(arg)
  {
	  if(anim_flag==1)
	  		return;
	  
	  cp_temp+=parseInt((option.width-10)/(20*option.autointerval));
	  if(cp_temp>=parseInt(option.width-10))
	  {
 		  seven_pause();
		  seven_next_slide();
		  cp_temp=parseInt(option.width-10);
	  }
	  handle.find("#lprogress").css("width",cp_temp);	  
  }
  //circle automate function
  function seven_circle_automate()
  {
	  if(anim_flag==1)
	  		return;
	   cp_temp+=5000/(option.autointerval*1000);
	   if(cp_temp>=100)
	   {
		   cp_temp=100;
		   seven_pause();
		   seven_next_slide();
	   }
   	   handle.find('.cprogress').val(Math.ceil(cp_temp)).trigger('change');
  }
  //move to the previous slide
  function seven_prev_slide()
  {
	  var temp;
	  temp=(option.animation==0)?(parseInt(Math.random()*249)+1):option.animation;
	  
	 if(option.circular==false)
	 {
		  if(index>0)
		 	seven_animation(index-1,temp);
		  else
		  	seven_pause();
	 }
	 else
	 {
		 	if(index==0) 
				seven_animation((handle.find(".seven_slide").length-1),temp);
			else
				seven_animation(index-1,temp);
	 }
  }
  //move to the next slide
  function seven_next_slide()
  {
	  if(anim_flag==1)
			return;			
	  var temp;
	  temp=(option.animation==0)?(parseInt(Math.random()*249)+1):option.animation;
	  if(option.circular==false)
	  {
		  if(index<handle.find(".seven_slide").length-1)
			seven_animation(index+1,temp);
		  else
		  {
			  seven_pause();
		  }		
	  }
	  else
	  {
			seven_animation((index+1)%handle.find(".seven_slide").length,temp);
	  }
  }
  //move to prev carousel
  function seven_prev_carousel()
  {
  	  var clength=handle.find(".seven_slide").length;
	  if(160*clength<option.width)	  return false;
	  var tb=parseInt(option.width/160);
	  var temp=Math.abs(Math.ceil(parseInt(handle.find("#seven_hviewport").css("left"))/160));
	  if(temp>=tb)
	  {
		 handle.find("#seven_hviewport").animate({
			 "left":-160*(temp-tb),													 
		 },
		 {
			 duration:200,
			 easing:"swing"
		 });
	 }
	 else
	 {
		  handle.find("#seven_hviewport").animate({
			 "left":"0px",													 
		 },
		 {
			 duration:200,
			 easing:"swing"
		 });
	}	  
  }
  //move to next carousel
  function seven_next_carousel()
  {
  	  var clength=handle.find(".seven_slide").length;
	  if(160*clength<option.width)	  return false;
	  anim_flag=1;
	  var tb=parseInt(option.width/160);
  	  var temp=Math.abs(Math.ceil(parseInt(handle.find("#seven_hviewport").css("left"))/160));
	  if(temp<(clength-(tb*2)))
	  {
		 handle.find("#seven_hviewport").animate({
			 "left":-160*(temp+tb),													 
		 },
		 {
			 duration:200,
			 easing:"swing",
			 complete:function()
			 {
			 }
		 });
	 }
	 else
	 {
		  handle.find("#seven_hviewport").animate({
			 "left":-(handle.find("#seven_hviewport").width()-option.width)+"px",																			 		  
		  },
		  {
			 duration:200,
			 easing:"swing",
			 complete:function()
			 {
			 }
		  });
	 }
  }
  //preview slide on mousehover
  function seven_thumb_preview(arg){	  	
	  var tleft=handle.find("#seven_bullet_inner_viewport").position().left;  
      var temp=parseInt(handle.find(".seven_circle").width())+2*parseInt(handle.find(".seven_circle").css("margin-left"));	  
      var tpad=parseInt(handle.find("#seven_bullet_inner_viewport").css("padding-left"));	
	  handle.find('.seven_bt_preview').css("left",tleft);
  	  switch(t_flag)
	  {
		  //When newly hover
		  case 0:
		  	  //calculate circle width including margin
			  handle.find('.seven_bt_preview').css("margin-left",tpad+arg*temp-50+temp/2);
			  handle.find('.seven_bt_container').css('margin-left',-100*arg);
		  	  handle.find(".seven_bt_preview").fadeIn(200);	
		  break;
		  //consequent hover
		  case 1:
			  //calculate circle width including margin
			  var temp=parseInt(handle.find(".seven_circle").width())+2*parseInt(handle.find(".seven_circle").css("margin-left"));
			  
			  handle.find('.seven_bt_preview').delay(50).animate({
					"margin-left":tpad+arg*temp-50+temp/2,
			   },
			   {
					duration:200,
					queue:false,
					easing:"easeOutSine",
					complete: function()
					{
						
					}
			   });
			  handle.find('.seven_bt_container').animate({
					"margin-left":-100*arg,
			   },
			   {
					duration:200,
					queue:false,
					easing:"easeOutSine",
					complete: function()
					{
						
					}
			   });
		  break;
	  }
	  //flag for thumbnail anim(switch to consequent hover from newly hover)
	  if(t_flag==0)	 t_flag=1;
  }
  //hide preview slide on mouseout
  function seven_thumb_hide()
  {
	 //thumbnail flag switch to newly hover
   	  t_flag=0;
	  handle.find(".seven_bt_preview").hide();
  }
  //adjust carousel pos 
  function seven_adjustcarousel(arg)
  {
	  var temp;
	  var tb=parseInt(option.width/160);
	  //offset to move
	  var offset;
	  //calculate carousel Index
	  temp=-parseInt(handle.find("#seven_hviewport").css("left"))/160;
	  //move forward or backward
	  if(arg>=temp+tb)
	  {
			//total carousel length
			var clength=handle.find(".seven_slide").length;
			//calculate the offset to move
			offset=(arg+tb)-clength;
			offset=(offset>=0)?(160*clength-option.width):(160*arg);
			//anim func
			handle.find("#seven_hviewport").animate({
				"left":-offset,
			},
			{
				duration:300,
				easing:"easeOutQuad"
			});
	}
	else if(arg<temp)
	{
			//calculate the offset to move
			offset=(arg-tb+1);
			offset=(offset<0)?0:(arg*160);
			//anim func
			handle.find("#seven_hviewport").animate({
				"left":-offset,
			},
			{
				duration:300,
				easing:"easeOutQuad"
		});
	}
  }
  //check if Browser is IE and return version No.
  function seven_isIE () {
	  var myNav = navigator.userAgent.toLowerCase();
	  return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
  }
  //random number generator
  function seven_rand_generator(limit)
  {
	  var order=new Array(limit);
	  for(var i=0;i<limit;i++)
	  {
		  var temp;
		  var tflag=true;
		  while(tflag)
		  {
			  tflag=false;
			  temp=Math.floor((Math.random()*limit));
			  for(var j=0;j<i;j++)
			  {
				  if(order[j]==temp)
						tflag=true;  
			  }
		  }
		  order[i]=temp;
	  }
	  return order;
  }
  //animation function
  function seven_animation(arg,code)
  {
	  //while animation is going on or same index
	  if(anim_flag==1||index==arg) return false;
	  option.onanimstart();
	  seven_pause();
	  //Initialization for anim
  	  anim_flag=1;
   	  newindex=arg;
	  handle.find("#seven_prev_slide").html(handle.find(".seven_slide:nth-child("+(index+1)+")").html());
	  handle.find("#seven_next_slide").html(handle.find(".seven_slide:nth-child("+(newindex+1)+")").html());
	  handle.find(".seven_video").remove();
	  //anim func
	  handle.find(".seven_func_slide").each(function(i)
	  {
					var arr=[index+1,newindex+1];
					var temp=seven_get_imagesize(handle.find(".seven_slide:nth-child("+arr[i]+")").find("img"));
					var temp_rate=temp.width/temp.height;
					if(rate>temp_rate)
						$(this).find("img").css("width",option.width);
					else 
						$(this).find("img").css("height",option.height);
	  });
	  
	 handle.find(".seven_circle").removeClass("active");
	 handle.find(".carousel").removeClass("active");
	 handle.find(".seven_circle:nth-child("+(newindex+1)+")").addClass("active");
	 handle.find(".carousel:nth-child("+(newindex+1)+")").addClass("active");
	 handle.find("#seven_prev_slide").css("left","0%");
	 handle.find("#seven_next_slide").css("left","100%");
  	 //adjust carousel Pos
	 if(option.carousel==true)
		  seven_adjustcarousel(arg);
	 //Initialization for Layer
	 handle.find("#seven_prev_slide .seven_slide_title").css("bottom","15px").animate({
			"bottom":"-40px",
			"opacity":0,
	 },
	 {
			duration:300,
			easing:"easeOutSine",
			complete: function()
			{	
				  //animation is done
				  handle.find("#seven_prev_slide .seven_slide_title").stop();
				  handle.find(".seven_slide_title").hide();
				  seven_bg_animate(arg,code);
			}
	});		
	  
  }
  function seven_linear_move(code)
  {
	  var p_arr=[[100,0,0,0],[-100,0,0,0],[0,100,0,0],[0,-100,0,0]];
	  var t_arr=[[-option.width,0],[option.width,0],[0,-option.height],[0,option.height]];
	  //initialize for anim
  	  handle.find("#seven_next_slide").css("left",p_arr[code][0]+"%").css("top",p_arr[code][1]+"%");
	  handle.find("#seven_prev_slide").css("left",p_arr[code][2]+"%").css("top",p_arr[code][3]+"%");
	  //anim func
	  handle.find('.seven_func_slide').each(function(i)
	  {
		  $(this).animate({
			"left":"+="+t_arr[code][0]+"px",
			"top":"+="+t_arr[code][1]+"px",
		  },
		  {
			duration:400,
			easing:"easeInOutSine",
 		    complete: function()
			{
				 //All animation is done
				 if(i==0) seven_end_animate(); 
			}
		  });
	  });
  }
  function seven_vbar_move(code)
  {
	  var p_arr=[-1,1,-1,1,-1,1];
	  var pease=["easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutBack","easeOutBack"];
	  var pfg=[1,0,1,0,0,1];
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
      var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  //initialize for anim
 	  handle.find("#seven_next_slide").css("left","100%").css("top","0%");
  	  handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

	  //prepare temp divs for transition
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
	  for(var i=0;i<20;i++)
	  {
	  		//calculate div pos
			var t_width=Math.ceil(option.width/20);
			var temp=t_width*i;		
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+(-p_arr[code]*option.width+temp)+"px;top:0%;'><img src='"+nsrc+"' style='position:absolute;left:-"+temp+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		

	  }
	  blind+="</div>";
	  $(blind).insertBefore(handle.find("#seven_next_slide"));			
	  handle.find(".seven_blind_slide").css("opacity",0);
	  //anim func
	  handle.find(".seven_blind_slide").each(function(i)
	  {
		  var temp;
		  if(pfg[code]==1)
		  	temp=i;
		  else
		  	temp=19-i;
			$(this).delay(temp*40).animate({
				"left":"+="+p_arr[code]*option.width+"px",
				"opacity":1,
			},
			{
				duration:800,
				easing:pease[code],
				complete: function()
				{	
					 // All animation is done
					 if(temp==19)						 
					 	seven_end_animate(); 
				}												 
			});
	  });	  
  }
  function seven_hbar_move(code)
  {
	  var p_arr=[-1,1,-1,1,-1,1];
	  var pease=["easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutBack","easeOutBack"];
	  var pfg=[0,1,0,1,1,0];
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
      var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/10);
			var temp=t_height*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0%;top:"+(p_arr[code]*option.height+temp)+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+temp+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
			
		handle.find(".seven_blind_slide").css("opacity",0);
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==1)
				temp=i;
			else
				temp=9-i;
			$(this).delay(temp*60).animate({
				"top":"+="+(-p_arr[code]*option.height)+"px",
				"opacity":1,
			},
			{
				duration:800,
				easing:pease[code],
				complete: function()
				{	
					 // All animation is done
					 if(temp==9)						 
					 	seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_hbar_rmove(code)
  {
	  var p_arr=[-1,1,-1,1,-1,1,-1,1,1,-1,1,-1,1,-1,1,-1,1,-1];
	  var pease=["easeOutSine","easeOutSine","easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutBack","easeOutBack","easeOutSine","easeOutSine","easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutBack","easeOutBack"];
	  var pfg=[0,0,1,1,0,0,1,1,-1,-1,-2,-2,-1,-1,-2,-2];
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
      var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  //initialize for anim
	  handle.find("#seven_next_slide").css("left","100%").css("top","0%");
	  handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

	  //prepare temp divs for transition
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
	  var sequence;
	  if(pfg[code]==-1)
	  		sequence=seven_rand_generator(10);
	  else
	  		sequence=[4,3,2,1,0,0,1,2,3,4];
	  for(var i=0;i<10;i++)
	  {
			//calculate div pos
			var t_height=Math.ceil(option.height/10);
			var temp=t_height*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:"+(-p_arr[code]*option.width)+"px;top:"+temp+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+temp+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
	  }
	  blind+="</div>";
	  $(blind).insertBefore(handle.find("#seven_next_slide"));
				
	  handle.find(".seven_blind_slide").css("opacity",0);
				
	  //anim func
	  handle.find(".seven_blind_slide").each(function(i)
	  {
		  	var temp;
		    if(pfg[code]==0) 
				temp=i;
			else if(pfg[code]>0)
				temp=9-i;
			else
				temp=sequence[i];
			$(this).delay(temp*60).animate({
				"left":"+="+p_arr[code]*option.width+"px",
				"opacity":1,
			},
			{
				duration:800,
				easing:pease[code],
				complete: function()
				{	
					 // All animation is done
					 if(pfg[code]!=-2){
					 	if(temp==9)		seven_end_animate(); 
					 }
					 else
					 {
						 if(i==9)	seven_end_animate(); 
					 }
				}												 
			});
	  });
  }
  function seven_vbar_rmove(code)
  {
		var p_arr=[-1,1,-1,1,-1,1,-1,1,1,-1,1,-1,1,-1,1,-1,1,-1];
	    var pease=["easeOutSine","easeOutSine","easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutBack","easeOutBack","easeOutSine","easeOutSine","easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutBack","easeOutBack"];
	  	var pfg=[0,0,1,1,0,0,1,1,-1,-1,-2,-2,-1,-1,-2,-2];
	  	var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		//initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	
		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		var sequence;
		if(pfg[code]==-1)
			sequence=seven_rand_generator(10);
		else
			sequence=[4,3,2,1,0,0,1,2,3,4];
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);
			var temp=t_width*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+temp+"px;top:"+(p_arr[code]*option.height)+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+temp+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		handle.find(".seven_blind_slide").css("opacity",0);
		
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{	
			var temp;
		    if(pfg[code]==0) 
				temp=i;
			else if(pfg[code]>0)
				temp=9-i;
			else
				temp=sequence[i];
			$(this).delay(temp*60).animate({
				"top":"+="+(-p_arr[code]*option.height)+"px",
				"opacity":1,


			},
			{
				duration:800,
				easing:pease[code],
				complete: function()
				{	
					 // All animation is done
					 if(pfg[code]!=-2){
					 	if(temp==9)		seven_end_animate(); 
					 }
					 else
					 {
						 if(i==9)	seven_end_animate(); 
					 }
				}												 
			});
		});	  
   }
   function seven_hbar_intersect(code)
   {
	    var pease=["easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutSine","easeOutBack"];
	  	var pfg=[0,1,0,1,2,2];
	  	var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   	//initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/10);
			var temp=option.width-option.width*2*(i%2);
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:"+temp+"px;top:"+t_height*i+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
			
		handle.find(".seven_blind_slide").css("opacity",0);
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==0)
				temp=i;
			else if(pfg[code]==1)
				temp=9-i;
			else
				temp=0;
			$(this).delay(temp*60).animate({
				"left":"0px",
				"opacity":1,
			},
			{
				duration:800,
				easing:pease[code],
				complete: function()
				{	
					 // All animation is done
					 if(pfg[code]==2)
					 {
						 if(i==9)
						 	seven_end_animate();
					 }
					 else if(temp==9)						 
					 	seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_vbar_intersect(code)
   {
	    var pease=["easeOutSine","easeOutSine","easeOutBack","easeOutBack","easeOutSine","easeOutBack"];
	  	var pfg=[0,1,0,1,2,2];
	  	var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);
			var temp=option.height-option.height*2*(i%2);
					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:"+temp+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
				
		handle.find(".seven_blind_slide").css("opacity",0);
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{					
			var temp;
			if(pfg[code]==0)
				temp=i;
			else if(pfg[code]==1)
				temp=9-i;
			else
				temp=0;
			$(this).delay(temp*60).animate({
				"top":"0px",
				"opacity":1,
			},
			{
				duration:800,
				easing:pease[code],
				complete: function()
				{	
					 // All animation is done
					 if(pfg[code]==2)
					 {
						 if(i==9)
						 	seven_end_animate();
					 }
					 else if(temp==9)						 
					 	seven_end_animate(); 
				}												 
			});
		});	
   }
   function seven_fade()
   {
	   //initialize for anim
 		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		
				
		//prepare for anim
		handle.find("#seven_next_slide").css("opacity",0);
				
		//anim func
		handle.find("#seven_next_slide").animate({
			"opacity":1,
		},
		{
			duration:800,
			easing:"easeOutSine",
			complete: function()
			{	
			    // All animation is done
			 	seven_end_animate(); 
			}												 
				
		});
   }
   function seven_fade_overlap()
   {
	   var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		var b_pos=[[-100,-100],[-100,100],[100,100],[100,-100]];
		for(var i=0;i<4;i++)
		{
			//calculate div pos
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:"+b_pos[i][0]+"px;top:"+b_pos[i][1]+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide img").css("opacity",0);
				
		//anim func
		handle.find(".seven_blind_slide img").each(function(i)
		{
			$(this).animate({
				"left":0,
				"top":0,
				"opacity":1,
			},
			{
				duration:600,
				easing:"easeOutQuad",
				complete: function()
				{	
					 // All animation is done
					 if(i==3)
					 	seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_blind(code)
   { 
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
  		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		for(var i=0;i<12;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/12);					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			temp=(code==1)?i:0;
			$(this).delay(temp*50).animate({
				"width":0,
			},
			{
				duration:500,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
				 	 if(i==11)
					 	seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_vblind(code)
   {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		for(var i=0;i<12;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/12);					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><img src='"+psrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			temp=(code==1)?i:0;
			$(this).delay(temp*50).animate({
				"height":0,
			},
			{
				duration:500,
				easing:"easeOutQuad",
				complete: function()
				{	
					 // All animation is done
				 	 if(i==11)
					 	seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_vcut(code)
   {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,1,-1,-1];
		var pfg=[0,1,0,1];
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
				
		for(var i=0;i<5;i++)
		{
			//add sub divs for transition
			var t_width=Math.ceil(option.width/5);		
			if(p_arr[code]==1)
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:200%;left:"+t_width*i+"px;top:-"+option.height+"px;'><div class='seven_sub_blind_slide' style='width:100%;height:50%;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_sub_blind_slide' style='width:100%;height:50%;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";		
			else
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:200%;left:"+t_width*i+"px;top:0px;'><div class='seven_sub_blind_slide' style='width:100%;height:50%;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div><div class='seven_sub_blind_slide' style='width:100%;height:50%;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";
			
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==0)
				temp=i;
			else
				temp=4-i;
			$(this).delay(150*temp).animate({
				"top":"+="+(p_arr[code]*option.height)+"px",
			},
			{
				duration:400,
				easing:"easeInOutQuart",
				complete: function()
				{	
					 // All animation is done
					 if(temp==4)
					 	seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_hcut(code)
   {
	   var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
     	var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1,1,-1];
		var pfg=[0,0,1,1];
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
				
		for(var i=0;i<5;i++)
		{
			//add sub divs for transition
			var t_height=Math.ceil(option.height/5);		
			if(p_arr[code]==1)
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:200%;height:"+t_height+"px;left:-"+option.width+"px;top:"+t_height*i+"px;'><div class='seven_sub_blind_slide' style='float:left;width:50%;height:100%;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_sub_blind_slide' style='float:left;width:50%;height:100%;'><img src='"+psrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";		
			else
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:200%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><div class='seven_sub_blind_slide' style='float:left;width:50%;height:100%;'><img src='"+psrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div><div class='seven_sub_blind_slide' style='float:left;width:50%;height:100%;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";	
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));

		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==0)
				temp=i;
			else
				temp=4-i;
			$(this).delay(100*temp).animate({
				"left":"+="+(p_arr[code]*option.width)+"px",
			},
			{
				duration:500,
				easing:"easeOutQuad",
				complete: function()
				{	
					 // All animation is done
					 if(temp==4)
					 	seven_end_animate(); 
				}												 
			});
		});				
   }
   function seven_square(code)
   {
	   var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
       var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   var p_arr=[[-1,1],[1,1],[-1,-1],[1,-1],[-1,1],[1,1],[-1,-1],[1,-1]];
	   var pfg=[0,1,0,1,0,1,0,1];
	   var pease=["easeOutExpo","easeOutExpo","easeOutExpo","easeOutExpo","easeOutBack","easeOutBack","easeOutBack","easeOutBack"];
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
				
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			var row=i%3;
			var col=parseInt(i/3);
					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+(t_width*col+p_arr[code][0]*200)+"px;top:"+(t_height*row+p_arr[code][1]*200)+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";

			
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide img").css("opacity",0);
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			temp=(pfg[code]==0)?17-i:i;				
			$(this).find("img").delay(60*temp).animate({
				"opacity":0.8,
			},
			{
				duration:300,
				easing:pease[code],
				complete: function()
				{
					// second anim func-> fadeTo opacity:1
					 $(this).animate({
						//opacity fade to final phase:1
						"opacity":"1",
					},
					{
						duration:50,
						easing:pease[code],
						complete: function()
						{	
							 // All animation is done
							 if(temp==17)
								seven_end_animate(); 
						}												 
					});
				}
			});
			$(this).delay(60*temp).animate({
				"left":"+="+(-p_arr[code][0]*200)+"px",
				"top":"+="+(-p_arr[code][1]*200)+"px",
			},
			{
				duration:300,
				easing:pease[code],
				complete: function()
				{	
				}												 
			});
		});		
   }
   function seven_square_fade(code)
   {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[[1,1],[-1,1],[1,-1],[-1,-1],[0,0],[0,0]];
		var pfg=[0,0,0,0,1,0];
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			var row=i%3;
			var col=parseInt(i/3);
				
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==0)
				temp=i;
			else
				temp=17-i;
			$(this).find("img").delay(40*temp).animate({
				"opacity":0,
			},
			{
				duration:500,
				easing:"easeOutQuad",
				complete: function()
				{	
				}												 
			});
			$(this).delay(40*temp).animate({
				"left":"+="+p_arr[code][0]*100+"px",
				"top":"+="+p_arr[code][1]*100+"px",
			},
			{
				duration:500,
				easing:"easeOutQuad",
				complete: function()
				{	
					 // All animation is done
					 if(temp==17)
						seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_hsquare_fade(code)
   {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[[0,0,0],[0,0,0],[1,1,1],[-1,1,1],[1,-1,1],[-1,-1,1]];
		var pfg=[0,1,0,0,0,0];
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
				
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			if(code<2)
			{
				row=parseInt(i/6);
				col=i%6;
			}
			else
			{
				row=i%3;
				col=parseInt(i/3);
			}
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==0) temp=i;
			else
				temp=17-i;
			$(this).find("img").delay(50*temp).animate({
				"opacity":0,
			},
			{
				duration:450,
				easing:"easeInOutBack",
				complete: function()
				{	
				}												 
			});
			$(this).delay(50*temp).animate({
				"left":"+="+p_arr[code][0]*20+"px",
				"top":"+="+p_arr[code][1]*20+"px",
				"width":"+="+p_arr[code][2]*100+"px",
				"height":"+="+p_arr[code][2]*100+"px",
			},
			{
				duration:450,
				easing:"easeInOutBack",
				complete: function()
				{	
					 // All animation is done
					 if(temp==17)
						seven_end_animate(); 
				}												 
			});
		});
   }
   function seven_border_hide(code)
   {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence;
		if(code==0)
			sequence=[0,1,2,3,4,5,13,14,15,16,17,6,12,11,10,9,8,7];
		else if(code==1)
			sequence=[0,1,2,3,4,5,11,10,9,8,7,6,12,13,14,15,16,17];
		else if(code==2)
			sequence=[17,16,15,14,13,12,6,7,8,9,10,11,5,4,3,2,1,0];
		else if(code==3)
			sequence=[0,5,6,11,12,17,1,4,7,10,13,16,2,3,8,9,14,15];
		else if(code==4)
			sequence=[15,14,9,8,3,2,16,14,10,7,4,1,17,12,11,6,5,0];
			
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			var row=parseInt(i/6);
			var col=i%6;
					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
			
		$(blind).insertBefore(handle.find("#seven_next_slide"));
			
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).find("img").delay(40*sequence[i]).animate({
				"opacity":"0",
			},
			{
				duration:500,
				easing:"easeOutQuad",
				complete: function()
				{	
					 // All animation is done
					 if(sequence[i]==17)
						seven_end_animate(); 
				}												 
			});
		});
   }
  function seven_random_hide()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
	    handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence=seven_rand_generator(25);
		for(var i=0;i<25;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/5);
			var t_height=Math.ceil(option.height/5);
			var row=parseInt(i/5);
			var col=i%5;
					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));
			
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).find("img").delay(40*sequence[i]).animate({
				"opacity":"0",
			},
			{
				duration:500,
				easing:"easeOutQuad",
				complete: function()
				{	
					 // All animation is done
					 if(sequence[i]==24)
						seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_square_plazma(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[-1,1];
		//initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence;
		if(code==0)
			sequence=[0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9];
		else
			sequence=[9,8,7,6,5,4,3,2,1,0,9,8,7,6,5,4,3,2,1,0];
				
		for(var i=0;i<20;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);
			var t_height=Math.ceil(option.height/2);
			var row=parseInt(i/10);
			var col=i%10;
			var temp;
			
			if(row==0)
			{
				temp=t_height*row-t_height;
			}
			else
			{
				temp=t_height*row+t_height;												
			}
				
			if(code==0)
				blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+temp+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:"+t_height+"px;left:50px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";		
			else
				blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+temp+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:"+t_height+"px;left:-50px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";

				
		}
		blind+="</div>";

		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			var ate;
			ate=(code==0)?9:0;
			if(i<10)
				temp="+="+Math.ceil(option.height/2)+"px";
			else
				temp="-="+Math.ceil(option.height/2)+"px";
			$(this).delay(100*sequence[i]).animate({
				  "top":temp,												
			},{
				  duration:400,
				  easing: "easeOutQuad",
				  complete: function(){
				  }
			});
					
			$(this).find(".seven_blind_slide").delay(100*sequence[i]).animate({
				  "left":"+="+p_arr[code]*50+"px",												
				  "opacity":1,
			},{
				  duration:700,
				  easing: "easeOutSine",
				  complete: function(){
					  if(i==ate)
					  {
						  //All animation is done
						  seven_end_animate();
					  }
				  }
			});
		});	  
  }
  function seven_vsquare_plazma(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,1,-1,-1];
		var pfg=[0,1,1,0];
		//initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence;
		if(pfg[code]==0)
			sequence=[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9];
		else
			sequence=[9,9,8,8,7,7,6,6,5,5,4,4,3,3,2,2,1,1,0,0];
		
		for(var i=0;i<20;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/2);
			var t_height=Math.ceil(option.height/10);
			var row=parseInt(i/2);
			var col=i%2;
			var temp;
					
			if(col==0)
			{
				temp=t_width*col-t_width;
			}
			else
			{
				temp=t_width*col+t_width;												
			}
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height*2+"px;left:"+temp+"px;top:"+t_height*row+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height*2+"px;top:"+p_arr[code]*50+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";		
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));

		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			var ate;
			ate=(pfg[code]==0)?19:0;
			if(i%2==0)
				temp="+="+Math.ceil(option.width/2)+"px";
			else
				temp="-="+Math.ceil(option.width/2)+"px";
			$(this).delay(100*sequence[i]).animate({
				  "left":temp,												
			},{
				  duration:400,
				  easing: "easeOutQuad",
				  complete: function(){
					  }
			});
				
			$(this).find(".seven_blind_slide").delay(100*sequence[i]).animate({
				  "top":"+="+(-p_arr[code]*50)+"px",												
				  "opacity":1,
			},{
				  duration:700,
				  easing: "easeOutSine",
				  complete: function(){
					  if(i==ate)
					  {
						  //All animation is done
						  seven_end_animate();
					  }
				  }
			});
		});	  
  }
  function seven_vplazma(code)
  {
	    var p_arr=[-1,1];
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
				
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:"+(t_width*i+p_arr[code]*50)+"px;top:0px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*i+"px;top:0px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		

					
		}
		blind+="</div>";
			
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide").find("img").css("opacity",0);
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			temp=(code==0)?9-i:i;
			$(this).find("img").delay(100*temp).animate({
				  "opacity":"1",	
			},{
				  duration:500,
				  easing: "easeInQuad",
				  complete: function()
				  {
				  }
			});
			$(this).delay(100*temp).animate({
				  "left":"+="+(-p_arr[code]*50)+"px",	
			},{
				  duration:500,
				  easing: "easeInQuad",
				  complete: function()
				  {
					  	//All animation is done
						if(temp==9)
							seven_end_animate();
				  }
			});
		});			
  }
  function seven_hplazma(code)
  {
	    var p_arr=[-1,1];
		var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
				
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/10);
					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height*2+"px;left:0px;top:"+(t_height*i+p_arr[code]*30)+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";				
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide img").css("opacity",0);
				
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			temp=(code==0)?9-i:i;
			$(this).find("img").delay(100*temp).animate({
				  "opacity":1,
			},{
				  duration:500,
				  easing: "easeInQuad",
				  complete: function()
				  {
				  }
			});
			$(this).delay(100*temp).animate({
				  "top":"+="+(-p_arr[code]*30)+"px",	
			},{
				  duration:500,
				  easing: "easeInQuad",
				  complete: function()
				  {
					  	//All animation is done
						if(temp==9)
							seven_end_animate();
				  }
			});
		});
  }
  function seven_water()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var radius=Math.ceil(Math.max(option.width,option.height)/20);				
		var centerx=Math.ceil(option.width/2);
		var centery=Math.ceil(option.height/2);
				
		for(var i=10;i>=0;i--)
		{
			//calculate div pos
			var tempx,tempy;
			if((centerx-radius*(i+1))>0)
				tempx="-"+(centerx-radius*(i+1));
			else
				tempx=Math.abs(centerx-radius*(i+1));
			
			if((centery-radius*(i+1))>0)
				tempy="-"+(centery-radius*(i+1));
			else
				tempy=Math.abs(centery-radius*(i+1));
					
			if(seven_isIE()!=false&&seven_isIE()<9)
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;'><img src='"+nsrc+"' style='position:absolute;left:"+tempx+"px;top:"+tempy+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
			}
			else
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;background:url("+nsrc+");background-size:"+nimagewidth+"px "+nimageheight+"px;background-position: "+tempx+"px "+tempy+"px;background-repeat:no-repeat;'></div>";
			}							
			
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		if(seven_isIE()<9&&seven_isIE()!=false)
		{
			handle.find(".seven_blind_slide img").css("opacity",0);
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				$(this).find("img").delay(50*(10-i)).animate({
					  "opacity":1,
				},{
					  duration:600,
					  easing: "easeInQuad",
					  complete: function()
					  {
							//All animation is done
							if(i==0)
								seven_end_animate();
					  }
				});
			});
		}
		else
		{
			handle.find(".seven_blind_slide").css("opacity",0);
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				$(this).delay(50*(10-i)).animate({
					  "opacity":1,
				},{
					  duration:600,
					  easing: "easeInQuad",
					  complete: function()
					  {
							//All animation is done
							if(i==0)
								seven_end_animate();
					  }
				});
			});
		}
  }
  function seven_water_inside()
  {
	  
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		//initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var radius=Math.ceil(Math.max(option.width,option.height)/20);				
		var centerx=Math.ceil(option.width/2);
		var centery=Math.ceil(option.height/2);
				
		for(var i=10;i>=0;i--)
		{
			//calculate div pos
			var tempx,tempy;
			if((centerx-radius*(i+1))>0)
				tempx="-"+(centerx-radius*(i+1));
			else
				tempx=Math.abs(centerx-radius*(i+1));
			
			if((centery-radius*(i+1))>0)
				tempy="-"+(centery-radius*(i+1));
			else
				tempy=Math.abs(centery-radius*(i+1));
			if(seven_isIE()!=false&&seven_isIE()<9)
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;'><img src='"+psrc+"' style='position:absolute;left:"+tempx+"px;top:"+tempy+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
			}
			else
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;background:url("+psrc+");background-size:"+pimagewidth+"px "+pimageheight+"px;background-position: "+tempx+"px "+tempy+"px;background-repeat:no-repeat;'></div>";
			}
		}
		blind+="</div>";
				
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		if(seven_isIE()!=false&&seven_isIE()<9)
		{
			handle.find(".seven_blind_slide img").css("opacity",1);
			//anim func
			handle.find(".seven_blind_slide img").each(function(i)
			{
				$(this).delay(50*i).animate({
					  "opacity":0,
				},{
					  duration:600,
					  easing: "easeInQuad",
					  complete: function()
					  {
							//All animation is done
							if(i==10)
								seven_end_animate();
					  }
				});
			});
		}
		else
		{
			handle.find(".seven_blind_slide").css("opacity",1);
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				$(this).delay(50*i).animate({
					  "opacity":0,
				},{
					  duration:600,
					  easing: "easeInQuad",
					  complete: function()
					  {
							//All animation is done
							if(i==10)
								seven_end_animate();
					  }
				});
			});
		}
  }
  function seven_water_rotate_cross()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	
	
		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var radius=Math.ceil(Math.max(option.width,option.height)/20);				
		var centerx=Math.ceil(option.width/2);
		var centery=Math.ceil(option.height/2);
		
		for(var i=10;i>=0;i--)
		{
			//calculate div pos
			var tempx,tempy;
			if((centerx-radius*(i+1))>0)
				tempx="-"+(centerx-radius*(i+1));
			else
				tempx=Math.abs(centerx-radius*(i+1));
			
			if((centery-radius*(i+1))>0)
				tempy="-"+(centery-radius*(i+1));
			else
				tempy=Math.abs(centery-radius*(i+1));
			
			if(seven_isIE()!=false&&seven_isIE()<9)
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;'><img src='"+psrc+"' style='position:absolute;left:"+tempx+"px;top:"+tempy+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
			}
			else
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;background:url("+psrc+");background-size:"+pimagewidth+"px "+pimageheight+"px;background-position: "+tempx+"px "+tempy+"px;background-repeat:no-repeat;'></div>";
			}		
	
			
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		if(seven_isIE()>8||seven_isIE()==false)
		{
			handle.find(".seven_blind_slide").css("opacity",1).each(function(i)
			{
				var temp;
				
				if(i%2==0)
					temp="20deg";
				else
					temp="-20deg";
				
				
					$(this).delay(80*i).animate({
					  "opacity":0,
					  "rotate":temp,
					},{
						  duration:500,
						  easing: "easeInQuad",
						  complete: function()
						  {
								//All animation is done
								if(i==10)
									seven_end_animate();
						  }
					});
				});
			
		}
		else
		{
			handle.find(".seven_blind_slide").each(function(i)
			{									  
				$(this).find("img").css("opacity",1);
				$(this).find("img").delay(80*i).animate({
				  "opacity":0,
				},{
					  duration:500,
					  easing: "easeInQuad",
					  complete: function()
					  {
							//All animation is done
							if(i==10)
								seven_end_animate();
					  }
				});
			});
		}
  }
  function seven_circle_rotate(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var radius=Math.ceil(Math.max(option.width,option.height)/20);				
		var centerx=Math.ceil(option.width/2);
		var centery=Math.ceil(option.height/2);
		
		for(var i=10;i>=0;i--)
		{
			//calculate div pos
			var tempx,tempy;
			if((centerx-radius*(i+1))>0)
				tempx="-"+(centerx-radius*(i+1));
			else
				tempx=Math.abs(centerx-radius*(i+1));
			
			if((centery-radius*(i+1))>0)
				tempy="-"+(centery-radius*(i+1));
			else
				tempy=Math.abs(centery-radius*(i+1));
			
			if(seven_isIE()!=false&&seven_isIE()<9)
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;'><img src='"+psrc+"' style='position:absolute;left:"+tempx+"px;top:"+tempy+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
			}
			else
			{
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+radius*2*(i+1)+"px;height:"+radius*2*(i+1)+"px;left:"+(centerx-radius*(i+1))+"px;top:"+(centery-radius*(i+1))+"px;border-radius:"+radius*2*(i+1)+"px;background:url("+psrc+");background-size:"+pimagewidth+"px "+pimageheight+"px;background-position: "+tempx+"px "+tempy+"px;background-repeat:no-repeat;'></div>";
			}		

			
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		if(seven_isIE()>8||seven_isIE()==false)
		{
			var temp;
			temp=(code==0)?"20deg":"-20deg";
			handle.find(".seven_blind_slide").css("opacity",1).each(function(i)
			{				
				
					$(this).delay(80*i).animate({
					  "opacity":0,
					  "rotate":temp,
					},{
						  duration:500,
						  easing: "easeInQuad",
						  complete: function()
						  {
								//All animation is done
								if(i==10)
									seven_end_animate();
						  }
					});
				});
			
		}
		else
		{
			handle.find(".seven_blind_slide").each(function(i)
			{									  
				$(this).find("img").css("opacity",1);
				$(this).find("img").delay(80*i).animate({
				  "opacity":0,
				},{
					  duration:500,
					  easing: "easeInQuad",
					  complete: function()
					  {
							//All animation is done
							if(i==10)
								seven_end_animate();
					  }
				});
			});
		}
  }
  function seven_swap_block()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	
		for(var i=0;i<2;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/2);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><div class='seven_blind_slide' style='position:absolute;width:100%;height:"+t_height+"px;top:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div><div class='seven_blind_slide_temp' style='position:absolute;width:100%;height:"+t_height+"px;top:"+(t_height-2*t_height*i)+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";		
								
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));

		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			
			//variables for Transition
			var temp;
			if(i%2==0)
				temp=Math.ceil(option.height/2);
			else
				temp=-(Math.ceil(option.height/2));
			
			$(this).find(".seven_blind_slide").animate({
				"top":temp+"px",
			},
			{
				 duration:500,
				 easing: "linear",
				 complete: function()
				 {
					//All animation is done
					if(i==1)
						seven_end_animate();
				 }
			});
			
			$(this).find(".seven_blind_slide_temp").animate({
				 "top":"0px",
			},
			{
				  duration:500,
				  easing: "linear",
				  complete: function()
				  {
				  }
			});		
			
		});
  }
  function seven_swap_hblock(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1,1,-1];
		var pease=["easeOutSine","easeOutSine","easeOutBack","easeOutBack"];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	
		for(var i=0;i<5;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/5);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+p_arr[code]*t_width+"px;z-index:10;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";		
				
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));

		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			
			//variables for Transition
			$(this).find(".seven_blind_slide").delay(i*100).animate({
				"left":"+="+(-p_arr[code]*Math.ceil(option.width/5))+"px",
			},
			{
				 duration:500,
				 easing: pease[code],
				 complete: function()
				 {
					//All animation is done
					if(i==4)
						seven_end_animate();
				 }
			});
			
		});
  }
  function seven_swap_vblock(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1,1,-1];
		var pease=["easeOutSine","easeOutSine","easeOutBack","easeOutBack"];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	
		for(var i=0;i<5;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/5);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;top:"+(-p_arr[code]*t_height)+"px;z-index:10;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;top:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";		
								
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));

		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			
			//variables for Transition
			$(this).find(".seven_blind_slide").delay(i*100).animate({
				"top":"+="+(p_arr[code]*Math.ceil(option.height/5))+"px",
			},
			{
				 duration:500,
				 easing: pease[code],
				 complete: function()
				 {
					//All animation is done
					if(i==4)
					{
						if($(this).index()==0)
								seven_end_animate();
					}
				 }
			});
			
		});
  }
  function seven_tile_sequence(code)
  {
		var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1];
  	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			var row=i%3;
			var col=parseInt(i/3);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+(-p_arr[code]*t_width)+"px;top:-"+t_height+"px;z-index:15;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_blind_slide_temp' style='position:absolute;width:"+t_width+"px;height:"+t_height+"px;top:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";
			
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide").css("opacity",0);
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			temp=(code==0)?i:17-i;
			$(this).delay(60*temp).animate({
				"left":"+="+(p_arr[code]*Math.ceil(option.width/6))+"px",
				"top":"+="+Math.ceil(option.height/3)+"px",
				"opacity":"1",
			},
			{
				duration:500,
				easing:"easeOutSine",
				complete: function()
				{	
					//All animation is done
					if(temp==17)
						seven_end_animate();
				}												 
			});
		});
  }
  function seven_tile_psequence(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1,1,-1];
		var pfg=[0,1,-1,-1];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence;
		if(code>1)
			sequence=seven_rand_generator(18);
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			var row=i%3;
			var col=parseInt(i/3);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><div class='seven_blind_slide' style='position:absolute;width:"+t_width+"px;height:"+t_height+"px;left:"+(-p_arr[code]*t_width)+"px;top:0px;z-index:15;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_blind_slide_temp' style='position:absolute;width:"+t_width+"px;height:"+t_height+"px;top:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";
			
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide").css("opacity",0);
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(pfg[code]==0)
				temp=i;
			else if(pfg[code]==1)
				temp=17-i;
			else
				temp=sequence[i];
			$(this).delay(60*temp).animate({
				"left":"+="+(p_arr[code]*Math.ceil(option.width/6))+"px",
				"opacity":"1",
			},
			{
				duration:500,
				easing:"easeOutSine",
				complete: function()
				{	
					//All animation is done
					if(temp==17)
						seven_end_animate();
				}												 
			});
		});
  }
  function seven_tile_random_direct(code)
  {
 	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[[-1,-1],[1,-1],[-1,1],[1,1]];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence=seven_rand_generator(18);
		
		for(var i=0;i<18;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			var t_height=Math.ceil(option.height/3);
			var row=i%3;
			var col=parseInt(i/3);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><div class='seven_blind_slide' style='position:absolute;width:"+t_width+"px;height:"+t_height+"px;left:"+(p_arr[code][0]*t_width)+"px;top:"+(p_arr[code][1]*t_height)+"px;z-index:15;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div><div class='seven_blind_slide_temp' style='position:absolute;width:"+t_width+"px;height:"+t_height+"px;top:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";
			
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		handle.find(".seven_blind_slide").css("opacity",0);
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).delay(60*sequence[i]).animate({
				"left":"+="+(-p_arr[code][0]*Math.ceil(option.width/6))+"px",
				"top":"+="+(-p_arr[code][1]*Math.ceil(option.height/3))+"px",
				"opacity":"1",
			},
			{
				duration:500,
				easing:"easeOutSine",
				complete: function()
				{	
					//All animation is done
					if(sequence[i]==17)
						seven_end_animate();
				}												 
			});
		});

  }
  function seven_htwist(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/10);		
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;top:-"+t_height*i+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide img").each(function(i)
		{
			var temp;
			temp=(code==0)?i:9-i;
			$(this).delay(temp*50).animate({
				"width":"0",
				"left":Math.ceil(option.width/2)+"px",
			},
			{
				duration:400,
				easing:"easeInQuart",
				complete: function()
				{	
					//change the new Img 
					$(this).attr("src",nsrc);
					$(this).animate({
						"width":pimagewidth+"px",
						"left":"0px",
					},
					{
						duration:500,
						easing:"easeOutQuad",
						complete: function()
						{	
							 // All animation is done
							 if(temp==9)
								seven_end_animate();
						}												 
					});						
				}												 
			});
		});
  }
  function seven_vtwist(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);		
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;left:-"+t_width*i+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide img").each(function(i)
		{
			var temp;
			temp=(code==0)?i:9-i;
			$(this).delay(temp*50).animate({
				"height":"0",
				"top":Math.ceil(option.height/2)+"px",
			},
			{
				duration:400,
				easing:"easeInQuart",
				complete: function()
				{	
					//change the new Img 
					$(this).attr("src",nsrc);
					$(this).animate({
						"height":pimageheight+"px",
						"top":"0px",
					},
					{
						duration:500,
						easing:"easeOutQuad",
						complete: function()
						{	
							 // All animation is done
							 if(temp==9)
								seven_end_animate();
						}												 
					});						
				}												 
			});
		});
  }
  function seven_chain(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		var t_width=Math.ceil(option.width/4);
		for(var i=0;i<4;i++)
		{
			//calculate div pos
			var temp;
			if(i==0)
				temp=t_width;
			else
				temp=-t_width*(i-1);

			if(code==0)
				blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:-"+t_width*2+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:-"+t_width*i+"px;'/></div></div>";
			else
				blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:"+t_width+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:"+-t_width*(i-1)+"px;'/></div></div>";
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			temp=(code==0)?i:3-i;
			$(this).find(".seven_blind_slide").delay(200*temp).animate({
				"left":"+="+(p_arr[code]*t_width*2)+"px",
			},
			{
				duration:600,
				easing:"easeOutSine",
				complete: function()
				{	
					//All animation is done
					if(temp==3)
						seven_end_animate();
				}												 
			});
		});

  }
  function seven_schain(code)
  {
	  	var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		var t_width=Math.ceil(option.width/3);
		for(var i=0;i<3;i++)
		{
			//calculate div pos
			var temp;
			if(i==0)
				temp=t_width;
			else
				temp=-t_width*(i-1);

			if(code==0)
				blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:-"+t_width*2+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:-"+t_width*i+"px;'/></div></div>";
			else
				blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:"+t_width+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:"+-t_width*(i-1)+"px;'/></div></div>";
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			temp=(code==0)?i:2-i;
			$(this).find(".seven_blind_slide").delay(250*temp).animate({
				"left":"+="+(p_arr[code]*t_width*2)+"px",
			},
			{
				duration:350,
				easing:"easeOutSine",
				complete: function()
				{	
					//All animation is done
					if(temp==2)
						seven_end_animate();
				}												 
			});
		});

  }
  function seven_tile_random()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var pos_arr=[[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]];
		var sequence=seven_rand_generator(10);

		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/5);
			var t_height=Math.ceil(option.height/2);
			var row=i%2;
			var col=parseInt(i/2);
			var temp=Math.floor((Math.random()*8));
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:"+t_height*2+"px;left:"+t_width*pos_arr[temp][0]*2+"px;top:"+t_height*pos_arr[temp][1]*2+"px;z-index:15;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:"+-t_width*col+"px;top:-"+t_height*row+"px;'/></div><div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;top:0px;z-index:10;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;left:"+-t_width*col+"px;top:-"+t_height*row+"px;'/></div></div>";
			
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));

		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).delay(120*sequence[i]).animate({
				"left":"0px",
				"top":"0px",
			},
			{
				duration:600,
				easing:"easeOutQuad",
				complete: function()
				{	
					//All animation is done
					if(sequence[i]==9)
						seven_end_animate();
				}												 
			});
		});
  }
  function seven_fadezoomout()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var t_rate=3;
		
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";			
		//calculate div pos
		blind+="<div class='seven_blind_subcontainer'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;z-index:15;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";

		blind+="<div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div>";
		
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));		
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			$(this).find(".seven_blind_slide img").animate({
				"width":t_rate*pimagewidth+"px",
				"height":t_rate*pimageheight+"px",
				"left":"-"+Math.ceil((t_rate-1)*option.width/2)+"px",
				"top":"-"+Math.ceil((t_rate-1)*option.height/2)+"px",
				"opacity":0,

			},
			{
				duration:600,
				easing:"easeInQuad",
				complete: function()
				{	
					//All animation is done
					seven_end_animate();
				}												 
			});
			
		});
  }
  function seven_fadezoomin()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var t_rate=3;
		
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";			
		//calculate div pos
		blind+="<div class='seven_blind_subcontainer'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0;top:0;z-index:15;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth*t_rate+"px;height:"+nimageheight*t_rate+"px;left:-"+nimagewidth*parseInt((t_rate-1)/2)+"px;top:-"+nimageheight*parseInt((t_rate-1)/2)+"px;'/></div>";

		blind+="<div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div></div>";
		
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));		
		handle.find(".seven_blind_slide img").css("opacity",0);
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			$(this).find(".seven_blind_slide img").animate({
				"width":pimagewidth+"px",
				"height":pimageheight+"px",
				"left":"0px",
				"top":"0px",
				"opacity":1,

			},
			{
				duration:600,
				easing:"easeInQuad",
				complete: function()
				{	
					//All animation is done
					seven_end_animate();
				}												 
			});

			
			
		});
	
  }
  function seven_htail(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,-1,1,-1,1,-1];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var	sequence=seven_rand_generator(10);
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/10);					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;top:-"+t_height*i+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(code<2)
				temp=i;
			else if(code<4)
				temp=9-i;
			else
				temp=sequence[i];
			$(this).find("img").delay(temp*80).animate({
				"left":"+="+(-p_arr[code]*300)+"px",
				"opacity":0,
			},
			{
				duration:600+temp*10,
				easing:"easeInOutSine",
				complete: function()
				{	
					 // All animation is done
					 if(temp==9)						 
						seven_end_animate(); 
				}												 
			});
		});	
  }
  function seven_vtail(code)
  {
	  	var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[1,1,-1,-1,1,-1];
		
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	
		var sequence=seven_rand_generator(10);
		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);					
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;left:-"+t_width*i+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp;
			if(code>3)
				temp=sequence[i];
			else if(code%2==0)
				temp=i;
			else
				temp=9-i;
			$(this).find("img").delay(temp*80).animate({
				"top":"+="+p_arr[code]*200+"px",
				"opacity":0,
			},
			{
				duration:600+temp*10,
				easing:"easeInOutSine",
				complete: function()
				{	
					 // All animation is done
					 if(temp==9)						 
						seven_end_animate(); 
				}												 
			});
		});	
  }
  function seven_fly(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var p_arr=[[1,0],[-1,0],[0,-1],[0,1]];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		//calculate div pos			
		blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;left:"+200*p_arr[code][0]+"px;top:"+p_arr[code][1]*200+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		blind+="<div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		

		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		if(seven_isIE()!=false&&seven_isIE()<9)
		{
			handle.find(".seven_blind_slide img").css("opacity",0);
			handle.find(".seven_blind_slide_temp img").css("opacity",0.7);
			
			//anim func
			handle.find(".seven_blind_slide").animate({
				"left":"+="+(-p_arr[code][0]*200)+"px",
				"top":"+="+(-p_arr[code][1]*200)+"px",
			},
			{
				duration:600,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
				}												 
			});
			handle.find(".seven_blind_slide img").animate({
				"opacity":1,

			},
			{
				duration:600,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
				}												 
			});
			//consequent animation
			handle.find(".seven_blind_slide_temp").each(function(i)
			{
				$(this).find("img").delay(200).animate({
				"left":"+="+(-p_arr[code][0]*200)+"px",
				"top":"+="+(-p_arr[code][1]*200)+"px",
				"opacity":0,
				},
				{
					duration:800,
					easing:"easeOutSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)
							seven_end_animate();
					}												 
				});
			});
			

		}
		else
		{
			handle.find(".seven_blind_slide").css("opacity",0);
			handle.find(".seven_blind_slide_temp").css("opacity",0.7);
			
			//anim func
			handle.find(".seven_blind_slide").animate({
				"left":"+="+(-p_arr[code][0]*200)+"px",
				"top":"+="+(-p_arr[code][1]*200)+"px",
				"opacity":1,
			},
			{
				duration:600,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
				}												 
			});
			//consequent animation
			handle.find(".seven_blind_slide_temp").each(function(i)
			{
				$(this).delay(200).animate({
				"left":"+="+(-p_arr[code][0]*200)+"px",
				"top":"+="+(-p_arr[code][1]*200)+"px",
				"opacity":0,
				},
				{
					duration:800,
					easing:"easeOutSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)
							seven_end_animate();
					}												 
				});
			});
		}
  }
  function seven_rotate()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var t_rate=Math.ceil(option.width/option.height)*4+1;
		
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";			
		//calculate div pos
		blind+="<div class='seven_blind_subcontainer'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;z-index:15;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div><div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:100%;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div></div></div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		if(seven_isIE()!=false&&seven_isIE()<9)
		{
			handle.find(".seven_blind_subcontainer").each(function(i)
			{
				$(this).find(".seven_blind_slide img").animate({
					"opacity":0,
				},
				{
					duration:600,
					easing:"easeOutSine",
					complete: function()
					{	
						//animation is done
						seven_end_animate();
					}												 
				});
				
				
			});
		}
		else
		{
			//anim func
			handle.find(".seven_blind_subcontainer").each(function(i)
			{
				$(this).find(".seven_blind_slide img").animate({
					"width":t_rate*option.width+"px",
					"height":t_rate*option.height+"px",
					"left":"-"+Math.ceil((t_rate-1)*option.width/2)+"px",
					"top":"-"+Math.ceil((t_rate-1)*option.height/2)+"px",
					"opacity":0,
					"rotate":"180deg",

				},
				{
					duration:600,
					easing:"easeInQuad",
					complete: function()
					{	
						seven_end_animate();
					}												 
				});
					
				
				
			});
		}
  }
  function seven_mirrow()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";

		for(var i=0;i<6;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;width:"+t_width+"px;height:100%;overflow:hidden;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:-"+(t_width)+"px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:-"+t_width*i+"px'/></div><div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+(t_width)+"px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:-"+t_width*i+"px'/></div></div>";		
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			if(i%2==0)
				temp=".seven_blind_slide";
			else
				temp=".seven_blind_slide_temp";
			$(this).find(temp).animate({
				"left":"0px",
			},
			{
				duration:1000,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
					if(i==5) seven_end_animate(); 
				}												 
			});
		});
		
  }
  function seven_mirrow_drag()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";

		for(var i=0;i<6;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/6);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;width:"+t_width+"px;height:100%;overflow:hidden;left:"+t_width*i+"px;top:0px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:-"+(t_width*2)+"px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:-"+t_width*i+"px'/></div><div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:"+t_width*2+"px;height:100%;left:"+(t_width)+"px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:-"+(t_width*i-t_width)+"px'/></div></div>";		
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			var temp_pos;
			if(i%2==0){
				temp=".seven_blind_slide";
				temp_pos="0px";
			}
			else{
				temp=".seven_blind_slide_temp";
				temp_pos="-"+Math.ceil(option.width/6)+"px";
			}
			$(this).find(temp).animate({
				"left":temp_pos,
			},
			{
				duration:1000,
				easing:"easeOutSine",
				complete: function()
				{	
					  // All animation is done
					if(i==5) seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_vmirrow()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";

		for(var i=0;i<6;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/6);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;width:100%;height:"+t_height+"px;overflow:hidden;left:0px;top:"+t_height*i+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:-"+t_height+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;top:-"+t_height*i+"px'/></div><div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;top:-"+t_height*i+"px'/></div></div>";		
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			if(i%2==0)
				temp=".seven_blind_slide";
			else
				temp=".seven_blind_slide_temp";
			$(this).find(temp).animate({
				"top":"0px",
			},
			{
				duration:700,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
					if(i==5) seven_end_animate(); 
				}												 
			});
		});
	
  }
  function seven_vmirrow_drag()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";

		for(var i=0;i<6;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/6);
			
			blind+="<div class='seven_blind_subcontainer' style='position:absolute;width:100%;height:"+t_height+"px;overflow:hidden;left:0px;top:"+t_height*i+"px;'><div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height*2+"px;left:0px;top:-"+t_height*2+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;top:-"+t_height*i+"px'/></div><div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:"+t_height*2+"px;left:0px;top:"+t_height+"px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;top:-"+(t_height*i-t_height)+"px'/></div></div>";		
		}
		blind+="</div>";
		
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_subcontainer").each(function(i)
		{
			var temp;
			var temp_pos;
			if(i%2==0){
				temp=".seven_blind_slide";
				temp_pos="0px";
			}
			else{
				temp=".seven_blind_slide_temp";
				temp_pos="-"+Math.ceil(option.height/6)+"px";
			}
			
			$(this).find(temp).animate({
				"top":temp_pos,
			},
			{
				duration:700,
				easing:"easeOutSine",
				complete: function()
				{	
					  // All animation is done
					if(i==5) seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_flipx(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		if((seven_isIE()!=false&&seven_isIE()<9)||(window.opera))
		{
			var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+option.width+"px;height:"+option.height+"px;left:0px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
			blind+="</div>";
			$(blind).insertBefore(handle.find("#seven_next_slide"));
			
			//anim func
			handle.find(".seven_blind_slide img").each(function(i)
			{
				$(this).delay(i*50).animate({
					"width":"0",
					"opacity":"0.3",
					"left":Math.ceil(option.width/2)+"px",
				},
				{
					duration:300,
					easing:"easeInSine",
					complete: function()
					{	
						//change the new Img 
						$(this).attr("src",nsrc);
						$(this).animate({
							"width":nimagewidth+"px",
							"left":"0px",
							"opacity":1,
						},
						{
							duration:300,
							easing:"easeOutSine",
							complete: function()
							{	
								 // All animation is done
								 if(i==0)
									seven_end_animate();
							}												 
						});						
					}												 
				});
			});
		}
		else
		{
			var temp=(code==0)?"left":"right";
			//prepare temp divs for transition
			blind='<div id="seven_blind_container" class="flipbox-container box100" style=position:absolute;width:100%;height:100%;z-index:30;perspective:1000;"><div  class="seven_flipbox" style="position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;">'+"<div class='seven_blind_slide' style='position:absolute;width:100%;height:100%;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>"+'</div></div>';
			$(blind).insertBefore(handle.find("#seven_next_slide"));
			//anim func
			$(".seven_flipbox").flippy({
				direction: temp,
				duration: "750",
				verso: "<div class='seven_blind_slide' style='position:absolute;width:100%;height:100%;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>",
				onFinish:function()
				{
					//All animation is done
					seven_end_animate();
				}
			 });
		}
		
  }
  function seven_flipy(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
        var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		if((seven_isIE()!=false&&seven_isIE()<9)||(window.opera))
		{
			var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+option.width+"px;height:"+option.height+"px;left:0px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
			blind+="</div>";
			$(blind).insertBefore(handle.find("#seven_next_slide"));
			
			//anim func
			handle.find(".seven_blind_slide img").each(function(i)
			{
				$(this).delay(i*50).animate({
					"height":"0",
					"opacity":"0.3",
					"top":Math.ceil(option.height/2)+"px",
				},
				{
					duration:300,
					easing:"easeInSine",
					complete: function()
					{	
						//change the new Img 
						$(this).attr("src",nsrc);
						$(this).animate({
							"height":nimageheight+"px",
							"top":"0px",
							"opacity":1,
						},
						{
							duration:300,
							easing:"easeOutSine",
							complete: function()
							{	
								 // All animation is done
								 if(i==0)
									seven_end_animate();
							}												 
						});						
					}												 
				});
			});
		}
		else
		{
			var temp=(code==0)?"bottom":"top";
			//prepare temp divs for transition
			blind='<div id="seven_blind_container" class="flipbox-container box100" style=position:absolute;width:100%;height:100%;z-index:30;perspective:1000;"><div  class="seven_flipbox" style="position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;">'+"<div class='seven_blind_slide' style='position:absolute;width:100%;height:100%;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>"+'</div></div>';
			$(blind).insertBefore(handle.find("#seven_next_slide"));
			//anim func
			$(".seven_flipbox").flippy({
				direction: temp,
				duration: "750",
				verso: "<div class='seven_blind_slide' style='position:absolute;width:100%;height:100%;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>",
				onFinish:function()
				{
					//All animation is done
					seven_end_animate();
				}
			 });
		}
		
  }
  function seven_cube(code)
  {
		if((seven_isIE()!=false&&seven_isIE()<9)||(window.opera)||(window.chrome))
		{
			var p_arr=[3,2,0,1];
			seven_linear_move(p_arr[code]);
		}
		else
		{
			var temp;
			if(code%4<2)
				temp='v';
			else
				temp='h';
			var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
			var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
			//initialize for anim
			handle.find("#seven_next_slide").css("left","100%").css("top","0%");
			handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	
			//prepare temp divs for transition
			blind='<div id="seven_blind_container" style=position:absolute;width:100%;height:100%;z-index:30;"><ul id="seven_slice_box" class="sb-slider" style="width:'+pimagewidth+'px;height:'+pimageheight+'px;position:absolute;left:0px;top:0px;margin:0;padding:0;"><li><a href="#" target="_blank"><img src="'+psrc+'" style="width:'+pimagewidth+'px;height:'+pimageheight+'px;" /></a></li><li><a href="#" target="_blank"><img src="'+nsrc+'" style="width:'+nimagewidth+'px;height:'+nimageheight+'px;" /></a></li></ul></div>';
			
			$(blind).insertBefore(handle.find("#seven_next_slide"));
			
			//anim func
			handle.find("#seven_slice_box li").css("width","100%").css("height","100%");	
			handle.find("#seven_slice_box li img").css("width","100%").css("height","100%");	
			var slicebox = handle.find( '#seven_slice_box' ).slicebox(
			{
				orientation:temp,
				cuboidsCount : 1+parseInt(code/4)*2,
				disperseFactor:3,
				onAfterChange:function(position)
				{
					//all Animation is done
					seven_end_animate();
				}
			});
			//wait for initialization and run the anim			
			setTimeout(function(){ 
					if(code%2==0)	slicebox.next();
					else	slicebox.previous();
			},100);
		}
  }
  function seven_ropen(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:40'>";
		for(var i=0;i<2;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/2);
			var temp=t_height*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0%;top:"+(temp)+"px;'><img src='"+psrc+"' style='position:absolute;top:-"+temp+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		if(seven_isIE()!=false&&seven_isIE()<9)
		{		
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				var arr=[-option.height/2,option.height];
				$(this).animate({
					"top":arr[i],
				},
				{
					duration:400,
					easing:"easeInSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)						 
							seven_end_animate(); 
					}												 
				});
			});
		}
		else
		{
			if(code==0)
			{
				handle.find(".seven_blind_slide:nth-child(1)").css("transform-origin","0% 100%");
				handle.find(".seven_blind_slide:nth-child(2)").css("transform-origin","0% 0");
			}
			else
			{
				handle.find(".seven_blind_slide:nth-child(1)").css("transform-origin","100% 100%");
				handle.find(".seven_blind_slide:nth-child(2)").css("transform-origin","100% 0");
			}
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				var arr;
				if(code==0)
					arr=["-90deg","90deg"];
				else
					arr=["90deg","-90deg"];
				$(this).animate({
					"rotate":arr[i],
					"opacity":0,
				},
				{
					duration:800,
					easing:"easeInSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)						 
							seven_end_animate(); 
					}												 
				});
			});
		}
		
  }
  function seven_rvopen(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:40'>";
		for(var i=0;i<2;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/2);
			var temp=t_width*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+temp+"px;top:0px;'><img src='"+psrc+"' style='position:absolute;left:-"+temp+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
	
		if(seven_isIE()!=false&&seven_isIE()<9)
		{		
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				var arr=[-option.width/2,option.width];
				$(this).animate({
					"left":arr[i],
				},
				{
					duration:400,
					easing:"easeInSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)						 
							seven_end_animate(); 
					}												 
				});
			});
		}
		else
		{
			if(code==0)
			{
				handle.find(".seven_blind_slide:nth-child(1)").css("transform-origin","0% 0%");
				handle.find(".seven_blind_slide:nth-child(2)").css("transform-origin","100% 100%");
			}
			else
			{
				handle.find(".seven_blind_slide:nth-child(1)").css("transform-origin","0% 100%");
				handle.find(".seven_blind_slide:nth-child(2)").css("transform-origin","100% 0%");
			}
			//anim func
			handle.find(".seven_blind_slide").each(function(i)
			{
				var arr;
				if(code==0)
					arr=["90deg","90deg"];
				else
					arr=["-90deg","-90deg"];
				$(this).animate({
					"rotate":arr[i],
					"opacity":0,
				},
				{
					duration:800,
					easing:"easeInSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)						 
							seven_end_animate(); 
					}												 
				});
			});
		}
  }
  function seven_vcenter_stretch()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var sequence=[4,3,2,1,0,0,1,2,3,4];
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_width=Math.ceil(option.width/10);
			var temp=t_width*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+temp+"px;top:0px;'><img src='"+nsrc+"' style='position:absolute;left:-"+temp+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;top:"+Math.ceil(option.height/2)+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		handle.find(".seven_blind_slide img").css("opacity",1);
		handle.find(".seven_blind_slide img").css("height",0);
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{					
			$(this).find("img").delay(sequence[i]*60).animate({
				"top":"0px",
				"height":nimageheight+"px",
				"opacity":1,
			},
			{
				duration:800,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
					 if(i==9)						 
						seven_end_animate(); 
				}												 
			});
		});	
  }
  function seven_hcenter_stretch()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var sequence=[4,3,2,1,0,0,1,2,3,4];
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:20'>";
		for(var i=0;i<10;i++)
		{
			//calculate div pos
			var t_height=Math.ceil(option.height/10);
			var temp=t_height*i;
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+temp+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+temp+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;left:"+Math.ceil(option.width/2)+"px'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		handle.find(".seven_blind_slide img").css("opacity",0);
		handle.find(".seven_blind_slide img").css("width",0);
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).find("img").delay(sequence[i]*80).animate({
				"left":"0px",
				"width":nimagewidth,
				"opacity":1,
			},
			{
				duration:800,
				easing:"easeOutSine",




				complete: function()
				{	
					 // All animation is done
					 if(i==9)						 
						seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_blind_spread(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var sequence=[7,6,5,4,3,2,1,0,0,1,2,3,4,5,6,7];
		var t_width=Math.ceil(option.width/16);			
		var t_height=Math.ceil(option.height/16);
		for(var i=0;i<16;i++)
		{
			//calculate div pos
			var temp;
			var temp_width,temp_height;
			if(i<8){
				temp="#seven_prev_slide img";
				temp_width=t_width;
				temp_height=t_height;
			}
			else{
				temp="#seven_next_slide img";
				temp_width=0;
				temp_height=0;
			}
			
			if(code==0)
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+temp_width+"px;height:100%;left:"+t_width*i+"px;top:0px;z-index:2;'><img src='"+$(temp).attr("src")+"' style='position:absolute;left:-"+t_width*i+"px;width:"+$(temp).width()+"px;height:"+$(temp).height()+"px;'/></div>";
			else
				blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+temp_height+"px;left:0px;top:"+t_height*i+"px;z-index:2;'><img src='"+$(temp).attr("src")+"' style='position:absolute;top:-"+t_height*i+"px;width:"+$(temp).width()+"px;height:"+$(temp).height()+"px;'/></div>";
			//for tentative bg
			if(i>7){
				if(code==0)
					blind+="<div class='temp_seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:100%;left:"+t_width*i+"px;top:0px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";
				else
					blind+="<div class='temp_seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height*i+"px;'><img src='"+psrc+"' style='position:absolute;top:-"+t_height*i+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";
			}
			
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			var temp=0;
			var abt="";
			if(i>7)	{
				if(code==0)
					temp=t_width;
				else
					temp=t_height;
			}

			if(code==0)
			{
				$(this).delay(sequence[i]*100).animate({
					"width":temp+"px",
				},
				{
					duration:400,
					easing:"easeOutQuad",
					complete: function()
					{	
						 // All animation is done
						 if(i==15)
							seven_end_animate(); 
					}												 
				});
			}
			else
			{
				$(this).delay(sequence[i]*100).animate({
					"height":temp+"px",
				},
				{
					duration:400,
					easing:"easeOutQuad",
					complete: function()
					{	
						 // All animation is done
						 if(i==15)
							seven_end_animate(); 
					}												 
				});
			}
		});
  }
  function seven_cut()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");		

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var pos_arr=[-option.width,0];
		var src_arr=[["#seven_next_slide img","#seven_prev_slide img"],["#seven_prev_slide img","#seven_next_slide img"]];
		for(var i=0;i<2;i++)
		{
			//add sub divs for transition
			var t_height=Math.ceil(option.height/2);
			
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:200%;height:"+t_height+"px;left:"+pos_arr[i]+"px;top:"+t_height*i+"px;'><div class='seven_sub_blind_slide' style='float:left;width:50%;height:100%;'><img src='"+$(src_arr[i][0]).attr("src")+"' style='position:absolute;top:-"+t_height*i+"px;width:"+$(src_arr[i][0]).width()+"px;height:"+$(src_arr[i][0]).height()+"px;'/></div><div class='seven_sub_blind_slide' style='float:left;width:50%;height:100%;'><img src='"+$(src_arr[i][1]).attr("src")+"' style='position:absolute;top:-"+t_height*i+"px;width:"+$(src_arr[i][1]).width()+"px;height:"+$(src_arr[i][1]).height()+"px;'/></div></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).animate({
				"left": pos_arr[1-i]+"px",
			},
			{
				duration:500,
				easing:"easeInOutQuart",
				complete: function()
				{	
					 // All animation is done
					if(i==0) seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_4sector(code)
  {
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
	  var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  var tw=Math.ceil(option.width/2),th=Math.ceil(option.height/2);
	  //initialize for anim
	  handle.find("#seven_next_slide").css("left","100%").css("top","0%");
	  handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	
	  //prepare temp divs for transition
	  var offset=[[[-1,0],[0,1],[0,-1],[1,0]],[[0,0],[0,0],[0,0],[0,0]],[[0,0],[0,0],[0,0],[0,0]],[[-1,-1],[-1,1],[1,-1],[1,1]],[[0,0],[0,0],[0,0],[0,0]]];
	  var rotate=[[0,0,0,0],[90,-90,-90,90],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
	  var scale=[1,1,0.5,1,0.1];
	  var t_flag=[0,1,1,0,0];
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	  for(var i=0;i<4;i++)
	  {
		   var col=i%2;
		   var row=parseInt(i/2);
			
		   blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+tw+"px;height:"+th+"px;left:"+row*tw+"px;top:"+col*th+"px;margin-left:"+offset[code][i][0]*tw+"px;margin-top:"+offset[code][i][1]*th+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+tw*row+"px;top:-"+th*col+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";	  
	  }
	  blind+="</div>";
	  $(blind).insertBefore(handle.find("#seven_next_slide"));
	  handle.find(".seven_blind_slide").each(function(i)
	  {
 		    var col=(code<4)?i%2:0.5;
		    var row=(code<4)?parseInt(i/2):0.5;
			var cache=(t_flag[code]==1)?$(this).find("img"):$(this);
		    cache.css("opacity",0).rotate(rotate[code][i]+"deg").css("transform-origin",100*col+"% "+100*row+"%").scale(scale[code]);
			cache.delay(300*i).animate({
				"marginLeft":0,
				"marginTop":0,
				"rotate":"0deg",
				"opacity":1,
				"scale":1,
			},
			{
				duration:800,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
					if(i==3) seven_end_animate(); 
				}												 
			});
	  });
  }
  function seven_4sector_fade(code)
  {
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
	  var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  var tw=Math.ceil(option.width/2),th=Math.ceil(option.height/2);
	  //initialize for anim
	  handle.find("#seven_next_slide").css("left","0%").css("top","0%");
	  handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	
	  //prepare temp divs for transition
	  var offset=[[[-1,0],[0,1],[0,-1],[1,0]],[[0,0],[0,0],[0,0],[0,0]],[[0,0],[0,0],[0,0],[0,0]],[[-1,-1],[-1,1],[1,-1],[1,1]],[[0,0],[0,0],[0,0],[0,0]]];
	  var rotate=[[0,0,0,0],[90,-90,-90,90],[0,0,0,0],[0,0,0,0],[0,0,0,0]];
	  var scale=[1,1,0.5,1,0.1];
	  var t_flag=[0,1,1,0,0];
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	  for(var i=0;i<4;i++)
	  {
		   var col=i%2;
		   var row=parseInt(i/2);
			
		   blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+tw+"px;height:"+th+"px;left:"+row*tw+"px;top:"+col*th+"px;'><img src='"+psrc+"' style='position:absolute;left:-"+tw*row+"px;top:-"+th*col+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";	  
	  }
	  blind+="</div>";
	  $(blind).insertBefore(handle.find("#seven_next_slide"));
	  handle.find(".seven_blind_slide").each(function(i)
	  {
 		    var col=(code<4)?i%2:0.5;
		    var row=(code<4)?parseInt(i/2):0.5;
			var cache=(t_flag[code]==1)?$(this).find("img"):$(this);
			var ttime=(code!=3)?200*i:0;
		    cache.css("transform-origin",100*col+"% "+100*row+"%");
			cache.delay(ttime).animate({
				"marginLeft":offset[code][i][0]*tw+"px",
				"marginTop":offset[code][i][1]*th+"px",
				"rotate":rotate[code][i]+"deg",
				"opacity":"0",
				"scale":scale[code],
			},
			{
				duration:800,
				easing:"easeInSine",
				complete: function()
				{	
					 // All animation is done
					if(i==3) seven_end_animate(); 
				}												 
			});
	  });
  }
  function seven_page(code)
  {
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
	  var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  var p_arr=[[0,-1],[0,-1],[0,-1],[0,-1],[0,1],[0,1],[0,1],[0,1]];
	  var rotate=[-90,-90,90,90,90,90,-90,-90];
	  //initialize for anim
	  handle.find("#seven_next_slide").css("left","100%").css("top","0%");
	  handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

	  //prepare temp divs for transition
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	  //calculate div pos			
	  blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
	  blind+="<div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";	
	  blind+="</div>";
	  $(blind).insertBefore(handle.find("#seven_next_slide"));
	  
	  if(seven_isIE()!=false&&seven_isIE()<9)
		{
			handle.find(".seven_blind_slide img").css("opacity",0);
			handle.find(".seven_blind_slide_temp img").css("opacity",0.7);
			
			//anim func
			handle.find(".seven_blind_slide img").animate({
				"opacity":1,

			},
			{
				duration:600,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
				}												 
			});
			//consequent animation
			handle.find(".seven_blind_slide_temp").each(function(i)
			{
				$(this).find("img").delay(200).animate({
				"left":"+="+(-p_arr[code][0]*200)+"px",
				"top":"+="+(-p_arr[code][1]*200)+"px",
				"opacity":0,
				},
				{
					duration:800,
					easing:"easeOutSine",
					complete: function()
					{	
						 // All animation is done
						 if(i==0)
							seven_end_animate();
					}												 
				});
			});
			

		}
		else
		{
		  var col=code%2,row=parseInt(code/2)%2;
		  handle.find(".seven_blind_slide").css("opacity",0).css("transform-origin",row*100+"% "+col*100+"%").rotate(rotate[code]+"deg");
		  handle.find(".seven_blind_slide_temp").css("opacity",0.7);
		
		  //anim func
		  handle.find(".seven_blind_slide").animate({
			"rotate":"0deg",
			"opacity":1,
		  },
		  {
			duration:600,
			easing:"easeOutSine",
			complete: function()
			{	
				 // All animation is done
			}												 
		  });
		  //consequent animation
		  handle.find(".seven_blind_slide_temp").each(function(i)
		  {
			$(this).delay(500).animate({
			"left":"+="+(-p_arr[code][0]*200)+"px",
			"top":"+="+(-p_arr[code][1]*200)+"px",
			"opacity":0,
			},
			{
				duration:800,
				easing:"easeOutSine",
				complete: function()
				{	
					 // All animation is done
					 if(i==0)
						seven_end_animate();
				}												 
			});
		  });
		}
  }
  function seven_carousel(code)
  {
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
	  var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  var trate=option.width/parseInt(handle.css("max-width"));	  
	  var p_offset=[[-500,0],[500,0],[0,-300],[0,300],[-500,0],[-500,0],[500,0],[500,0]];
	  var t_origin=[[50,50],[50,50],[50,50],[50,50],[100,0],[100,100],[0,0],[0,100]];
	  //initialize for anim
	  handle.find("#seven_next_slide").css("left","100%").css("top","0%");
	  handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	
	  //prepare temp divs for transition
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
      var th=Math.ceil(option.height/1);	  
	  for(var i=0;i<1;i++)
	  {		
		   blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+th+"px;left:0px;top:"+th*i+"px;z-index:30;'><img src='"+psrc+"' style='position:absolute;width:"+pimagewidth+"px;height:"+pimageheight+"px;top:-"+th*i+"px;'/></div>";		
	  blind+="<div class='seven_blind_slide_temp' style='position:absolute;overflow:hidden;width:100%;height:"+th+"px;left:0px;top:"+th*i+"px;z-index:10;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;top:-"+th*i+"px;'/></div>";
	  }
	  blind+="</div>";		
	  $(blind).insertBefore(handle.find("#seven_next_slide"));
	  handle.find(".seven_blind_slide_temp img").css("marginLeft",-p_offset[code][0]*trate).css("marginTop",-p_offset[code][1]*trate).scale(0.4).css("opacity",0).css("transform-origin",t_origin[code][0]+"% "+t_origin[code][1]+"%");
	  //consequent animation
	  handle.find(".seven_blind_slide").each(function(i)
	  {
			$(this).find("img").delay(250).animate({
			"marginLeft":p_offset[code][0]*trate+"px",
			"marginTop":p_offset[code][1]*trate+"px",
			"scale":"0.4",
			"opacity":"0",
			},
			{
				duration:800,
				easing:"easeOutSine",
				complete: function()
				{	

				}												 
			});
	  });
	  handle.find(".seven_blind_slide_temp").each(function(i){
			$(this).find("img").delay(200).animate({
			"marginLeft":"0",
			"marginTop":"0px",
			"scale":"1",
			"opacity":"1",
		  },
		  {
			duration:800,
			easing:"easeOutSine",
			complete: function()
			{	
				 // All animation is done
				 if(i==0)
					seven_end_animate();
			}
		  });
	 });
  }
  function seven_emerge(code)
  {
	  var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
	  var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	  var p_offset=[[-1,0],[1,0],[0,-1],[0,1],[-1,0],[1,0],[0,-1],[0,1],[-1,0],[1,0],[0,-1],[0,1]];
	  var rotate=[0,0,0,0,-90,90,-60,60];
	  var easing=(code<8)?"easeOutSine":"easeOutBounce";
	  //initialize for anim
	  handle.find("#seven_next_slide").css("left","100%").css("top","0%");
	  handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	
	  //prepare temp divs for transition
	  var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
	  //calculate div pos			
	  blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:100%;left:0px;top:0px;'><img src='"+nsrc+"' style='position:absolute;width:"+nimagewidth+"px;height:"+nimageheight+"px;margin-left:"+p_offset[code][0]*option.width+"px;margin-top:"+p_offset[code][1]*option.height+"px;'/></div>";			
	  blind+="</div>";
	  $(blind).insertBefore(handle.find("#seven_next_slide"));
	  handle.find(".seven_blind_slide img").css("opacity",0).rotate(rotate[code]+"deg");
	  handle.find(".seven_blind_slide img").delay(250).animate({
		 "marginLeft":"0px",
		 "marginTop":"0px",
		 "rotate":"0deg",
		 "opacity":"1",
	  },
	  {
		 duration:800,
		 easing:easing,
		 complete: function()
		 {	
		  	 seven_end_animate();
		 }												 
	  });
	  
  }
  function seven_fancy_rect(code)
  {
	   var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
       var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	   	var sequence=[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],[0,6,12,18,1,7,13,19,2,8,14,20,3,9,15,21,4,10,16,22,5,11,17,23],[0,11,12,23,1,10,13,22,2,9,14,21,3,8,15,20,4,7,16,19,5,6,17,18],[0,1,2,3,7,6,5,4,8,9,10,11,15,14,13,12,16,17,18,19,23,22,21,20],[0,15,14,13,1,16,23,12,2,17,22,11,3,18,21,10,4,19,20,9,5,6,7,8],[12,8,8,12,8,4,4,8,4,0,0,4,4,0,0,4,8,5,5,8,12,8,8,12],[0,2,5,9,1,4,8,13,3,7,12,17,6,11,16,20,10,15,19,22,14,18,21,23],[0,11,12,23,10,1,22,13,2,9,14,21,8,3,20,15,4,7,16,19,6,5,18,17],[0,9,8,6,10,1,7,5,3,11,2,4,4,2,11,3,5,7,1,10,6,8,9,0],[6,12,18,22,2,7,13,19,0,3,8,14,1,4,9,15,5,10,16,20,11,17,21,23]];
		var endpoint=[23,23,3,20,6,0,23,3,9,23];
	   //initialize for anim
		handle.find("#seven_next_slide").css("left","0%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","100%").css("top","0%");	
		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var t_width=Math.ceil(option.width/6);
		var t_height=Math.ceil(option.height/4);				
		for(var i=0;i<24;i++)
		{
			//calculate div pos
			var row=i%4;
			var col=parseInt(i/4);
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><img src='"+psrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+pimagewidth+"px;height:"+pimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{	
			$(this).delay(80*sequence[code][i]).animate({
				"scale":0.3,
			},
			{
				duration:350,
				easing:"easeInSine",
				complete: function()
				{
					 // All animation is done
					 if(i==endpoint[code]) 
						seven_end_animate(); 
				}
			});
			$(this).find("img").delay(80*sequence[code][i]).animate({
					"opacity":0,										
			},
			{
					duration:350,
					easing:"easeOutSine"
			});
		});	
	  
  }
  function seven_fancy_rect_emerge(code)
  {
		var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var sequence=[[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],[0,6,12,18,1,7,13,19,2,8,14,20,3,9,15,21,4,10,16,22,5,11,17,23],[0,11,12,23,1,10,13,22,2,9,14,21,3,8,15,20,4,7,16,19,5,6,17,18],[0,1,2,3,7,6,5,4,8,9,10,11,15,14,13,12,16,17,18,19,23,22,21,20],[0,15,14,13,1,16,23,12,2,17,22,11,3,18,21,10,4,19,20,9,5,6,7,8],[12,8,8,12,8,4,4,8,4,0,0,4,4,0,0,4,8,5,5,8,12,8,8,12],[0,2,5,9,1,4,8,13,3,7,12,17,6,11,16,20,10,15,19,22,14,18,21,23],[0,11,12,23,10,1,22,13,2,9,14,21,8,3,20,15,4,7,16,19,6,5,18,17],[0,9,8,6,10,1,7,5,3,11,2,4,4,2,11,3,5,7,1,10,6,8,9,0],[6,12,18,22,2,7,13,19,0,3,8,14,1,4,9,15,5,10,16,20,11,17,21,23]];
		var endpoint=[23,23,3,20,6,0,23,3,9,23];
		//initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	
		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:30'>";
		var t_width=Math.ceil(option.width/6);
		var t_height=Math.ceil(option.height/4);				
		for(var i=0;i<24;i++)
		{
			//calculate div pos
			var row=i%4;
			var col=parseInt(i/4);
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+t_width*col+"px;top:"+t_height*row+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*col+"px;top:-"+t_height*row+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_slide").scale(1.5);
		handle.find(".seven_blind_slide").find("img").css("opacity",0);
		handle.find(".seven_blind_slide").each(function(i)
		{	
			$(this).delay(80*sequence[code][i]).animate({
				"scale":1,
			},
			{
				duration:350,
				easing:"easeInSine",
				complete: function()
				{
					 // All animation is done
					 if(i==endpoint[code]) 
						seven_end_animate(); 
				}
			});
			$(this).find("img").delay(80*sequence[code][i]).animate({
					"opacity":1,										
			},
			{
					duration:350,
					easing:"easeOutSine"
			});
		});		  
  }
  function seven_door(code)
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
		var toffset=[[0,1],[0,1],[1,0],[1,0]];
		var angle=[0,-40,0,-40];
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:40'>";
		var t_width=Math.ceil(option.width/2);				
		var t_height=Math.ceil(option.height/2);		
		var tsize=[[option.width,t_height],[option.width,t_height],[t_width,option.height],[t_width,option.height]];		
		for(var i=0;i<2;i++)
		{
			//calculate div pos
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+tsize[code][0]+"px;height:"+tsize[code][1]+"px;left:"+toffset[code][0]*(t_width*i)+"px;top:"+toffset[code][1]*(t_height*i)+"px;margin-left:"+toffset[code][0]*(t_width*(2*i-1))+"px;margin-top:"+toffset[code][1]*(t_height*(2*i-1))+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+toffset[code][0]*t_width*i+"px;top:-"+toffset[code][1]*t_height*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).rotate(angle[code]+"deg").css("transform-origin",100*i+"% "+100*i+"%");
			$(this).animate({
				"marginLeft":0,
				"marginTop":0,				
				"rotate":"0deg",
			},
			{
				duration:800,
				easing:"easeOutBounce",
				complete: function()
				{	
					 // All animation is done
					 if(i==0)						 
						seven_end_animate(); 
				}												 
			});
		});
  }
  function seven_3door()
  {
	    var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
		var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");
	    //initialize for anim
		handle.find("#seven_next_slide").css("left","100%").css("top","0%");
		handle.find("#seven_prev_slide").css("left","0%").css("top","0%");	

		//prepare temp divs for transition
		var blind="<div id='seven_blind_container' style='position:absolute;width:100%;height:100%;z-index:140'>";
		var t_width=Math.ceil(option.width/2);				
		var t_height=Math.ceil(option.height/2);		
		for(var i=0;i<2;i++)
		{
			//calculate div pos
			blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:"+t_width+"px;height:"+t_height+"px;left:"+(t_width*i)+"px;top:0px;margin-left:"+(t_width*(2*i-1))+"px;'><img src='"+nsrc+"' style='position:absolute;left:-"+t_width*i+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		}
		blind+="<div class='seven_blind_slide' style='position:absolute;overflow:hidden;width:100%;height:"+t_height+"px;left:0px;top:"+t_height+"px;margin-top:"+t_height+"px;'><img src='"+nsrc+"' style='position:absolute;top:-"+t_height+"px;width:"+nimagewidth+"px;height:"+nimageheight+"px;'/></div>";		
		blind+="</div>";
		$(blind).insertBefore(handle.find("#seven_next_slide"));
		//anim func
		handle.find(".seven_blind_slide").each(function(i)
		{
			$(this).animate({
				"marginLeft":0,
				"marginTop":0,				
			},
			{
				duration:800,
				easing:"easeOutBounce",
				complete: function()
				{	
					 // All animation is done
					 if(i==0)						 
						seven_end_animate(); 
				}												 
			});
		});
  }
  //main animate core
  function seven_bg_animate(arg,code)
  {
      var pimagewidth=handle.find("#seven_prev_slide img").width(),pimageheight=handle.find("#seven_prev_slide img").height(),psrc=handle.find("#seven_prev_slide img").attr("src");
      var nimagewidth=handle.find("#seven_next_slide img").width(),nimageheight=handle.find("#seven_next_slide img").height(),nsrc=handle.find("#seven_next_slide img").attr("src");

		switch(code)
		{
		  //Basic right to left
		  case 1:
		  		seven_linear_move(0);
		  break;
		  //Basic left to right
		  case 2:
				seven_linear_move(1);
		  break;
		  //Basic bottom to top
		  case 3:
			    seven_linear_move(2);
		  break;
		  //Basic top to bottom
		  case 4:
		  		seven_linear_move(3);
		  break;
		  //bar right to left
		  case 5:
				seven_vbar_move(0);
		  break;
		  //bar left to right
		  case 6:
				seven_vbar_move(1);				
		  break;
		  //bar right to left bounce
		  case 7:
		  		seven_vbar_move(2);
		  break;
		  //bar left to right bounce
		  case 8:
				seven_vbar_move(3);				
		  break;
		  //bar back right to left
		  case 9:
				seven_vbar_move(4);
		  break;
		  //bar back left to right
		  case 10:
				seven_vbar_move(5);
		  break;
		  //horizontal bar top bottom
		  case 11:
				seven_hbar_move(0);
		  break;
		  //horizontal bar bottom top
		  case 12:
				seven_hbar_move(1);
		  break;
		  //horizontal bar top bottom bounce
		  case 13:
				seven_hbar_move(2);
		  break;
		  //horizontal bar bottom top bounce
		  case 14:
				seven_hbar_move(3);
		  break;
		  //horizontal bar back top bottom 
		  case 15:
				seven_hbar_move(4);
		  break;
		  //horizontal bar back bottom top 
		  case 16:
				seven_hbar_move(5);
		  break;
		  //horizontal bar right to left
		  case 17:
		  		seven_hbar_rmove(0);
		  break;
		  //horizontal bar left to right
		  case 18:
		  		seven_hbar_rmove(1);
		  break;
		  //horizontal bar right to left bottom top
		  case 19:
		  		seven_hbar_rmove(2);
		  break;
		  //horizontal bar left to right bottom top
		  case 20:
		  		seven_hbar_rmove(3);
		  break;
		  //horizontal bar right to left bounce
		  case 21:
		  		seven_hbar_rmove(4);
		  break;
		  //horizontal bar left to right bounce
		  case 22:
		  		seven_hbar_rmove(5);
		  break;
		  //horizontal bar right to left bottom top bounce
		  case 23:
		  		seven_hbar_rmove(6);
		  break;
		  //horizontal bar left to right bottom top bounce
		  case 24:
		  		seven_hbar_rmove(7);
		  break;
		  //vertical bar top to bottom
		  case 25:
		  		seven_vbar_rmove(0);	
		  break;
		  //vertical bar bottom top
		  case 26:
		  		seven_vbar_rmove(1);
		  break;
		  //vertical bar top to bottom right left
		  case 27:
		  		seven_vbar_rmove(2);
		  break;
		  //vertical bar bottom top right left
		  case 28:
		  		seven_vbar_rmove(3);	
		  break;
		  //vertical bar top to bottom bounce
		  case 29:
		  		seven_vbar_rmove(4);	
		  break;
		  //vertical bar bottom top bounce
		  case 30:
		  		seven_vbar_rmove(5);
		  break;
		  //vertical bar top to bottom right left bounce
		  case 31:
		  		seven_vbar_rmove(6);	
		  break;
		  //vertical bar bottom top right left bounce
		  case 32:
		  		seven_vbar_rmove(7);
		  break;
		  //horizontal bar left to right random
		  case 33:
		  		seven_hbar_rmove(8);
		  break;
		  //horizontal bar right to left random
		  case 34:
		  		seven_hbar_rmove(9);
		  break;
		  //horizontal bar left right symmetry
		  case 35:
		  		seven_hbar_rmove(10);
		  break;
		  //horizontal bar right to left symmetry
		  case 36:
		  		seven_hbar_rmove(11);
		  break; 
		  //horizontal bar center stretch
		  case 37:
		  		seven_hcenter_stretch();
		  break; 
		  //horizontal bar left to right random bounce
		  case 38:
		  		seven_hbar_rmove(12);
		  break;
		  //horizontal bar right to left random bounce
		  case 39:
		  		seven_hbar_rmove(13);
		  break; 
		  //horizontal bar left right symmetry bounce
		  case 40:
		  		seven_hbar_rmove(14);
		  break;
		  //horizontal bar right left symmetry bounce
		  case 41:
		  		seven_hbar_rmove(15);
		  break;
		  //vertical bar top to bottom random
		  case 42:
		  		seven_vbar_rmove(8);
		  break;
		  //vertical bar bottom to top random
		  case 43:
		  		seven_vbar_rmove(9);
		  break;
		  //vertical bar top to bottom symmetry
		  case 44:
		  		seven_vbar_rmove(10);
		  break;
		  //vertical bar bottom to top symmetry
		  case 45:
		  		seven_vbar_rmove(11);	
		  break;
		  //vertical bar center stretch
		  case 46:
		  		seven_vcenter_stretch();
		  break;
		  //vertical bar top to bottom random bounce
		  case 47:
		  		seven_vbar_rmove(12);	
		  break;
		  //vertical bar bottom to top random bounce
		  case 48:
		  		seven_vbar_rmove(13);	
		  break;
		  //vertical bar top to bottom symmetry bounce
		  case 49:
		  		seven_vbar_rmove(14);
		  break;
		  //vertical bar bottom to top symmetry bounce
		  case 50:
		  		seven_vbar_rmove(15);	
		  break;
		  //horizontal bar intersect
		  case 51:
		  		seven_hbar_intersect(0);
		  break;
		  //horizontal bar intersect bottom top
		  case 52:
		  		seven_hbar_intersect(1);
		  break;
		  //horizontal bar intersect bounce
		  case 53:
		  		seven_hbar_intersect(2);
		  break;
		  //horizontal bar intersect bottom top bounce
		  case 54:
		  		seven_hbar_intersect(3);
		  break;
		  //vertical bar left right intersect
		  case 55:
		  		seven_vbar_intersect(0);	
		  break;
		  //vertical bar right left intersect
		  case 56:
		  		seven_vbar_intersect(1);
		  break;
		  //vertical bar left right intersect bounce
		  case 57:
		  		seven_vbar_intersect(2);
		  break;
		  //vertical bar right left intersect bounce
		  case 58:
		  		seven_vbar_intersect(3);
		  break;
		  //fadeIn
		  case 59:
		  		seven_fade();				
		  break;
		  //overlap fadeIn
		  case 60:
		  		seven_fade_overlap();
		  break;
		  //horizontal bar intersect no sequence
		  case 61:
		  		seven_hbar_intersect(4);
		  break;
		  //horizontal bar intersect no sequence bounce
		  case 62:
		  		seven_hbar_intersect(5);
		  break;
		  //vertical intersect no sequence
		  case 63:
		 		seven_vbar_intersect(4);
		  break;
		  //vertical intersect no sequence bounce
		  case 64:
		 		seven_vbar_intersect(5);
		  break;
		  //blind Effect
		  case 65:
		  		seven_blind(0);
		  break;
		  //blind effect sequence
		  case 66:
		  		seven_blind(1);
		  break;
		  //blind Effect spread
		  case 67:
		  		seven_blind_spread(0);				
		  break;
		  //blind vertical effect
		  case 68:
		  		seven_vblind(0);
		  break;
		  //blind vertical sequence
		  case 69:
		  		seven_vblind(1);
		  break;
		  //blind vertical spread
		  case 70:
		  		seven_blind_spread(1);
		  break;
		  //cut Effect
		  case 71:
		  		seven_cut();				
		  break;
		  //cut vertical left to right top bottom
		  case 72:
		  		seven_vcut(0);
		  break;
		  //cut vertical right to left top bottom
		  case 73:
		  		seven_vcut(1);
		  break;
		  //cut vertical left to right bottom top
		  case 74:
		  		seven_vcut(2);
		  break;
		  //cut vertical right to left bottom top
		  case 75:
		  		seven_vcut(3);
		  break;
		  //cut effect horizontal left right
		  case 76:
		  		seven_hcut(0);
		  break;
		  //cut effect horizontal right to left 
		  case 77:
		  		seven_hcut(1);
		  break;
		  //cut effect horizontal left right bottom top
		  case 78:
		  		seven_hcut(2);
		  break;
		  //cut effect horizontal right to left bottom top
		  case 79:
		  		seven_hcut(3);
		  break;
		  //square bottom left to right
		  case 80:
			  	seven_square(0);
		  break;
		  //square bottom right to left 
		  case 81:
			  	seven_square(1);				
		  break;
		  //square top left to right
		  case 82:
			  	seven_square(2);
		  break;
		  //square top right to left 
		  case 83:
			  	seven_square(3);
		  break;
		  //square bottom left to right bounce
		  case 84:
			  	seven_square(4);				
		  break;
		  //square bottom right to left bounce
		  case 85:
			  	seven_square(5);				
		  break;
		  //square top left to right bounce
		  case 86:
			  	seven_square(6);
		  break;
		  //square top right to left bounce 
		  case 87:
			  	seven_square(7);				
		  break;
		  //square fade left right top bottom
		  case 88:
			  	seven_square_fade(0);				
		  break;
		  //square fade right left top bottom
		  case 89:
			  	seven_square_fade(1);
		  break;
		  //square fade left right bottom top
		  case 90:
			  	seven_square_fade(2);
		  break;
		  //square fade right left bottom top
		  case 91:
			  	seven_square_fade(3);
		  break;
		  //square hide left right
		  case 92:
			  	seven_square_fade(4);
		  break;
		  //square hide right left
		  case 93:
			  	seven_square_fade(5);
		  break;
		  //square horizontal hide top left to right
		  case 94:
			  	seven_hsquare_fade(0);				
		  break;
		  //square horizontal hide bottom right top left
		  case 95:
			  	seven_hsquare_fade(1);
		  break;
		  //square hide bounce right to left top bottom
		  case 96:
			  	seven_hsquare_fade(2);				
		  break;
		  //square hide bounce right left top bottom
		  case 97:
			  	seven_hsquare_fade(3);
		  break;
		  //square hide bounce right to left bottom top
		  case 98:
			  	seven_hsquare_fade(4);				
		  break;
		  //square hide bounce right left bottom top
		  case 99:
			  	seven_hsquare_fade(5);
		  break;
		  //square border hide
		  case 100:
			  	seven_border_hide(0);				
		  break;
		  //square hoziontal sequence hide
		  case 101:
			  	seven_border_hide(1);
		  break;
		  //square hoziontal reverse sequence hide
		  case 102:
			  	seven_border_hide(2);
		  break;
		  //square vertical sequence hide
		  case 103:
			  	seven_border_hide(3);
		  break;
		  //square vertical reverse sequence hide
		  case 104:
			  	seven_border_hide(4);
		  break;
		  //square random hide
		  case 105:
			  	seven_random_hide();				
		  break;
		  //square plazma effect right left
		  case 106:
				seven_square_plazma(0);
		  break;
		  //square plazma effect left right
		  case 107:
			  	seven_square_plazma(1);				
		  break;
		  //square vertical plazma effect
		  case 108:
			  	seven_vsquare_plazma(0);
		  break;
		  //square vertical plazma effect reverse
		  case 109:
			  	seven_vsquare_plazma(1);
		  break;
		  //square vertical plazma effect top bottom
		  case 110:
			  	seven_vsquare_plazma(2);

		  break;
		  //square vertical plazma effect top bottom reverse
		  case 111:
			  	seven_vsquare_plazma(3);
		  break;
		  //vertical bar plazma effect left to right
		  case 112:
			  	seven_vplazma(0);				
		  break;
		  //vertical bar plazma effect right to left
		  case 113:
			  	seven_vplazma(1);
		  break;
		  //horizontal bar plazma effect top bottom
		  case 114:
			  	seven_hplazma(0);
		  break;
		  //horizontal bar plazma effect bottom top
		  case 115:
			  	seven_hplazma(1);
		  break;
		  //water spread effect
		  case 116:
				seven_water();		  	
		  break;
		  //water effect inside
		  case 117:
		  		seven_water_inside();
		  break;
		  //circle rotate cross(not working on ie8)
		  case 118:
		  	    seven_water_rotate_cross();
		  break;
		   //circle rotate right(not working on ie8)
		  case 119:
		  	   seven_circle_rotate(0);
		  break;
		  //circle rotate left(not working on ie8)
		  case 120:
		  	   seven_circle_rotate(1);
		  break;
		  //swap Block effect
		  case 121:
				seven_swap_block();				
		  break;
		  //swap block horizontal effect right to left
		  case 122:
				seven_swap_hblock(0);
		  break;
		  //swap block horizontal effect left to right
		  case 123:
		 		seven_swap_hblock(1);
		  break;
		  //swap block vertical effect top to bottom
		  case 124:
				seven_swap_vblock(0);
		  break;
		  //swap block vertical effect bottom top
		  case 125:
		 		seven_swap_vblock(1);
		  break;
		  //swap block horizontal effect right to left back
		  case 126:
		 		seven_swap_hblock(2);
		  break;
		  //swap block horizontal effect left to right back
		  case 127:
		 		seven_swap_hblock(3);
		  break;
		  //swap block vertical effect top to bottom back
		  case 128:
				seven_swap_vblock(2);
		  break;
		  //swap block vertical effect bottom top back
		  case 129:
		 		seven_swap_vblock(3);
		  break;
		  //tile show sequence left to right
		  case 130:
			  	seven_tile_sequence(0);	
		  break;
		  //tile show sequence right to left
		  case 131:
			  	seven_tile_sequence(1);
		  break;
		  //tile show sequence push left to right
		  case 132:
			  	seven_tile_psequence(0);
		  break;
		  //tile show sequence push right to left
		  case 133:
			  	seven_tile_psequence(1);
		  break;
		  //tile show random push left to right
		  case 134:
			  	seven_tile_psequence(2);
		  break;
		  //tile show random push right to left
		  case 135:
			  	seven_tile_psequence(3);
		  break;
		  //tile show random top left bottom right
		  case 136:
			  	seven_tile_random_direct(0);
		  break;
		  //tile show random top right bottom left
		  case 137:
			  	seven_tile_random_direct(1);
		  break;
		  //tile show random bottom left top right
		  case 138:
			  	seven_tile_random_direct(2);
		  break;
		  //tile show random bottom right top left
		  case 139:
			  	seven_tile_random_direct(3);
		  break;
		  //horizontal Bar twist
		  case 140:
		  		seven_htwist(0);
		  break;
		  //horizontal Bar twist reverse
		  case 141:
		  		seven_htwist(1);
		  break;
		  //vertical bar twist 
		  case 142:
		  		seven_vtwist(0);
		  break;
		  //vertical bar twist reverse
		  case 143:
		  		seven_vtwist(1);
		  break;
		  //chain effect left right
		  case 144:
		  		seven_chain(0);
		  break;
		  //chain effect right to left
		  case 145:
		  		seven_chain(1);
		  break;
		  //chain sequence effect left right
		  case 146:
		  		seven_schain(0);
		  break;
		  //chain effect right to left
		  case 147:
		  		seven_schain(1);
		  break;
		  //tile show random direction
		  case 148:
			  	seven_tile_random();	
		  break;
		  //fadeOut zoomOut
		  case 149:
		  		seven_fadezoomout();		
		  break;
		  //fadeIn zoomIn
		  case 150:
		  		seven_fadezoomin();
		  break;
		  //horizontal tail fade right to left 
		  case 151:
		  		seven_htail(0);
		  break;
		  //horizontal tail fade left right
		  case 152:
		  		seven_htail(1);
		  break;
		  //horizontal tail fade right to left bottom to top
		  case 153:
		  		seven_htail(2);
		  break;
		  //horizontal tail fade left right bottom top
		  case 154:
		  		seven_htail(3);
		  break;
		  //horizontal tail fade random right left
		  case 155:
		  		seven_htail(4);
		  break;
		  //horizontal tail fade random left right
		  case 156:
		  		seven_htail(5);
		  break;
		  //vertical Tail fade top bottom left right
		  case 157:
		  		seven_vtail(0);
		  break;
		  //vertical Tail fade top bottom right left
		  case 158:
		  		seven_vtail(1);
		  break;
		  //vertical Tail fade bottom top left right
		  case 159:
		  		seven_vtail(2);
		  break;
		  //vertical Tail fade bottom top right left
		  case 160:
		  		seven_vtail(3);
		  break;
		   //vertical Tail random fade top bottom
		  case 161:
		  		seven_vtail(4);
		  break;
		  //vertical Tail random fade bottom top
		  case 162:
		  		seven_vtail(5);
		  break;
		  //fly left
		  case 163:
		  		seven_fly(0);
		  break;
		  //fly right
		  case 164:
		  		seven_fly(1);
		  break;
		  //fly bottom
		  case 165:
		  		seven_fly(2);
		  break;
		  //fly top
		  case 166:
		  		seven_fly(3);
		  break;
		  //rotate effect(not supported in ie8)
		  case 167:
		  		seven_rotate();
		  break;
		  //mirrow effect
		  case 168:
			  	seven_mirrow();
		  break;
		  //mirrow effect drag
		  case 169:
			  	seven_mirrow_drag();
		  break;
		  //mirrow effect vertical
		  case 170:
			  	seven_vmirrow();
		  break;
		  //mirrow effect vertical drag
		  case 171:
			  	seven_vmirrow_drag();
		  break;
		  //flip-x effect right to left(supported in firefox,safari,opera12+,chrome)
		  case 172:
		  		seven_flipx(0);				
		  break;
		  //flip-x effect left to right(supported in firefox,safari,opera12+,chrome)
		  case 173:
		  		seven_flipx(1);
		  break;
		  //flip-y effect top bottom(supported in firefox,safari,opera12+,chrome)
		  case 174:
		  		seven_flipy(0);				
		  break;
		  //flip-y effect bottom top(supported in firefox,safari,opera12+,chrome)
		  case 175:
		  		seven_flipy(1);
		  break;
		  //3d Cube effect top down(supported in firefox,safari)
		  case 176:
		  		seven_cube(0);
		  break;
		  //3d Cube down top(supported in firefox,safari)
		  case 177:
		  		seven_cube(1);
		  break;
		  //3d Cube effect left right(supported in firefox,safari)
		  case 178:
		  		seven_cube(2);
		  break;
		  //3d Cube effect right left(supported in firefox,safari)
		  case 179:
		  		seven_cube(3);
		  break;
		  //rotate open left(not supported in ie8)
		  case 180:
		  		seven_ropen(0);
		  break;
		  //rotate open right(not supported in ie8)
		  case 181:
		  		seven_ropen(1);
		  break;
		  //rotate open top(not supported in ie8)
		  case 182:
		  		seven_rvopen(0);				
		  break;
		  //rotate open bottom(not supported in ie8)
		  case 183:
		  		seven_rvopen(1);
		  break;
		  //4 sector linear
		  case 184:
		  		seven_4sector(0);
		  break;
		  //4 sector rotate
		  case 185:
		  		seven_4sector(1);
		  break;
		  //4 sector scale 
		  case 186:
		  		seven_4sector(2);
		  break;
		  //4 sector center point
		  case 187:
		  		seven_4sector(3);
		  break;
		  //4 sector center scale
		  case 188:
		  		seven_4sector(4);
		  break;
		  //4 sector linear fade
		  case 189:
		  		seven_4sector_fade(0);
		  break;
		  //4 sector rotate fade
		  case 190:
		  		seven_4sector_fade(1);
		  break;
		  //4 sector scale fade
		  case 191:
		  		seven_4sector_fade(2);
		  break;
		  //4 sector center point fade
		  case 192:
		  		seven_4sector_fade(3);
		  break;
		  //4 sector center scale fade
		  case 193:
		  		seven_4sector_fade(4);
		  break;
		  //page lt down effect
		  case 194:
		  		seven_page(0);
		  break;
		  //page lb down effect
		  case 195:
		  		seven_page(1);
		  break;
		  //page rt down effect
		  case 196:
		  		seven_page(2);
		  break;
		  //page rb down effect
		  case 197:
		  		seven_page(3);
		  break;
		   //page lt top effect
		  case 198:
		  		seven_page(4);
		  break;
		  //page lb top effect
		  case 199:
		  		seven_page(5);
		  break;
		  //page rt top effect
		  case 200:
		  		seven_page(6);
		  break;
		  //page rb top effect
		  case 201:
		  		seven_page(7);
		  break;
		  //carousel effect right left
		  case 202:
		  		seven_carousel(0);
		  break;
		  //carousel effect left right
		  case 203:
		  		seven_carousel(1);
		  break;
		  //carousel effect down top
		  case 204:
		  		seven_carousel(2);
		  break;
		  //carousel effect top down
		  case 205:
		  		seven_carousel(3);
		  break;
		  //carousel corner t effect right left
		  case 206:
		  		seven_carousel(4);
		  break;
		  //carousel corner b effect right left
		  case 207:
		  		seven_carousel(5);
		  break;
		  //carousel corner t effect left right
		  case 208:
		  		seven_carousel(6);
		  break;
		  //carousel corner b effect left right
		  case 209:
		  		seven_carousel(7);
		  break;
		  // simple emerge effect left right
		  case 210:
		  		seven_emerge(0);
		  break;
		  // simple emerge effect right left
		  case 211:
		  		seven_emerge(1);
		  break;
		  // simple emerge effect top down
		  case 212:
		  		seven_emerge(2);
		  break;
		  // simple emerge effect down top
		  case 213:
		  		seven_emerge(3);
		  break;
		  //emerge rotate left right
		  case 214:
		  		seven_emerge(4);
		  break;
		  //emerge rotate right left
		  case 215:
		  		seven_emerge(5);
		  break;
		  //emerge rotate top down
		  case 216:
		  		seven_emerge(6);
		  break;
		  //emerge rotate right left
		  case 217:
		  		seven_emerge(7);
		  break;
		  //emerge bounce left right
		  case 218:
		  		seven_emerge(8);
		  break;
		  //emerge bounce right left
		  case 219:
		  		seven_emerge(9);
		  break;
		  //emerge bounce top down
		  case 220:
		  		seven_emerge(10);
		  break;
		  //emerge bounce down top
		  case 221:
		  		seven_emerge(11);
		  break;
		  //rect fancy vertical sequence effect
		  case 222:
		  		seven_fancy_rect(0);
		  break;
		  //rect fancy horizontal sequence effect
		  case 223:
		  		seven_fancy_rect(1);
		  break;
		  //rect fancy sepentine effect
		  case 224:
		  		seven_fancy_rect(2);
		  break;
		  //rect fancy vertical sepentine effect
		  case 225:
		  		seven_fancy_rect(3);
		  break;
		  //rect fancy border sequence effect
		  case 226:
		  		seven_fancy_rect(4);
		  break;
		  //rect fancy cross effect
		  case 227:
		  		seven_fancy_rect(5);
		  break;
		  //rect fancy diagonal effect
		  case 228:
		  		seven_fancy_rect(6);
		  break;
		  //rect fancy v effect
		  case 229:
		  		seven_fancy_rect(7);
		  break;
		  //rect fancy 2 sides effect
		  case 230:
		  		seven_fancy_rect(8);
		  break;
		  //rect fancy wave effect
		  case 231:
		  		seven_fancy_rect(9);
		  break;
		  //rect fancy emerge vertical sequence effect
		  case 232:
		  		seven_fancy_rect_emerge(0);
		  break;
		  //rect fancy emerge horizontal sequence effect
		  case 233:
		  		seven_fancy_rect_emerge(1);
		  break;
		  //rect fancy emerge sepentine effect
		  case 234:
		  		seven_fancy_rect_emerge(2);
		  break;
		  //rect fancy emerge vertical sepentine effect
		  case 235:
		  		seven_fancy_rect_emerge(3);
		  break;
		  //rect fancy emerge border sequence effect
		  case 236:
		  		seven_fancy_rect_emerge(4);
		  break;
		  //rect fancy emerge cross effect
		  case 237:
		  		seven_fancy_rect_emerge(5);
		  break;
		  //rect fancy emerge diagonal effect
		  case 238:
		  		seven_fancy_rect_emerge(6);
		  break;
		  //rect fancy emerge v effect
		  case 239:
		  		seven_fancy_rect_emerge(7);
		  break;
		  //rect fancy emerge 2 sides effect
		  case 240:
		  		seven_fancy_rect_emerge(8);
		  break;
		  //rect fancy emerge wave effect
		  case 241:
		  		seven_fancy_rect_emerge(9);
		  break;
		  //3d cube slice top down
		  case 242:
		  		seven_cube(4);
		  break;
		  //3d cube slice down top
		  case 243:
		  		seven_cube(5);
		  break;
		  //3d cube slice right left
		  case 244:
		  		seven_cube(6);
		  break;
		  //3d cube slice left right
		  case 245:
		  		seven_cube(7);
		  break;
		  //door close vertical effect
		  case 246:
		  		seven_door(0);
		  break;
		  //door close vertical rotate effect
		  case 247:
		  		seven_door(1);
		  break;
		  //door close horizontal effect
		  case 248:
		  		seven_door(2);
		  break;
		  //door close horizontal rotate effect
		  case 249:
		  		seven_door(3);
		  break;
		  //door 3 close
		  case 250:
		  		seven_3door();
		  break;
		}
  }
  //play automate
  function seven_play()
  {
	  if(anim_flag==1)
	  		return;
	  cp_temp=0;
	  
	  handle.find(".progressbar").show();	  
	  if(!handle.find(".progressbar").hasClass("on"))
	  {
		  handle.find(".progressbar").addClass("on");
		  switch(option.progresstype)
		  {
			  case 'linear':
					a_timer=setInterval(seven_linear_automate,50);
					handle.find("#lprogress").css("width",0);
			  break;
			  case 'circle':
				a_timer=setInterval(seven_circle_automate,50);		  
				handle.find('.cprogress').val(0).trigger('change'); 
			  break;
		  }
  }
  }
  //pause automate
  function seven_pause()
  {
	  clearInterval(a_timer);
	  handle.find(".progressbar").hide();
	  handle.find(".progressbar").removeClass("on");
	  switch(option.progresstype)
	  {
		  case 'linear':
		  	handle.find("#lprogress").css("width",0);
		  break;
		  case 'circle':
		  	handle.find('.cprogress').val(0).trigger('change'); 
		  break;
	  }
  }
  //end animate 
  function seven_end_animate()
  {
	  var opacity;

	  handle.find("#seven_blind_container").remove();
	  handle.find(".seven_func_slide").stop();
	  handle.find("#seven_next_slide .seven_slide_title").css("bottom","-40px").css("opacity",0);
	  handle.find("#seven_prev_slide").css("left","100%").css("top","0%");
	  handle.find("#seven_next_slide").css("left","0%").css("top","0%");	  
	  
	  if(seven_isIE()<9&&seven_isIE()!=false)
	  		opacity=0.4;
	  else
	  		opacity=1;

	  handle.find(".seven_slide_title").show();
      handle.find("#seven_next_slide .seven_slide_title").animate({
			"bottom":"15px",
			"opacity":opacity,
	  },
	  {
			 duration:300,
			 easing:"easeOutSine",
			 complete: function()
			 {	
				  //changes the screen resolution
				  if(option.responsive==true)
						seven_resize_screen();
				  anim_flag=0;
				  index=newindex;
				  handle.find("#seven_next_slide .seven_slide_title").stop();
				  option.onanimend();
				  //if autoplay is on
				  if(a_flag==1)
						seven_play();
			}
	  }); 
  }
  //lightbox
  function seven_show_lightbox()
  {
	  if(option.lightbox==true&&handle.find("#seven_lightbox").size()<1)
	  {
		  handle.find(".seven_a_play").addClass("seven_a_pause");	
		  a_flag=0;
		  seven_pause();
		  var attr=$(".seven_slide:nth-child("+(index+1)+")").find("img");
		  var size=seven_get_imagesize(attr);
		  var width=($(window).width()>=size.width)?size.width-16:$(window).width()-16;
		  var height;
	  	  var trate=size.height/size.width;
		  height=trate*width;
		
		  if(typeof(attr.attr("data-src"))=='undefined') 
		  	handle.append("<div id='seven_lightbox'><div id='seven_sublightbox'><img src='"+attr.attr("src")+"' width='"+width+"'/></div></div>");
		  else
		  	handle.append("<div id='seven_lightbox'><div id='seven_sublightbox'><iframe src='"+attr.attr("data-src")+"' width="+width+" height="+height+"></iframe></div></div>");
		  handle.find("#seven_sublightbox").css("width",width+16).css("height",height+16).css("margin-top",Math.ceil(($(window).height()-height)/2)).css("opacity",0).scale(0.5);
		  handle.find("#seven_sublightbox").animate({
				"opacity":1,
				"scale":1,
		  },
		  {
			    duration:400,
				easing:"easeOutSine"
		  });
	  }
  }
  // jQuery event handlers
  $(document).ready(function()
  {	
	  	//bind event handler
	    $(document).bind("dragstart", function() { return false; });
		$(document).keyup(function(e) {
				if(option.keyboard==true)
				{
					  if(e.keyCode == 37) { // left
			 				seven_prev_slide();
					  }
					  else if(e.keyCode == 39) { // right
						    seven_next_slide();
					  }	
					  else if(e.keyCode==27)
					  {	
						  handle.find("#seven_sublightbox").animate({
								"opacity":0,
								"scale":0.1,
						  },
						  {
								duration:200,
								easing:"easeOutSine",
								complete:function()
								{
									handle.find("#seven_lightbox").remove();  
								}
						  });
					  }
				}
		});
		handle.on('click','#seven_lightbox',function(e)
		{
			var container = handle.find("#seven_sublightbox");
			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				handle.find("#seven_lightbox").remove(); 
			}
		});
		//navigation button
	  	handle.find("#left_btn").bind("click", seven_prev_slide);
		handle.find("#right_btn").bind("click", function(){
				clearInterval(a_timer);
				seven_next_slide();
		});
		//mousemove
		var atro,ntro;
		handle.find('#seven_viewport').mousedown(function(e){									  
			if(option.swipe==true&&anim_flag==0) 														  
			{
				 var length=handle.find(".seven_slide").length;														  
				 mp_temp=e.pageX;
				 mouseflag=1;
				 handle.find("#seven_viewport").prepend("<div id='seven_tprev_slide' class='seven_func_slide seven_temp' style='left:-100%;z-index:50;'></div>").prepend("<div id='seven_tnext_slide' class='seven_func_slide seven_temp' style='left:100%;z-index:50;'></div>");
				 atro=(index-1<0)?length-1:index-1;
				 ntro=(index+1)%length;
				 handle.find("#seven_tprev_slide").html(handle.find(".seven_slide:nth-child("+(atro+1)+")").html());
				 handle.find("#seven_tnext_slide").html(handle.find(".seven_slide:nth-child("+(ntro+1)+")").html());
				 handle.find(".seven_temp").find(".seven_slide_title").remove();
				 newindex=index;
				 seven_resize_screen();
  				 seven_pause();
			}															  
		}).mousemove(function(e){	
			 if(mouseflag==1&&anim_flag==0)
			 {
				 var temp=parseInt($(this).find("#seven_hviewport").css("left"));
				 var offset=0;
				 offset=e.pageX-mp_temp;
				 handle.find(".seven_func_slide").css("marginLeft",offset);
			 }
		}).mouseup(function(e){									  
			if(anim_flag==1) return;
			if(option.swipe==true&&mouseflag==1)
			{
				mouseflag=0;
				if(e.pageX>mp_temp+10)
				{
					handle.find(".seven_func_slide").animate({
					"marginLeft":"100%",
					},
					{
						duration:200,
						easing:"easeOutSine",
						complete: function()
						{
							index=newindex=atro;
							handle.find(".seven_temp").remove();
							handle.find(".seven_func_slide").css("marginLeft",0);
							handle.find("#seven_next_slide").html(handle.find(".seven_slide:nth-child("+(index+1)+")").html());
							//adjust carousel Pos
							handle.find(".seven_circle").removeClass("active");
							handle.find(".carousel").removeClass("active");
							handle.find(".seven_circle:nth-child("+(atro+1)+")").addClass("active");
							handle.find(".carousel:nth-child("+(atro+1)+")").addClass("active");
							 if(option.carousel==true)
								  seven_adjustcarousel(atro);
							 //initialization for layer
							 seven_resize_screen();
							 handle.find(".seven_slide_title").hide();
							 seven_end_animate();
						}
					});
				}
				else if(e.pageX<mp_temp-10)
				{
					handle.find(".seven_func_slide").animate({
					"marginLeft":"-100%",
					},
					{
						duration:200,
						easing:"easeOutSine",
						complete: function()
						{
							index=newindex=ntro;
							handle.find(".seven_temp").remove();
							handle.find(".seven_func_slide").css("marginLeft",0);
							handle.find("#seven_next_slide").html(handle.find(".seven_slide:nth-child("+(index+1)+")").html());
							//adjust carousel Pos
							handle.find(".seven_circle").removeClass("active");
							handle.find(".carousel").removeClass("active");
							handle.find(".seven_circle:nth-child("+(ntro+1)+")").addClass("active");
							handle.find(".carousel:nth-child("+(ntro+1)+")").addClass("active");
							 if(option.carousel==true)
								  seven_adjustcarousel(ntro);
							 //initialization for layer
							 seven_resize_screen();
							 handle.find(".seven_slide_title").hide();
							 seven_end_animate();
						}
					});
				}
			}
			if(Math.abs(e.pageX-mp_temp)<10)
			{
				handle.find(".seven_temp").remove();
				handle.find(".seven_func_slide").css("marginLeft",0);
				seven_show_lightbox();
			}
		}).mouseleave(function()
		{
			mouseflag=0;
			handle.find(".seven_func_slide").animate({
					"marginLeft":"0px",
					},
					{
						duration:200,
						easing:"easeOutSine",
						complete: function()
						{
							handle.find(".seven_temp").remove();
							handle.find(".seven_func_slide").css("marginLeft",0);
							//initialization for layer
							seven_resize_screen();
						}
					});
		});
		handle.find("#seven_viewport").on('touchstart',function(e)
		{
			if(option.swipe==true&&anim_flag==0) 														  
			{
				 var length=handle.find(".seven_slide").length;														  
				 mp_temp=e.originalEvent.touches[0].pageX;
				 mouseflag=1;
				 handle.find("#seven_viewport").prepend("<div id='seven_tprev_slide' class='seven_func_slide seven_temp' style='left:-100%;z-index:50;'></div>").prepend("<div id='seven_tnext_slide' class='seven_func_slide seven_temp' style='left:100%;z-index:50;'></div>");
				 atro=(index-1<0)?length-1:index-1;
				 ntro=(index+1)%length;
				 handle.find("#seven_tprev_slide").html(handle.find(".seven_slide:nth-child("+(atro+1)+")").html());
				 handle.find("#seven_tnext_slide").html(handle.find(".seven_slide:nth-child("+(ntro+1)+")").html());
				 handle.find(".seven_temp").find(".seven_slide_title").remove();
				 newindex=index;
				 seven_resize_screen();
			}			
		}).on('touchmove',function(e)
		{
			 if(mouseflag==1&&anim_flag==0)
			 {
				 var temp=parseInt($(this).find("#seven_hviewport").css("left"));
				 var offset=0;
				 offset=e.originalEvent.changedTouches[0].pageX-mp_temp;
				 handle.find(".seven_func_slide").css("marginLeft",offset);
			 }
		}).on('touchend',function(e)
		{
			if(anim_flag==1) return;
			if(option.swipe==true&&mouseflag==1)
			{
				mouseflag=0;
				if(e.originalEvent.changedTouches[0].pageX>mp_temp+10)
				{
					handle.find(".seven_func_slide").animate({
					"marginLeft":"100%",
					},
					{
						duration:200,
						easing:"easeOutSine",
						complete: function()
						{
							index=newindex=atro;
							handle.find(".seven_temp").remove();
							handle.find(".seven_func_slide").css("marginLeft",0);
							handle.find("#seven_next_slide").html(handle.find(".seven_slide:nth-child("+(index+1)+")").html());
							//adjust carousel Pos
							handle.find(".seven_circle").removeClass("active");
							handle.find(".carousel").removeClass("active");
							handle.find(".seven_circle:nth-child("+(atro+1)+")").addClass("active");
							handle.find(".carousel:nth-child("+(atro+1)+")").addClass("active");
							 if(option.carousel==true)
								  seven_adjustcarousel(atro);
							 //initialization for layer
							 seven_resize_screen();
							 handle.find("#seven_next_slide").find(".seven_slide_title").hide();
							 seven_end_animate();
						}
					});
				}
				else if(e.originalEvent.changedTouches[0].pageX<mp_temp-10)
				{
					handle.find(".seven_func_slide").animate({
					"marginLeft":"-100%",
					},
					{
						duration:200,
						easing:"easeOutSine",
						complete: function()
						{
							index=newindex=ntro;
							handle.find(".seven_temp").remove();
							handle.find(".seven_func_slide").css("marginLeft",0);
							handle.find("#seven_next_slide").html(handle.find(".seven_slide:nth-child("+(index+1)+")").html());
							//adjust carousel Pos
							handle.find(".seven_circle").removeClass("active");
							handle.find(".carousel").removeClass("active");
							handle.find(".seven_circle:nth-child("+(ntro+1)+")").addClass("active");
							handle.find(".carousel:nth-child("+(ntro+1)+")").addClass("active");
							 if(option.carousel==true)
								  seven_adjustcarousel(ntro);
							 //initialization for layer
							 seven_resize_screen();
							 handle.find("#seven_next_slide").find(".seven_slide_title").hide();
							 seven_end_animate();
						}
					});
				}
				seven_pause();
			}
			if(Math.abs(e.originalEvent.changedTouches[0].pageX-mp_temp)<10)
			{
				handle.find(".seven_temp").remove();
				handle.find(".seven_func_slide").css("marginLeft",0);
				seven_show_lightbox();
			}
		});
		handle.on('click','#seven_lclose',function()
		{
			handle.find("#seven_lightbox").remove();
		});
		//preview Slide event
		handle.find(".seven_circle").mouseover(function(){														
			  seven_thumb_preview($(this).index());

		});

		handle.find(".seven_a_play").mouseover(function()
		{
			seven_thumb_hide();
		});
		handle.find(".seven_a_play").click(function(){
			  if(!$(this).hasClass("seven_a_pause"))
			  {
			  		$(this).addClass("seven_a_pause");													
					a_flag=0;
					seven_pause();
			  }
			  else
			  {
			  		$(this).removeClass("seven_a_pause");													
					if(a_flag==-1)
					{
						 seven_showprogressbar();
						 
					}
					a_flag=1;
					seven_play();
			  }
		});
		//hide Slide-preview
		handle.find("#seven_bullet_inner_viewport").mouseleave(function(){										
			 seven_thumb_hide();
		});
		//bullet click
		handle.find(".seven_circle").click(function(){
			 seven_animation($(this).index(),1);
		});
		//carousel prev
		handle.find(".seven_carousel_nav.cn_left").click(function()
		{
			seven_prev_carousel();
		});
		//carousel mext
		handle.find(".seven_carousel_nav.cn_right").click(function()
		{
			seven_next_carousel();
		});
		handle.find(".seven_hcarousel").mouseover(function()
		{
			handle.find(".seven_carousel_nav").show();
		}).mouseleave(function()
		{
			handle.find(".seven_carousel_nav").hide();
		});
		//carousel click
		handle.find(".carousel").click(function(){
			if(o_flag==1)
				seven_animation($(this).index(),1);
		});
		handle.find(".carousel").on('touchstart',function(e){
			tp_temp=e.originalEvent.touches[0].pageX;
		}).on('touchend',function(e){
			var temp=e.originalEvent.changedTouches[0].pageX;
			if(o_flag==1&&Math.abs(temp-tp_temp)<3)
				seven_animation($(this).index(),1);
		});
		//Carousel swipe
		handle.find('.seven_hcarousel').mousedown(function(e){									  
			 cr_temp=e.pageX;	
			 mouseflag=1;
			 o_flag=0;
		}).mousemove(function(e){	
			 if(mouseflag==1&&Math.abs(e.pageX-cr_temp)>5)
			 {
				 var temp=parseInt($(this).find("#seven_hviewport").css("left"));
				 var offset=0;
				 offset=(e.pageX-cr_temp)/1.5;
				 $(this).find("#seven_hviewport").css("marginLeft",offset);
			 }
		}).mouseup(function(e){	
			mouseflag=0;
			//In case carousel overflows the board
			var length=handle.find(".seven_slide").length;	
			// Carousel is less than window width
			if(160*length<option.width)
			{
				$(this).find("#seven_hviewport").animate({
					"left":0,
					"marginLeft":0,
				},
				{
					duration:200,
					easing:"easeOutSine"
				});
				if(Math.abs(e.pageX-cr_temp)<5) o_flag=1;
				return false;
			}
			if(option.swipe==true)
			{
				var temp=parseInt($(this).find("#seven_hviewport").css("left"))
				var margin=parseInt($(this).find("#seven_hviewport").css("marginLeft"));
				$(this).find("#seven_hviewport").css("left",temp+margin).css("marginLeft",0);
				if(e.pageX>cr_temp+5)
				{
					if(parseInt(temp+margin)>0)
					{
						$(this).find("#seven_hviewport").animate({
							"left":0,
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
					else
					{
						offset=parseInt(Math.abs(parseInt(temp+margin))/160);
						$(this).find("#seven_hviewport").animate({
							"left":-offset*160,
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
				}
				else if(e.pageX<cr_temp-5)
				{
					if(Math.abs(parseInt(temp+margin))>parseInt($(this).find("#seven_hviewport").width())-option.width-160)
					{
						$(this).find("#seven_hviewport").animate({
							"left":-(parseInt($(this).find("#seven_hviewport").width())-option.width),
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
					else
					{
						offset=Math.ceil(Math.abs(parseInt(temp+margin))/160);
						$(this).find("#seven_hviewport").animate({
							"left":-offset*160,
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
				}
				else
					o_flag=1;
			}
		}).mouseleave(function(e){
			mouseflag=0;
			handle.find("#seven_hviewport").animate({
				"marginLeft":0,
			},
			{
				duration:200,
				easing:"easeOutSine"
			});
		}).on('touchstart',function(e)
		{
			 cr_temp=e.originalEvent.touches[0].pageX;	
			 mouseflag=1;
			 o_flag=0;
		}).on('touchmove',function(e)
		{
			if(mouseflag==1&&Math.abs(e.originalEvent.changedTouches[0].pageX-cr_temp)>5)
			 {
				 var temp=parseInt($(this).find("#seven_hviewport").css("left"));
				 var offset=0;

				 offset=(e.originalEvent.changedTouches[0].pageX-cr_temp)/1.5;
				 $(this).find("#seven_hviewport").css("marginLeft",offset);
			 }
			var temp=e.originalEvent.changedTouches[0].pageX;
			if(option.swipe==true)
			{
				if(temp>cr_temp)
					seven_prev_carousel();
				else if(temp<cr_temp)
					seven_next_carousel();
			}
		}).on('touchend',function(e)
		{
			var temp=e.originalEvent.changedTouches[0].pageX;
			mouseflag=0;
			//In case carousel overflows the board
			var length=handle.find(".seven_slide").length;	
			// Carousel is less than window width
			if(160*length<option.width)
			{
				$(this).find("#seven_hviewport").animate({
					"left":0,
					"marginLeft":0,
				},
				{
					duration:200,
					easing:"easeOutSine"
				});
				if(Math.abs(temp-cr_temp)<5) o_flag=1;
				return false;
			}
			if(option.swipe==true)
			{
				var temp=parseInt($(this).find("#seven_hviewport").css("left"))
				var margin=parseInt($(this).find("#seven_hviewport").css("marginLeft"));
				$(this).find("#seven_hviewport").css("left",temp+margin).css("marginLeft",0);
				if(temp>cr_temp+5)
				{
					if(parseInt(temp+margin)>0)
					{
						$(this).find("#seven_hviewport").animate({
							"left":0,
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
					else
					{
						offset=parseInt(Math.abs(parseInt(temp+margin))/160);
						$(this).find("#seven_hviewport").animate({
							"left":-offset*160,
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
				}
				else if(temp<cr_temp-5)
				{
					if(Math.abs(parseInt(temp+margin))>parseInt($(this).find("#seven_hviewport").width())-option.width-160)
					{
						$(this).find("#seven_hviewport").animate({
							"left":-(parseInt($(this).find("#seven_hviewport").width())-option.width),
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
					else
					{
						offset=Math.ceil(Math.abs(parseInt(temp+margin))/160);
						$(this).find("#seven_hviewport").animate({
							"left":-offset*160,
						},
						{
							duration:200,
							easing:"easeOutSine"
						});
					}
				}
				else
					o_flag=1;
			}
		});
		handle.mouseover(function()
		{
			handle.find(".seven_nav").show();
		}).mouseleave(function()
		{
			handle.find(".seven_nav").hide();
		});
		//automate play
		handle.on('click','.seven_play',function()
		{
			if(option.lightbox==false)
			{
				 var temp=$(this).parent().find("img").attr("data-src");
				 $(this).parent().append("<iframe class='seven_video' src='"+temp+"&autoplay=1"+"'></iframe>");
			}
			handle.find(".seven_a_play").addClass("seven_a_pause");	
			a_flag=0;
		    seven_pause();
			
		});
		$(window).resize(function()
		{
			if(anim_flag==1)
				return false;		
			if(option.responsive==true)
				seven_resize_screen();	
		});

     });
  
  //object.prev() function
  sevenslider.prototype.prev=function()
  {
	  seven_prev_slide();
  }
  
  //object.next() function
  sevenslider.prototype.next=function()
  {
	  seven_next_slide();
  }
  //object.setanimation
  sevenslider.prototype.setanimation=function(arg)
  {
	  option.animation=arg;
  }
  
  }
  /*!
	/**
	* Monkey patch jQuery 1.3.1+ to add support for setting or animating CSS
	* scale and rotation independently.
	* https://github.com/zachstronaut/jquery-animate-css-rotate-scale
	* Released under dual MIT/GPL license just like jQuery.
	* 2009-2012 Zachary Johnson www.zachstronaut.com
	*/		
	// Updated 2010.11.06
    // Updated 2012.10.13 - Firefox 16 transform style returns a matrix rather than a string of transform functions. This broke the features of this jQuery patch in Firefox 16. It should be possible to parse the matrix for both scale and rotate (especially when scale is the same for both the X and Y axis), however the matrix does have disadvantages such as using its own units and also 45deg being indistinguishable from 45+360deg. To get around these issues, this patch tracks internally the scale, rotation, and rotation units for any elements that are .scale()'ed, .rotate()'ed, or animated. The major consequences of this are that 1. the scaled/rotated element will blow away any other transform rules applied to the same element (such as skew or translate), and 2. the scaled/rotated element is unaware of any preset scale or rotation initally set by page CSS rules. You will have to explicitly set the starting scale/rotation value.
    
    function initData($el) {
        var _ARS_data = $el.data('_ARS_data');
        if (!_ARS_data) {
            _ARS_data = {
                rotateUnits: 'deg',
                scale: 1,
                rotate: 0
            };
            
            $el.data('_ARS_data', _ARS_data);
        }
        
        return _ARS_data;
    }
    
    function setTransform($el, data) {
        $el.css('transform', 'rotate(' + data.rotate + data.rotateUnits + ') scale(' + data.scale + ',' + data.scale + ')');
    }
    
    $.fn.rotate = function (val) {
        var $self = $(this), m, data = initData($self);
                        
        if (typeof val == 'undefined') {
            return data.rotate + data.rotateUnits;
        }
        
        m = val.toString().match(/^(-?\d+(\.\d+)?)(.+)?$/);
        if (m) {
            if (m[3]) {
                data.rotateUnits = m[3];
            }
            
            data.rotate = m[1];
            
            setTransform($self, data);
        }
        
        return this;
    };
    
    // Note that scale is unitless.
    $.fn.scale = function (val) {
        var $self = $(this), data = initData($self);
        
        if (typeof val == 'undefined') {
            return data.scale;
        }
        
        data.scale = val;
        
        setTransform($self, data);
        
        return this;
    };

    // fx.cur() must be monkey patched because otherwise it would always
    // return 0 for current rotate and scale values
    var curProxied = $.fx.prototype.cur;
    $.fx.prototype.cur = function () {
        if (this.prop == 'rotate') {
            return parseFloat($(this.elem).rotate());
            
        } else if (this.prop == 'scale') {
            return parseFloat($(this.elem).scale());
        }
        
        return curProxied.apply(this, arguments);
    };
    
    $.fx.step.rotate = function (fx) {
        var data = initData($(fx.elem));
        $(fx.elem).rotate(fx.now + data.rotateUnits);
    };
    
    $.fx.step.scale = function (fx) {
        $(fx.elem).scale(fx.now);
    };
    
    /*
	Starting on line 3905 of jquery-1.3.2.js we have this code:
	// We need to compute starting value
	if ( unit != "px" ) {
	self.style[ name ] = (end || 1) + unit;
	start = ((end || 1) / e.cur(true)) * start;
	self.style[ name ] = start + unit;
	}
	This creates a problem where we cannot give units to our custom animation
	because if we do then this code will execute and because self.style[name]
	does not exist where name is our custom animation's name then e.cur(true)
	will likely return zero and create a divide by zero bug which will set
	start to NaN.
	The following monkey patch for animate() gets around this by storing the
	units used in the rotation definition and then stripping the units off.
	*/
    
    var animateProxied = $.fn.animate;
    $.fn.animate = function (prop) {
        if (typeof prop['rotate'] != 'undefined') {
            var $self, data, m = prop['rotate'].toString().match(/^(([+-]=)?(-?\d+(\.\d+)?))(.+)?$/);
            if (m && m[5]) {
                $self = $(this);
                data = initData($self);
                data.rotateUnits = m[5];
            }
            
            prop['rotate'] = m[1];
        }
        
        return animateProxied.apply(this, arguments);
    };
	
	// Monkey patch jQuery 1.3.1+ css() method to support CSS 'transform'
    // property uniformly across Safari/Chrome/Webkit, Firefox 3.5+, IE 9+, and Opera 11+.
    // 2009-2011 Zachary Johnson www.zachstronaut.com
    // Updated 2011.05.04 (May the fourth be with you!)
    function getTransformProperty(element)
    {
        // Try transform first for forward compatibility
        // In some versions of IE9, it is critical for msTransform to be in
        // this list before MozTranform.
        var properties = ['transform', 'WebkitTransform', 'msTransform', 'MozTransform', 'OTransform'];
        var p;
        while (p = properties.shift())
        {
            if (typeof element.style[p] != 'undefined')
            {
                return p;
            }
        }
        
        // Default to transform also
        return 'transform';
    }
    
    var _propsObj = null;
    
    var proxied = $.fn.css;
    $.fn.css = function (arg, val)
    {
        // Temporary solution for current 1.6.x incompatibility, while
        // preserving 1.3.x compatibility, until I can rewrite using CSS Hooks
        if (_propsObj === null)
        {
            if (typeof $.cssProps != 'undefined')
            {
                _propsObj = $.cssProps;
            }
            else if (typeof $.props != 'undefined')
            {
                _propsObj = $.props;
            }
            else
            {
                _propsObj = {}
            }
        }
        
        // Find the correct browser specific property and setup the mapping using
        // $.props which is used internally by jQuery.attr() when setting CSS
        // properties via either the css(name, value) or css(properties) method.
        // The problem with doing this once outside of css() method is that you
        // need a DOM node to find the right CSS property, and there is some risk
        // that somebody would call the css() method before body has loaded or any
        // DOM-is-ready events have fired.
        if
        (
            typeof _propsObj['transform'] == 'undefined'
            &&
            (
                arg == 'transform'
                ||
                (
                    typeof arg == 'object'
                    && typeof arg['transform'] != 'undefined'
                )
            )
        )
        {
            _propsObj['transform'] = getTransformProperty(this.get(0));
        }
        
        // We force the property mapping here because jQuery.attr() does
        // property mapping with jQuery.props when setting a CSS property,
        // but curCSS() does *not* do property mapping when *getting* a
        // CSS property. (It probably should since it manually does it
        // for 'float' now anyway... but that'd require more testing.)
        //
        // But, only do the forced mapping if the correct CSS property
        // is not 'transform' and is something else.
        if (_propsObj['transform'] != 'transform')
        {
            // Call in form of css('transform' ...)
            if (arg == 'transform')
            {
                arg = _propsObj['transform'];
                
                // User wants to GET the transform CSS, and in jQuery 1.4.3
                // calls to css() for transforms return a matrix rather than
                // the actual string specified by the user... avoid that
                // behavior and return the string by calling jQuery.style()
                // directly
                if (typeof val == 'undefined' && jQuery.style)
                {
                    return jQuery.style(this.get(0), arg);
                }
            }

            // Call in form of css({'transform': ...})
            else if
            (
                typeof arg == 'object'
                && typeof arg['transform'] != 'undefined'
            )
            {
                arg[_propsObj['transform']] = arg['transform'];
                delete arg['transform'];
            }
        }
        
        return proxied.apply(this, arguments);
    };
})(jQuery);