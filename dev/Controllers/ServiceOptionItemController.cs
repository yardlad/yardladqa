﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize(Roles = "admin")]
    public class ServiceOptionItemController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /ServiceOptionItem/

        public ViewResult Index(int id)
        {
            var serviceOptionItems = db.ServiceOptionItems.Where(soi => soi.ServiceOptionId == id).OrderBy(soi => soi.Price);
            ViewBag.ServiceOption = db.ServiceOptions.Where(so => so.ServiceOptionId == id).Single();
            return View(serviceOptionItems.ToList());
        }

        //
        // GET: /ServiceOptionItem/Details/5

        public ViewResult Details(int id)
        {
            ServiceOptionItem serviceOptionItem = db.ServiceOptionItems.Find(id);
            return View(serviceOptionItem);
        }

        //
        // GET: /ServiceOptionItem/Create

        public ActionResult Create(int id)
        {
            ServiceOptionItemCreateViewModel model = new ServiceOptionItemCreateViewModel()
            {
                ServiceOptionId = id,
                ServiceOption = db.ServiceOptions.Where(so => so.ServiceOptionId == id).Single(),
                ServiceOptionItems = new List<ServiceOptionItem>()
            };

            return View(model);
        } 

        //
        // POST: /ServiceOptionItem/Create

        [HttpPost]
        public ActionResult Create(ServiceOptionItemCreateViewModel model)
        {
            foreach (var serviceOptionItem in model.ServiceOptionItems)
            {
                if (serviceOptionItem.Name == null)
                {
                    ModelState.AddModelError("", "each service option must have a name");
                    break;
                }
            }

            if (ModelState.IsValid)
            {
                var currentOptionItems = db.ServiceOptionItems.Where(so => so.ServiceOptionId == model.ServiceOptionId);

                // the user has already inserted service option items
                if (currentOptionItems.Count() >= 1)
                {
                    var currentCount = currentOptionItems.Count();
                    for (int i = 1; i <= model.ServiceOptionItems.Count; i++)
                    {
                        var serviceOptionItem = new ServiceOptionItem()
                        {
                            IsActive = true,
                            Name = model.ServiceOptionItems[i - 1].Name,
                            Order = currentCount + i,
                            ServiceOptionId = model.ServiceOptionId,
                            Price = model.ServiceOptionItems[i - 1].Price,
                            DateCreated = DateTime.Now.AddHours(2)
                        };

                        db.ServiceOptionItems.Add(serviceOptionItem);
                    }
                }

                // the user is inserting service option items for the first time
                if (currentOptionItems.Count() == 0)
                {
                    int count = model.ServiceOptionItems.Count;

                    for (int i = 1; i <= model.ServiceOptionItems.Count; i++)
                    {
                        var serviceOptionItem = new ServiceOptionItem()
                        {
                            IsActive = true,
                            Name = model.ServiceOptionItems[i - 1].Name,
                            Order = i,
                            ServiceOptionId = model.ServiceOptionId,
                            Price = model.ServiceOptionItems[i - 1].Price,
                            DateCreated = DateTime.Now.AddHours(2)
                        };

                        db.ServiceOptionItems.Add(serviceOptionItem);
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index", "ServiceOption", new { id = model.ServiceOption.ServiceId });
            }

            return View(model);
        }
        
        //
        // GET: /ServiceOptionItem/Edit/5
 
        public ActionResult Edit(int id)
        {
            ServiceOptionItem serviceOptionItem = db.ServiceOptionItems.Find(id);
            return View(serviceOptionItem);
        }

        //
        // POST: /ServiceOptionItem/Edit/5

        [HttpPost]
        public ActionResult Edit(ServiceOptionItem model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = model.ServiceOptionId });
            }

            return View(model);
        }

        //
        // GET: /ServiceOptionItem/Delete/5
 
        public ActionResult Delete(int id)
        {
            ServiceOptionItem serviceOptionItem = db.ServiceOptionItems.Find(id);
            return View(serviceOptionItem);
        }

        //
        // POST: /ServiceOptionItem/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceOptionItem serviceOptionItem = db.ServiceOptionItems.Find(id);
            db.ServiceOptionItems.Remove(serviceOptionItem);
            db.SaveChanges();
            return RedirectToAction("Index", new { id = serviceOptionItem.ServiceOptionId });
        }

        public ActionResult Requests()
        {
            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"].ToString();
            }

            List<ServiceOptionItemRequest> requests = db.ServiceOptionItemRequests.ToList();
            return View(requests);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}