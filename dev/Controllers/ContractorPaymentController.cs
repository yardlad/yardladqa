﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class ContractorPaymentController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /ContractorPayment/

        public ViewResult Index(int id = 0 /*contractor id*/)
        {
            ContractorPaymentIndexViewModel model = new ContractorPaymentIndexViewModel();

            var user = System.Web.Security.Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var contractorpayments = db.ContractorPayments.Include(c => c.Contractor).OrderByDescending(cp => cp.DateSubmitted).ToList();

            // if the user isn't a yard lad administrator
            if (!System.Web.Security.Roles.IsUserInRole(user.UserName, "admin"))
            {
                // pull up the contractor using the first user id associated
                var contractor = db.ContractorUsers.Where(cu => cu.UserId == userId).FirstOrDefault().Contractor;

                // if there is no contractor id passed
                if (id == 0)
                {
                    // find the contractor id by default (security measure)
                    contractorpayments = db.ContractorPayments.Where(c => c.ContractorId == contractor.ContractorId).OrderByDescending(cp => cp.DateSubmitted).ToList();
                }

                if (id != 0)
                {
                    // insert security measure here to prevent contractors from manipulating url to see other contractors payments
                    contractorpayments = db.ContractorPayments.Where(c => c.ContractorId == id).OrderByDescending(cp => cp.DateSubmitted).ToList();
                }

                model.CustomerPayments = db.Payments.Where(p => p.RequestedService.ContractorId == contractor.ContractorId).ToList();
                ViewBag.ContractorId = contractor.ContractorId;
            }

            if (id == 0 && User.IsInRole("admin"))
            {
                // user is an admin
            }

            if (contractorpayments.Count > 0 && !User.IsInRole("admin"))
	        {
                var contractor = db.ContractorUsers.Where(cu => cu.UserId == userId).FirstOrDefault().Contractor;
		        var expirationDate = contractor.ExpiresOn;
                var expired = false;

                if (expirationDate <= DateTime.Now.AddHours(2))
                {
                    expired = true;
                }

                ViewBag.ExpirationDate = expirationDate;
                ViewBag.Expired = expired;
	        }

            model.ContractorPayments = contractorpayments;
            

            return View(model);
        }

        //
        // GET: /ContractorPayment/Details/5

        public ViewResult Details(int id)
        {
            ContractorPayment contractorpayment = db.ContractorPayments.Find(id);
            return View(contractorpayment);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}