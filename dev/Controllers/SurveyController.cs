﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{
    public class SurveyController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Survey/

        public ActionResult Index(int id /* requestedServiceId */)
        {
            // look up requested service
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

            // see if there is a survey already associated with the requested service
            if (requestedService.Surveys.Count == 0)
            {
                // create a new survey object
                Survey survey = new Survey()
                {
                    UserId = requestedService.UserId,
                    RequestedServiceId = id,
                    IsCompleted = false,
                    IsActive = true,
                };

                db.Surveys.Add(survey); // add new survey to the db
                db.SaveChanges();

                // populate current survey questions that have been added by the admin
                foreach (var question in db.Questions.Where(q => q.IsActive))
                {

                    SurveyQuestion surveyQuestion = new SurveyQuestion()
                    {
                        SurveyId = survey.SurveyId,
                        QuestionId = question.QuestionId,
                        Answer = null,
                        IsActive = true,
                    };

                    survey.SurveyQuestions.Add(surveyQuestion);
                }

                db.SaveChanges();
                return View(survey);
            }

            // survey already exists, return existing survey for completion/viewing
            var model = requestedService.Surveys.FirstOrDefault();

            if (model.IsCompleted == true)
            {
                return View("SurveyComplete");
            }

            return View(model);
        }

        // POST: /CompleteSurvey/

        [HttpPost]
        public ActionResult CompleteSurvey(int id)
        {
            var survey = db.Surveys.Where(s => s.SurveyId == id).FirstOrDefault();
            survey.IsCompleted = true;
            survey.DateCompleted = DateTime.Now.AddHours(2);

            db.Entry(survey).State = EntityState.Modified;
            db.SaveChanges();

            return View("SurveyComplete");
        }

        [HttpPost]
        public JsonResult AnswerQuestion(int id /* surveyQuestionId */, string answer)
        {
            var surveyQuestion = db.SurveyQuestions.Where(sq => sq.SurveyQuestionId == id).FirstOrDefault();
            if (answer != null)
            {
                surveyQuestion.Answer = answer;
            }

            db.Entry(surveyQuestion).State = EntityState.Modified;
            db.SaveChanges();

            return null;
        }

        // GET: /SurveyComplete
        public ActionResult SurveyComplete()
        {
            return View();
        }

        public ActionResult SurveyData()
        {
            return View();
        }

    }
}
