﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{
    public class ServiceAreaController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: ServiceArea/Index

        [Authorize(Roles = "admin")]
        public ActionResult Index()
        {
            var model = db.ServiceAreas.OrderBy(sa => sa.State.Name).ThenBy(sa => sa.Name).ToList();

            return View(model);
        }

        //
        // GET: ServiceArea/Create

        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            ServiceArea model = new ServiceArea()
            {
                IsActive = true
            };

            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(x => x.Name), "StateId", "Name");

            return View(model);
        }

        //
        // POST: ServiceArea/Create

        [HttpPost]
        public ActionResult Create(ServiceArea model)
        {
            if (ModelState.IsValid)
            {
                db.ServiceAreas.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        //
        // GET: ServiceArea/Edit/5

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            ServiceArea model = db.ServiceAreas.Where(sa => sa.ServiceAreaId == id).Single();
            
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true), "StateId", "Name");
            return View(model);
        }

        //
        // GET: ServiceArea/Edit/5

        [HttpPost]
        public ActionResult Edit(ServiceArea model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);

        }

        //
        // GET: ServiceArea/GetServiceAreasByState/5

        public JsonResult GetServiceAreasByState(int stateId)
        {
            var serviceAreas = db.ServiceAreas.Where(sa => sa.StateId == stateId).OrderBy(sa => sa.Name)
                .Select(x => new {
                    ServiceAreaId = x.ServiceAreaId,
                    Name = x.Name
                }).ToList();

            return this.Json(serviceAreas, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: ServiceArea/GetSecondaryServiceAreasByState/

        public JsonResult GetSecondaryServiceAreasByState(int stateId, int contractorId)
        {
            // the contractor who is going to add a secondary service area
            var contractor = db.Contractors.Where(c => c.ContractorId == contractorId).Single();

            // the primary service area id on the contractor account
            var primaryServiceAreaId = contractor.ServiceAreaId;

            // the list of secondary contractor service areas currently in the database
            var contractorServiceAreas = db.ContractorServiceAreas.Where(csa => csa.ContractorId == contractorId);
            HashSet<int> serviceAreaIds = new HashSet<int>(contractorServiceAreas.Select(x => x.ServiceAreaId));
            
            // data to send back to page (as ddl)
            var serviceAreas = db.ServiceAreas.Where(sa => sa.ServiceAreaId != primaryServiceAreaId)
                .Where(sa => sa.StateId == stateId).OrderBy(sa => sa.Name)
                .Select(x => new
                {
                    ServiceAreaId = x.ServiceAreaId,
                    Name = x.Name
                }).ToList();

            // remove any secondary contractor services that have already been added in the db from the results
            serviceAreas.RemoveAll(x => serviceAreaIds.Contains(x.ServiceAreaId));

            return this.Json(serviceAreas, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
