﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    public class ServiceController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Service/

        public ActionResult Index(string manage = "", string mode = "", string type = "")
        {
            ViewBag.Content = db.Contents.ToList(); // for admin-editable content (used foreach loops in the view)

            // the view is rendered differently (admin control) if the manage and mode request parameters are supplied
            if (manage == "yes")
            {
                // default data source (only show admin service types, contractor types can be managed through each respective contractor)
                var serviceList = db.Services.Include("ServiceCategory").Where(s => s.IsActive == true)
                    .OrderBy(s => s.ServiceCategory.Name.Contains("Test"))
                    .ThenBy(s => s.ServiceCategory.Name.Contains("No Category"))
                    .ThenBy(s => s.ServiceCategory.Name)
                    .ThenBy(s => s.Name).ToList();

                if (mode == "all") 
                {
                    serviceList = db.Services.Include("ServiceCategory")
                    .OrderByDescending(s => s.IsActive)
                    .ThenBy(s => s.ServiceCategory.Name.Contains("Test"))
                    .ThenBy(s => s.ServiceCategory.Name.Contains("No Category"))
                    .ThenBy(s => s.ServiceCategory)
                    .ThenBy(s => s.Name).ToList();
                }

                return View(serviceList);

            }

            return View();
        }

        //
        // GET: /Service/Details

        public ActionResult Details(int id)
        {
            var service = db.Services.Where(s => s.ServiceId == id).Single();

            return View(service);
        }

        //
        // GET: /Service/Create

        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            // create model to send to the view
            ServiceViewModel model = new ServiceViewModel()
            {
                IsActive = false // set default
            };

            ViewBag.ServiceCategory = new SelectList(db.ServiceCategories.ToList(), "ServiceCategoryId", "Name");
            return View(model);
        }

        //
        // POST: /Service/Create

        [HttpPost]
        public ActionResult Create(ServiceViewModel model)
        {
            // create the new service
            Service newService = new Service()
            {
                Name = model.Name,
                Description = model.Description,
                IsActive = false, // make sure it is set to active
                ServiceCategoryId = model.ServiceCategoryId,
                BasePrice = model.BasePrice,
                DateCreated = DateTime.Now.AddHours(2)
            };

            if (ModelState.IsValid)
            {
                db.Services.Add(newService);
                db.SaveChanges();
                return RedirectToAction("Index", new { manage = "yes", mode = "all" });
            }
            else
            {
                ViewBag.ServiceCategory = new SelectList(db.ServiceCategories.ToList().OrderBy(sc => sc.Name), "ServiceCategoryId", "Name", model.ServiceCategoryId);
                return View(model);
            }
        }

        //
        // GET: /Service/Edit

        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            // lookup the service that will be edited
            var serviceToEdit = db.Services.Where(s => s.ServiceId == id).Single();

            // create the view model
            ServiceViewModel model = new ServiceViewModel()
            {
                ServiceId = id,
                Name = serviceToEdit.Name,
                Description = serviceToEdit.Description,
                BasePrice = serviceToEdit.BasePrice,
                ServiceCategoryId = serviceToEdit.ServiceCategoryId,
                IsActive = serviceToEdit.IsActive,
                ServiceOptions = db.ServiceOptions.Where(so => so.ServiceId == serviceToEdit.ServiceId).ToList(),
            };

            ViewBag.ServiceCategory = new SelectList(db.ServiceCategories, "ServiceCategoryId", "Name", model.ServiceCategoryId);
            return View(model);
        }

        //
        // POST: /Service/Edit

        [HttpPost]
        public ActionResult Edit(ServiceViewModel model)
        {
            // lookup the service that will be edited
            var serviceToEdit = db.Services.Where(s => s.ServiceId == model.ServiceId).Single();

            if (ModelState.IsValid)
            {
                // update that service
                serviceToEdit.Name = model.Name;
                serviceToEdit.Description = model.Description;
                serviceToEdit.ServiceCategoryId = model.ServiceCategoryId;
                serviceToEdit.IsActive = model.IsActive;
                serviceToEdit.BasePrice = model.BasePrice;

                // save changes
                db.Entry(serviceToEdit).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { manage = "yes", mode = "all" });
            }

            ViewBag.ServiceCategory = new SelectList(db.ServiceCategories, "ServiceCategoryId", "Name", model.ServiceCategoryId);
            return View(model);
        }

        //
        // GET: /Service/Activate

        [Authorize(Roles = "admin")]
        public ActionResult Activate(int id)
        {
            Service serviceToActivate = db.Services.Where(s => s.ServiceId == id).Single();
            return View(serviceToActivate);
        }

        //
        // POST: /Service/Activate

        [HttpPost, ActionName("Activate")]
        public ActionResult ActivateConfirm(int id)
        {
            Service serviceToActivate = db.Services.Where(s => s.ServiceId == id).Single();
            serviceToActivate.IsActive = true;

            db.Entry(serviceToActivate).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", new { manage = "yes", mode = "all" });
        }

        //
        // GET: /Service/Deactivate

        [Authorize(Roles = "admin")]
        public ActionResult Deactivate(int id)
        {
            Service serviceToDeactivate = db.Services.Where(s => s.ServiceId == id).Single();
            return View(serviceToDeactivate);
        }

        //
        // POST: /Service/Deactivate

        [HttpPost, ActionName("Deactivate")]
        public ActionResult DeactivateConfirm(int id)
        {
            Service serviceToDeactivate = db.Services.Where(s => s.ServiceId == id).Single();
            serviceToDeactivate.IsActive = false;

            db.Entry(serviceToDeactivate).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", new { manage = "yes", mode = "all" });
        }

        //
        // GET: /Service/Delete

        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            Service serviceToDelete = db.Services.Where(s => s.ServiceId == id).Single();
            return View(serviceToDelete);
        }

        //
        // POST: /Service/Delete

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirm(int id)
        {
            var serviceToDelete = db.Services.Where(s => s.ServiceId == id).Single();

            // remove the service option items
            var serviceOptionItems = db.ServiceOptionItems.Where(soi => soi.ServiceOption.ServiceId == serviceToDelete.ServiceId);
            foreach (var serviceOptionItem in serviceOptionItems)
            {
                db.ServiceOptionItems.Remove(serviceOptionItem);
            }

            // remove the service options
            var serviceOptions = db.ServiceOptions.Where(so => so.ServiceId == serviceToDelete.ServiceId);
            foreach (var serviceOption in serviceOptions)
	        {
                db.ServiceOptions.Remove(serviceOption);
	        }

            // remove the service
            db.Services.Remove(serviceToDelete);

            // save all changes
            db.SaveChanges();
            return RedirectToAction("Index", new { manage = "yes", mode = "all" });
        }

        //
        // GET: /Service/RequestService

        public ActionResult RequestService()
        {
            int contractorServices = db.ContractorServices.Where(cs => cs.IsActive == true).Count();
            if (contractorServices == 0)
            {
                TempData["message"] = "We're sorry, there are currently no active contractors in the system to request services from. Please try again later.";
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        //
        // POST: /Service/RequestService

        [HttpPost]
        public ActionResult RequestService(RequestServiceViewModel model)
        {
            TempData["model"] = model;
            Session["RequestServiceViewModel"] = model;
            return RedirectToAction("SelectContractor");
        }

        //
        // GET: Service/SelectContractor/

        public ActionResult SelectContractor()
        {
            RequestServiceViewModel model = null;
            if (TempData["model"] != null)
            {
                model = (RequestServiceViewModel)TempData["model"];
            }
            else if (Session["RequestServiceViewModel"] != null)
            {
                model = (RequestServiceViewModel)Session["RequestServiceViewModel"];
            }
            else if (TempData["model"] == null && Session["RequestServiceViewModel"] == null)
            {
                return RedirectToAction("RequestService");
            }

            model.SelectedState = db.States.Where(s => s.StateId == model.StateId).SingleOrDefault();
            model.SelectedServiceArea = db.ServiceAreas.Where(sa => sa.ServiceAreaId == model.ServiceAreaId).SingleOrDefault();
            model.SelectedService = db.Services.Where(s => s.ServiceId == model.ServiceId).SingleOrDefault();

            model.SelectedServiceOptions = new List<ServiceOption>();
            foreach (var id in model.SelectedServiceOptionItemIds)
            {
                var key = int.Parse(id);
                var option = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == key).SingleOrDefault().ServiceOption;
                model.SelectedServiceOptions.Add(option);
            }

            model.SelectedServiceOptionItems = new List<ServiceOptionItem>();
            foreach (var id in model.SelectedServiceOptionItemIds)
            {
                var key = int.Parse(id);
                var optionItem = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == key).SingleOrDefault();
                model.SelectedServiceOptionItems.Add(optionItem);
            }

            model.AvailableContractors = new List<Contractor>();
            foreach (var id in model.AvailableContractorIds)
            {
                var key = int.Parse(id);
                var contractor = db.Contractors.Where(c => c.ContractorId == key).SingleOrDefault();
                model.AvailableContractors.Add(contractor);
            }

            return View(model);
        }

        //
        // POST: Service/SelectContractor/

        [HttpPost]
        public ActionResult SelectContractor(RequestServiceViewModel model)
        {
            TempData["model"] = model;
            Session["RequestServiceViewModel"] = model;
            return RedirectToAction("SubmitRequest");
        }

        //
        // GET: Service/SubmitRequest/

        public ActionResult SubmitRequest()
        {
            RequestServiceViewModel model = null;
            if (TempData["model"] != null)
            {
                model = (RequestServiceViewModel)TempData["model"];
            }
            else if (Session["RequestServiceViewModel"] != null)
            {
                model = (RequestServiceViewModel)Session["RequestServiceViewModel"];
            }
            else if (TempData["model"] == null && Session["RequestServiceViewModel"] == null)
            {
                return RedirectToAction("RequestService");
            }

            model.SelectedState = db.States.Where(s => s.StateId == model.StateId).SingleOrDefault();
            model.SelectedServiceArea = db.ServiceAreas.Where(sa => sa.ServiceAreaId == model.ServiceAreaId).SingleOrDefault();
            model.SelectedService = db.Services.Where(s => s.ServiceId == model.ServiceId).SingleOrDefault();

            model.SelectedServiceOptions = new List<ServiceOption>();
            foreach (var id in model.SelectedServiceOptionItemIds)
            {
                var key = int.Parse(id);
                var option = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == key).SingleOrDefault().ServiceOption;
                model.SelectedServiceOptions.Add(option);
            }

            model.SelectedServiceOptionItems = new List<ServiceOptionItem>();
            foreach (var id in model.SelectedServiceOptionItemIds)
            {
                var key = int.Parse(id);
                var optionItem = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == key).SingleOrDefault();
                model.SelectedServiceOptionItems.Add(optionItem);
            }

            model.AvailableContractors = new List<Contractor>();
            foreach (var id in model.AvailableContractorIds)
            {
                var key = int.Parse(id);
                var contractor = db.Contractors.Where(c => c.ContractorId == key).SingleOrDefault();
                model.AvailableContractors.Add(contractor);
            }

            model.SelectedContractor = db.Contractors.Where(c => c.ContractorId == model.SelectedContractorId).SingleOrDefault();
            model.SubTotal = model.SelectedContractor.ContractorServices.Where(cs => cs.ServiceId == model.ServiceId).SingleOrDefault().BasePrice;
            
            foreach (var serviceOptionItem in model.SelectedServiceOptionItems)
            {
                foreach (var contractorServiceOptionItem in db.ContractorServiceOptionItems
                    .Where(csoi => csoi.ContractorServiceOption.ContractorService.ContractorId == model.SelectedContractor.ContractorId)
                    .Where(csoi => csoi.ContractorServiceOption.ContractorService.ServiceId == model.ServiceId)
                    .Where(csoi => csoi.ContractorServiceOption.Name == serviceOptionItem.ServiceOption.Name).ToList())
                {
                    if (contractorServiceOptionItem.Name == serviceOptionItem.Name)
                    {
                        model.SubTotal += contractorServiceOptionItem.Price;
                    }
                }
            }

            decimal contractorTaxRate = (decimal)model.SelectedContractor.Taxes.Select(x => x.Rate).SingleOrDefault() * .01M;
            decimal tax = (decimal)(contractorTaxRate * model.SubTotal);

            model.SelectedContractorTaxRate = contractorTaxRate;
            model.Tax = tax;
            model.Total = model.SubTotal + model.Tax;

            if (User.Identity.IsAuthenticated)
            {
                var user = Membership.GetUser();
                var userId = (int)user.ProviderUserKey;
                var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();
                if (userProfile.AddressId != null)
                {
                    model.UserAddressId = (int)userProfile.AddressId;
                    model.UserAddress = userProfile.Address;
                    model.CreateCustomAddress = false;
                }
                else
                {
                    model.CreateCustomAddress = true;
                }

                model.UserIsLoggedIn = true;
            }
            else
            {
                model.UserIsLoggedIn = false;
                model.CreateCustomAddress = true;
            }

            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name).ToList(), "StateId", "Name", model.SelectedState.StateId);

            return View(model);
        }

        //
        // GET: Service/SubmitRequest/

        [HttpPost]
        public ActionResult SubmitRequest(RequestServiceViewModel model)
        {
            if (model.CreateCustomAddress == true)
            {
                if (model.CustomAddress.Line1 == null || model.CustomAddress.City == null || model.CustomAddress.StateId == 0 || model.CustomAddress.PostalCode == null)
                {
                    ModelState.AddModelError("", "please enter the service address");
                    ViewBag.State = new SelectList(db.States.Where(s => s.IsActive == true).OrderBy(s => s.Name).ToList(), "StateId", "Name", model.CustomAddress.StateId);
                    return View(model);
                }
            }

            TempData["model"] = model;
            Session["RequestServiceViewModel"] = model;
            return RedirectToAction("ValidateCommand", "PayPal");
        }

        //
        // GET: /Service/Providers

        public ActionResult Providers(int page = 1)
        {
            // first step
            if (page == 1)
            {
                // set default values
                Contractor newContractor = new Contractor()
                {
                    ApprovedOn = null,
                    IsApproved = false,
                    IsActive = false, // default to inactive
                    DateCreated = DateTime.Now.AddHours(2)
                };

                Address newAddress = new Address()
                {
                    IsActive = true
                };

                ContractorUserViewModel model = new ContractorUserViewModel()
                {
                    Page = 1, // set the page in model to 1
                    DateCreated = DateTime.Now.AddHours(2), // log the date the contractor user was created (will be updated upon creation)
                    Contractor = newContractor,
                    UserInfo = null,
                    TermsAccepted = false // default to false
                };

                Dictionary<int, string> contractorTypes = new Dictionary<int, string>();
                contractorTypes.Add(3, "Professional Business");
                contractorTypes.Add(1, "Independent Contractor");
                contractorTypes.Add(2, "Apprentice");

                ViewBag.ContractorType = new SelectList(contractorTypes, "Key", "Value");
                ViewBag.State = new SelectList(db.States.OrderBy(s => s.Name), "StateId", "Name");
                ViewBag.Content = db.Contents.Where(c => c.SectionName == "Service Providers (Services)").Single();
                return View(model);

            }

            // we are on the second step
            else
            {
                RegisterViewModel newRegisterModel = new RegisterViewModel()
                {
                    Address = (Address)TempData["Address"], // pull the address from the first page (saved in temp data)
                    DateOfBirth = DateTime.Now.AddHours(2).AddYears(-18) // default date of birth to 18 years from todays date
                };

                ContractorUserViewModel model = new ContractorUserViewModel()
                {
                    Page = 2, // set the page to 2
                    DateCreated = DateTime.Now.AddHours(2),
                    Contractor = (Contractor)TempData["Contractor"], // pull the contractor from the first page (saved in temp data)
                    UserInfo = newRegisterModel,
                    TermsAccepted = false // default to false
                };

                // save the contractor and address in session
                Session["Contractor"] = model.Contractor;
                Session["Address"] = model.UserInfo.Address;

                ViewBag.Content = db.Contents.Where(c => c.SectionName == "Service Providers (Services)").Single();
                return View(model);
            }
        }

        //
        // POST: /Service/Providers

        [HttpPost]
        public ActionResult Providers(ContractorUserViewModel model)
        {
            // we are on step 1
            if (model.Page == 1)
            {
                // save the contractor and address in temp data for filling model on page 2
                TempData["Contractor"] = model.Contractor;
                TempData["Address"] = model.UserInfo.Address;

                // begin step 2 render
                return RedirectToAction("Providers", "Service", new { page = 2 });
            }
            else // we are on step 2
            {
                // pull the contractor and address out of session and store it in the model
                model.Contractor = (Contractor)Session["Contractor"];
                model.UserInfo.Address = (Address)Session["Address"];

                // variable to hold user info
                my_aspnet_users user;

                bool hasErrors = false;

                // checks to make sure that the length of the phone number input fields is correct
                if (model.UserInfo.Phone.Length != 14)
                {
                    ModelState.AddModelError("", "please enter a valid phone number");
                    hasErrors = true;
                }

                // checks to make sure that the length of the phone number input fields is correct
                if (model.UserInfo.Mobile != null && model.UserInfo.Mobile.Length != 14)
                {
                    ModelState.AddModelError("", "please enter a valid mobile number");
                    hasErrors = true;
                }

                // checks to make sure the user is 18 years old
                if (model.UserInfo.DateOfBirth.Date > DateTime.Now.AddHours(2).Date.AddYears(-18))
                {
                    ModelState.AddModelError("", "you must be at least 18 years of age");
                    hasErrors = true;
                }

                if (model.UserInfo.AcceptSMS == true && model.UserInfo.Mobile == null)
                {
                    ModelState.AddModelError("", "you must enter your mobile number if you wish to receive SMS texts");
                    hasErrors = true;
                }

                if (model.TermsAccepted == false)
                {
                    ModelState.AddModelError("", "you must accept the terms and conditions before continuing");
                    hasErrors = true;
                }

                // there are model errors, return to display all messages
                if (hasErrors == true)
                {
                    ViewBag.Content = db.Contents.Where(c => c.SectionName == "Service Providers (Services)").Single();
                    return View(model);
                }

                // these should have been taken from the stored session variables...
                if (model.Contractor != null && model.UserInfo.Address != null)
                {
                    // Attempt to register the user
                    MembershipCreateStatus createStatus;
                    Membership.CreateUser(model.UserInfo.Username, model.UserInfo.Password, model.UserInfo.Email, null, null, true, null, out createStatus);

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        Roles.AddUserToRole(model.UserInfo.Username, "contractor"); // assign contractor role

                        // lookup the user
                        user = db.my_aspnet_users.Where(u => u.name == model.UserInfo.Username).Single();

                        // create a new address
                        Address newAddress = new Address()
                        {
                            Line1 = model.UserInfo.Address.Line1,
                            Line2 = model.UserInfo.Address.Line2,
                            City = model.UserInfo.Address.City,
                            StateId = model.UserInfo.Address.StateId,
                            PostalCode = model.UserInfo.Address.PostalCode,
                            IsActive = true // default to active
                        };

                        db.Addresses.Add(newAddress);

                        // create a new profile
                        UserProfile newProfile = new UserProfile()
                        {
                            UserId = user.id,
                            FirstName = model.UserInfo.FirstName,
                            LastName = model.UserInfo.LastName,
                            DateOfBirth = model.UserInfo.DateOfBirth,
                            Gender = model.UserInfo.Gender,
                            Phone = model.UserInfo.Phone,
                            Mobile = model.UserInfo.Mobile,
                            AcceptSMS = model.UserInfo.AcceptSMS,
                            IsActive = true, // default to active
                            Address = newAddress,
                            LastLoginDate = DateTime.Now.AddHours(2)
                        };

                        db.UserProfiles.Add(newProfile);
                        db.SaveChanges();

                        
                    }
                    else
                    {
                        ModelState.AddModelError("", ErrorCodeToString(createStatus));
                        ViewBag.Content = db.Contents.Where(c => c.SectionName == "Service Providers (Services)").Single();

                        return View(model);
                    }

                    // add the contractor
                    Contractor newContractor = model.Contractor;
                    model.Contractor.IsActive = false; // set to inactive
                    model.Contractor.IsApproved = false; // set to false
                    model.DateCreated = DateTime.Now.AddHours(2);
                    db.Contractors.Add(newContractor);

                    // lookup the user (look up again -- check to see if this variable is null at runtime)
                    user = db.my_aspnet_users.Where(u => u.name == model.UserInfo.Username).Single();

                    ContractorUser newContractorUser = new ContractorUser()
                    {
                        Contractor = newContractor,
                        UserId = user.id,
                        DateCreated = DateTime.Now.AddHours(2),
                        IsActive = true // set to active
                    };

                    db.ContractorUsers.Add(newContractorUser);
                    db.SaveChanges();

                    // add a tax record for the contractor, set a default tax value
                    Tax tax = new Tax()
                    {
                        AddressId = (int)db.UserProfiles.Where(up => up.UserId == newContractorUser.UserId).Single().AddressId,
                        ContractorId = newContractorUser.ContractorId,
                        Rate = 8.75M // hard coded default value
                    };

                    db.Taxes.Add(tax);
                    db.SaveChanges();

                    // add contractor availability
                    ContractorAvailability availability = new ContractorAvailability()
                    {
                        ContractorId = newContractor.ContractorId,
                        Monday = true,
                        Tuesday = true,
                        Wednesday = true,
                        Thursday = true,
                        Friday = true,
                        Saturday = true,
                        Sunday = true,
                        HasBeenSet = false,
                        UpdatedOn = DateTime.Now.AddHours(2),
                    };

                    db.ContractorAvailabilities.Add(availability);
                    db.SaveChanges();

                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    var adminEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").SingleOrDefault().Value;

                    // send an email to the new contractor who just signed up
                    string subject = "Thank you for registering!";
                    string body = null;

                    body += "<p>Thank you for signing your business up with Yard Lad!<br />You can now login with your account ";
                    body += "<a href=\"http://www.design-develop-deploy.com/yl/Account/MyAccount\">here</a></p>";

                    body += "<p style=\"margin-bottom: .5em;\">These are the next steps you will need to take before you can get started:</p>";

                    body += "<ul>";
                    body += "<li>Submit your qualifications <a href=\"http://www.design-develop-deploy.com/yl/Account/MyAccount\">here</a>.</li>";
                    body += "<li>Once submitted, an admin will review those qualifications and you will be notified when you are approved.</li>";
                    body += "<li>Once approved, you will need to submit payment to continue with the service.</li>";
                    body += "<li>After payment is completed, update your availability and the number of jobs you can accept per day.</li>";
                    body += "<li>Manage the services you or your company offer along with your pricing model (unless you accept our default pricing).</li>";
                    body += "</ul>";

                    body += "<p>Thank you for becoming a part of our network! If you have any questions or concerns please contact us.<br />-The Yard Lad team</p>";

                    // send email (will need to replace the emails when deploying)
                    try
                    {
                        emailsender.SendEmail(subject, body, adminEmail, model.UserInfo.Email);
                    }
                    catch (Exception ex)
                    {
                        
                    }

                    // send an email to the admin notifying them about a new contractor registration
                    var contractorType = db.ContractorTypes.Where(ct => ct.ContractorTypeId == model.Contractor.ContractorTypeId).Single();

                    subject = "New Contractor Registered";
                    body = string.Format("<p>New contractor registered!</p>" + "<p>Contractor Name: {0}<br />Contractor Type: {1}<br />Username: {2}<br />"
                        + "Email: {3}<br>Phone: {4}<br>Mobile: {5}<br>Date of Birth: {6:d}<br>Gender: {7}</p>",
                        model.Contractor.Name, contractorType.Type, model.UserInfo.Username, model.UserInfo.Email,
                        model.UserInfo.Phone, model.UserInfo.Mobile, model.UserInfo.DateOfBirth, model.UserInfo.Gender);

                    // send email (will need to replace the emails when deploying)
                    try
                    {
                        emailsender.SendEmail(subject, body, adminEmail, adminEmail);
                    }
                    catch (Exception)
                    {
                        
                    }

                    // set authorization cookie
                    // set createPersistentCookie to false, user can overwrite when the login (using remember me checkbox)
                    FormsAuthentication.SetAuthCookie(model.UserInfo.Username, true /* createPersistentCookie */);

                    // send the user to the submitted page
                    return RedirectToAction("ContractorSubmitted");
                }

                ViewBag.Content = db.Contents.Where(c => c.SectionName == "Service Providers (Services)").Single();
                return View(model);
            }
        }

        //
        // GET: Service/GetAvailableStates/

        public JsonResult GetAvailableStates()
        {
            // list of contractors that are listed and have at least one contractor service added
            var contractors = db.Contractors.Where(c => c.IsActive == true).Where(c => c.ContractorServices.Count >= 1).ToList();
            // hash set of contractor ids (used for filtering results)
            HashSet<int> contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));
            // only return the states that the contractors have added services in (via added service areas)

            // primary service area of contractors
            var primary = db.Contractors.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Select(x => x.ServiceArea).Distinct()
                .Select(x => x.State).Distinct().OrderBy(s => s.Name).ToList();
            
            // secondary service area of contractors
            var secondary = db.ContractorServiceAreas.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Select(x => x.ServiceArea).Distinct()
                .Select(x => x.State).Distinct().OrderBy(s => s.Name).ToList();

            // combine secondary with primary if the state doesn't already exist in the collection
            foreach (var state in secondary)
            {
                if (primary.Contains(state) == false)
	            {
                    primary.Add(state);
	            }
            }

            // select necessary fields from the full list
            var states = primary.OrderBy(s => s.Name).Select(x => new
            {
                StateId = x.StateId,
                Name = x.Name
            });

            // return to populate the ddl
            return this.Json(states, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: Service/GetAvailableServiceAreas/5

        public JsonResult GetAvailableServiceAreas(int stateId)
        {
            // list of contractors that are listed and have at least one contractor service added
            var contractors = db.Contractors.Where(c => c.IsActive == true).Where(c => c.ContractorServices.Count >= 1).ToList();
            // hash set of contractor ids (used for filtering results)
            HashSet<int> contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));
            // only return the service areas under the selected state id that active/listed contractors have added services in
            var primary = db.Contractors.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Select(x => x.ServiceArea).Where(sa => sa.StateId == stateId).Distinct().OrderBy(sa => sa.Name).ToList();

            var secondary = db.ContractorServiceAreas.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Select(x => x.ServiceArea).Where(sa => sa.StateId == stateId).Distinct().OrderBy(sa => sa.Name).ToList();

            foreach (var serviceArea in secondary)
            {
                if (primary.Contains(serviceArea) == false)
                {
                    primary.Add(serviceArea);
                }
            }

            var serviceAreas = primary.OrderBy(sa => sa.Name)
                .Select(x => new
                {
                    ServiceAreaId = x.ServiceAreaId,
                    Name = x.Name
                });

            return this.Json(serviceAreas, JsonRequestBehavior.AllowGet);
                
        }

        //
        // GET: Service/GetServices/

        public JsonResult GetServices(int serviceAreaId)
        {
            // selected service area
            var selectedServiceArea = db.ServiceAreas.Where(sa => sa.ServiceAreaId == serviceAreaId).SingleOrDefault();
            var currentDate = DateTime.Now.AddHours(2);

            // list of contractors that are listed (and not expired) and have at least one contractor service added
            var contractors = db.Contractors.Where(c => c.IsActive == true).Where(c => c.ExpiresOn.Value >= currentDate).Where(c => c.ContractorServices.Count >= 1).ToList();
            HashSet<int> contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));

            // filter list of contractors to include only those who offer services in the selected service area
            var primary = db.Contractors.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Where(c => c.ServiceAreaId == serviceAreaId).Distinct().ToList();

            var secondary = db.ContractorServiceAreas.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Where(csa => csa.ServiceAreaId == serviceAreaId).Select(x => x.Contractor).Distinct().ToList();

            foreach (var contractor in secondary)
            {
                if (primary.Contains(contractor) == false)
                {
                    primary.Add(contractor);
                }
            }

            // only return services in the selected service area that contractors have added
            HashSet<int> filteredContractorIds = new HashSet<int>(primary.Select(x => x.ContractorId));

            var services = db.ContractorServices.ToList().FindAll(x => filteredContractorIds.Contains(x.ContractorId))
                .Select(x => x.Service).Distinct().OrderBy(s => s.ServiceCategory.Name).ThenBy(s => s.Name)
                .Select(x => new {
                    ServiceId = x.ServiceId,
                    Name = x.Name
                });
              
            return this.Json(services, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: Service/GetServiceDetails/1

        public JsonResult GetServiceDetails(int serviceId)
        {
            var details = db.Services.Where(s => s.ServiceId == serviceId)
                .Select(x => new
                {
                    Description = x.Description,
                    BasePrice = x.BasePrice
                });

            return this.Json(details, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: Service/GetServiceOptions/1

        public JsonResult GetServiceOptions(int serviceId)
        {
            var service = db.Services.Where(s => s.ServiceId == serviceId).SingleOrDefault();
            var serviceOptions = db.ServiceOptions.Where(s => s.ServiceId == serviceId).ToList()
                .Select(x => new
                {
                    ServiceOptionId = x.ServiceOptionId,
                    Name = x.Name,
                    ServiceOptionItems = x.ServiceOptionItems.OrderBy(soi => soi.Order).Select(soi => new {
                        ServiceOptionItemId = soi.ServiceOptionItemId,
                        Name = soi.Name,
                        Price = soi.Price
                    })
                });

            return this.Json(serviceOptions, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: Service/CheckForAvailableContractors/

        public JsonResult CheckForAvailableContractors(int serviceId, string[] serviceOptionIds, string[] serviceOptionItemIds, int serviceAreaId)
        {
            var contractorsAvailable = false;

            // list of contractors that are listed and have at least one contractor service added
            var contractors = db.Contractors.Where(c => c.IsActive == true).Where(c => c.ContractorServices.Count >= 1).ToList();
            HashSet<int> contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));

            // all the contractor services that match the service id, filtered by active/listed contractors
            var contractorServices = db.ContractorServices.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Where(cs => cs.ServiceId == serviceId).Where(cs => cs.IsActive == true);

            // filter the previous collection of contractors to include only the contractors that have the service selected
            contractors = contractorServices.Select(x => x.Contractor).ToList();
            contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));

            // filter list of contractors to include only those who offer services in the selected service area
            var primary = db.Contractors.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Where(c => c.ServiceAreaId == serviceAreaId).Distinct().ToList();

            var secondary = db.ContractorServiceAreas.ToList().FindAll(x => contractorIds.Contains(x.ContractorId))
                .Where(csa => csa.ServiceAreaId == serviceAreaId).Select(x => x.Contractor).Distinct().ToList();

            foreach (var contractor in secondary)
            {
                if (primary.Contains(contractor) == false)
                {
                    primary.Add(contractor);
                }
            }

            HashSet<int> filteredContractorIds = new HashSet<int>(primary.Select(x => x.ContractorId));
            contractors = contractors.FindAll(x => filteredContractorIds.Contains(x.ContractorId));
            contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));

            // gather selected service, options, and option items to filter by
            var service = db.Services.Where(s => s.ServiceId == serviceId).Where(s => s.IsActive == true).SingleOrDefault(); // selected service
            List<ServiceOption> serviceOptions = new List<ServiceOption>(); // selected service options
            List<ServiceOptionItem> serviceOptionItems = new List<ServiceOptionItem>(); // selected service option items

            // get the service options (populate list)
            foreach (var serviceOptionId in serviceOptionIds)
            {
                int id = int.Parse(serviceOptionId);
                serviceOptions.Add(db.ServiceOptions.Where(so => so.ServiceOptionId == id)
                    .Where(so => so.IsActive).SingleOrDefault());
            }

            // extract the names
            HashSet<string> serviceOptionNames = new HashSet<string>(serviceOptions.Select(x => x.Name));

            // get the service option items (populate list)
            foreach (var serviceOptionItemId in serviceOptionItemIds)
            {
                int id = int.Parse(serviceOptionItemId);
                serviceOptionItems.Add(db.ServiceOptionItems.Where(soi => soi.ServiceOptionItemId == id)
                    .Where(soi => soi.IsActive == true).SingleOrDefault());
            }

            // extract the names
            HashSet<string> serviceOptionItemNames = new HashSet<string>(serviceOptionItems.Select(x => x.Name));

            // get contractor availabilities from the filtered list
            var contractorAvailabilities = db.ContractorAvailabilities.ToList().FindAll(x => contractorIds.Contains(x.ContractorId)).ToList();

            // if there is an option for selecting the date, store what the users selected
            var selectedDayOptionId = serviceOptions.Where(so => so.Name.ToLower() == "scheduled day").SingleOrDefault().ServiceOptionId;
            var selectedDayValue = serviceOptionItems.Where(soi => soi.ServiceOptionId == selectedDayOptionId).SingleOrDefault().Name;

            // initialize list of contractor id's to remove from the pool
            //List<int> unavailableContractors = new List<int>();
            HashSet<int> unavailableContractors = new HashSet<int>();

            // gather list of unavailable contractors, based on their availability settings
            foreach (var availability in contractorAvailabilities)
            {
                bool isAvailable = true;

                if (availability.Monday == false && selectedDayValue.ToLower() == "monday")
                {
                    isAvailable = false;
                }

                if (availability.Tuesday == false && selectedDayValue.ToLower() == "tuesday")
                {
                    isAvailable = false;
                }

                if (availability.Wednesday == false && selectedDayValue.ToLower() == "wednesday")
                {
                    isAvailable = false;
                }

                if (availability.Thursday == false && selectedDayValue.ToLower() == "thursday")
                {
                    isAvailable = false;
                }

                if (availability.Friday == false && selectedDayValue.ToLower() == "friday")
                {
                    isAvailable = false;
                }

                if (availability.Saturday == false && selectedDayValue.ToLower() == "saturday")
                {
                    isAvailable = false;
                }

                if (availability.Sunday == false && selectedDayValue.ToLower() == "sunday")
                {
                    isAvailable = false;
                }

                // the contractor is not available based on their availability settings
                if (isAvailable == false)
                {
                    unavailableContractors.Add(availability.ContractorId); // add to variable
                    contractors.Remove(availability.Contractor); // filter out of master list
                }
            }

            contractorIds = new HashSet<int>(contractors.Select(x => x.ContractorId));

            // all of the service options under the service selected filtered by the active/listed contractors that offer the service selected
            // that contain the options selected
            var contractorServiceOptions = db.ContractorServiceOptions.Where(cso => cso.ContractorService.ServiceId == serviceId)
                .Where(cso => cso.IsActive == true).ToList()
                .FindAll(x => contractorIds.Contains(x.ContractorService.ContractorId))
                .FindAll(x => serviceOptionNames.Contains(x.Name)).ToList();

            // extract the ids
            HashSet<int> contractorServiceOptionIds = new HashSet<int>(contractorServiceOptions.Select(x => x.ContractorServiceOptionId));

            // all of the service option items under the service selected filtered by the active/listed contractors that offer the service selected
            var contractorServiceOptionItems = db.ContractorServiceOptionItems.Where(csoi => csoi.IsActive == true).ToList()
                .FindAll(x => contractorServiceOptionIds.Contains(x.ContractorServiceOptionId))
                .FindAll(x => serviceOptionItemNames.Contains(x.Name)).ToList();

            if (contractorServiceOptions.Count != 0 && contractorServiceOptionItems.Count != 0)
            {
                contractorsAvailable = true;
            }

            var data = new { contractorsAvailable = contractorsAvailable, contractorIds = contractorIds };

            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContractorSubmitted()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
