﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;

namespace YardLad.Controllers
{
    [Authorize(Roles = "admin")]
    public class ServiceOptionRequestsController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /ServiceOptionRequests/Approve
        public ActionResult Approve(int id)
        {
            return null;
        }

        public ActionResult ApproveItemRequest(int id)
        {
            // gather necessary objects
            var serviceOptionItemRequest = db.ServiceOptionItemRequests.Where(soir => soir.ServiceOptionItemRequestId == id).Single(); // the request object
            var contractor = db.Contractors.Where(c => c.ContractorId == serviceOptionItemRequest.ContractorId).Single(); // who made the request
            var contractorUsers = db.ContractorUsers.Where(cu => cu.ContractorId == contractor.ContractorId).ToList();
            var service = db.Services.Where(s => s.Name == serviceOptionItemRequest.ServiceName).Single(); // the service the request was for
            // the option requested under the service (only needed if the request is for option items only)
            var serviceOption = db.ServiceOptions.Where(so => so.Service.Name == service.Name).Where(so => so.Name == serviceOptionItemRequest.OptionName).SingleOrDefault();

            //// add option items (service option already exists)
            //if (serviceOptionItemRequest.ServiceOptionRequestId == null)
            //{
            //    ServiceOptionItem serviceOptionItem = new ServiceOptionItem()
            //    {
            //        Name = serviceOptionItemRequest.Name,
            //        ServiceOption = db.ServiceOptions.Where(so => so.ServiceOptionId == serviceOption.ServiceOptionId).Single(),
            //        Order = serviceOption.ServiceOptionItems.Count + 1,
            //        Price = serviceOptionItemRequest.Price,
            //        IsActive = false, // default to false
            //    };

            //    serviceOptionItem.ServiceOptionId = serviceOptionItem.ServiceOption.ServiceOptionId;

            //    // add option item to db
            //    db.ServiceOptionItems.Add(serviceOptionItem);

            //    // deactivate the request (so it stays in the system but isn't viewable unless necessary
            //    serviceOptionItemRequest.IsActive = false;

            //    db.Entry(serviceOptionItemRequest).State = EntityState.Modified;
            //    db.SaveChanges();
            //}

            TempData["Message"] = "You have successfully approved the request! An email has been sent to the contractor notifying them of the new addition";

            foreach (var contractorUser in contractorUsers)
            {
                // send emails notifying contractor users of new addition
                IEmailSender sender = new GoDaddyEmailSender();
                string subject = "Request Approved!";
                string body = null;
                var contactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;
                var recipientEmail = System.Web.Security.Membership.GetUser(contractorUser.UserId, false).Email;

                body += String.Format("<p>Hi {0},</p>"
                    + "<p>Your recent request to add additional options/items has been approved!<br>"
                + "Login to your account and click manage services to manage your service and add the new additions</p>",
                contractorUser.my_aspnet_users.UserProfile.FirstName);

                try
                {
                    sender.SendEmail(subject, body, from: contactEmail, recipient: "ksexton@technopole.us");
                }
                catch (Exception)
                {
                    TempData["Message"] = "There was an error sending the email to the contractor";
                }
            }

            // remove item request (inactivate)
            serviceOptionItemRequest.IsActive = false;
            db.Entry(serviceOptionItemRequest).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Requests", "ServiceOptionItem");
        }

        public ActionResult Delete(int id)
        {
            // remove request
            var request = db.ServiceOptionRequests.Where(sor => sor.ServiceOptionRequestId == id).Single();

            foreach (var itemRequest in db.ServiceOptionItemRequests.Where(soir => soir.ServiceOptionRequestId == request.ServiceOptionRequestId))
            {
                db.ServiceOptionItemRequests.Remove(itemRequest);
            }

            db.ServiceOptionRequests.Remove(request);
            db.SaveChanges();

            return RedirectToAction("Requests", "ServiceOption");
        }

        public ActionResult DeleteItemRequest(int id, string description)
        {
            if (description == null)
            {
                TempData["Message"] = "Please enter a description as to why you are denying the request and resubmit";
                return RedirectToAction("Requests", "ServiceOptionItem");
            }

            // gather necessary objects
            var serviceOptionItemRequest = db.ServiceOptionItemRequests.Where(soir => soir.ServiceOptionItemRequestId == id).Single(); // the request object
            var contractor = db.Contractors.Where(c => c.ContractorId == serviceOptionItemRequest.ContractorId).Single(); // who made the request
            var contractorUsers = db.ContractorUsers.Where(cu => cu.ContractorId == contractor.ContractorId).ToList();
            var service = db.Services.Where(s => s.Name == serviceOptionItemRequest.ServiceName).Single(); // the service the request was for
            // the option requested under the service (only needed if the request is for option items only)
            var serviceOption = db.ServiceOptions.Where(so => so.Service.Name == service.Name).Where(so => so.Name == serviceOptionItemRequest.OptionName).SingleOrDefault();

            // remove item request (inactivate)
            serviceOptionItemRequest.IsActive = false;
            db.Entry(serviceOptionItemRequest).State = EntityState.Modified;
            db.SaveChanges();

            TempData["Message"] = "An email has been sent to the contractor notifying them that their request has been denied";

            foreach (var contractorUser in contractorUsers)
            {
                // send emails notifying contractor users of new addition
                IEmailSender sender = new GoDaddyEmailSender();
                string subject = "Request Denied";
                string body = null;
                var contactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;
                var recipientEmail = System.Web.Security.Membership.GetUser(contractorUser.UserId, false).Email;

                body += String.Format("<p>Hi {0},</p>"
                    + "<p>Your recent request to add additional options/items has been denied.<br>"
                + "Reasoning: {1}</p>",
                contractorUser.my_aspnet_users.UserProfile.FirstName, description);

                try
                {
                    sender.SendEmail(subject, body, from: contactEmail, recipient: "ksexton@technopole.us");
                }
                catch (Exception)
                {
                    TempData["Message"] = "There was an error sending the email to the contractor";
                }
            }

            return RedirectToAction("Requests", "ServiceOptionItem");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}