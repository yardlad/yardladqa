﻿using PayPal;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    public class PayPalController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        public ActionResult ValidateCommand()
        {
            // retrieve the model from session
            RequestServiceViewModel model = null;
            if (TempData["model"] != null)
            {
                model = (RequestServiceViewModel)TempData["model"];
            }
            else if (Session["RequestServiceViewModel"] != null)
            {
                model = (RequestServiceViewModel)Session["RequestServiceViewModel"];
            }
            else if (TempData["model"] == null && Session["RequestServiceViewModel"] == null)
            {
                return RedirectToAction("RequestService", "Service"); // catch all
            }

            // make sure that the user is logged in
            if (!User.Identity.IsAuthenticated)
            {
                TempData["registerBeforePay"] = "yes"; // store session variable (will be checked in the account controller)
                return RedirectToAction("Register", "Account"); // redirect them to the register page
            }

            // populate the model
            model.SelectedState = db.States.Where(s => s.StateId == model.StateId).SingleOrDefault();
            model.SelectedServiceArea = db.ServiceAreas.Where(sa => sa.ServiceAreaId == model.ServiceAreaId).SingleOrDefault();
            model.SelectedService = db.Services.Where(s => s.ServiceId == model.ServiceId).SingleOrDefault();

            model.SelectedServiceOptions = new List<ServiceOption>();
            foreach (var id in model.SelectedServiceOptionItemIds)
            {
                var key = int.Parse(id);
                var option = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == key).SingleOrDefault().ServiceOption;
                model.SelectedServiceOptions.Add(option);
            }

            model.SelectedServiceOptionItems = new List<ServiceOptionItem>();
            foreach (var id in model.SelectedServiceOptionItemIds)
            {
                var key = int.Parse(id);
                var optionItem = db.ServiceOptionItems.Where(so => so.ServiceOptionItemId == key).SingleOrDefault();
                model.SelectedServiceOptionItems.Add(optionItem);
            }

            model.AvailableContractors = new List<Contractor>();
            foreach (var id in model.AvailableContractorIds)
            {
                var key = int.Parse(id);
                var contractor = db.Contractors.Where(c => c.ContractorId == key).SingleOrDefault();
                model.AvailableContractors.Add(contractor);
            }

            model.SelectedContractor = db.Contractors.Where(c => c.ContractorId == model.SelectedContractorId).SingleOrDefault();
            model.SelectedContractorService = db.ContractorServices.Where(c => c.ContractorId == model.SelectedContractorId).Where(c => c.ServiceId == model.ServiceId).SingleOrDefault();
            model.SubTotal = model.SelectedContractor.ContractorServices.Where(cs => cs.ServiceId == model.ServiceId).SingleOrDefault().BasePrice;

            foreach (var serviceOptionItem in model.SelectedServiceOptionItems)
            {
                foreach (var contractorServiceOptionItem in db.ContractorServiceOptionItems
                    .Where(csoi => csoi.ContractorServiceOption.ContractorService.ContractorId == model.SelectedContractor.ContractorId)
                    .Where(csoi => csoi.ContractorServiceOption.ContractorService.ServiceId == model.ServiceId)
                    .Where(csoi => csoi.ContractorServiceOption.Name == serviceOptionItem.ServiceOption.Name).ToList())
                {
                    if (contractorServiceOptionItem.Name == serviceOptionItem.Name)
                    {
                        model.SubTotal += contractorServiceOptionItem.Price;
                    }
                }
            }

            decimal contractorTaxRate = (decimal)model.SelectedContractor.Taxes.Select(x => x.Rate).SingleOrDefault() * .01M;
            decimal tax = (decimal)(contractorTaxRate * model.SubTotal);

            model.SelectedContractorTaxRate = contractorTaxRate;
            model.Tax = Math.Round(tax, 2);
            model.Total = model.SubTotal + model.Tax;

            // begin paypal process
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSandbox"]); // set in web.config
            var paypal = new PayPalModel(useSandbox); // creates a new paypal model

            foreach (var serviceOptionItem in model.SelectedServiceOptionItems)
            {
                // add the service option with the corresponding option item name to the model
                paypal.Options.Add(serviceOptionItem.ServiceOption.Name, serviceOptionItem.Name);
            }

            paypal.item_name = model.SelectedService.Name;
            paypal.tax = model.Tax.ToString(); // this tax value should be calculated via the rate set by the contractor
            paypal.amount = model.SubTotal.ToString(); // the total amount

            // retrieve the current user and their profile information to populate fields on the paypal form
            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();

            // make sure the user has an address associated with their profile
            if (userProfile.AddressId == null && model.CreateCustomAddress == true)
            {
                // create an address to associate with their profile using the custom value
                Address address = new Address()
                {
                    Line1 = model.CustomAddress.Line1,
                    Line2 = model.CustomAddress.Line2,
                    City = model.CustomAddress.City,
                    StateId = model.CustomAddress.StateId,
                    PostalCode = model.CustomAddress.PostalCode,
                    IsActive = true
                };

                db.Addresses.Add(address);
                userProfile.AddressId = address.AddressId;
                userProfile.Address = address;
                db.Entry(userProfile).State = EntityState.Modified;
                db.SaveChanges(); // associate and save changes
            }

            if (User.Identity.IsAuthenticated && model.CreateCustomAddress == false)
            {
                // use the address associated with their profile
                if (userProfile.AddressId != null)
                {
                    model.UserAddressId = (int)userProfile.AddressId;
                    model.UserAddress = userProfile.Address;
                }
            }

            paypal.email = user.Email;
            paypal.first_name = userProfile.FirstName;
            paypal.last_name = userProfile.LastName;
            if (userProfile.AddressId != null)
            {
                paypal.address1 = userProfile.Address.Line1;
                paypal.address2 = userProfile.Address.Line2;
                paypal.city = userProfile.Address.City;
                paypal.state = userProfile.Address.State.Abbreviation;
                paypal.zip = userProfile.Address.PostalCode;
            }
            paypal.day_phone_a = userProfile.Phone.Substring(1, 3);
            paypal.day_phone_b = userProfile.Phone.Substring(6, 3);
            paypal.day_phone_c = userProfile.Phone.Substring(10, 4);
            if (userProfile.Mobile != null)
            {
                paypal.night_phone_a = userProfile.Mobile.Substring(1, 3);
                paypal.night_phone_b = userProfile.Mobile.Substring(6, 3);
                paypal.night_phone_c = userProfile.Mobile.Substring(10, 4);
            }

            // if the user entered a service address on the page (use the custom address)
            if (User.Identity.IsAuthenticated && model.CreateCustomAddress == true)
            {
                paypal.address1 = model.CustomAddress.Line1;
                paypal.address2 = model.CustomAddress.Line2;
                paypal.city = model.CustomAddress.City;
                paypal.state = db.States.Where(s => s.StateId == model.CustomAddress.StateId).SingleOrDefault().Abbreviation;
                paypal.zip = model.CustomAddress.PostalCode;
            }

            // clear out session variables
            //Session["RequestServiceViewModel"] = null;
            Session["RequestSubmitted"] = "no";

            if ((Session["RequestSubmitted"].ToString() == "no"))
            {
                // create a service request object
                RequestedService requestedService = new RequestedService()
                {
                    AddressId = (int)userProfile.AddressId,
                    AdminComment = null,
                    ContractorComment = null,
                    ContractorId = model.SelectedContractor.ContractorId,
                    ContractorServiceId = model.SelectedContractorService.ContractorServiceId,
                    DateRequestMade = DateTime.Now.AddHours(2),
                    IsActive = false, // default to false, look up and activate once payment is verified
                    IsCompleted = false,
                    Rating = null,
                    Recurring = 0,
                    RequestedEndDate = null,
                    RequestedStartDate = null,
                    UserId = userId,
                };

                db.RequestedServices.Add(requestedService);
                db.SaveChanges();

                if (User.Identity.IsAuthenticated && model.CreateCustomAddress == true)
                {
                    Address serviceAddress = new Address()
                    {
                        Line1 = model.CustomAddress.Line1,
                        Line2 = model.CustomAddress.Line2,
                        City = model.CustomAddress.City,
                        StateId = model.CustomAddress.StateId,
                        PostalCode = model.CustomAddress.PostalCode,
                        IsActive = true
                    };

                    db.Addresses.Add(serviceAddress);
                    db.SaveChanges();

                    requestedService.AddressId = serviceAddress.AddressId;
                    db.Entry(requestedService).State = EntityState.Modified;
                    db.SaveChanges();
                }

                // create records in the db under the requested service (save the selected options and option items)
                foreach (var serviceOptionItem in model.SelectedServiceOptionItems)
                {
                    RequestedServiceOption requestedServiceOption = new RequestedServiceOption()
                    {
                        Name = serviceOptionItem.ServiceOption.Name,
                        IsActive = serviceOptionItem.ServiceOption.IsActive,
                        Order = serviceOptionItem.ServiceOption.Order,
                        RequestedServiceId = requestedService.RequestedServiceId,
                    };

                    db.RequestedServiceOptions.Add(requestedServiceOption);

                    RequestedServiceOptionItem requestedServiceOptionItem = new RequestedServiceOptionItem()
                    {
                        Name = serviceOptionItem.Name,
                        Price = serviceOptionItem.Price,
                        IsActive = serviceOptionItem.IsActive,
                        RequestedServiceOptionId = requestedServiceOption.RequestedServiceOptionId
                    };

                    db.RequestedServiceOptionItems.Add(requestedServiceOptionItem);
                    db.SaveChanges();
                }

                // set the item number to the requested service id
                Session["RequestSubmitted"] = "yes"; // store in session that the request has been submitted
                Session["RequestedServiceId"] = requestedService.RequestedServiceId;
            }

            // if the request has been submitted, and the user has returned to this page (back button from paypal..)
            if (Session["RequestSubmitted"].ToString() == "yes" && Session["RequestedServiceId"] != null)
            {
                // look up the request by the id stored in session
                int requestedServiceId = int.Parse(Session["RequestedServiceId"].ToString());
                paypal.item_number = requestedServiceId.ToString();
            }

            return View(paypal);
        }

        public ActionResult RedirectFromPaypal()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before creating a new payment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.Payments.Where(p => p.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // if a payment with that id hasn't been made, create one

                    Payment payment = new Payment()
                    {
                        DateSubmitted = DateTime.Now.AddHours(2),
                        IsActive = true,
                        IsCompleted = false,
                        RequestedServiceId = id,
                        TransactionId = transactionID,
                        Amount = amount,
                        Email = payerEmail,
                    };

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        payment.IsCompleted = true;
                    }

                    db.Payments.Add(payment);
                    requestedService.IsActive = true; // set to active
                    db.Entry(requestedService).State = EntityState.Modified;
                    db.SaveChanges();

                    // send an email notifying the contractor that a new service has been requested
                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    string subject = "New Service Requested!";
                    string body = null;

                    var customer = db.my_aspnet_users.Where(u => u.id == requestedService.UserId).SingleOrDefault();

                    string yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;
                    
                    foreach (var user in requestedService.Contractor.ContractorUsers)
                    {
                        var recipientEmail = Membership.GetUser(user.UserId, false).Email;
                        var userProfile = db.UserProfiles.Where(up => up.UserId == user.UserId).SingleOrDefault();

                        body += "<p>A new " + requestedService.ContractorService.Service.Name.ToLower() + " service has been requested!</p>";
                        body += "<p>Login to your account <a href=\"http://www.design-develop-deploy.com/yl/Contractor/ServiceHistory/" + requestedService.ContractorId + "\">here</a> to view the new request</p>";

                        // send email to the users (will need to replace the emails when deploying)
                        try
                        {
                            emailsender.SendEmail(subject, body, from: yardladEmail, recipient: recipientEmail);
                        }
                        catch (Exception ex)
                        {
                            // log?
                        } 
                    }

                    // send an email to the customer who just submitted the payment
                    subject = "Payment Complete";
                    body = null;

                    body += "<p>Thank you for your recent purchase and request of a new service!</p>"
                        + "<p>Login to your account <a href=\"http://www.yardlad.design-develop-deploy.com/Account/History\">here</a> to view your service history.</p>";

                    string customerEmail = Membership.GetUser(customer.name, false).Email;

                    try
                    {
                        emailsender.SendEmail(subject, body, yardladEmail, customerEmail);
                    }
                    catch (Exception)
                    {
                        // log?
                    }

                    ViewBag.Success = "yes";
                    Session["RequestSubmitted"] = null;
                    Session["RequestedServiceId"] = null;
                    Session["RequestServiceViewModel"] = null;
                    return View();

                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here

                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();
                    db.RequestedServices.Remove(requestedService); // remove the requested service
                    db.SaveChanges();
                    Session["RequestSubmitted"] = null;
                }
            }

            return View();
        }

        public ActionResult CancelFromPaypal()
        {
            return RedirectToAction("RequestService", "Service");
        }

        public ActionResult NotifyFromPaypal()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before creating a new payment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.Payments.Where(p => p.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // if a payment with that id hasn't been made, create one

                    Payment payment = new Payment()
                    {
                        DateSubmitted = DateTime.Now.AddHours(2),
                        IsActive = true,
                        IsCompleted = false,
                        RequestedServiceId = id,
                        TransactionId = transactionID,
                        Amount = amount,
                        Email = payerEmail,
                    };

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        payment.IsCompleted = true;
                    }

                    db.Payments.Add(payment);
                    requestedService.IsActive = true; // set to active
                    db.Entry(requestedService).State = EntityState.Modified;
                    db.SaveChanges();

                    // send an email notifying the contractor that a new service has been requested
                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    string subject = "New Service Requested!";
                    string body = null;

                    var customer = db.my_aspnet_users.Where(u => u.id == requestedService.UserId).SingleOrDefault();

                    string yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                    foreach (var user in requestedService.Contractor.ContractorUsers)
                    {
                        var recipientEmail = Membership.GetUser(user.UserId, false).Email;
                        var userProfile = db.UserProfiles.Where(up => up.UserId == user.UserId).SingleOrDefault();

                        body += "<p>A new " + requestedService.ContractorService.Service.Name.ToLower() + " service has been requested!</p>";
                        body += "<p>Login to your account <a href=\"http://www.design-develop-deploy.com/yl/Account/MyAccount\">here</a> to view the new request</p>";

                        // send email to the users (will need to replace the emails when deploying)
                        emailsender.SendEmail(subject, body, from: yardladEmail, recipient: recipientEmail);
                    }

                    // send an email to the customer who just submitted the payment
                    subject = "Payment Complete";
                    body = null;

                    body += "<p>Thank you for your recent purchase and request of a new service!</p>"
                        + "<p>Login to your account <a href=\"http://www.yardlad.design-develop-deploy.com/Account/History\">here</a> to view your service history.</p>";

                    string customerEmail = Membership.GetUser(customer.name, false).Email;

                    try
                    {
                        emailsender.SendEmail(subject, body, yardladEmail, customerEmail);
                    }
                    catch (Exception)
                    {
                        // log?
                    }

                    ViewBag.Success = "yes";
                    Session["RequestSubmitted"] = null;
                    Session["RequestedServiceId"] = null;
                    Session["RequestServiceViewModel"] = null;
                    return View();

                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here

                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();
                    db.RequestedServices.Remove(requestedService); // remove the requested service
                    db.SaveChanges();
                    Session["RequestSubmitted"] = null;
                }
            }

            return View();

        }

        public ActionResult ContractorPayment(int contractorId, int contractorTypeId, string price)
        {
            // BEGIN PAYPAL PROCESS

            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSandbox"]); // set in web config
            var paypal = new PayPalModel(useSandbox); // creates a new paypal model

            paypal.item_name = "Yard Lad Service Subscription";
            paypal.item_number = contractorId.ToString();
            paypal.amount = price; // the total amount
            //paypal.cancel_return = "http://localhost:51830/Account/MyAccount";
            //paypal.@return = "http://localhost:51830/Paypal/ContractorPaymentReturn";
            //paypal.notify_url = "http://localhost:51830/Paypal/ContractorPaymentNotify";

            paypal.cancel_return = "http://yardlad.design-develop-deploy.com/Account/MyAccount";
            paypal.@return = "http://yardlad.design-develop-deploy.com/Paypal/ContractorPaymentReturn";
            paypal.notify_url = "http://yardlad.design-develop-deploy.com/Paypal/ContractorPaymentNotify";

            // retrieve the current user and their profile information to populate fields on the paypal form
            var currentUser = Membership.GetUser();
            var userId = (int)currentUser.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();

            // set all the variables to be sent into the view
            paypal.email = currentUser.Email;
            paypal.first_name = userProfile.FirstName;
            paypal.last_name = userProfile.LastName;
            paypal.address1 = userProfile.Address.Line1;
            paypal.address2 = userProfile.Address.Line2;
            paypal.city = userProfile.Address.City;
            paypal.state = userProfile.Address.State.Abbreviation;
            paypal.zip = userProfile.Address.PostalCode;
            paypal.day_phone_a = userProfile.Phone.Substring(1, 3);
            paypal.day_phone_b = userProfile.Phone.Substring(6, 3);
            paypal.day_phone_c = userProfile.Phone.Substring(10, 4);
            if (userProfile.Mobile != null)
            {
                paypal.night_phone_a = userProfile.Mobile.Substring(1, 3);
                paypal.night_phone_b = userProfile.Mobile.Substring(6, 3);
                paypal.night_phone_c = userProfile.Mobile.Substring(10, 4);
            }

            return View(paypal);
        }

        public ActionResult ContractorPaymentNotify()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var contractor = db.Contractors.Where(c => c.ContractorId == id).SingleOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before creating a new payment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.ContractorPayments.Where(cp => cp.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // if a payment with that id hasn't been made, create one
                    ContractorPayment payment = new ContractorPayment()
                    {
                        DateSubmitted = DateTime.Now.AddHours(2),
                        IsActive = true,
                        IsCompleted = false,
                        ContractorId = id,
                        TransactionId = transactionID,
                        Amount = amount,
                        Email = payerEmail,
                    };

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        payment.IsCompleted = true;
                    }

                    db.ContractorPayments.Add(payment);
                    db.SaveChanges();

                    // mark the expiration date for the contractor
                    if (contractor.ExpiresOn == null)
                    {
                        // the contractor is making their first payment
                        contractor.ExpiresOn = DateTime.Now.AddHours(2).AddYears(1);
                    }
                    else
                    {
                        var currentExpirationDate = contractor.ExpiresOn;
                        var currentDate = DateTime.Now.AddHours(2);

                        var difference = currentExpirationDate.Value - currentDate;

                        // tack on the extra remaining time to the new expiration date
                        contractor.ExpiresOn = (currentExpirationDate.Value - difference).AddYears(1);

                        db.Entry(contractor).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    ViewBag.Success = "yes";

                    return View();
                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here
                }
            }

            return View();
        }

        public ActionResult ContractorPaymentReturn()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var contractor = db.Contractors.Where(c => c.ContractorId == id).SingleOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before creating a new payment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.ContractorPayments.Where(cp => cp.TransactionId == transactionID).SingleOrDefault();

                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // if a payment with that id hasn't been made, create one
                    ContractorPayment payment = new ContractorPayment()
                    {
                        DateSubmitted = DateTime.Now.AddHours(2),
                        IsActive = true,
                        IsCompleted = false,
                        ContractorId = id,
                        TransactionId = transactionID,
                        Amount = amount,
                        Email = payerEmail,
                    };

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        payment.IsCompleted = true;
                    }

                    db.ContractorPayments.Add(payment);
                    db.SaveChanges();

                    // mark the expiration date for the contractor
                    if (contractor.ExpiresOn == null)
                    {
                        // the contractor is making their first payment
                        contractor.ExpiresOn = DateTime.Now.AddHours(2).AddYears(1);
                    }
                    else
                    {
                        var currentExpirationDate = contractor.ExpiresOn;
                        var currentDate = DateTime.Now.AddHours(2);

                        var difference = currentExpirationDate.Value - currentDate;

                        // tack on the extra remaining time to the new expiration date
                        contractor.ExpiresOn = (currentExpirationDate.Value - difference).AddYears(1);

                        db.Entry(contractor).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    ViewBag.Success = "yes";

                    return View();
                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here
                }
            }

            return View();
        }

        public ActionResult PaymentAdjustment(int id /* paymentAdjustmentId */)
        {
            // BEGIN PAYPAL PROCESS

            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSandbox"]); // set in web config
            var paypal = new PayPalModel(useSandbox); // creates a new paypal model

            var paymentAdjustment = db.PaymentAdjustments.Where(pa => pa.PaymentAdjustmentId == id).SingleOrDefault();

            paypal.item_name = "Payment Adjustment for " + paymentAdjustment.RequestedService.ContractorService.Service.Name;
            paypal.item_number = paymentAdjustment.PaymentAdjustmentId.ToString();

            // get the first payment and add the negative value of the payment adjustment
            decimal paymentAmount = paymentAdjustment.RequestedService.Payments.FirstOrDefault().Amount + paymentAdjustment.Amount;

            paypal.amount = paymentAmount.ToString(); // the total amount

            paypal.cancel_return = "http://yardlad.design-develop-deploy.com/Account/History";
            paypal.@return = "http://yardlad.design-develop-deploy.com/Paypal/PaymentAdjustmentReturn";
            paypal.notify_url = "http://yardlad.design-develop-deploy.com/Paypal/PaymentAdjustmentNotify";

            // retrieve the current user and their profile information to populate fields on the paypal form
            var currentUser = Membership.GetUser();
            var userId = (int)currentUser.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();

            // set all the variables to be sent into the view
            paypal.email = currentUser.Email;
            paypal.first_name = userProfile.FirstName;
            paypal.last_name = userProfile.LastName;
            paypal.address1 = userProfile.Address.Line1;
            paypal.address2 = userProfile.Address.Line2;
            paypal.city = userProfile.Address.City;
            paypal.state = userProfile.Address.State.Abbreviation;
            paypal.zip = userProfile.Address.PostalCode;
            paypal.day_phone_a = userProfile.Phone.Substring(1, 3);
            paypal.day_phone_b = userProfile.Phone.Substring(6, 3);
            paypal.day_phone_c = userProfile.Phone.Substring(10, 4);
            if (userProfile.Mobile != null)
            {
                paypal.night_phone_a = userProfile.Mobile.Substring(1, 3);
                paypal.night_phone_b = userProfile.Mobile.Substring(6, 3);
                paypal.night_phone_c = userProfile.Mobile.Substring(10, 4);
            }

            return View(paypal);
        }

        public ActionResult PaymentAdjustmentNotify()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the payment adjustment using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var paymentAdjustment = db.PaymentAdjustments.Where(c => c.PaymentAdjustmentId == id).SingleOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before editing the paymentAdjustment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.PaymentAdjustments.Where(pa => pa.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // payment hasn't been made for the adjustment, edit the existing record
                    paymentAdjustment.TransactionId = transactionID;
                    paymentAdjustment.Email = payerEmail;
                    paymentAdjustment.DateSubmitted = DateTime.Now.AddHours(2);

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        paymentAdjustment.IsCompleted = true;
                    }

                    db.Entry(paymentAdjustment).State = EntityState.Modified;
                    db.SaveChanges();

                    ViewBag.Success = "yes";
                    return View();
                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here
                }
            }

            return View();
        }

        public ActionResult PaymentAdjustmentReturn()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the payment adjustment using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var paymentAdjustment = db.PaymentAdjustments.Where(c => c.PaymentAdjustmentId == id).SingleOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before editing the paymentAdjustment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.PaymentAdjustments.Where(pa => pa.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // payment hasn't been made for the adjustment, edit the existing record
                    paymentAdjustment.TransactionId = transactionID;
                    paymentAdjustment.Email = payerEmail;
                    paymentAdjustment.DateSubmitted = DateTime.Now.AddHours(2);

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        paymentAdjustment.IsCompleted = true;
                    }

                    db.Entry(paymentAdjustment).State = EntityState.Modified;
                    db.SaveChanges();

                    ViewBag.Success = "yes";
                    return View();
                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here
                }
            }

            return View();
        }

        public ActionResult RequestedService(int id /* requested service id */)
        {
            // look up the requested service
            var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

            // begin paypal process
            bool useSandbox = Convert.ToBoolean(ConfigurationManager.AppSettings["IsSandbox"]); // set in web.config
            var paypal = new PayPalModel(useSandbox); // creates a new paypal model

            var amount = (requestedService.Payments.Where(p => p.IsActive == false).SingleOrDefault().Amount + requestedService.PaymentAdjustments.SingleOrDefault().Amount);
            var tax = amount * (requestedService.Contractor.Taxes.FirstOrDefault().Rate * .01M);
            var total = amount + tax;

            paypal.item_number = requestedService.RequestedServiceId.ToString();
            paypal.item_name = requestedService.ContractorService.Service.Name + " - Payment (Adjustment)";
            paypal.tax = String.Format("{0:F2}", tax);
            paypal.amount = String.Format("{0:F2}", total); // the total amount

            // retrieve the current user and their profile information to populate fields on the paypal form
            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();

            paypal.email = user.Email;
            paypal.first_name = userProfile.FirstName;
            paypal.last_name = userProfile.LastName;
            paypal.address1 = userProfile.Address.Line1;
            paypal.address2 = userProfile.Address.Line2;
            paypal.city = userProfile.Address.City;
            paypal.state = userProfile.Address.State.Abbreviation;
            paypal.zip = userProfile.Address.PostalCode;
            paypal.day_phone_a = userProfile.Phone.Substring(1, 3);
            paypal.day_phone_b = userProfile.Phone.Substring(6, 3);
            paypal.day_phone_c = userProfile.Phone.Substring(10, 4);
            if (userProfile.Mobile != null)
            {
                paypal.night_phone_a = userProfile.Mobile.Substring(1, 3);
                paypal.night_phone_b = userProfile.Mobile.Substring(6, 3);
                paypal.night_phone_c = userProfile.Mobile.Substring(10, 4);
            }

            paypal.cancel_return = "http://yardlad.design-develop-deploy.com/Account/History";
            paypal.@return = "http://yardlad.design-develop-deploy.com/Paypal/RequestedServiceReturn";
            paypal.notify_url = "http://yardlad.design-develop-deploy.com/Paypal/RequestedServiceNotify";

            return View(paypal);
        }

        public ActionResult RequestedServiceReturn()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

                    // look up the payment adjustment
                    var paymentAdjustment = requestedService.PaymentAdjustments.FirstOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before creating a new payment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.Payments.Where(p => p.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // if a payment with that id hasn't been made, create one

                    Payment payment = new Payment()
                    {
                        DateSubmitted = DateTime.Now.AddHours(2),
                        IsActive = true,
                        IsCompleted = false,
                        RequestedServiceId = id,
                        TransactionId = transactionID,
                        Amount = amount,
                        Email = payerEmail,
                    };

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        payment.IsCompleted = true;
                        paymentAdjustment.IsCompleted = true;
                    }

                    db.Payments.Add(payment);
                    db.Entry(paymentAdjustment).State = EntityState.Modified;
                    db.SaveChanges();

                    // send an email notifying the contractor that a new service has been requested
                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    string subject = "Full Payment Adjustment Completed!";
                    string body = null;

                    var customer = db.my_aspnet_users.Where(u => u.id == requestedService.UserId).SingleOrDefault();

                    var yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                    foreach (var user in requestedService.Contractor.ContractorUsers)
                    {
                        var recipientEmail = Membership.GetUser(user.UserId, false).Email;
                        var userProfile = db.UserProfiles.Where(up => up.UserId == user.UserId).SingleOrDefault();

                        body += "<p>A new payment amount for the " + requestedService.ContractorService.Service.Name.ToLower() + " service has been completed!</p>";
                        body += "<p>Login to your account <a href=\"http://www.design-develop-deploy.com/yl/Contractor/ServiceHistory/" + requestedService.ContractorId + "\">here</a> to view the changes</p>";

                        // send email to the users (will need to replace the emails when deploying)
                        try
                        {
                            emailsender.SendEmail(subject, body, from: yardladEmail, recipient: recipientEmail);
                        }
                        catch (Exception ex)
                        {
                            // log
                        }
                    }

                    ViewBag.Success = "yes";
                    return View();

                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here

                }
            }

            return View();
        }

        public ActionResult RequestedServiceNotify()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");

            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);

            if (response == "VERIFIED")
            {
                string transactionID = Request["txn_id"];
                string sAmountPaid = Request["mc_gross"];

                //validate the order
                Decimal amountPaid = 0;
                Decimal.TryParse(sAmountPaid, out amountPaid);

                if (sAmountPaid != null)
                {
                    // look up the requested service using the item_number
                    int id = int.Parse(Request["item_number"]);
                    var requestedService = db.RequestedServices.Where(rs => rs.RequestedServiceId == id).SingleOrDefault();

                    // look up the payment adjustment
                    var paymentAdjustment = requestedService.PaymentAdjustments.FirstOrDefault();

                    decimal amount = decimal.Parse(Request["mc_gross"]); // pull out the total amount
                    string payerEmail = Request["payer_email"]; // and the paypal user email

                    // before creating a new payment object, make sure there isn't an item with the transaction id already
                    // in the database

                    var paymentCheck = db.Payments.Where(p => p.TransactionId == transactionID).SingleOrDefault();
                    if (paymentCheck != null)
                    {
                        ViewBag.Success = "yes";
                        return View();
                    }

                    // if a payment with that id hasn't been made, create one

                    Payment payment = new Payment()
                    {
                        DateSubmitted = DateTime.Now.AddHours(2),
                        IsActive = true,
                        IsCompleted = false,
                        RequestedServiceId = id,
                        TransactionId = transactionID,
                        Amount = amount,
                        Email = payerEmail,
                    };

                    // set the payment completion status based on the result
                    if (Request["payment_status"].ToLower() == "completed")
                    {
                        payment.IsCompleted = true;
                        paymentAdjustment.IsCompleted = true;
                    }

                    db.Payments.Add(payment);
                    db.Entry(paymentAdjustment).State = EntityState.Modified;
                    db.SaveChanges();

                    // send an email notifying the contractor that a new service has been requested
                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    string subject = "Full Payment Adjustment Completed!";
                    string body = null;

                    var customer = db.my_aspnet_users.Where(u => u.id == requestedService.UserId).SingleOrDefault();

                    var yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                    foreach (var user in requestedService.Contractor.ContractorUsers)
                    {
                        var recipientEmail = Membership.GetUser(user.UserId, false).Email;
                        var userProfile = db.UserProfiles.Where(up => up.UserId == user.UserId).SingleOrDefault();

                        body += "<p>A new payment amount for the " + requestedService.ContractorService.Service.Name.ToLower() + " service has been completed!</p>";
                        body += "<p>Login to your account <a href=\"http://www.design-develop-deploy.com/yl/Contractor/ServiceHistory/" + requestedService.ContractorId + "\">here</a> to view the changes</p>";

                        // send email to the users (will need to replace the emails when deploying)
                        try
                        {
                            emailsender.SendEmail(subject, body, from: yardladEmail, recipient: recipientEmail);
                        }
                        catch (Exception ex)
                        {
                            // log
                        }
                    }

                    ViewBag.Success = "yes";
                    return View();

                }
                else
                {
                    // let fail - this is the IPN so there is no viewer
                    // you may want to log something here

                }
            }

            return View();
        }

        string GetPayPalResponse(Dictionary<string, string> DictionaryformVals, bool useSandbox)
        {
            // Parse the variables
            // Choose whether to use sandbox or live environment
            string paypalUrl = useSandbox ? "https://www.sandbox.paypal.com/cgi-bin/webscr"
            : "https://www.paypal.com/cgi-bin/webscr";
 
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(paypalUrl);
 
            // Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
 
            byte[] param = Request.BinaryRead(Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);
 
            StringBuilder sb = new StringBuilder();
            sb.Append(strRequest);

            foreach (string key in DictionaryformVals.Keys)
            {
                sb.AppendFormat("&{0}={1}", key, DictionaryformVals[key]);
            }

            strRequest += sb.ToString();
            req.ContentLength = strRequest.Length;
 
            // for proxy
            //WebProxy proxy = new WebProxy(new Uri("http://urlort#");
            //req.Proxy = proxy;

            // Send the request to PayPal and get the response
            string response = "";
            using (StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII))
            {
                streamOut.Write(strRequest);
                streamOut.Close();
                using (StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    response = streamIn.ReadToEnd();
                }
            }
 
            return response;
        }
    }
}
