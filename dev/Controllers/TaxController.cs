﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class TaxController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Tax/Edit/5
 
        public ActionResult Edit(int id)
        {
            Tax tax = db.Taxes.Find(id);
            return View(tax);
        }

        //
        // POST: /Tax/Edit/5

        [HttpPost]
        public ActionResult Edit(Tax tax)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tax).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MyAccount", "Account");
            }
            return View(tax);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}