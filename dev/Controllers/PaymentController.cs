﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class PaymentController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Payment/

        public ViewResult Index(int id = 0)
        {
            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;


            var payments = db.Payments.Include(p => p.RequestedService);

            if (id != 0)
            {
                // security measure
                if (!User.IsInRole("admin") && userId == id)
                {
                    payments = db.Payments.Where(p => p.RequestedService.UserId == id); 
                }
                else
                {
                    throw new Exception("you can only access your own payments");
                }
            }

            return View(payments.ToList());
        }

        //
        // GET: /Payment/Details/5

        public ViewResult Details(int id)
        {
            Payment payment = db.Payments.Find(id);
            return View(payment);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}