﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{   
    [Authorize]
    public class CartController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Cart/

        public ActionResult Index(string returnUrl)
        {
            CartIndexViewModel model = new CartIndexViewModel()
            {
                Cart = GetCart(),
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        public ActionResult AddToCart(string returnUrl)
        {
            return RedirectToAction("~/" + returnUrl);
        }

        public ActionResult RemoveFromCart(string returnUrl)
        {
            return RedirectToAction("~/" + returnUrl);
        }

        public Cart GetCart()
        {
            Cart cart = (Cart)Session["Cart"];
            if (cart == null)
            {
                cart = new Cart();
                Session["Cart"] = cart;
            }

            return cart;
        }

    }
}
