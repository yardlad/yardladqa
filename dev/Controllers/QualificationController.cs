﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class QualificationController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Qualification/Details/5

        public ViewResult Details(int id = 0)
        {
            if (id == 0)
            {
                // current logged in user
                int userId = (int)Membership.GetUser().ProviderUserKey;

                id = db.ContractorUsers.Where(cu => cu.UserId == userId).Single().ContractorId;
            }

            // look up the contractor by id
            var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
            var qualification = db.Qualifications.Where(q => q.QualificationId == contractor.QualificationId).Single();

            if (contractor.IsApproved == true)
            {
                ViewBag.IsApproved = true;
                ViewBag.ExpirationDate = Convert.ToDateTime(contractor.ApprovedOn).AddYears(1);
            }

            return View(qualification);
        }

        //
        // GET: /Qualification/Create

        public ActionResult Create(int id)
        {
            Qualification newQualification = new Qualification()
            {
                IsActive = true, // default to active
                DateSubmitted = DateTime.Now.AddHours(2),
            };

            ViewBag.ContractorType = db.Contractors.Where(c => c.ContractorId == id).Single().ContractorTypeId;
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive).OrderBy(s => s.Name), "StateId", "Name");

            return View(newQualification);
        } 

        //
        // POST: /Qualification/Create

        [HttpPost]
        public ActionResult Create(Qualification model, string returnUrl, int id, HttpPostedFileBase source)
        {
            // the user has to fill out at least one
            if (model.EIN == null && model.TaxId == null)
            {
                ModelState.AddModelError("", "make sure that you have entered your EIN or your Tax ID number");
            }

            if (ModelState.IsValid)
            {
                Address address = new Address()
                {
                    Line1 = model.Address.Line1,
                    Line2 = model.Address.Line2,
                    City = model.Address.City,
                    StateId = model.Address.StateId,
                    PostalCode = model.Address.PostalCode,
                    IsActive = true,
                };

                db.Addresses.Add(address);

                // was creating a duplicate address when adding the qualification to the db
                // NEED TO TEST THIS LIVE
                model.Address.AddressId = address.AddressId;
                model.DateSubmitted = DateTime.Now.AddHours(2);

                model.Address = address;
                model.AddressId = address.AddressId;

                // add the qualification
                db.Qualifications.Add(model);

                // add the qualificiationId to the associated contractor
                var contractor = db.Contractors.Where(c => c.ContractorId == id).Single();
                contractor.Qualification = model;

                // deal with the uploaded file
                if (model.Source != null)
                {
                    var filename = Path.GetFileName(source.FileName); // get the name of the file
                    string extension = Path.GetExtension(source.FileName).ToLower(); // get the extension (in lowercase)

                    // check to make sure the extension can be accepted as an image
                    if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".bmp"
                        && extension != ".pdf" && extension != ".doc" && extension != ".docx")
                    {
                        ModelState.AddModelError("", "the upload is not in the right format, please review the list of accepted file types");
                        return View(model);
                    }

                    Guid fileName = Guid.NewGuid(); // make a new guid to uniquely identify the qualification on the server
                    filename = fileName + extension; // reattach the extension

                    model.FileName = fileName.ToString(); // save filename to model
                    model.MimeType = extension; // save extension to model
                    

                    // store the file inside ~/Content/Qualifications/
                    var path = Path.Combine(Server.MapPath("~/Content/Qualifications"), filename); // assign path
                    source.SaveAs(path); // save to the server
                    model.Source = "~/Content/Qualifications/" + filename; // save filepath
                }
                else // if there was already a qualification path associated in the db
                {
                    // look up the previous qualification that was uploaded
                    Qualification prevUpload = (from q in db.Qualifications
                                            where q.QualificationId == model.QualificationId
                                            select q).Single();

                    if (prevUpload != null)
                    {
                        // if it isn't null, set the path

                        // try to remove the current qualification from the server and upload the new one in its place
                        string oldQualification = Server.MapPath("~/Content/Qualifications");
                        FileInfo fi = new FileInfo(oldQualification);
                        if (fi.Name == prevUpload.FileName)
                        {
                            fi.Delete();
                        }

                        // save the new path
                        model.Source = prevUpload.Source;
                    }
                }

                db.Entry(contractor).State = EntityState.Modified;
                db.SaveChanges();

                if (returnUrl != null)
                {
                    return Redirect("~/" + returnUrl);
                }
                else
                {
                    return RedirectToAction("MyAccount", "Account");  
                }
                
            }

            ViewBag.ContractorType = db.Contractors.Where(c => c.ContractorId == id).Single().ContractorTypeId;
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive).OrderBy(s => s.Name), "StateId", "Name", model.Address.StateId);
            return View(model);
        }
        
        //
        // GET: /Qualification/Edit/5
 
        public ActionResult Edit(int id)
        {
            // look up the currently logged in user Id
            int userId = (int)Membership.GetUser().ProviderUserKey;

            // look up the contractor Id
            var contractorId = db.ContractorUsers.Where(cu => cu.UserId == userId).Single().ContractorId;

            // if the contractor exists, they can only upload/edit their qualifications if they are unapproved
            if (contractorId != 0)
            {
                var contractor = db.Contractors.Where(c => c.ContractorId == contractorId).Single();

                if (contractor.IsApproved == true)
                {
                    ViewBag.IsApproved = true;
                    return RedirectToAction("Details");
                }
            }

            var model = db.Qualifications.Where(q => q.QualificationId == id).Single();
            ViewBag.ContractorType = db.Contractors.Where(c => c.ContractorId == contractorId).Single().ContractorTypeId;
            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive).OrderBy(s => s.Name), "StateId", "Name");
            ViewBag.ContractorId = contractorId;

            return View(model);
        }

        //
        // POST: /Qualification/Edit/5

        [HttpPost]
        public ActionResult Edit(Qualification model, HttpPostedFileBase source)
        {
            if (ModelState.IsValid)
            {
                YardLadEntities x = new YardLadEntities();

                // look up the previous qualification that was uploaded
                Qualification prevUpload = (from q in x.Qualifications
                                        where q.QualificationId == model.QualificationId
                                        select q).Single();

                // the user has uploaded a qualification before but chose to upload a new one
                if (prevUpload != null && source != null)
                {
                    // check first to make sure the qualication filetype is valid before deleting original file
                    var filename = Path.GetFileName(source.FileName); // get the name of the file
                    string extension = Path.GetExtension(source.FileName).ToLower(); // get the extension (in lowercase)

                    // check to make sure the extension can be accepted as an image
                    if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".bmp"
                        && extension != ".pdf" && extension != ".doc" && extension != ".docx")
                    {
                        ModelState.AddModelError("", "the upload is not in the right format, please review the list of accepted file types");
                        return View(model);
                    }

                    // else - continue to remove previously uploaded file

                    // try to remove the current qualification from the server and upload the new one in its place
                    DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/Content/Qualifications"));
                    
                    // look through the directory of files in the qualification content folder
                    foreach (var item in dirInfo.GetFiles())
                    {
                        if (item.Name == (prevUpload.FileName + prevUpload.MimeType)) // if the filenames match
                        {
                            FileInfo fi = new FileInfo(dirInfo + "/" + item.Name);
                            fi.Delete();
                        }
                    }
                }

                // deal with the uploaded file
                if (source != null)
                {
                    var filename = Path.GetFileName(source.FileName); // get the name of the file
                    string extension = Path.GetExtension(source.FileName).ToLower(); // get the extension (in lowercase)

                    // check to make sure the extension can be accepted as an image
                    if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".bmp"
                        && extension != ".pdf" && extension != ".doc" && extension != ".docx")
                    {
                        ModelState.AddModelError("", "the upload is not in the right format, please review the list of accepted file types");
                        return View(model);
                    }

                    Guid fileName = Guid.NewGuid(); // make a new guid to uniquely identify the qualification on the server
                    filename = fileName + extension; // reattach the extension

                    model.FileName = fileName.ToString(); // save filename to model
                    model.MimeType = extension; // save extension to model

                    // store the file inside ~/Content/Qualifications/
                    var path = Path.Combine(Server.MapPath("~/Content/Qualifications"), filename); // assign path
                    source.SaveAs(path); // save to the server
                    model.Source = "~/Content/Qualifications/" + filename; // save filepath
                    model.IsActive = true;
                }

                x.Dispose();

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("MyAccount", "Account");

            }

            ViewBag.State = new SelectList(db.States.Where(s => s.IsActive).OrderBy(s => s.Name), "StateId", "Name");
            return View(model);
        }

        //
        // GET: /Qualification/Download/5

        public FileResult Download(int id)
        {
            if (id != 0)
            {
                // lookup the qualification by id
                var qualification = db.Qualifications.Where(q => q.QualificationId == id).Single();
                DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/Content/Qualifications"));

                // look through the directory of files in the qualification content folder
                foreach (var item in dirInfo.GetFiles())
                {
                    if (item.Name == (qualification.FileName + qualification.MimeType)) // if the filenames match
                    {
                        // lookup the contractor name to attach to file name
                        var contractor = db.Contractors.Where(c => c.QualificationId == qualification.QualificationId).Single();

                        string fileName = String.Format("{0} {1}", contractor.Name, "Qualification");
                        string filePath = dirInfo.FullName + @"\" + qualification.FileName + qualification.MimeType;

                        return File(filePath, qualification.MimeType, fileName);
                    }
                }
            }

            return null;
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}