﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    public class ContactController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Contact/

        public ActionResult Index()
        {
            ViewBag.Address = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Address").Single().Value;
            ViewBag.Email = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;
            ViewBag.Phone = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Phone Number").Single().Value;

            return View();
        }

        //
        // POST: /Contact/

        [HttpPost, HandleError]
        public ActionResult Index(ContactViewModel model)
        {
            // check to make sure that the length of the phone number entered is correct
            if (model.Area.Length != 3 || model.Prefix.Length != 3 || model.Suffix.Length != 4)
            {
                ModelState.AddModelError("", "please enter a valid phone number");
            }

            if (ModelState.IsValid)
            {
                // format the phone number that will be sent in the email
                model.Phone = "(" + model.Area + ") " + model.Prefix + "-" + model.Suffix;

                // send the email via GoDaddy (implementing IEmailSender interface)
                IEmailSender emailsender = new GoDaddyEmailSender();
                string body = string.Format("<p>New contact form submission!</p>" + "<p>Name: {0}<br />Email: {1}<br />Phone: {2}<br />"
                    + "Message: {3}</p>", model.Name, model.Email, model.Phone, model.Message);

                var contactEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                try
                {
                    // send email to contact@yardlad.com (will need to replace the emails when deploying)
                    emailsender.SendEmail(model.Subject, body, from: contactEmail, recipient: contactEmail);
                }
                catch (SmtpFailedRecipientException)
                {
                    throw new Exception("Your email was not sent, please try again later.");
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                // let the user know that their message was sent
                return View("Submitted", model);
            }

            return View(model);
        }

        //
        // GET: /Contact/Submitted

        public ActionResult Submitted(ContactViewModel model)
        {
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
