using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Web.Security;
using YardLad.Models;
using YardLad.Models.Domain;
using YardLad.Models.Interfaces;
using YardLad.Models.View;
//try catch for validation error
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Text;

namespace YardLad.Controllers
{
    public class AccountController : Controller
    {
        private static readonly log4net.ILog _logger = log4net.LogManager.GetLogger(typeof(AccountController));
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Account/LogOn

        public ActionResult LogOn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                // check to make sure that the user even exists
                if (Membership.FindUsersByName(model.Username).Count == 0)
                {
                    ModelState.AddModelError("", "the username entered does not exist, please try again");
                    return View(model); // if not, return them to the view
                }

                // try to validate the user
                if (Membership.ValidateUser(model.Username, model.Password))
                {
                    // set an authorization cookie
                    // if remember me is checked, it will save login across browser sessions
                    FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);

                    // redirect the user to the return url
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else if (Roles.IsUserInRole("admin", model.Username))
                    {
                        // if they are an admin, redirect them to the control panel
                        return RedirectToAction("ControlPanel", "Admin");
                    }
                    else if (Roles.GetRolesForUser(model.Username).Count() == 0)
                    {
                        // if they are a user, redirect them to the request a service page
                        return RedirectToAction("RequestService", "Service");
                    }
                    else
                    {
                        // redirect them to their account
                        return RedirectToAction("MyAccount", "Account");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        [Authorize]
        public ActionResult LogOff()
        {
                // get user information
                var user = Membership.GetUser();
                var userId = (int)user.ProviderUserKey;
                var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

                // used to display notifications, last login date is set when the user logs off
                var profile = db.UserProfiles.Where(up => up.UserId == userId).Single();
                profile.LastLoginDate = DateTime.Now.AddHours(2);
                db.Entry(profile).State = EntityState.Modified;

                try
                {
                    // doing my logic here
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceInformation("Property: {0} Error: {1}", validationError.
              PropertyName, validationError.ErrorMessage);
                        }
                    }
                }

                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Account"); // return user to login screen
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            // if redirected from paypal controller (when user is requesting a service)
            // this check makes sure that the user is registered and logged in before purchasing
            if (TempData["registerBeforePay"] != null)
            {
                // send message to the view
                ViewBag.Message = "You must register an account with us before requesting a service.<br />"
                    + "Once registered you can resume where you left off.";
            }

            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            // checks to make sure that the length of the phone number input fields is correct
            if (model.Phone.Length != 14)
                ModelState.AddModelError("", "please enter a valid phone number");

            if (model.Mobile != null && model.Mobile.Length != 14)
                ModelState.AddModelError("", "please enter a valid mobile number");

            // checks to make sure the user is 18 years old
            if (model.DateOfBirth.Date > DateTime.Now.AddHours(2).Date.AddYears(-18))
            {
                ModelState.AddModelError("", "you must be at least 18 years of age");
            }

            // make sure that the user enters a mobile number if they check to receive SMS texts
            if (model.AcceptSMS == true && model.Mobile == null)
            {
                ModelState.AddModelError("", "you must enter your mobile number if you wish to receive SMS texts");
            }

            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.Username, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success) // if the new user was successfully created
                {
                    // find the user that was just created
                    var user = db.my_aspnet_users.Where(u => u.name == model.Username).Single();

                    // create a new profile, associate values from the model
                    UserProfile newProfile = new UserProfile()
                    {
                        UserId = user.id, // pulled from the local variable
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        DateOfBirth = model.DateOfBirth,
                        Phone = model.Phone,
                        Mobile = model.Mobile,
                        AcceptSMS = model.AcceptSMS, // save this in db for SMS text service
                        Gender = model.Gender,
                        IsActive = true, // set default to true
                        AddressId = null, // set address to null, user will supply this later
                        LastLoginDate = DateTime.Now.AddHours(2)
                    };

                    // add and save the new profile

                    try
                    {
                        db.UserProfiles.Add(newProfile);
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException ex)
                    {
                        var sb = new StringBuilder();
                        foreach (var failure in ex.EntityValidationErrors)
                        {
                            sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                            foreach (var error in failure.ValidationErrors)
                            {
                                sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                                sb.AppendLine();
                            }
                        }
                        throw new DbEntityValidationException(
                            "Entity Validation Failed - errors follow:\n" +
                            sb.ToString(), ex
                        );
                    }
                    

                    // set authorization cookie
                    // set createPersistentCookie to false, user can overwrite when the login (using remember me checkbox)
                    FormsAuthentication.SetAuthCookie(model.Username, true /* createPersistentCookie */);

                    _logger.Info(model.Username + "registered successfully");

                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    // send an email to the new user who just signed up
                    string subject = "Thank you for registering!";
                    string body = null;

                    body += "<p>Thank you for registering with Yard Lad!<br />You can now login with your account ";
                    body += "<a href=\"http://yardlad.com/Account/MyAccount\">here</a>.</p>";

                    var yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                    // send email to the user (will need to replace the emails when deploying)
                    try
                    {
                        emailsender.SendEmail(subject, body, from: yardladEmail, recipient: model.Email);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("there was an error sending a welcome email to the new user", ex);
                    }

                    string acceptSMS = model.AcceptSMS ? "yes" : "no";

                    // send an email to the admin notifying them about a new contractor registration
                    subject = "New User Registered";
                    body = string.Format("<p>New user registered!</p>" + "<p>Username: {0}<br />Email: {1}<br />"
                        + "Name: {2} {3}<br>Phone: {4}<br>Mobile: {5}<br>Date of Birth: {6:d}<br>Gender: {7}<br>Accepted SMS Text Service: {8}</p>",
                        model.Username, model.Email, model.FirstName, model.LastName,
                        model.Phone, model.Mobile, model.DateOfBirth, model.Gender, acceptSMS);

                    try
                    {
                        // send email (will need to replace the emails when deploying)
                        emailsender.SendEmail(subject, body, from: yardladEmail, recipient: yardladEmail);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("new user registration email to the admin was NOT sent successfully", ex);
                    }

                    // if the user has selected a contractor (part of the request service flow path)
                    if (Session["SelectContractorViewModel"] != null)
                    {
                        // the user needs to add their address
                        // send them to the address view and have them enter their address
                        TempData["createAddressBeforePay"] = true;
                        return RedirectToAction("Create", "Address", new { id = user.id, returnUrl = "PayPal/ValidateCommand" });
                    }

                    if (Session["RequestServiceViewModel"] != null)
                    {
                        var requestModel = (RequestServiceViewModel)Session["RequestServiceViewModel"];
                        if (requestModel.UserAddressId == 0)
                        {
                            // the user has no address associated with their profile
                            if (requestModel.CustomAddress != null)
                            {
                                // they entered an address on the service page
                                // make a new address and associate it with their profile
                                Address address = new Address()
                                {
                                    Line1 = requestModel.CustomAddress.Line1,
                                    Line2 = requestModel.CustomAddress.Line2,
                                    City = requestModel.CustomAddress.City,
                                    StateId = requestModel.CustomAddress.StateId,
                                    PostalCode = requestModel.CustomAddress.PostalCode,
                                    IsActive = true // default to active
                                };

                                db.Addresses.Add(address);

                                var newUser = Membership.GetUser(model.Username);
                                var userId = (int)newUser.ProviderUserKey;
                                var userProfile = db.UserProfiles.Where(up => up.UserId == userId).SingleOrDefault();

                                userProfile.AddressId = address.AddressId;
                                db.Entry(userProfile).State = EntityState.Modified;
                                db.SaveChanges();

                                TempData["model"] = requestModel;
                                return RedirectToAction("ValidateCommand", "PayPal");
                            }
                        }
                    }

                    // else return them to their account overview
                    return RedirectToAction("MyAccount", "Account");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    // get the currently logged in user, update that they are online (userIsOnline)
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);

                    // this method returns a bool as to whether the change was successful or not
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception) // if there are any exceptions/errors, set the success variable to false regardless
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    // send an email to the account holder notifying them that their password has been changed
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);

                    IEmailSender emailsender = new GoDaddyEmailSender(); // send the emails via GoDaddy (implementing IEmailSender interface)

                    // send an email to the user notifying them of success on their password change
                    string subject = "Password Change Success!";
                    string body = null;

                    body += "<p>Your password has successfully be changed!<br />If you did not make this request, please contact a Yard Lad admin.</p>";

                    var yardladEmail = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email").Single().Value;

                    // send email to the user (will need to replace the emails when deploying)
                    try
                    {
                        emailsender.SendEmail(subject, body, from: yardladEmail, recipient: currentUser.Email);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("there was an error sending the change password success email to the user: " + currentUser.UserName, ex);
                    }

                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        [Authorize]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        //
        // GET: /Account/MyAccount

        [Authorize]
        public ActionResult MyAccount(string access)
        {
            MyAccountViewModel model = new MyAccountViewModel()
            {
                // look up the currently logged in user, get their info
                UserId = (int)Membership.GetUser().ProviderUserKey,
                Username = Membership.GetUser().UserName,
                Email = Membership.GetUser().Email,
                CreationDate = Membership.GetUser().CreationDate.ToLocalTime(),
                AccountType = "standard account"
            };

            // see if the user has made any payments
            if (db.RequestedServices.Where(rs => rs.UserId == model.UserId).Count() != 0)
            {
                // the user has requested a service
                ViewBag.HasMadePayment = true;
            }

            // if the user is in an admin role
            if (User.IsInRole("admin"))
            {
                model.AccountType = "admin account";
            }

            // if the user is in a contractor role
            if (User.IsInRole("contractor") || User.IsInRole("contractor2"))
            {
                model.AccountType = "contractor account";

                // look up their contractor id
                model.ContractorId = db.ContractorUsers.Where(cu => cu.UserId == model.UserId).Single().ContractorId;
                var contractor = db.Contractors.Where(c => c.ContractorId == model.ContractorId).Single(); // used to lookup the service area

                model.ServiceArea = db.ServiceAreas.Where(sa => sa.ServiceAreaId == contractor.ServiceAreaId).SingleOrDefault();
                model.QualificationId = db.Contractors.Where(c => c.ContractorId == model.ContractorId).Single().QualificationId;
                model.IsQualified = model.QualificationId == null ? false : true; // ternary operator, indicates whether the contractor is qualified or not
                model.ContractorPayments = contractor.ContractorPayments.ToList(); // a list of all the contractors payments
                model.TaxRate = db.Taxes.Where(t => t.ContractorId == contractor.ContractorId).Single().Rate.ToString();
                model.AccountUsers = db.ContractorUsers.Where(cu => cu.ContractorId == contractor.ContractorId).ToList();
                model.Contractor = contractor;
                model.ContractorAvailability = contractor.ContractorAvailabilities.Where(ca => ca.ContractorId == contractor.ContractorId).SingleOrDefault();

                var user = Membership.GetUser();
                var userId = (int)user.ProviderUserKey;
                var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

                ViewBag.NewServices = db.Services.Where(s => s.DateCreated > userProfile.LastLoginDate).Count();
                ViewBag.NewServiceOptions = db.ServiceOptions.Where(so => so.DateCreated > userProfile.LastLoginDate).Count();
                ViewBag.NewServiceOptionItems = db.ServiceOptionItems.Where(soi => soi.DateCreated > userProfile.LastLoginDate).Count();
                if (db.Contractors.Where(c => c.ContractorId == contractor.ContractorId).Where(c => c.ApprovedOn > userProfile.LastLoginDate).Count() != 0)
                {
                    ViewBag.QualificationsApproved = true;
                }
                ViewBag.NewServiceRequests = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Where(rs => rs.DateRequestMade > userProfile.LastLoginDate).Count();

                // dashboard statistics
                ViewBag.NumberOfServices = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Count();
                ViewBag.NumberOfRatings = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Where(r => r.RatingId != null).Count();
                ViewBag.NumberOfServicesOffered = db.ContractorServices.Where(cs => cs.ContractorId == contractor.ContractorId).Count();

                if (ViewBag.NumberOfRatings != 0)
                {
                    ViewBag.HighestRating = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Where(rs => rs.RatingId != null).Where(rs => rs.Rating.IsActive == true).OrderByDescending(rs => rs.Rating.Value).Take(1).Select(x => x.Rating.Value).SingleOrDefault();
                    ViewBag.LowestRating = db.RequestedServices.Where(rs => rs.ContractorId == contractor.ContractorId).Where(rs => rs.RatingId != null).Where(rs => rs.Rating.IsActive == true).OrderBy(rs => rs.Rating.Value).Take(1).Select(x => x.Rating.Value).SingleOrDefault();
                }

                if (contractor.Payees.Count >= 1)
                {
                    model.HasSubmittedPayeeInfo = true;
                    model.DepositVia = db.Payees.Where(p => p.ContractorId == contractor.ContractorId).Single().DepositVia;
                }

                if (contractor.IsActive == true)
                {
                    model.IsListed = true;
                }
                else
                {
                    model.IsListed = false;
                }

                ViewBag.ContractorServiceAreas = db.ContractorServiceAreas.Where(csa => csa.ContractorId == model.ContractorId).OrderBy(csa => csa.ServiceArea.State.Name)
                    .ThenBy(csa => csa.ServiceArea.Name).ToList();

                // if the user is a contractor and they currently aren't listed, show them a checklist page each time they view their account
                if (model.ContractorId != 0 || model.ContractorId != null)
                {
                    //var contractor = db.Contractors.Where(c => c.ContractorId == model.ContractorId).Single();
                    if (contractor.IsActive == false && access == null) // they are not listed
                    {
                        return RedirectToAction("Checklist", "Contractor", new { id = model.ContractorId });
                    }
                }
            }

            return View(model);
        }

        [Authorize]
        public ActionResult History()
        {
            // get the currently logged in user
            int userId = (int)Membership.GetUser().ProviderUserKey;
            var currentUser = db.my_aspnet_users.Where(u => u.id == userId).Single();

            AccountHistoryViewModel model = new AccountHistoryViewModel()
            {
                UserId = userId,
                User = currentUser,
                IsContractor = false,
                RequestedServices = db.RequestedServices.Where(rs => rs.UserId == userId).ToList(), // requested services tied to the currently logged in user's id
                ContractorUsers = new List<ContractorUser>(),
            };

            foreach (var requestedService in model.RequestedServices)
            {
                foreach (var contractorUser in requestedService.Contractor.ContractorUsers)
                {
                    model.ContractorUsers.Add(contractorUser);
                }
            }

            // ternary operator, set to true if the user is in the contractor role
            model.IsContractor = true ? User.IsInRole("contractor") : false;

            // remove any inactive requests (typically a result of user backing out of purchase)
            foreach (var requestedService in db.RequestedServices.Where(rs => rs.UserId == userId).Where(rs => rs.IsActive == false).ToList())
            {
                // remove option items first
                foreach (var requestedServiceOptionItem in db.RequestedServiceOptionItems
                    .Where(rsoi => rsoi.RequestedServiceOption.RequestedServiceId == requestedService.RequestedServiceId).ToList())
                {
                    db.RequestedServiceOptionItems.Remove(requestedServiceOptionItem);
                }

                // remove options next
                foreach (var requestedServiceOption in db.RequestedServiceOptions
                    .Where(rso => rso.RequestedServiceId == requestedService.RequestedServiceId).ToList())
                {
                    db.RequestedServiceOptions.Remove(requestedServiceOption);
                }

                // finally, remove the inactive requested service object
                db.RequestedServices.Remove(requestedService);

            }

            // and save the changes
            db.SaveChanges();

            if (TempData["RequestedServiceId"] != null)
            {
                ViewBag.RequestedServiceId = (int)TempData["RequestedServiceId"];
            }

            if (TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"];
            }

            // REMOVE THESE AFTER TESTING
            //ViewBag.Message = "Thanks for rating! Would you like to take an anonymous survey about using Yard Lad Services? Your feedback is appreciated.";
            //ViewBag.RequestedServiceId = 1;

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
