﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{ 
    [Authorize(Roles = "admin")]
    public class ContentController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Content/

        public ViewResult Index()
        {
            ContentListViewModel model = new ContentListViewModel()
            {
                // content in db ordered by section name
                ContentList = db.Contents.OrderBy(c => c.SectionName).ToList()
            };

            return View(model);
        }

        //
        // GET: /Content/Details/5

        public ViewResult Details(int id)
        {
            ContentDetailsViewModel model = new ContentDetailsViewModel()
            {
                ContentId = id,
                Content = db.Contents.Where(c => c.ContentId == id).Single()
            };

            return View(model);
        }
        
        //
        // GET: /Content/Edit/5
 
        public ActionResult Edit(int id, string returnUrl)
        {
            ContentEditViewModel model = new ContentEditViewModel()
            {
                ContentId = id,
                Content = db.Contents.Where(c => c.ContentId == id).Single(),
                ReturnUrl = returnUrl
            };

            return View(model);
        }

        //
        // POST: /Content/Edit/5

        [HttpPost]
        public ActionResult Edit(ContentEditViewModel model, HttpPostedFileBase uploadedImage)
        {
            if (ModelState.IsValid)
            {
                // if the section to edit is the featured yard, and and an image hasn't been uploaded before
                if (model.Content.SectionName == "Featured Yard" && model.Content.ImageId == 1) // (no image id == 1)
                {
                    var filename = Path.GetFileName(uploadedImage.FileName); // get the name of the file
                    string extension = Path.GetExtension(uploadedImage.FileName).ToLower(); // get the extension (in lowercase)

                    // check to make sure the extension can be accepted as an image
                    if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".bmp")
                    {
                        ModelState.AddModelError("", "the image must be in the right format");
                        return View(model);
                    }

                    Guid photoName = Guid.NewGuid(); // make a new guid to uniquely identify the image on the server
                    //string photoName = "featuredyard";
                    filename = photoName + extension; // reattach the extension

                    //store the file inside ~/Content/Images/FeaturedYard
                    var path = Path.Combine(Server.MapPath("~/Content/Images/FeaturedYard"), filename); // assign path
                    uploadedImage.SaveAs(path); // save to the server

                    // if there was no image associated or the image id is 1 (no image)
                    if (model.Content.Image == null || model.Content.ImageId == 1)
                    {
                        // add a new Image
                        Image newImage = new Image()
                        {
                            Source = "~/Content/Images/FeaturedYard/" + filename,
                            Name = photoName.ToString(),
                            MimeType = extension,
                            IsActive = true
                        };

                        // save the image
                        db.Images.Add(newImage);
                        db.SaveChanges();

                        // associate the image with the content
                        // look up the newly added image
                        Image contentImage = db.Images.Where(i => i.Source == newImage.Source).Single();
                        model.Content.ImageId = contentImage.ImageId;
                        model.Content.Image = contentImage;
                    }
                }
                else if (uploadedImage != null) // the user uploaded a new image 
                {
                    // look up the previous image path
                    Image prevImage = (from c in db.Contents
                                    where c.ContentId == model.Content.ContentId
                                    select c.Image).Single();

                    // if the image path is null (double check..)
                    if (prevImage == null)
                    {
                        // the id should already be set to 1 (no image)
                        model.Content.ImageId = 1;
                        // set the image path to the default (no image)
                    }
                    else
                    {
                        // if it isn't null, set the new image path (by deleting old image and adding new one)

                        // try to remove the current image from the server and upload the new one in its place
                        string oldImagePath = Server.MapPath(prevImage.Source);
                        FileInfo fi = new FileInfo(oldImagePath);
                        if (fi.Name == (prevImage.Name + prevImage.MimeType)) // make sure the Guid matches
                        {
                            fi.Delete();
                        }

                        // add the new image
                        var filename = Path.GetFileName(uploadedImage.FileName); // get the name of the file
                        string extension = Path.GetExtension(uploadedImage.FileName).ToLower(); // get the extension (in lowercase)

                        // check to make sure the extension can be accepted as an image
                        if (extension != ".jpg" && extension != ".jpeg" && extension != ".png" && extension != ".gif" && extension != ".bmp")
                        {
                            ModelState.AddModelError("", "the image must be in the right format");
                            return View(model);
                        }

                        Guid photoName = Guid.NewGuid(); // make a new guid to uniquely identify the image on the server
                        filename = photoName + extension; // reattach the extension

                        //store the file inside ~/Content/Images/FeaturedYard
                        var path = Path.Combine(Server.MapPath("~/Content/Images/FeaturedYard"), filename); // assign path
                        uploadedImage.SaveAs(path); // save to the server

                        // lookup image to edit
                        Image imageEdit = db.Images.Where(i => i.ImageId == model.Content.ImageId).Single();
                        imageEdit.Source = "~/Content/Images/FeaturedYard/" + filename;
                        imageEdit.MimeType = extension;
                        imageEdit.Name = photoName.ToString();
                        imageEdit.IsActive = true;

                        db.Entry(imageEdit).State = EntityState.Modified; // update the record
                    }
                }

                db.Entry(model.Content).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect("~/" + model.ReturnUrl);
            }

            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}