﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    [Authorize(Roles = "admin")]
    public class ServiceOptionController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /ServiceOption/

        public ViewResult Index(int id)
        {
            // service options are for a particular service, the service id will need to be passed to the controller here
            // look up the service options under that particular service
            var serviceOptions = db.ServiceOptions.Where(so => so.ServiceId == id).Include(s => s.ServiceOptionItems).Include(s => s.Service);
            ViewBag.Service = db.Services.Where(s => s.ServiceId == id).Single(); // used to display any necessary service info (ex. Name)
            return View(serviceOptions.ToList());
        }

        //
        // GET: /ServiceOption/Details/5

        public ViewResult Details(int id)
        {
            ServiceOption serviceoption = db.ServiceOptions.Find(id);
            ViewBag.ServiceOptionItems = db.ServiceOptionItems.ToList();
            return View(serviceoption);
        }

        //
        // GET: /ServiceOption/Create

        public ActionResult Create(int id)
        {
            ServiceOptionCreateViewModel model = new ServiceOptionCreateViewModel();
            model.ServiceId = id;
            model.Service = db.Services.Where(s => s.ServiceId == id).Single();
            model.ServiceOptions = new List<ServiceOption>();
            
            return View(model);
        }

        //
        // POST: /ServiceOption/Create

        [HttpPost]
        public ActionResult Create(ServiceOptionCreateViewModel model)
        {
            foreach (var serviceOption in model.ServiceOptions)
            {
                if (serviceOption.Name == null)
                {
                    ModelState.AddModelError("", "each service option must have a name");
                    break;
                }
            }

            // if the service option already has an option of the same name, throw an error
            var currentServiceOptions = db.ServiceOptions.Where(so => so.ServiceId == model.ServiceId);

            foreach (var serviceOption in currentServiceOptions)
            {
                foreach (var so in model.ServiceOptions.Where(so => so.Name != null))
                {
                    if (serviceOption.Name.ToLower() == so.Name.ToLower())
                    {
                        ModelState.AddModelError("", "this option is already associated with this service");
                    }
                }
            }

            if (ModelState.IsValid)
            {
                var currentOptions = db.ServiceOptions.Where(so => so.ServiceId == model.ServiceId);

                // the user has already inserted service options
                if (currentOptions.Count() >= 1)
                {
                    var currentCount = currentOptions.Count();
                    for (int i = 1; i <= model.ServiceOptions.Count; i++)
                    {
                        var serviceOption = new ServiceOption()
                        {
                            IsActive = true,
                            Name = model.ServiceOptions[i - 1].Name,
                            Order = currentCount + i,
                            ServiceId = model.ServiceId,
                            DateCreated = DateTime.Now.AddHours(2)
                        };

                        db.ServiceOptions.Add(serviceOption);
                    }
                }

                // the user is inserting service options for the first time
                if (currentOptions.Count() == 0)
	            {
                    for (int i = 1; i <= model.ServiceOptions.Count; i++)
                    {
                        var serviceOption = new ServiceOption()
                        {
                            IsActive = true,
                            Name = model.ServiceOptions[i - 1].Name,
                            Order = i,
                            ServiceId = model.ServiceId,
                            DateCreated = DateTime.Now.AddHours(2)
                        };

                        db.ServiceOptions.Add(serviceOption);
                    }
                }

                db.SaveChanges();
                return RedirectToAction("Index", new { id = model.ServiceId });
            }

            model.Service = db.Services.Where(s => s.ServiceId == model.ServiceId).Single();
            return View(model);
        }

        //
        // GET: /ServiceOption/Edit/5

        public ActionResult Edit(int id)
        {
            ServiceOption serviceoption = db.ServiceOptions.Find(id);
            return View(serviceoption);
        }

        //
        // POST: /ServiceOption/Edit/5

        [HttpPost]
        public ActionResult Edit(ServiceOption model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { id = model.ServiceId });
            }

            return View(model);
        }

        //
        // GET: /ServiceOption/Delete/5

        public ActionResult Delete(int id)
        {
            ServiceOption serviceoption = db.ServiceOptions.Find(id);
            return View(serviceoption);
        }

        //
        // POST: /ServiceOption/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            // remove the service option
            ServiceOption serviceOption = db.ServiceOptions.Find(id);
            db.ServiceOptions.Remove(serviceOption);

            //remove all the service option items
            foreach (var serviceOptionItem in db.ServiceOptionItems.Where(soi => soi.ServiceOptionId == id))
            {
                db.ServiceOptionItems.Remove(serviceOptionItem);
            }

            db.SaveChanges();
            return RedirectToAction("Index", new { id = serviceOption.ServiceId });
        }

        public ActionResult Requests()
        {
            List<ServiceOptionRequest> requests = db.ServiceOptionRequests.ToList();
            return View(requests);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}