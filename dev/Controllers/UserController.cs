﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    [Authorize]
    public class UserController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /User/

        public ViewResult Index()
        {
            var users = db.my_aspnet_users.Include(m => m.UserProfile).OrderBy(u => u.name);
            return View(users.ToList());
        }

        //
        // GET: /User/Details/5

        public ViewResult Details(int id, string returnUrl)
        {
            my_aspnet_users my_aspnet_users = db.my_aspnet_users.Find(id);

            ViewBag.ReturnUrl = returnUrl;

            return View(my_aspnet_users);
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.UserProfiles, "UserId", "FirstName");
            return View();
        } 

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(my_aspnet_users my_aspnet_users)
        {
            if (ModelState.IsValid)
            {
                db.my_aspnet_users.Add(my_aspnet_users);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.id = new SelectList(db.UserProfiles, "UserId", "FirstName", my_aspnet_users.id);
            return View(my_aspnet_users);
        }
        
        //
        // GET: /User/Edit/5
 
        public ActionResult Edit(int id)
        {
            my_aspnet_users my_aspnet_users = db.my_aspnet_users.Find(id);
            ViewBag.id = new SelectList(db.UserProfiles, "UserId", "FirstName", my_aspnet_users.id);
            return View(my_aspnet_users);
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(my_aspnet_users my_aspnet_users)
        {
            if (ModelState.IsValid)
            {
                db.Entry(my_aspnet_users).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.UserProfiles, "UserId", "FirstName", my_aspnet_users.id);
            return View(my_aspnet_users);
        }

        //
        // GET: /User/Delete/5
 
        public ActionResult Delete(int id)
        {
            my_aspnet_users my_aspnet_users = db.my_aspnet_users.Find(id);
            return View(my_aspnet_users);
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            my_aspnet_users my_aspnet_users = db.my_aspnet_users.Find(id);
            db.my_aspnet_users.Remove(my_aspnet_users);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}