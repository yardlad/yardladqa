﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Controllers
{
    public class ContractorServiceController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /ContractorService/

        [Authorize]
        public ActionResult Index(int id = 0)
        {
            var user = Membership.GetUser();
            var userId = (int)user.ProviderUserKey;
            var userProfile = db.UserProfiles.Where(up => up.UserId == userId).Single();

            if (id == 0)
            {
                // the user used a nav link instead of the link in my account, will have to look up their contractor id
                userId = (int)Membership.GetUser().ProviderUserKey;
                var contractorId = db.ContractorUsers.Where(cu => cu.UserId == userId).Single().ContractorId;
                id = contractorId;
            }

            ContractorServiceListViewModel model = new ContractorServiceListViewModel()
            {
                Contractor = db.Contractors.Where(c => c.ContractorId == id).Single(),
                Services = db.ContractorServices.Where(cs => cs.ContractorId == id).OrderBy(cs => cs.Service.ServiceCategory.Name).ThenBy(cs => cs.Service.Name).ToList()
            };

            ViewBag.LastLoggedIn = userProfile.LastLoginDate;
            return View(model);
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            ContractorService contractorService = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();

            return View(contractorService);
        }

        [Authorize]
        public ActionResult Create(int id)
        {
            ContractorServiceCreateViewModel model = new ContractorServiceCreateViewModel()
            {
                Contractor = db.Contractors.Where(c => c.ContractorId == id).Single(),
                ServiceId = 0
            };

            var services = db.Services.Where(s => s.IsActive == true).ToList();
            HashSet<int> serviceIds = new HashSet<int>(db.ContractorServices.Where(cs => cs.ContractorId == model.Contractor.ContractorId).Select(x => x.ServiceId).ToList());
            services.RemoveAll(x => serviceIds.Contains(x.ServiceId));

            ViewBag.ServiceId = new SelectList(services, "ServiceId", "Name");

            if (services.Count == 0)
            {
                ViewBag.AddedAll = true;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ContractorServiceCreateViewModel model)
        {
            // make sure the user selected a service to add
            if (model.ServiceId == 0)
            {
                ModelState.AddModelError("", "you must select a service to add");
                ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", model.ServiceId);
                return View(model);
            }

            // look through all the contractor services under the current contractor
            foreach (var contractorService in db.ContractorServices.Where(cs => cs.ContractorId == model.Contractor.ContractorId))
            {
                // prevent them from adding the same service
                if (contractorService.ServiceId == model.ServiceId)
                {
                    ModelState.AddModelError("", "you have already added this service, please select another");
                    ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "Name", model.ServiceId);
                    return View(model);
                }
            }

            if (ModelState.IsValid)
            {
                // look up the (admin) service
                var adminService = db.Services.Where(s => s.ServiceId == model.ServiceId).Single();

                // add a new contractor service
                ContractorService contractorService = new ContractorService()
                {
                    ServiceId = adminService.ServiceId,
                    ContractorId = model.Contractor.ContractorId,
                    BasePrice = adminService.BasePrice, // copy value from admin (can be changed later)
                    IsActive = true,
                };

                // add and save the new contractor service to db
                db.ContractorServices.Add(contractorService);
                db.SaveChanges();

                // add the contractor service options
                foreach (var serviceOption in db.ServiceOptions
                    .Where(so => so.ServiceId == adminService.ServiceId).ToList())
                {
                    ContractorServiceOption contractorServiceOption = new ContractorServiceOption()
                    {
                        ContractorServiceId = contractorService.ContractorServiceId,
                        Order = serviceOption.Order,
                        Name = serviceOption.Name,
                        IsActive = true // set default to true
                    };

                    db.ContractorServiceOptions.Add(contractorServiceOption);
                    db.SaveChanges();

                    // add the contractor service option items
                    foreach (var serviceOptionItem in db.ServiceOptionItems
                        .Where(soi => soi.ServiceOption.ServiceOptionId == serviceOption.ServiceOptionId).ToList())
                    {
                        ContractorServiceOptionItem contractorServiceOptionItem = new ContractorServiceOptionItem()
                        {
                            ContractorServiceOptionId = contractorServiceOption.ContractorServiceOptionId,
                            Order = serviceOptionItem.Order,
                            Name = serviceOptionItem.Name,
                            Price = serviceOptionItem.Price,
                            IsActive = serviceOptionItem.IsActive
                        };

                        db.ContractorServiceOptionItems.Add(contractorServiceOptionItem);
                        db.SaveChanges();
                    }
                }

                db.SaveChanges(); // save all changes
            }

            return RedirectToAction("Index", "ContractorService", new { id = model.Contractor.ContractorId });
        }

        [Authorize]
        public ActionResult Edit(int id)
        {
            ContractorService contractorService = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();
            return View(contractorService);
        }

        [HttpPost]
        public ActionResult Edit(ContractorService service)
        {
            if (ModelState.IsValid)
            {
                db.Entry(service).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index", new { id = service.ContractorId });
            }

            return View(service);
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            var contractorServiceToDelete = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();
            return View(contractorServiceToDelete);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var contractorServiceToDelete = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();
            var contractorId = contractorServiceToDelete.ContractorId;

            // remove the contractor service option items
            foreach (var contractorServiceOptionItem in db.ContractorServiceOptionItems
                .Where(csoi => csoi.ContractorServiceOption.ContractorServiceId == contractorServiceToDelete.ContractorServiceId))
            {
                db.ContractorServiceOptionItems.Remove(contractorServiceOptionItem);
            }

            // remove the contractor service options
            foreach (var contractorServiceOption in db.ContractorServiceOptions
                .Where(cso => cso.ContractorServiceId == contractorServiceToDelete.ContractorServiceId))
            {
                db.ContractorServiceOptions.Remove(contractorServiceOption);
            }

            // remove the contractor service
            db.ContractorServices.Remove(contractorServiceToDelete);

            db.SaveChanges(); // save all changes

            return RedirectToAction("Index", "ContractorService", new { id = contractorId });
        }

        [Authorize]
        public ActionResult Activate(int id)
        {
            var contractorService = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();
            contractorService.IsActive = true;

            db.SaveChanges();

            return RedirectToAction("Index", new { id = id });
        }

        [Authorize]
        public ActionResult Deactivate(int id)
        {
            var contractorService = db.ContractorServices.Where(cs => cs.ContractorServiceId == id).Single();
            contractorService.IsActive = false;

            db.SaveChanges();

            return RedirectToAction("Index", new { id = id });
        }

        public JsonResult GetServiceDescription(int serviceId)
        {
            var description = db.Services.Where(s => s.ServiceId == serviceId).Single().Description;
            return this.Json(description, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
