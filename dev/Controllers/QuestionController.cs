﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YardLad.Models.Domain;

namespace YardLad.Controllers
{ 
    public class QuestionController : Controller
    {
        private YardLadEntities db = new YardLadEntities();

        //
        // GET: /Question/

        public ViewResult Index()
        {
            var questions = db.Questions.Include(q => q.QuestionType);
            return View(questions.ToList());
        }

        //
        // GET: /Question/Create

        public ActionResult Create()
        {
            ViewBag.QuestionTypeId = new SelectList(db.QuestionTypes.OrderBy(qt => qt.Name), "QuestionTypeId", "Name");
            return View();
        } 

        //
        // POST: /Question/Create

        [HttpPost]
        public ActionResult Create(Question question)
        {
            if (ModelState.IsValid)
            {
                question.IsActive = false; // default to inactive, activate once populated with answers
                db.Questions.Add(question);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.QuestionTypeId = new SelectList(db.QuestionTypes.OrderBy(qt => qt.Name), "QuestionTypeId", "Name", question.QuestionTypeId);
            return View(question);
        }
        
        //
        // GET: /Question/Edit/5
 
        public ActionResult Edit(int id)
        {
            Question question = db.Questions.Find(id);
            ViewBag.QuestionTypeId = new SelectList(db.QuestionTypes, "QuestionTypeId", "Name", question.QuestionTypeId);
            return View(question);
        }

        //
        // POST: /Question/Edit/5

        [HttpPost]
        public ActionResult Edit(Question question)
        {
            if (ModelState.IsValid)
            {
                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuestionTypeId = new SelectList(db.QuestionTypes, "QuestionTypeId", "Name", question.QuestionTypeId);
            return View(question);
        }

        //
        // GET: /Question/Delete/5
 
        public ActionResult Delete(int id)
        {
            Question question = db.Questions.Find(id);
            return View(question);
        }

        //
        // POST: /Question/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Question question = db.Questions.Find(id);
            question.IsActive = false;
            db.Entry(question).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}