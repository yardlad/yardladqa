﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddRequestedService(RequestedService requestedService, int quantity)
        {
            CartLine line = lineCollection
                .Where(lc => lc.RequestedService.ContractorServiceId == requestedService.ContractorServiceId)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine { RequestedService = requestedService, Quantity = quantity });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveRequestedService(RequestedService requestedService)
        {
            lineCollection.RemoveAll(l => l.RequestedService.ContractorServiceId == requestedService.ContractorServiceId);
        }

        public decimal ComputeTotalValue()
        {
            // FIX THIS LATER - CALCULATE USING SUBTOTAL, TAX, AND TOTAL FROM SUBMIT REQUEST PAGE
            return lineCollection.Sum(e => e.RequestedService.ContractorService.BasePrice * e.Quantity);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }

    public class CartLine
    {
        // REVIEW THESE PROPERTIES - MAY NEED TO CHANGE
        public RequestedService RequestedService { get; set; }
        public List<ContractorServiceOption> SelectedServiceOptions { get; set; }
        public List<ContractorServiceOptionItem> SelectedOptionItems { get; set; }
        public decimal Subtotal { get; set; }
        public decimal Tax { get; set; }
        public decimal Total { get; set; }
        public int Quantity { get; set; }
    }
}