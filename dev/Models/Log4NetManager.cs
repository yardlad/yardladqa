﻿using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.EntityClient;
using System.IO;
using System.Linq;
using System.Web;

namespace YardLad.Web.Models
{
    public static class Log4NetManager
    {
        public static void InitializeLog4Net()
        {
            //initialize the log4net configuration based on the log4net.config file
            XmlConfigurator.ConfigureAndWatch(new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + @"\Config\Log4Net.config"));

            Hierarchy hier = log4net.LogManager.GetRepository() as Hierarchy;
            if (hier != null)
            {
                // Get ADONetAppender by name
                AdoNetAppender adoAppender = (from appender in hier.GetAppenders()
                                              where appender.Name.Equals("ADONetAppender", StringComparison.InvariantCultureIgnoreCase)
                                              select appender).FirstOrDefault() as AdoNetAppender;

                // Change only when the auto setting is set
                if (adoAppender != null && adoAppender.ConnectionString.Contains("{auto}"))
                {
                    adoAppender.ConnectionString = ExtractConnectionStringFromEntityConnectionString(
                            GetEntitiyConnectionStringFromWebConfig());

                    //refresh settings of appender
                    adoAppender.ActivateOptions();
                }
            }
        }

        private static string GetEntitiyConnectionStringFromWebConfig()
        {
            return ConfigurationManager.ConnectionStrings["MySQLConnection"].ConnectionString;
        }

        private static string ExtractConnectionStringFromEntityConnectionString(string entityConnectionString)
        {
            // create a entity connection string from the input
            EntityConnectionStringBuilder entityBuilder = new EntityConnectionStringBuilder(entityConnectionString);

            // read the db connection string
            return entityBuilder.ProviderConnectionString;
        }
    }
}