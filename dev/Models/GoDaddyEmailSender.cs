﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using YardLad.Models.Interfaces;
using YardLad.Models.Domain;

namespace YardLad.Models
{
    public class GoDaddyEmailSender : IEmailSender
    {
        private YardLadEntities db = new YardLadEntities();

        public void SendEmail(string subject, string body, string from, string recipient)
        {
            var username = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email (login)").SingleOrDefault().Value;
            var password = db.SiteSettings.Where(ss => ss.Name == "Yard Lad Contact Email (password)").SingleOrDefault().Value;

            if (username == null || username == "" || username.ToLower() == "not set" || username.ToLower() == "default")
            {
                username = "ksexton@technopole.co";
            }

            if (password == null || password == "" || password.ToLower() == "not set" || password.ToLower() == "default")
            {
                password = "DeadEnd012010";
            }

            SmtpClient client = new SmtpClient()
            {
                Host = "relay-hosting.secureserver.net",
                Port = 25,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(username, password)
            };

            MailMessage message = new MailMessage(from, recipient, subject, body);
            message.IsBodyHtml = true;
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            message.From = new MailAddress(from, "Yard Lad");
            client.Send(message);
        }
    }
}