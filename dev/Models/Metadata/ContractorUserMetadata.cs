﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContractorUserMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Contractor User")]
        public int ContractorUserId { get; set; }

        [Display(Name = "Contractor")]
        public int ContractorId { get; set; }

        [Display(Name = "User")]
        public int UserId { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date Created")]
        public DateTime DateCreated { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContractorUserMetadata))]
    public partial class ContractorUser
    {
        
    }
}