﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContractorServiceOptionMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Contractor Service Option Id")]
        public int ContractorServiceOptionId { get; set; }

        [Required(ErrorMessage = "please select a contractor service")]
        [Display(Name = "Contractor Service")]
        public int ContractorServiceId { get; set; }

        public Nullable<int> Order { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContractorServiceOptionMetadata))]
    public partial class ContractorServiceOption
    {

    }
}