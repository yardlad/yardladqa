﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContractorServiceMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Contractor Service Id")]
        public int ContractorServiceId { get; set; }

        [Required(ErrorMessage = "please select a contractor")]
        [Display(Name = "Contractor")]
        public int ContractorId { get; set; }

        [Required(ErrorMessage = "please select a service")]
        [Display(Name = "Service")]
        public int ServiceId { get; set; }

        [Required(ErrorMessage = "please enter a base price")]
        [Display(Name = "Base Price")]
        public decimal BasePrice { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContractorServiceMetadata))]
    public partial class ContractorService
    {
        
    }
}