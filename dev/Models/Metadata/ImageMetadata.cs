﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ImageMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Image Id")]
        public int ImageId { get; set; }

        [Required(ErrorMessage = "please enter an image source")]
        public string Source { get; set; }

        public string Name { get; set; }

        public string MimeType { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ImageMetadata))]
    public partial class Image
    {

    }

    
}