﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class StateMetadata
    {
        [Display(Name = "State Id")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        public string Abbreviation { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(StateMetadata))]
    public partial class State
    {
        
    }
}