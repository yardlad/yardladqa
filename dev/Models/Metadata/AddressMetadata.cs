﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class AddressMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Address Id")]
        public int AddressId { get; set; }

        [Required(ErrorMessage = "please enter an address")]
        [Display(Name = "Line 1")]
        public string Line1 { get; set; }

        [Display(Name = "Line 2")]
        public string Line2 { get; set; }

        [Required(ErrorMessage = "please enter a city")]
        public string City { get; set; }

        [Required(ErrorMessage = "please select a state")]
        [Display(Name = "State")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "please enter a zip code")]
        [Display(Name = "Zip Code")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(AddressMetadata))]
    public partial class Address
    {
        
    }
}