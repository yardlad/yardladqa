﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ServiceOptionItemMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Service Option Item Id")]
        public int ServiceOptionItemId { get; set; }

        [Display(Name = "Service Option Id")]
        public int ServiceOptionId { get; set; }

        public Nullable<int> Order { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:c}")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "please indicate an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ServiceOptionItemMetadata))]
    public partial class ServiceOptionItem
    {
        
    }
}