﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class SiteSettingMetadata
    {
        [Display(Name = "Site Setting Id")]
        public int SiteSettingId { get; set; }

        [Required(ErrorMessage = "please enter a name for the setting")]
        public string Name { get; set; }

        [Required(ErrorMessage = "you must enter a value for the setting")]
        public string Value { get; set; }
    }

    [MetadataType(typeof(SiteSettingMetadata))]
    public partial class SiteSetting
    {
        
    }
}