﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class RequestServiceViewModel
    {

        public State State { get; set; }

        public int StateId{get; set;}
        public string Name{get; set;}

        public Service Service { get; set; }
        public int ServiceId { get; set; }

        public ServiceArea ServiceArea { get; set; }
        public int ServiceArea { get; set; }
    }

    public class State
    { 
        
    }
}