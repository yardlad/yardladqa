﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YardLad.Models.View
{
    public class ChecklistViewModel
    {
        public int ContractorId { get; set; }
        public int QualificationId  { get; set; }
        public bool IsListed { get; set; }
        public bool HasPaid { get; set; }
        public bool IsQualified { get; set; }
        public bool HasSubmittedQualifications { get; set; }
        public bool HasSetPrimaryServiceArea { get; set; }
        public Nullable<bool> HasActiveSubscription { get; set; }
        public bool HasAddedAService { get; set; }
        public bool HasSetTax { get; set; }
        public bool HasSetAvailability { get; set; }
        public bool ReadyToBeListed { get; set; }
    }
}