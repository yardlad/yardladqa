﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;
using YardLad.Models;

namespace YardLad.Models.View
{
    public class ContractorServiceHistoryViewModel
    {
        public int ContractorId { get; set; }
        public Contractor Contractor { get; set; }
        public List<RequestedService> RequestedServices { get; set; }
        public List<IGrouping<DateTime, RequestedService>> GroupedRequestedServices { get; set; }
        public List<IGrouping<DateTime, RequestedService>> GroupedCompletedRequestedServices { get; set; }
        public int CompletedServices { get; set; }
        public int IncompleteServices { get; set; }
    }
}