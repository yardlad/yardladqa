﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class AddRatingViewModel
    {
        public int RequestedServiceId { get; set; }
        //public RequestedService RequestedService { get; set; }

        // rating members
        [Required(ErrorMessage = "please indicate the user rating the service")]
        [Display(Name = "User")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "please indicate the contractor that is being rated")]
        [Display(Name = "Contractor")]
        public int ContractorId { get; set; }

        [Required(ErrorMessage = "please select a rating")]
        public decimal Value { get; set; }

        [Required(ErrorMessage = "please insert your comments")]
        [StringLength(160, ErrorMessage = "comments cannot exceed 160 characters")]
        [UIHint("multilinetext")]
        public string Comments { get; set; }
        //public DateTime DateRated { get; set; }
    }
}