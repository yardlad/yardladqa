﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ServiceOptionRequestViewModel
    {
        public ServiceOptionRequest ServiceOption { get; set; }
        public List<ServiceOptionItemRequest> ServiceOptionItems { get; set; }
    }
}