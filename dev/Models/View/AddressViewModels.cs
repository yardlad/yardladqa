﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class AddressCreateViewModel
    {
        public Address Address { get; set; }
    }

    public class AddressEditViewModel
    {
        public Address Address { get; set; }
    }
}