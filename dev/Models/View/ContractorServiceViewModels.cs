﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ContractorServiceListViewModel
    {
        public Contractor Contractor { get; set; }
        public List<ContractorService> Services { get; set; }
    }

    public class ContractorServiceDetailsViewModel
    {
        public ContractorService ContractorService { get; set; }
    }

    public class ContractorServiceCreateViewModel
    {
        public Contractor Contractor { get; set; }
        public int ServiceId { get; set; } // the service the contractor user selects
    }
}