﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;
using YardLad.Models.View;

namespace YardLad.Models.View
{
    public class ContractorUserViewModel
    {
        public Contractor Contractor { get; set; }
        public RegisterViewModel UserInfo { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { get; set; }

        public int Page { get; set; }

        [Display(Name = "Accept Terms?")]
        [Required(ErrorMessage = "you must accept the terms before continuing")]
        public bool TermsAccepted { get; set; }
    }
}