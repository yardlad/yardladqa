﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ServiceOptionItemCreateViewModel
    {
        public int ServiceOptionId { get; set; }
        public ServiceOption ServiceOption { get; set; }
        public List<ServiceOptionItem> ServiceOptionItems { get; set; }
    }
}