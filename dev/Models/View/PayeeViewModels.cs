﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.View
{
    public class PayeeCreateViewModel
    {
        [Display(Name = "Contractor Id")]
        [HiddenInput(DisplayValue = false)]
        public int ContractorId { get; set; }

        [Display(Name = "Deposit via")]
        [Required(ErrorMessage = "please make a selection")]
        public string DepositVia { get; set; }

        // deposit via check
        [Display(Name = "Pay to")]
        [Required(ErrorMessage = "please enter the name the check should be written out to")]
        public string PayTo { get; set; }

        [Display(Name = "Line 1")]
        [Required(ErrorMessage = "please enter your address")]
        public string Line1 { get; set; }

        [Display(Name = "Line 2")]
        public string Line2 { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "please enter a city")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "please select a state")]
        public int StateId { get; set; }

        [Display(Name = "Zip code")]
        [Required(ErrorMessage = "please enter your zip code")]
        public string PostalCode { get; set; }

        [Display(Name = "Daytime phone")]
        public string DaytimePhone { get; set; }

        [Display(Name = "Evening phone")]
        public string EveningPhone { get; set; }

        public string Fax { get; set; }

        // deposit via EFT

        [Display(Name = "Account type")]
        [Required(ErrorMessage = "please select an account type")]
        public string AccountType { get; set; }

        [Display(Name = "Business or personal")]
        [Required(ErrorMessage = "please make a selection")]
        public string BusinessOrPersonal { get; set; }

        [Display(Name = "Name On account")]
        [Required(ErrorMessage = "please enter the name on the account")]
        public string NameOnAccount { get; set; }

        [Display(Name = "Account number")]
        [Required(ErrorMessage = "please enter your account number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account number confirm")]
        [System.Web.Mvc.Compare("AccountNumber", ErrorMessage = "the account numbers must match")]
        public string AccountNumberConfirm { get; set; }

        [Display(Name = "Routing number")]
        [Required(ErrorMessage = "please enter the routing number")]
        public string RoutingNumber { get; set; }
        
        // deposit via PayPal
        [Display(Name = "PayPal email")]
        [Required(ErrorMessage = "please enter your PayPal email address")]
        public string PayPalEmail { get; set; }

        [Display(Name = "PayPal email confirm")]
        [System.Web.Mvc.Compare("PayPalEmail", ErrorMessage = "the email addresses must match")]
        public string PayPalEmailConfirm { get; set; }
    }

    public class PayeeEditViewModel
    {
        [Display(Name = "Contractor Id")]
        [HiddenInput(DisplayValue = false)]
        public int PayeeId { get; set; }

        [Display(Name = "Contractor Id")]
        [HiddenInput(DisplayValue = false)]
        public int ContractorId { get; set; }

        [Display(Name = "Deposit via")]
        [Required(ErrorMessage = "please make a selection")]
        public string DepositVia { get; set; }

        // deposit via check
        [Display(Name = "Pay to")]
        [Required(ErrorMessage = "please enter the name the check should be written out to")]
        public string PayTo { get; set; }

        [Display(Name = "Address Id")]
        public int AddressId { get; set; }

        [Display(Name = "Line 1")]
        [Required(ErrorMessage = "please enter your address")]
        public string Line1 { get; set; }

        [Display(Name = "Line 2")]
        public string Line2 { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "please enter a city")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "please select a state")]
        public int StateId { get; set; }

        [Display(Name = "Zip code")]
        [Required(ErrorMessage = "please enter your zip code")]
        public string PostalCode { get; set; }

        [Display(Name = "Daytime phone")]
        public string DaytimePhone { get; set; }

        [Display(Name = "Evening phone")]
        public string EveningPhone { get; set; }

        public string Fax { get; set; }

        // deposit via EFT

        [Display(Name = "Account type")]
        [Required(ErrorMessage = "please select an account type")]
        public string AccountType { get; set; }

        [Display(Name = "Business or personal")]
        [Required(ErrorMessage = "please make a selection")]
        public string BusinessOrPersonal { get; set; }

        [Display(Name = "Name On account")]
        [Required(ErrorMessage = "please enter the name on the account")]
        public string NameOnAccount { get; set; }

        [Display(Name = "Account number")]
        [Required(ErrorMessage = "please enter your account number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Account number confirm")]
        [System.Web.Mvc.Compare("AccountNumber", ErrorMessage = "the account numbers must match")]
        public string AccountNumberConfirm { get; set; }

        [Display(Name = "Routing number")]
        [Required(ErrorMessage = "please enter the routing number")]
        public string RoutingNumber { get; set; }

        // deposit via PayPal
        [Display(Name = "PayPal email")]
        [Required(ErrorMessage = "please enter your PayPal email address")]
        public string PayPalEmail { get; set; }

        [Display(Name = "PayPal email confirm")]
        [System.Web.Mvc.Compare("PayPalEmail", ErrorMessage = "the email addresses must match")]
        public string PayPalEmailConfirm { get; set; }
    }
}