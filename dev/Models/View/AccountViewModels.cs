﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "please enter your current password")]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "please enter a new password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LogOnViewModel
    {
        [Required(ErrorMessage = "please enter your username")]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "please enter your password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage = "please enter a username")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "your username must be at least {2} characters long and less than {1} characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "please enter a first name")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "please enter a last name")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "please enter an email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "please enter a password")]
        [StringLength(100, ErrorMessage = "the password must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "the password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "please enter your phone number")]
        public string Phone { get; set; }

        //[Required(ErrorMessage = "please enter your mobile number")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "please enter a date of birth")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of birth")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "please select a gender")]
        public string Gender { get; set; }

        public Address Address { get; set; }

        [Display(Name = "Accept SMS Text Service?")]
        public bool AcceptSMS { get; set; }
    }

    public class MyAccountViewModel
    {
        [Display(Name = "User Id")]
        public int UserId { get; set; }

        public string Username { get; set; }

        public string Email { get; set; }

        [Display(Name = "Creation Date")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Account Type")]
        public string AccountType { get; set; }

        [Display(Name = "Contractor Id")]
        public Nullable<int> ContractorId { get; set; }

        public Contractor Contractor { get; set; }

        [Display(Name = "Qualification")]
        public Nullable<int> QualificationId { get; set; }

        [Display(Name = "Service Area")]
        public ServiceArea ServiceArea { get; set; }

        [Display(Name = "Is Qualified")]
        public bool IsQualified { get; set; }

        [Display(Name = "Payments")]
        public List<ContractorPayment> ContractorPayments { get; set; }

        public List<ContractorUser> AccountUsers { get; set; }

        public bool IsListed { get; set; }

        public string TaxRate { get; set; }

        public bool HasSubmittedPayeeInfo { get; set; }

        public string DepositVia { get; set; }

        public ContractorAvailability ContractorAvailability { get; set; }
    }

    public class AccountHistoryViewModel
    {
        public int UserId { get; set; }
        public my_aspnet_users User { get; set; }
        public bool IsContractor { get; set; }
        public List<RequestedService> RequestedServices { get; set; }
        public List<ContractorUser> ContractorUsers { get; set; }
    }

    public class AddContractorUserViewModel
    {
        public int ContractorId { get; set; }
        public Contractor Contractor { get; set; }

        [Required(ErrorMessage = "please select a role")]
        public string Role { get; set; }

        [Required(ErrorMessage = "please enter a username")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "your username must be at least {2} characters long and less than {1} characters")]
        public string Username { get; set; }

        [Required(ErrorMessage = "please enter a first name")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "please enter a last name")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "please enter an email")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "please enter a password")]
        [StringLength(100, ErrorMessage = "the password must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "the password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "please enter your phone number")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "please enter your mobile number")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "please enter a date of birth")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of birth")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "please select a gender")]
        public string Gender { get; set; }

        public Address Address { get; set; }

        [Display(Name = "Accept SMS Text Service?")]
        public bool AcceptSMS { get; set; }

    }
}
