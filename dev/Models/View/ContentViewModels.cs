﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ContentListViewModel
    {
        public List<Content> ContentList { get; set; }
    }

    public class ContentDetailsViewModel
    {
        public int ContentId { get; set; }
        public Content Content { get; set; }
    }

    public class ContentEditViewModel
    {
        public int ContentId { get; set; }
        public Content Content { get; set; }
        public string ReturnUrl { get; set; }
    }
}