﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ContractorPaymentIndexViewModel
    {
        public IEnumerable<ContractorPayment> ContractorPayments { get; set; }
        public IEnumerable<Payment> CustomerPayments { get; set; }
    }
}