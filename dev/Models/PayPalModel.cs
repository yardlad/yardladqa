﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace YardLad.Models
{
    public class PayPalModel
    {
        public string cmd { get; set; }
        public string business { get; set; }
        public string no_shipping { get; set; }
        public string @return { get; set; }
        public string cancel_return { get; set; }
        public string notify_url { get; set; }
        public string currency_code { get; set; }
        public string item_number { get; set; }
        public string item_name { get; set; }
        public string tax { get; set; }
        public string amount { get; set; }
        public string actionURL { get; set; }
        public string image_url { get; set; }

        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string day_phone_a { get; set; }
        public string day_phone_b { get; set; }
        public string day_phone_c { get; set; }
        public string night_phone_a { get; set; }
        public string night_phone_b { get; set; }
        public string night_phone_c { get; set; }

        public Dictionary<string, string> Options { get; set; }

        public PayPalModel(bool useSandbox)
        {
            this.cmd = "_xclick";
            this.business = ConfigurationManager.AppSettings["business"];
            this.image_url = "https://yardlad.com/rw_common/images/yard%20lad%20logo%20final%20125.png";
            this.cancel_return = ConfigurationManager.AppSettings["cancel_return"];
            this.@return = ConfigurationManager.AppSettings["return"];
            if (useSandbox)
            {
                this.actionURL = ConfigurationManager.AppSettings["test_url"];
            }
            else
            {
                this.actionURL = ConfigurationManager.AppSettings["prod_url"];
            }
            // We can add parameters here, for example OrderId, CustomerId, etc….
            this.notify_url = ConfigurationManager.AppSettings["notify_url"];
            // We can add parameters here, for example OrderId, CustomerId, etc….
            this.currency_code = ConfigurationManager.AppSettings["currency_code"];
            this.Options = new Dictionary<string, string>();
        }
    }
}