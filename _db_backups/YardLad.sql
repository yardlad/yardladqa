-- MySQL dump 10.13  Distrib 5.5.19, for Linux (x86_64)
--
-- Host: 173.201.136.194    Database: YardLad
-- ------------------------------------------------------
-- Server version	5.0.96-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Address`
--

DROP TABLE IF EXISTS `Address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Address` (
  `AddressId` int(11) NOT NULL auto_increment,
  `Line1` varchar(50) NOT NULL,
  `Line2` varchar(50) default NULL,
  `City` varchar(50) NOT NULL,
  `StateId` int(11) NOT NULL,
  `PostalCode` varchar(10) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`AddressId`),
  KEY `FK_Address_State_idx` (`StateId`),
  CONSTRAINT `FK_Address_State` FOREIGN KEY (`StateId`) REFERENCES `State` (`StateId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Address`
--

LOCK TABLES `Address` WRITE;
/*!40000 ALTER TABLE `Address` DISABLE KEYS */;
INSERT INTO `Address` VALUES (8,'1234 Test Avenue',NULL,'Mission',118,'66202','');
INSERT INTO `Address` VALUES (9,'2106 Green Meadow Ct',NULL,'Saint Joseph',127,'64506','');
INSERT INTO `Address` VALUES (10,'2106 Green Meadow Ct.',NULL,'Saint Joseph',127,'64506','');
INSERT INTO `Address` VALUES (11,'5658 Waverly Avenue',NULL,'kansas city',118,'66112','');
INSERT INTO `Address` VALUES (12,'333 Hipster Avenue',NULL,'kansas city',118,'66112','');
INSERT INTO `Address` VALUES (14,'Address 1',NULL,'City 2',102,'89551','');
INSERT INTO `Address` VALUES (15,'123 street','456 street','kansas city',118,'66112','');
INSERT INTO `Address` VALUES (16,'3916 East Hills Drive',NULL,'St Joseph',127,'64503','');
INSERT INTO `Address` VALUES (17,'3916 East Hills Drive',NULL,'St Joseph',127,'64503','');
INSERT INTO `Address` VALUES (18,'5658 Waverly Street','123 street','kansas city',118,'66112','');
INSERT INTO `Address` VALUES (19,'Near But Far Avenue','123 street','kansas city',118,'66112','');
INSERT INTO `Address` VALUES (20,'1600 Genessee St, Suite 822',NULL,'Kansas City',127,'64102','');
INSERT INTO `Address` VALUES (21,'Tester Avenue','nope','Kansas City',127,'64102','');
INSERT INTO `Address` VALUES (22,'Tester Avenue','nope','Kansas City',127,'64102','');
INSERT INTO `Address` VALUES (23,'Tester Avenue','nope','Kansas City',127,'64102','');
INSERT INTO `Address` VALUES (24,'123 Street','456 Avenue','Kansas City',118,'66112','');
INSERT INTO `Address` VALUES (25,'123 Street',NULL,'Kansas City',118,'66112','');
INSERT INTO `Address` VALUES (26,'Tester Avenue',NULL,'Kansas City',127,'64102','');
INSERT INTO `Address` VALUES (27,'123 Street',NULL,'Kansas City',118,'66104','');
INSERT INTO `Address` VALUES (28,'123 street','123 street','kansas city',118,'66112','');
INSERT INTO `Address` VALUES (29,'123 Street',NULL,'Kansas City',118,'66112','');
INSERT INTO `Address` VALUES (30,'123 Street',NULL,'Kansas City',118,'66104','');
INSERT INTO `Address` VALUES (31,'123 Street',NULL,'Kansas City',118,'66104','');
INSERT INTO `Address` VALUES (32,'123 Street',NULL,'kansas city',118,'66112','');
INSERT INTO `Address` VALUES (33,'123 Street',NULL,'kansas city',118,'66112','');
INSERT INTO `Address` VALUES (34,'123 Street',NULL,'kansas city',118,'66112','');
/*!40000 ALTER TABLE `Address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Answer`
--

DROP TABLE IF EXISTS `Answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Answer` (
  `AnswerId` int(11) NOT NULL auto_increment,
  `QuestionId` int(11) NOT NULL,
  `Text` varchar(250) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`AnswerId`),
  KEY `FK_Answer_Question_idx` (`QuestionId`),
  CONSTRAINT `FK_Answer_Question` FOREIGN KEY (`QuestionId`) REFERENCES `Question` (`QuestionId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Answer`
--

LOCK TABLES `Answer` WRITE;
/*!40000 ALTER TABLE `Answer` DISABLE KEYS */;
/*!40000 ALTER TABLE `Answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Content`
--

DROP TABLE IF EXISTS `Content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Content` (
  `ContentId` int(11) NOT NULL auto_increment,
  `SectionName` varchar(50) NOT NULL,
  `ContentBody` text,
  `Source` varchar(255) default NULL,
  `ImageId` int(11) NOT NULL default '1',
  `YardOwner` varchar(50) default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContentId`),
  KEY `FK_Content_Image_idx` (`ImageId`),
  CONSTRAINT `FK_Content_Image` FOREIGN KEY (`ImageId`) REFERENCES `Image` (`ImageId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Content`
--

LOCK TABLES `Content` WRITE;
/*!40000 ALTER TABLE `Content` DISABLE KEYS */;
INSERT INTO `Content` VALUES (1,'About Us','<h2>About Us</h2>\r\n<p>Our crews service the Greater Kansas City area with one goal in mind - provide high quality lawn care services to our neighbors at reasonable prices. Unlike most (if not all) our competitors we publish our prices online so you know how much your job will cost before you set up services. You can pay online and if you over or under estimated your job, we&rsquo;ll adjust the difference after the job is done!</p>\r\n<p>We are fully insured and offer the following lawn services:</p>\r\n<ul>\r\n<li>Commercial/Residential Mowing Packages</li>\r\n<li>Lawn renovations to include sodding, aerating, verti-cutting and over-seeding</li>\r\n<li>Residential Snow Removal</li>\r\n<li>General maintenance to include installation of seasonal color, mulching, weeding, trimming, drainage, leaf removal, etc.</li>\r\n</ul>\r\n<p><strong>If you have any questions or would like a quote for a job not listed on the website, please do not hesitate to contact us at info@yardlad.com or call us at (913) 712-9222.</strong></p>\r\n<p>Thank you for your time - we look forward to serving you and your family!</p>',NULL,1,NULL,'');
INSERT INTO `Content` VALUES (2,'Home (Welcome)','<h1>Welcome to Yard Lad</h1>\r\n<p>Yard Lad, LLC is a full-service landscaping broker providing quality service to residential and commercial clients in the Greater Kansas City area. Our company is dedicated to finding you the best services at the best prices.</p>\r\n<p>We are dedicated to 100% customer satisfaction. We strive to bring you the best in quality service.</p>',NULL,1,NULL,'');
INSERT INTO `Content` VALUES (3,'Home (Services)','<h1>Our Services Include</h1>\r\n<ul>\r\n<li>Weekly lawn mowing for residential and commercial properties.</li>\r\n<li>Six stage granular fertilizer programs.</li>\r\n<li>Landscape design and installation to include retaining walls, patios and walkways, fire pits, water features and outdoor kitchens.</li>\r\n<li>Tree trimming and brush removal.</li>\r\n<li>General maintenance to include installation of seasonal color, mulching, weeding, trimming, drainage, leaf removal, etc.</li>\r\n<li>Lawn renovations to include sodding, aerating, verti-cutting and over-seeding</li>\r\n<li>Snow Removal for commercial properties and homeowner associations.</li>\r\n</ul>',NULL,1,NULL,'');
INSERT INTO `Content` VALUES (4,'Featured Yard',NULL,NULL,2,'John Smith (Olathe, KS)','');
INSERT INTO `Content` VALUES (5,'Service Index (Service List)','<ul>\r\n<li>Weekly lawn mowing for residential and commercial properties.</li>\r\n<li>Six stage granular fertilizer programs</li>\r\n<li>Landscape design and installation to include retaining walls, patios and walkways, fire pits, water features and outdoor kitchens.</li>\r\n<li>Sprinkler installation, repairs, turn-on and winterization.</li>\r\n<li>Landscape lighting</li>\r\n<li>General maintenance to include installation of seasonal color, mulching, weeding, trimming, drainage, leaf removal, etc.</li>\r\n<li>Lawn renovations to include sodding, aerating, verti-cutting and over-seeding</li>\r\n<li>Snow Removal for commercial properties and homeowner associations.</li>\r\n</ul>',NULL,1,NULL,'');
INSERT INTO `Content` VALUES (6,'Yard Lad Video',NULL,'//www.youtube.com/embed/Ya3n05bOHLQ?rel0',1,NULL,'');
INSERT INTO `Content` VALUES (7,'Terms and Conditions','<h2>Service Provider\'s User Agreement</h2>\r\n<p>Yard Lad, LLC allows consumers who subscribe to be a member of Yard Lad (collectively, the \"Members\") to provide reviews and ratings on a variety of service companies and providers (collectively, a \"Service Provider\") with whom they have had actual experiences. As a Service Provider, on behalf and as representative of a Service Provider (\"You\" or \"Company\"), You are permitted to use the web site (www.yardlad.com) (the \"Website\") and the information contained therein subject to the terms and conditions contained in this Service Provider\'s User Agreement, which may be modified, amended or replaced by Yard Lad from time to time at Yard Lad\'s sole discretion (collectively, the \"Agreement\"). Such modifications will become effective immediately upon the posting thereof. In consideration of Yard Lad granting the Company access to its Website and the information contained therein, and in order to use the Website, You must read and accept all of the Terms and Conditions in, and linked to, this Agreement. It is the Company\'s responsibility to review this Agreement on a regular basis to keep itself informed of any modifications. BY ACCEPTING THE TERMS AND CONDITIONS OF THIS AGREEMENT, THE COMPANY ACKNOWLEDGES IT HAS READ, UNDERSTANDS AND AGREES TO BE BOUND BY ALL OF THE TERMS, CONDITIONS, AND NOTICES CONTAINED IN THIS AGREEMENT JUST AS IF YOU HAD SIGNED THIS AGREEMENT.</p>\r\n<h3>Terms and Conditions</h3>\r\n<ol>\r\n<li>Members may submit a review or report and other information (collectively, \"Member Content\") on any Service Provider with which they have communicated whether work was completed or not. If the Company disputes any Member Content, the Company\'s sole course of action with respect to such Member Content as it relates to Yard Lad and the Website is to utilize the Services (as defined below) which are available online at the Website.</li>\r\n<li>By agreeing to the Terms and Conditions of this Agreement, the Company is hereby permitted to use the services offered to Service Provider\'s including, without limitation, access to\"Business Center\", the ability to review Member Content pertaining to the Company, the ability to submit responses to Member Content, the ability to update and maintain profile information on the Company, the ability to utilize the dispute resolution process offered through the Website and facilitated by Yard Lad and such other services that Yard Lad may offer to Service Providers from time to time (collectively, the \"Services\").</li>\r\n<li>By agreeing to the Terms and Conditions of this Agreement, Yard Lad grants the Company a limited license to access and use the Website and the Services. Notwithstanding the foregoing, the Company acknowledges and agrees that it will not access, reproduce, duplicate, copy, sell, re-sell, visit or otherwise exploit the Website (or any of the content therein including, without limitation, any Member Content or any Member profiles) or Services for any commercial or other purpose, without the express written consent of Yard Lad.</li>\r\n<li>Yard Lad does not endorse and is not responsible or liable for any Member Content, SP Content (as defined below), data, advertising, products, goods or services available or unavailable from, or through, Yard Lad. The statements, information and ratings contained in any Member Content are solely the opinion of the Member submitting such Member Content and do not reflect the opinion of Yard Lad or any of its affiliates or subsidiaries or any of their respective owners, managers, officers, employees, agents or representatives.</li>\r\n<li>The Company acknowledges and understands that Yard Lad simply acts as a passive conduit and an interactive computer provider for the publication and distribution of Member Content and SP Content. Yard Lad does not have any duty or obligation to investigate the accuracy of Member Content or the quality of the work performed by the Company or any other Service Provider which is the subject of any Member Content. By using the Services, the Company agrees that it is solely the Company\'s responsibility to evaluate the Company\'s risks associated with the use, accuracy, usefulness, completeness, appropriateness or legality of any information, responses, writings or other materials that the Company submits, transmits or otherwise conveys through the Services (collectively, \"SP Content\"). Under no circumstances will Yard Lad be liable in any way for any Member Content or SP Content including, but not limited to, any Member Content or SP Content that contains, errors, omissions or defamatory statements, or for any loss or damage of any kind incurred as a result of the use of any Member Content or SP Content submitted, accessed, transmitted or otherwise conveyed via the Services or otherwise. The Company hereby waives any claims, rights or actions that it may have against Yard Lad or any of its affiliates or subsidiaries with respect to any Member Content or SP Content and releases Yard Lad and each of its affiliates and subsidiaries from any and all liability for or relating to Member Content or SP Content. The Company agrees to indemnify and hold Yard Lad and each of its affiliates and subsidiaries and their respective owners, managers, officers, employees, agents or representatives harmless for any damages that may arise, directly or indirectly, from any claim or right it may have against Yard Lad with respect to any statements made by a Member or Member Content submitted by a Member which is communicated, posted or published by Yard Lad on its Website or to a third party.</li>\r\n<li>The Company acknowledges and agrees that the Company can neither require Yard Lad to place the Company on its Website nor remove the Company or any Member Content fromYard Lad. The Company further acknowledges and understands that the Company is not a Member of Yard Lad, cannot refer to itself as a Member of Yard Lad, and is not afforded the same access to the Website as a Member nor the benefits afforded to a Member.</li>\r\n<li>The Company and its current or former owners, directors, managers, employees, agents and family members are expressly prohibited from purchasing gift memberships to Yard Lad or reimbursing clients or customers for their Yard Lad membership cost. In addition, individuals affiliated with the Company including, without limitation, current or former owners, current or former employees or officers, family members, or current or former partners, investors, managers or directors (collectively, the \"Affiliated Persons\") may not submit Member Content to Yard Lad on the Company. The Company hereby acknowledges and agrees that to the extent an Affiliated Person has submitted or posted any Member Content on the Company or any company or person competitive to the Company, or believes that Member Content was posted by an Affiliated Person that Yard Lad may immediately remove such Member Content without notice or recourse against Yard Lad.</li>\r\n<li>SP Content shall not contain any unauthorized content which includes but is not limited to:<ol style=\"list-style: lower-alpha;\">\r\n<li>Offensive, harmful and/or abusive language, including without limitation: expletives, profanities, obscenities, harassment, vulgarities, sexually explicit language and hate speech (e.g., racist/discriminatory speech.)</li>\r\n<li>Comments that do not address the Member Content or comments with no qualitative value as determined by Yard Lad in its sole discretion</li>\r\n<li>Content that contains personal attacks or describes physical confrontations and/or sexual harassment</li>\r\n<li>Messages that are advertising or commercial in nature, or are inappropriate based on the applicable subject matter</li>\r\n<li>Language that violates the standards of good taste or the standards of the Website, as determined by Yard Lad in its sole discretion</li>\r\n<li>Content determined by Yard Lad, in its sole discretion, to be illegal, or to violate any federal, state, or local law or regulation or the rights of any other person or entity</li>\r\n<li>Language intended to impersonate other users (including names of other individuals) or to be offensive or inappropriate user names or signatures and/or</li>\r\n<li>Content that is not in English, that is encrypted or that contains viruses, Trojan horses, worms, time bombs, cancelbots or other computer programming routines that are intended to damage, interfere with, intercept or appropriate any system, data or personal information.</li>\r\n</ol>The Company acknowledges and agrees that Yard Lad in its sole discretion may remove without notice any SP Content or any portion thereof that Yard Lad believes violates the foregoing.</li>\r\n<li>Yard Lad may suspend, restrict or terminate the Company\'s use of the Services or any portion thereof if the Company breaches or fails to comply with any of the Terms and Conditions of this Agreement.</li>\r\n<li>Although Yard Lad does not claim ownership of any SP Content or other communications or materials submitted by or given by the Company to Yard Lad, by providing SP Content for the Website or other mediums, the Company automatically grants, and the Company represents and warrants that the Company has the right to grant, to Yard Lad an irrevocable, perpetual, non-exclusive, fully paid, worldwide license to use, copy, perform, display, reproduce, adapt, modify, and distribute such SP Consent and to prepare derivative works of, or incorporate into other works, such SP Content, and to grant and to authorize sublicenses (through multiple tiers) of the foregoing. &nbsp;In addition, by providing Yard Lad with SP Content, the Company automatically grants Yard Lad all rights necessary to prohibit the subsequent aggregation, display, copying, duplication, reproduction or exploitation of SP Content on the Website or in any other medium by any other party. &nbsp;No compensation will be paid with respect to Yard Lad use of SP Content. &nbsp;Yard Lad is under no obligation to post or use any of SP Content or maintain SP Content. &nbsp;Yard Lad may remove SP Content at any time in Yard Lad sole discretion.</li>\r\n<li>&nbsp;It is the Company\'s sole responsibility to review and monitor any Member Content regarding the Company that is posted by Members and to submit responses it deems necessary to any Member Content. &nbsp;Yard Lad does not have any obligation to provide a notice or update to the Company with respect to any new information or Member Content that it learns of or receives about the Company from its Members.</li>\r\n<li>The Company has the sole responsibility of updating any and all of its information on the Website including, without limitation, the Company\'s description and profile information.</li>\r\n<li>The Company agrees not to use or cause any robot, bot, spider, other automatic device, or computer program routine or manual process to monitor, duplicate, take, obtain, transfer, modify, use, reproduce, aggregate or copy Yard Lad, any Member Content, any Member profiles, SP Content (including SP profiles) or any other content contained on the Website or any other publication of Yard Lad. &nbsp;You shall not use or cause any device, software, or routine to interfere or attempt to interfere with the proper working of the Website.</li>\r\n<li>The Company hereby represents and warrants to Yard Lad that (a) all information provided to Yard Lad by the Company is true, complete and accurate in all respects, and (b) the Company is authorized to submit information to Yard Lad. &nbsp;Yard Lad is authorized by the Company to rely upon the truthfulness, completeness and accuracy of SP Content in order to serve its Members.</li>\r\n<li>The Company acknowledges that the Website utilizes one or more website analytic services, including, without limitation, ClickTale, which may record mouse clicks, mouse movements, scrolling activity and text entered into the Website by users. These services do not collect personally identifiable information that is not voluntarily entered into the Website by the user. Yard Lad uses the information collected by these service providers to improve the usability and other features of the Website. Users may choose to disable the ClickTale service at http://www.clicktale.net/disable.html.</li>\r\n<li>The Company acknowledges that Yard Lad will use the telephone numbers, email addresses and facsimile numbers that are submitted to Yard Lad in connection with registering withYard Lad to contact the Company with information regarding Yard Lad. &nbsp;Yard Lad agrees not to sell, trade, rent or share such information with any third parties.</li>\r\n<li>To the extent a third party posts or submits any SP Content or manages the Company\'s profile or information on the Website, the Company hereby acknowledges and agrees that the Company shall remain fully responsible for any SP Content or information posted or submitted by such third party.</li>\r\n<li>The Company agrees unless expressly authorized by Yard Lad not to access, copy, duplicate use, reproduce, alter, modify, create derivative works, display, sell, re-sell, advertise or market with or otherwise exploit for any commercial, educational or other purpose any Member Content, any Member profiles, any SP profiles, or any other content from the Website or Yard Lad including, without limitation, any reviews or ratings or any other content contained in any Member Content.</li>\r\n<li>Yard Lad is the owner and/or authorized user of any trademark and/or service mark, including, without limitation, the name \"Yard Lad\", appearing on the Website and is copyright owner or licensee of the content and/or information on the Website. &nbsp;By placing them on the Website, Yard Lad does not grant the Company any license or other authorization to copy or use its trademarks, service marks, copyrighted material, or other intellectual property, except as provided herein.</li>\r\n<li>Yard Lad reserves the right to exercise any rights or remedies which may be available to it against the Company if the Terms and Conditions of this Agreement are violated by the Company. These remedies include, but are not limited to, revocation of (a) Super Service Awards (present &amp; past) and any associated license, (b) advertising privileges, (c) use of the Services, or (d) appearances on the Website and/or any other appearances in any Yard Lad\'s publication, and Company agrees that the exercise of one remedy shall not preclude the availability of any other remedy.</li>\r\n<li>SP Content shall not contain any unauthorized content which includes but is not limited to:<ol style=\"list-style: lower-alpha;\">\r\n<li>If the Company posts SP Content in violation of this Agreement, the Company agrees to promptly pay Yard Lad One Thousand Dollars ($1,000) for each item of SP Content posted in violation of this Agreement. &nbsp;Yard Lad may (but is not required) issue the Company a warning before assessing damages.</li>\r\n<li>If the Company exploits for any purpose (commercial or otherwise) any Member Content, Member profiles or any other information contained on the Website including, without limitation, ratings and/or reviews in violation of this Agreement, the Company agrees to pay Ten Thousand Dollars ($10,000) per report, record or review exploited.</li>\r\n<li>&nbsp;If the Company uses or causes any robot, bot, spider, other automatic device or computer program routine or any manual process to monitor, duplicate, take, aggregate, obtain, modify, use, reproduce or copy any Member Content, any member profiles, SP content (including SP profiles) or any other content contained on the Website or in any other publication of Yard Lad, the Company agrees to pay One Hundred Dollars ($100) for each report, record, review or other information that is monitored, duplicated, transferred, taken, obstructed, modified, used, reproduced, aggregated or copied.</li>\r\n<li>Except as set forth in the foregoing subparagraphs (a) through (c), inclusive, the Company agrees to pay the actual damages suffered by Yard Lad to the extent such actual damages can be reasonably calculated.<br /><br />Notwithstanding any other provision of this Agreement, the Company reserves the right to seek the remedy of specific performance of any term contained herein, or a preliminary or permanent injunction against the breach of any such term or in aid of the exercise of any power granted in this Agreement, or any combination thereof.</li>\r\n</ol></li>\r\n<li>THE COMPANY EXPRESSLY UNDERSTANDS AND AGREES THAT Yard Lad WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, COMPENSATORY, CONSEQUENTIAL OR EXEMPLARY DAMAGES (EVEN IF Yard Lad HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES) (COLLECTIVELY, \"DAMAGES\"), RESULTING FROM: (A) THE USE OR INABILITY TO USE THE SERVICES; (B) THE COST OF ANY GOODS AND/OR SERVICES PURCHASED OR OBTAINED AS A RESULT OF THE USE OF THE SERVICES; (C) DISCLOSURE OF, UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR INFORMATION OR SP CONTENT; (D) SP CONTENT THE COMPANY MAY SUBMIT, RECEIVE, ACCESS, TRANSMIT OR OTHERWISE CONVEY THROUGH THE SERVICES OR THIS AGREEMENT; (E) STATEMENTS OR CONDUCT OF ANY MEMBER OR OTHER THIRD PARTY THROUGH THE SERVICES; (F) ANY OTHER MATTER RELATING TO THE SERVICES; (G) ANY BREACH OF THIS AGREEMENT BY Yard Lad OR THE FAILURE OF Yard Lad TO PROVIDE THE SERVICES UNDER THIS AGREEMENT; (H) ANY MEMBER CONTENT POSTED; OR (I) ANY OTHER DEALINGS OR INTERACTIONS THE COMPANY HAS WITH ANY SERVICE PROVIDER (OR ANY OF THEIR REPRESENTATIVES OR AGENTS). &nbsp;THESE LIMITATIONS SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW. &nbsp;In some jurisdictions, limitations of liability are not permitted. In such jurisdictions, some of the foregoing limitations may not apply to the Company.<br /><br />TO THE EXTENT Yard Lad IS FOUND LIABLE FOR ANYTHING RELATED TO THIS AGREEMENT OR THE USE OF THE SERVICES, Yard Lad\'S LIABILITY FOR DAMAGES WILL NOT EXCEED ONE HUNDRED DOLLARS ($100.00).</li>\r\n<li>This Agreement and the relationship between the Company and Yard Lad will be governed by the internal laws of the State of Kansas, notwithstanding the choice of law provisions or conflict of law analysis of the venue where any action is brought, where the violation occurred, where the Company may be located or any other jurisdiction. &nbsp;The Company agrees and consents to the exclusive jurisdiction of the state or federal courts located in Olathe, Kansas and waives any defense of lack of personal jurisdiction or improper venue or forum non conveniens to a claim brought in such court, except that Yard Lad may elect, in its sole discretion, to litigate the action in the county or state where any breach by the Company occurred or where the Company can be found. &nbsp;The Company agrees that regardless of any statute or law to the contrary, any claim or cause of action arising out or related to your use of the Service or this Agreement shall be filed within one (1) year after such claim or cause of action arose or will forever be barred.</li>\r\n<li>In the event this Agreement is terminated, certain provisions of this Agreement will continue to remain in effect, including, but not limited to, <span style=\"text-decoration: underline;\">Sections 5, 7, 10, 21, 22, 23, 24, 25</span> and <span style=\"text-decoration: underline;\">27</span>.</li>\r\n<li>The Company agrees to indemnify and hold Yard Lad and each of its affiliates and subsidiaries and each of their respective owners, officers, agents, managers, partners, employees, agents and representatives harmless from any loss, liability, claim, or demand, including reasonable attorneys\' fees (whether incurred in enforcing this Agreement or otherwise), made by any third party due to or arising out of the Company\'s use of Yard Lad.</li>\r\n<li>Yard Lad may modify or restate the Terms and Conditions of this Agreement and such modification(s) will be effective immediately upon being posted on the Website. &nbsp;Yard Lad will make note of the date of the last update to the Agreement on the first page of this Agreement. &nbsp;The Company is responsible for reviewing these terms and conditions regularly. &nbsp;The Company\'s continued use of the Services after such modifications will be deemed to be the Company\'s conclusive acceptance of all modifications to this Agreement.</li>\r\n<li>The Company agrees that Yard Lad shall be entitled to payment from the Company for any and all out-of-pocket costs, including, without limitation, attorneys\' fees, incurred by Yard Lad in connection with enforcing these Terms and Conditions and this Agreement or otherwise.</li>\r\n<li>The Services may be subject to limitations, delays and other problems inherent in the use of the internet and electronic communications. &nbsp;Yard Lad is not responsible for any delays, failures or other damage resulting from such problems.</li>\r\n<li>This Agreement may not be re-sold or assigned by the Company. &nbsp;If the Company assigns, or tries to assign, this Agreement, such assignment or attempted assignment will be void and unenforceable. &nbsp;It will not be considered a waiver of Yard Lad\'s rights if Yard Lad fails to enforce any of the terms or conditions of this Agreement against the Company. &nbsp;In the event a court finds a provision in this Agreement to not be valid, the Company and Yard Lad agrees that such court should incorporate a similar provision that would be considered valid, with all other provisions remaining valid in the Agreement. &nbsp;No joint venture, partnership, employment or agency relationship exists between the Company and Yard Lad as a result of this Agreement or use of the Services.&nbsp;</li>\r\n<li>The person agreeing to this Agreement and the Terms and Conditions on behalf of the Company hereby represents and warrants that he/she has the power and authority to bind the Company and that this Agreement and the Terms and Conditions constitutes a valid and binding agreement of the Company.<br /><br />IF YOU DO NOT AGREE TO ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT, YOU MUST NOT USE THE SERVICES. &nbsp;BY USING THE SERVICES, YOU ACKNOWLEDGE THAT YOU HAVE READ AND UNDERSTOOD THE TERMS AND CONDITIONS OF THIS AGREEMENT AND YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS.</li>\r\n</ol>',NULL,1,NULL,'');
INSERT INTO `Content` VALUES (8,'Service Providers (Services)','<p>Are you a business that provides landscaping and lawn maintenance services? Yard Lad, LLC is a full-service landscaping broker providing quality service to residential and commercial clients in the Greater Kansas City area. If you provide these services and would like to be a part of our vendor network please contact us.</p>\r\n<h3>How to Sign Up</h3>\r\n<ol>\r\n<li>Select Your Contractor Type\r\n<ul style=\"margin-bottom: 0;\">\r\n<li><span style=\"text-decoration: underline;\" title=\"professional businesses need to provide proof of insurance and their EIN or Tax ID\"> Professional Business</span> - $250 per year</li>\r\n<li><span style=\"text-decoration: underline;\" title=\"independent contractors need to provide proof of insurance and their Tax ID\"> Independent Contractor</span> - $100 per year</li>\r\n<li><span style=\"text-decoration: underline;\" title=\"students need to provide proof that they are currently enrolled and their Tax ID\"> Student/Apprentice</span> - $50 per year</li>\r\n</ul>\r\n</li>\r\n<li>Submit Your Qualifications</li>\r\n<li>List Your Services and Prices</li>\r\n<li>Publish Your Listing and Get More Customers</li>\r\n</ol>\r\n<h3>Service Providers For:</h3>\r\n<ul>\r\n<li>Weekly lawn mowing for residential and commercial properties.</li>\r\n<li>Landscape design and installation to include retaining walls, patios and walkways, fire pits, water features and outdoor kitchens.</li>\r\n<li>Sprinkler installation, repairs, turn-on and winterization.</li>\r\n<li>Landscape lighting.</li>\r\n<li>General maintenance to include installation of seasonal color, mulching, weeding, trimming, drainage, leaf removal, etc.</li>\r\n<li>Lawn renovations to include sodding, aerating, verti-cutting and over-seeding.</li>\r\n<li>Snow Removal for commercial properties and homeowner associations.</li>\r\n</ul>',NULL,1,NULL,'');
/*!40000 ALTER TABLE `Content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Contractor`
--

DROP TABLE IF EXISTS `Contractor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Contractor` (
  `ContractorId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) NOT NULL,
  `ContractorTypeId` int(11) NOT NULL,
  `BusinessPhone` varchar(14) NOT NULL,
  `ServiceAreaId` int(11) default NULL,
  `QualificationId` int(11) default NULL,
  `IsApproved` bit(1) NOT NULL default b'0',
  `ApprovedOn` datetime default NULL,
  `DateCreated` datetime default NULL,
  `ExpiresOn` datetime default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorId`),
  KEY `FK_Contractor_ContractorType_idx` (`ContractorTypeId`),
  KEY `FK_Contractor_Qualification_idx` (`QualificationId`),
  KEY `FK_Contractor_ServiceArea_idx` (`ServiceAreaId`),
  CONSTRAINT `FK_Contractor_ContractorType` FOREIGN KEY (`ContractorTypeId`) REFERENCES `ContractorType` (`ContractorTypeId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_Contractor_Qualification` FOREIGN KEY (`QualificationId`) REFERENCES `Qualification` (`QualificationId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_Contractor_ServiceArea` FOREIGN KEY (`ServiceAreaId`) REFERENCES `ServiceArea` (`ServiceAreaId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Contractor`
--

LOCK TABLES `Contractor` WRITE;
/*!40000 ALTER TABLE `Contractor` DISABLE KEYS */;
INSERT INTO `Contractor` VALUES (3,'The Student Contractor',2,'(913) 555-6789',6,NULL,'','2016-01-05 16:24:26','2016-01-06 16:24:29','2016-03-01 16:24:32','');
INSERT INTO `Contractor` VALUES (4,'Adams Lawn Care',3,'(816) 752-1525',4,3,'\0',NULL,'2015-07-14 09:45:51',NULL,'\0');
INSERT INTO `Contractor` VALUES (5,'Daves Lawn Service',3,'(816) 830-8407',6,5,'','2016-01-25 09:45:54','2016-01-12 08:59:46',NULL,'');
INSERT INTO `Contractor` VALUES (6,'Billy',2,'(888) 888-8888',6,NULL,'\0',NULL,'2016-01-12 11:31:12',NULL,'\0');
INSERT INTO `Contractor` VALUES (8,'Adams Lawn Care',3,'(816) 752-1525',4,4,'','2016-01-22 14:22:13',NULL,NULL,'\0');
INSERT INTO `Contractor` VALUES (9,'CoolKid',2,'(000) 000-0000',6,NULL,'\0',NULL,NULL,NULL,'\0');
INSERT INTO `Contractor` VALUES (10,'YardPros',1,'(000) 000-0000',3,6,'\0',NULL,NULL,NULL,'\0');
INSERT INTO `Contractor` VALUES (11,'createcontractor2',3,'(000) 000-0000',NULL,NULL,'\0',NULL,'2016-02-08 14:55:53',NULL,'\0');
INSERT INTO `Contractor` VALUES (12,'createcontractor3',3,'(000) 000-0000',NULL,NULL,'\0',NULL,'2016-02-09 10:43:07',NULL,'\0');
INSERT INTO `Contractor` VALUES (13,'createcontractor5',3,'(000) 000-0000',NULL,NULL,'\0',NULL,'2016-02-09 11:00:06',NULL,'\0');
INSERT INTO `Contractor` VALUES (14,'createcontractor6',3,'(000) 000-0000',NULL,NULL,'\0',NULL,'2016-02-09 10:42:57',NULL,'\0');
/*!40000 ALTER TABLE `Contractor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorAvailability`
--

DROP TABLE IF EXISTS `ContractorAvailability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorAvailability` (
  `ContractorAvailabilityId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `Monday` bit(1) NOT NULL default b'1',
  `Tuesday` bit(1) NOT NULL default b'1',
  `Wednesday` bit(1) NOT NULL default b'1',
  `Thursday` bit(1) NOT NULL default b'1',
  `Friday` bit(1) NOT NULL default b'1',
  `Saturday` bit(1) NOT NULL default b'1',
  `Sunday` bit(1) NOT NULL default b'1',
  `HasBeenSet` bit(1) NOT NULL default b'0',
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY  (`ContractorAvailabilityId`),
  KEY `FK_ContractorAvailability_Contractor_idx` (`ContractorId`),
  CONSTRAINT `FK_ContractorAvailability_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorAvailability`
--

LOCK TABLES `ContractorAvailability` WRITE;
/*!40000 ALTER TABLE `ContractorAvailability` DISABLE KEYS */;
INSERT INTO `ContractorAvailability` VALUES (3,3,'','','','','','','','\0','2015-07-05 20:59:30');
INSERT INTO `ContractorAvailability` VALUES (4,4,'','','','','','\0','\0','','2015-07-21 17:41:19');
INSERT INTO `ContractorAvailability` VALUES (5,5,'','','','','','','','','2016-02-10 11:02:30');
INSERT INTO `ContractorAvailability` VALUES (7,8,'','','','','','','','','2016-01-22 14:17:21');
INSERT INTO `ContractorAvailability` VALUES (8,9,'','','','','','','','\0','2016-01-29 12:32:55');
INSERT INTO `ContractorAvailability` VALUES (9,10,'','','','','','','','','2016-02-10 11:13:42');
INSERT INTO `ContractorAvailability` VALUES (10,11,'','','','','','','','\0','2016-02-08 14:55:54');
INSERT INTO `ContractorAvailability` VALUES (11,12,'','','','','','','','\0','2016-02-09 10:43:08');
INSERT INTO `ContractorAvailability` VALUES (12,13,'','','','','','','','\0','2016-02-09 11:00:07');
INSERT INTO `ContractorAvailability` VALUES (13,14,'','','','','','','','\0','2016-02-09 10:42:57');
/*!40000 ALTER TABLE `ContractorAvailability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorPayment`
--

DROP TABLE IF EXISTS `ContractorPayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorPayment` (
  `ContractorPaymentId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `DateSubmitted` datetime NOT NULL,
  `TransactionId` varchar(64) NOT NULL,
  `Amount` decimal(16,2) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `IsCompleted` bit(1) NOT NULL default b'0',
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorPaymentId`),
  KEY `FK_ContractorPayment_Contractor_idx` (`ContractorId`),
  CONSTRAINT `FK_ContractorPayment_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorPayment`
--

LOCK TABLES `ContractorPayment` WRITE;
/*!40000 ALTER TABLE `ContractorPayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ContractorPayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorService`
--

DROP TABLE IF EXISTS `ContractorService`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorService` (
  `ContractorServiceId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `ServiceId` int(11) NOT NULL,
  `BasePrice` decimal(16,2) NOT NULL default '0.00',
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorServiceId`),
  KEY `FK_ContractorService_Contractor_idx` (`ContractorId`),
  KEY `FK_ContractorService_Service_idx` (`ServiceId`),
  CONSTRAINT `FK_ContractorService_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_ContractorService_Service` FOREIGN KEY (`ServiceId`) REFERENCES `Service` (`ServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorService`
--

LOCK TABLES `ContractorService` WRITE;
/*!40000 ALTER TABLE `ContractorService` DISABLE KEYS */;
INSERT INTO `ContractorService` VALUES (4,5,5,20.00,'');
INSERT INTO `ContractorService` VALUES (6,5,6,55.00,'');
INSERT INTO `ContractorService` VALUES (7,5,4,50.00,'');
INSERT INTO `ContractorService` VALUES (9,8,5,20.00,'');
INSERT INTO `ContractorService` VALUES (10,8,6,55.00,'');
INSERT INTO `ContractorService` VALUES (11,8,7,15.00,'');
INSERT INTO `ContractorService` VALUES (12,3,3,75.00,'');
INSERT INTO `ContractorService` VALUES (13,3,1,45.00,'');
INSERT INTO `ContractorService` VALUES (14,5,9,35.00,'');
INSERT INTO `ContractorService` VALUES (15,5,7,15.00,'');
INSERT INTO `ContractorService` VALUES (16,5,1,45.00,'');
INSERT INTO `ContractorService` VALUES (17,5,2,25.00,'');
INSERT INTO `ContractorService` VALUES (18,5,3,75.00,'');
INSERT INTO `ContractorService` VALUES (19,10,9,35.00,'');
/*!40000 ALTER TABLE `ContractorService` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorServiceArea`
--

DROP TABLE IF EXISTS `ContractorServiceArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorServiceArea` (
  `ContractorServiceAreaId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `ServiceAreaId` int(11) NOT NULL,
  `Price` decimal(16,2) NOT NULL default '0.00',
  PRIMARY KEY  (`ContractorServiceAreaId`),
  KEY `ContractorServiceArea_Contractor_idx` (`ContractorId`),
  KEY `ContractorServiceArea_ServiceArea_idx` (`ServiceAreaId`),
  CONSTRAINT `ContractorServiceArea_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION,
  CONSTRAINT `ContractorServiceArea_ServiceArea` FOREIGN KEY (`ServiceAreaId`) REFERENCES `ServiceArea` (`ServiceAreaId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorServiceArea`
--

LOCK TABLES `ContractorServiceArea` WRITE;
/*!40000 ALTER TABLE `ContractorServiceArea` DISABLE KEYS */;
INSERT INTO `ContractorServiceArea` VALUES (2,8,5,0.00);
/*!40000 ALTER TABLE `ContractorServiceArea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorServiceOption`
--

DROP TABLE IF EXISTS `ContractorServiceOption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorServiceOption` (
  `ContractorServiceOptionId` int(11) NOT NULL auto_increment,
  `ContractorServiceId` int(11) NOT NULL,
  `Order` int(11) default NULL,
  `Name` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorServiceOptionId`),
  KEY `ContractorServiceOption_ServiceId_idx` (`ContractorServiceId`),
  CONSTRAINT `ContractorServiceOption_ContractorServiceId` FOREIGN KEY (`ContractorServiceId`) REFERENCES `ContractorService` (`ContractorServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorServiceOption`
--

LOCK TABLES `ContractorServiceOption` WRITE;
/*!40000 ALTER TABLE `ContractorServiceOption` DISABLE KEYS */;
INSERT INTO `ContractorServiceOption` VALUES (2,14,1,'Fire Pits','');
INSERT INTO `ContractorServiceOption` VALUES (3,16,1,'GMO Free','');
INSERT INTO `ContractorServiceOption` VALUES (4,19,1,'Fire Pits','');
/*!40000 ALTER TABLE `ContractorServiceOption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorServiceOptionItem`
--

DROP TABLE IF EXISTS `ContractorServiceOptionItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorServiceOptionItem` (
  `ContractorServiceOptionItemId` int(11) NOT NULL auto_increment,
  `ContractorServiceOptionId` int(11) NOT NULL,
  `Order` int(11) default NULL,
  `Name` varchar(50) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorServiceOptionItemId`),
  KEY `FK_ContractorServiceOptionItem_ContractorServiceOption_idx` (`ContractorServiceOptionId`),
  CONSTRAINT `FK_ContractorServiceOptionItem_ContractorServiceOption` FOREIGN KEY (`ContractorServiceOptionId`) REFERENCES `ContractorServiceOption` (`ContractorServiceOptionId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorServiceOptionItem`
--

LOCK TABLES `ContractorServiceOptionItem` WRITE;
/*!40000 ALTER TABLE `ContractorServiceOptionItem` DISABLE KEYS */;
INSERT INTO `ContractorServiceOptionItem` VALUES (1,2,1,'Small Fire Pit',65.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (2,4,1,'Small Stone Circle',65.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (3,4,2,'Small Red Brick Circle',0.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (4,4,3,'Medium Red Brick Circle',0.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (5,3,1,'Brand 1',50.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (6,3,2,'Brand 2',60.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (7,3,3,'Brand 3',20.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (8,2,1,'Small Stone Circle',65.00,'');
INSERT INTO `ContractorServiceOptionItem` VALUES (9,2,NULL,'Small Red Brick Circle',65.00,'');
/*!40000 ALTER TABLE `ContractorServiceOptionItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorType`
--

DROP TABLE IF EXISTS `ContractorType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorType` (
  `ContractorTypeId` int(11) NOT NULL auto_increment,
  `Type` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorType`
--

LOCK TABLES `ContractorType` WRITE;
/*!40000 ALTER TABLE `ContractorType` DISABLE KEYS */;
INSERT INTO `ContractorType` VALUES (1,'Freelance','');
INSERT INTO `ContractorType` VALUES (2,'Student','');
INSERT INTO `ContractorType` VALUES (3,'Business','');
/*!40000 ALTER TABLE `ContractorType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ContractorUser`
--

DROP TABLE IF EXISTS `ContractorUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ContractorUser` (
  `ContractorUserId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `DateCreated` datetime NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ContractorUserId`),
  KEY `FK_ContractorUser_Contractor_idx` (`ContractorId`),
  KEY `FK_ContractorUser_User_idx` (`UserId`),
  CONSTRAINT `FK_ContractorUser_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_ContractorUser_User` FOREIGN KEY (`UserId`) REFERENCES `my_aspnet_users` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ContractorUser`
--

LOCK TABLES `ContractorUser` WRITE;
/*!40000 ALTER TABLE `ContractorUser` DISABLE KEYS */;
INSERT INTO `ContractorUser` VALUES (3,3,2,'2015-07-05 20:59:30','');
INSERT INTO `ContractorUser` VALUES (4,4,3,'2015-07-14 09:45:51','');
INSERT INTO `ContractorUser` VALUES (5,5,4,'2016-01-12 08:59:46','');
INSERT INTO `ContractorUser` VALUES (6,6,5,'2016-01-12 11:31:12','');
INSERT INTO `ContractorUser` VALUES (8,8,24,'2016-01-22 13:52:43','');
INSERT INTO `ContractorUser` VALUES (9,9,29,'2016-01-29 12:32:55','');
INSERT INTO `ContractorUser` VALUES (10,10,57,'2016-02-08 13:29:08','');
INSERT INTO `ContractorUser` VALUES (11,11,58,'2016-02-08 14:55:54','');
INSERT INTO `ContractorUser` VALUES (12,12,60,'2016-02-09 10:43:08','');
INSERT INTO `ContractorUser` VALUES (13,13,61,'2016-02-09 11:00:07','');
INSERT INTO `ContractorUser` VALUES (14,14,62,'2016-02-09 10:42:57','');
/*!40000 ALTER TABLE `ContractorUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Dispute`
--

DROP TABLE IF EXISTS `Dispute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dispute` (
  `DisputeId` int(11) NOT NULL auto_increment,
  `RatingId` int(11) NOT NULL,
  `Reasoning` varchar(2000) NOT NULL,
  `ResolutionStatus` varchar(50) NOT NULL,
  PRIMARY KEY  (`DisputeId`),
  KEY `FK_Dispute_Rating_idx` (`RatingId`),
  CONSTRAINT `FK_Dispute_Rating` FOREIGN KEY (`RatingId`) REFERENCES `Rating` (`RatingId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Dispute`
--

LOCK TABLES `Dispute` WRITE;
/*!40000 ALTER TABLE `Dispute` DISABLE KEYS */;
/*!40000 ALTER TABLE `Dispute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Image`
--

DROP TABLE IF EXISTS `Image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Image` (
  `ImageId` int(11) NOT NULL auto_increment,
  `Source` varchar(255) NOT NULL,
  `Name` varchar(100) default NULL,
  `MimeType` varchar(50) default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ImageId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Image`
--

LOCK TABLES `Image` WRITE;
/*!40000 ALTER TABLE `Image` DISABLE KEYS */;
INSERT INTO `Image` VALUES (1,'http://wsamarketplace.com/wp-content/themes/classifiedstheme/thumbs/no-image.jpg',NULL,NULL,'');
INSERT INTO `Image` VALUES (2,'~/Content/Images/FeaturedYard/bcc3f20d-0f62-4285-bacf-2a092c64c7a7.jpg','bcc3f20d-0f62-4285-bacf-2a092c64c7a7','.jpg','');
/*!40000 ALTER TABLE `Image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payee`
--

DROP TABLE IF EXISTS `Payee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Payee` (
  `PayeeId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `DepositVia` varchar(50) NOT NULL,
  `PayTo` varchar(50) default NULL,
  `AddressId` int(11) default NULL,
  `DaytimePhone` varchar(20) default NULL,
  `EveningPhone` varchar(20) default NULL,
  `Fax` varchar(20) default NULL,
  `AccountType` varchar(50) default NULL,
  `BusinessOrPersonal` varchar(50) default NULL,
  `NameOnAccount` varchar(50) default NULL,
  `AccountNumber` varchar(75) default NULL,
  `RoutingNumber` varchar(45) default NULL,
  `PayPalEmail` varchar(75) default NULL,
  `IsActive` bit(1) default b'1',
  PRIMARY KEY  (`PayeeId`),
  KEY `FK_Payee_Contractor_idx` (`ContractorId`),
  KEY `FK_Payee_Address_idx` (`AddressId`),
  CONSTRAINT `FK_Payee_Address` FOREIGN KEY (`AddressId`) REFERENCES `Address` (`AddressId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_Payee_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payee`
--

LOCK TABLES `Payee` WRITE;
/*!40000 ALTER TABLE `Payee` DISABLE KEYS */;
/*!40000 ALTER TABLE `Payee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Payment`
--

DROP TABLE IF EXISTS `Payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Payment` (
  `PaymentId` int(11) NOT NULL auto_increment,
  `RequestedServiceId` int(11) NOT NULL,
  `DateSubmitted` datetime NOT NULL,
  `TransactionId` varchar(64) NOT NULL,
  `Amount` decimal(16,2) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `IsCompleted` bit(1) NOT NULL default b'0',
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`PaymentId`),
  KEY `FK_Payment_RequestedService_idx` (`RequestedServiceId`),
  CONSTRAINT `FK_Payment_RequestedService` FOREIGN KEY (`RequestedServiceId`) REFERENCES `RequestedService` (`RequestedServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Payment`
--

LOCK TABLES `Payment` WRITE;
/*!40000 ALTER TABLE `Payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `Payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PaymentAdjustment`
--

DROP TABLE IF EXISTS `PaymentAdjustment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaymentAdjustment` (
  `PaymentAdjustmentId` int(11) NOT NULL auto_increment,
  `RequestedServiceId` int(11) NOT NULL,
  `DateSubmitted` datetime default NULL,
  `TransactionId` varchar(50) default NULL,
  `Amount` decimal(16,2) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `Email` varchar(75) default NULL,
  `IsCompleted` bit(1) NOT NULL default b'0',
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`PaymentAdjustmentId`),
  KEY `FK_PaymentAdjustment_RequestedService_idx` (`RequestedServiceId`),
  CONSTRAINT `FK_PaymentAdjustment_RequestedService` FOREIGN KEY (`RequestedServiceId`) REFERENCES `RequestedService` (`RequestedServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PaymentAdjustment`
--

LOCK TABLES `PaymentAdjustment` WRITE;
/*!40000 ALTER TABLE `PaymentAdjustment` DISABLE KEYS */;
/*!40000 ALTER TABLE `PaymentAdjustment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Qualification`
--

DROP TABLE IF EXISTS `Qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Qualification` (
  `QualificationId` int(11) NOT NULL auto_increment,
  `Citizenship` varchar(50) default NULL,
  `FilingStatus` varchar(50) default NULL,
  `BusinessOwner` varchar(50) default NULL,
  `EIN` varchar(10) default NULL,
  `TaxId` varchar(50) default NULL,
  `AddressId` int(11) default NULL,
  `TaxExemption` varchar(200) default NULL,
  `Source` varchar(500) NOT NULL,
  `FileName` varchar(75) default NULL,
  `MimeType` varchar(50) default NULL,
  `DateSubmitted` datetime NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`QualificationId`),
  KEY `FK_Qualification_Address_idx` (`AddressId`),
  CONSTRAINT `FK_Qualification_Address` FOREIGN KEY (`AddressId`) REFERENCES `Address` (`AddressId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Qualification`
--

LOCK TABLES `Qualification` WRITE;
/*!40000 ALTER TABLE `Qualification` DISABLE KEYS */;
INSERT INTO `Qualification` VALUES (3,'U.S. person (including U.S. resident alien)','Sole Proprietor','Scott Adams','46-4705704',NULL,10,NULL,'~/Content/Qualifications/acc888be-3399-4d87-b0e1-b0e7c818a20f.jpg','acc888be-3399-4d87-b0e1-b0e7c818a20f','.jpg','2015-07-21 17:39:10','');
INSERT INTO `Qualification` VALUES (4,'U.S. person (including U.S. resident alien)','Sole Proprietor','Scott Adams','46-4705704',NULL,17,NULL,'~/Content/Qualifications/9576f0a9-c520-4746-a807-0eeebca50d2d.jpeg','9576f0a9-c520-4746-a807-0eeebca50d2d','.jpeg','2016-01-22 14:02:27','');
INSERT INTO `Qualification` VALUES (5,'U.S. person (including U.S. resident alien)','Individual','Test','10-1122334','456-12-1212',18,'Corporation','~/Content/Qualifications/ebe40b6b-bd6e-4795-90c7-4c88242115b0.png','ebe40b6b-bd6e-4795-90c7-4c88242115b0','.png','2016-01-25 09:38:57','');
INSERT INTO `Qualification` VALUES (6,'U.S. person (including U.S. resident alien)','Individual','test','00-0000000','000-00-0000',30,NULL,'~/Content/Qualifications/043c5393-cc48-4205-a0cd-a1683b819d4d.png','043c5393-cc48-4205-a0cd-a1683b819d4d','.png','2016-02-08 13:35:53','');
/*!40000 ALTER TABLE `Qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Question`
--

DROP TABLE IF EXISTS `Question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Question` (
  `QuestionId` int(11) NOT NULL auto_increment,
  `Text` varchar(250) NOT NULL,
  `QuestionTypeId` int(11) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`QuestionId`),
  KEY `FK_Question_QuestionTypeId_idx` (`QuestionTypeId`),
  CONSTRAINT `FK_Question_QuestionType` FOREIGN KEY (`QuestionTypeId`) REFERENCES `QuestionType` (`QuestionTypeId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Question`
--

LOCK TABLES `Question` WRITE;
/*!40000 ALTER TABLE `Question` DISABLE KEYS */;
/*!40000 ALTER TABLE `Question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `QuestionType`
--

DROP TABLE IF EXISTS `QuestionType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `QuestionType` (
  `QuestionTypeId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`QuestionTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `QuestionType`
--

LOCK TABLES `QuestionType` WRITE;
/*!40000 ALTER TABLE `QuestionType` DISABLE KEYS */;
/*!40000 ALTER TABLE `QuestionType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rating`
--

DROP TABLE IF EXISTS `Rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Rating` (
  `RatingId` int(11) NOT NULL auto_increment,
  `UserId` int(11) NOT NULL,
  `ContractorId` int(11) NOT NULL,
  `Value` decimal(3,2) NOT NULL,
  `Comments` varchar(160) NOT NULL,
  `DateRated` datetime NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`RatingId`),
  KEY `FK_Rating_User_idx` (`UserId`),
  KEY `FK_Rating_Contractor_idx` (`ContractorId`),
  CONSTRAINT `FK_Rating_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_Rating_User` FOREIGN KEY (`UserId`) REFERENCES `UserProfiles` (`UserId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rating`
--

LOCK TABLES `Rating` WRITE;
/*!40000 ALTER TABLE `Rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `Rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RequestedService`
--

DROP TABLE IF EXISTS `RequestedService`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RequestedService` (
  `RequestedServiceId` int(11) NOT NULL auto_increment,
  `UserId` int(11) NOT NULL,
  `ContractorId` int(11) NOT NULL,
  `ContractorServiceId` int(11) NOT NULL,
  `DateRequestMade` datetime NOT NULL,
  `RequestedStartDate` datetime default NULL,
  `RequestedEndDate` datetime default NULL,
  `AddressId` int(11) NOT NULL,
  `Recurring` int(11) NOT NULL,
  `ContractorComment` varchar(255) default NULL,
  `AdminComment` varchar(255) default NULL,
  `IsCompleted` bit(1) NOT NULL default b'0',
  `DateCompleted` datetime default NULL,
  `RatingId` int(11) default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`RequestedServiceId`),
  KEY `FK_RequestedService_User_idx` (`UserId`),
  KEY `FK_RequestedService_Service_idx` (`ContractorServiceId`),
  KEY `FK_RequestedService_Contractor_idx` (`ContractorId`),
  KEY `FK_RequestedService_Rating_idx` (`RatingId`),
  CONSTRAINT `FK_RequestedService_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_RequestedService_ContractorService` FOREIGN KEY (`ContractorServiceId`) REFERENCES `ContractorService` (`ContractorServiceId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_RequestedService_Rating` FOREIGN KEY (`RatingId`) REFERENCES `Rating` (`RatingId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_RequestedService_User` FOREIGN KEY (`UserId`) REFERENCES `UserProfiles` (`UserId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RequestedService`
--

LOCK TABLES `RequestedService` WRITE;
/*!40000 ALTER TABLE `RequestedService` DISABLE KEYS */;
/*!40000 ALTER TABLE `RequestedService` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RequestedServiceOption`
--

DROP TABLE IF EXISTS `RequestedServiceOption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RequestedServiceOption` (
  `RequestedServiceOptionId` int(11) NOT NULL auto_increment,
  `RequestedServiceId` int(11) NOT NULL,
  `Order` int(11) default NULL,
  `Name` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL,
  PRIMARY KEY  (`RequestedServiceOptionId`),
  KEY `FK_RequestedServiceOption_RequestedService_idx` (`RequestedServiceId`),
  CONSTRAINT `FK_RequestedServiceOption_RequestedService` FOREIGN KEY (`RequestedServiceId`) REFERENCES `RequestedService` (`RequestedServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RequestedServiceOption`
--

LOCK TABLES `RequestedServiceOption` WRITE;
/*!40000 ALTER TABLE `RequestedServiceOption` DISABLE KEYS */;
/*!40000 ALTER TABLE `RequestedServiceOption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RequestedServiceOptionItem`
--

DROP TABLE IF EXISTS `RequestedServiceOptionItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RequestedServiceOptionItem` (
  `RequestedServiceOptionItemId` int(11) NOT NULL auto_increment,
  `RequestedServiceOptionId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`RequestedServiceOptionItemId`),
  KEY `FK_RequestedServiceOptionItem_RequestedServiceOption_idx` (`RequestedServiceOptionId`),
  CONSTRAINT `FK_RequestedServiceOptionItem_RequestedServiceOption` FOREIGN KEY (`RequestedServiceOptionId`) REFERENCES `RequestedServiceOption` (`RequestedServiceOptionId`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RequestedServiceOptionItem`
--

LOCK TABLES `RequestedServiceOptionItem` WRITE;
/*!40000 ALTER TABLE `RequestedServiceOptionItem` DISABLE KEYS */;
/*!40000 ALTER TABLE `RequestedServiceOptionItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Service`
--

DROP TABLE IF EXISTS `Service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Service` (
  `ServiceId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(2000) default NULL,
  `ServiceCategoryId` int(11) NOT NULL,
  `BasePrice` decimal(16,2) NOT NULL default '0.00',
  `DateCreated` datetime default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ServiceId`),
  KEY `FK_Service_ServiceCategory_idx` (`ServiceCategoryId`),
  CONSTRAINT `FK_Service_ServiceCategory` FOREIGN KEY (`ServiceCategoryId`) REFERENCES `ServiceCategory` (`ServiceCategoryId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Service`
--

LOCK TABLES `Service` WRITE;
/*!40000 ALTER TABLE `Service` DISABLE KEYS */;
INSERT INTO `Service` VALUES (1,'Bermuda Grass','Revitalize your lawn with a different breed of grass',1,45.00,'2016-01-20 14:02:24','');
INSERT INTO `Service` VALUES (2,'Grub Worms','All natural non-toxic deterrent for Grub Worms',4,25.00,'2016-01-20 14:04:12','');
INSERT INTO `Service` VALUES (3,'Irrigation System','Prepare your irrigation system for the winter.',2,75.00,'2016-01-20 14:07:59','');
INSERT INTO `Service` VALUES (4,'Irrigation','Winterize your sprinkler system',2,50.00,'2016-01-22 11:08:42','');
INSERT INTO `Service` VALUES (5,'Mowing','Cut and Trim lawn',6,20.00,'2016-01-22 12:23:29','');
INSERT INTO `Service` VALUES (6,'Snow Removal','Plow snow, clear parking lots, driveways, and sidewalks.',2,55.00,'2016-01-22 12:24:33','');
INSERT INTO `Service` VALUES (7,'Weed Eating','Cut down unsightly weeds.',3,15.00,'2016-01-22 12:25:27','\0');
INSERT INTO `Service` VALUES (9,'Hardscapes','Retaining walls, tree rings, fire pits, etc.',6,35.00,'2016-01-22 14:47:51','');
INSERT INTO `Service` VALUES (10,'Brick Walk Ways','Lay a beautiful brick walkway',6,0.00,'2016-02-04 12:42:17','');
INSERT INTO `Service` VALUES (11,'Swimming Pool','Install a swimming pool',6,0.00,'2016-02-09 11:05:33','');
/*!40000 ALTER TABLE `Service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceArea`
--

DROP TABLE IF EXISTS `ServiceArea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceArea` (
  `ServiceAreaId` int(11) NOT NULL auto_increment,
  `StateId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ServiceAreaId`),
  KEY `FK_ServiceArea_State_idx` (`StateId`),
  CONSTRAINT `FK_ServiceArea_State` FOREIGN KEY (`StateId`) REFERENCES `State` (`StateId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceArea`
--

LOCK TABLES `ServiceArea` WRITE;
/*!40000 ALTER TABLE `ServiceArea` DISABLE KEYS */;
INSERT INTO `ServiceArea` VALUES (1,127,'Downtown Kansas City','');
INSERT INTO `ServiceArea` VALUES (2,118,'Olathe','');
INSERT INTO `ServiceArea` VALUES (3,118,'Overland Park','');
INSERT INTO `ServiceArea` VALUES (4,127,'St. Joseph','');
INSERT INTO `ServiceArea` VALUES (5,127,'Savannah','');
INSERT INTO `ServiceArea` VALUES (6,127,'Liberty','');
/*!40000 ALTER TABLE `ServiceArea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceCategory`
--

DROP TABLE IF EXISTS `ServiceCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceCategory` (
  `ServiceCategoryId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ServiceCategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceCategory`
--

LOCK TABLES `ServiceCategory` WRITE;
/*!40000 ALTER TABLE `ServiceCategory` DISABLE KEYS */;
INSERT INTO `ServiceCategory` VALUES (1,'Seeding','');
INSERT INTO `ServiceCategory` VALUES (2,'Winterizing','');
INSERT INTO `ServiceCategory` VALUES (3,'Weed Control','');
INSERT INTO `ServiceCategory` VALUES (4,'Pest Control','');
INSERT INTO `ServiceCategory` VALUES (5,'Fertilizing','');
INSERT INTO `ServiceCategory` VALUES (6,'Landscaping','');
/*!40000 ALTER TABLE `ServiceCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceOption`
--

DROP TABLE IF EXISTS `ServiceOption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceOption` (
  `ServiceOptionId` int(11) NOT NULL auto_increment,
  `ServiceId` int(11) NOT NULL,
  `Order` int(11) default NULL,
  `Name` varchar(50) NOT NULL,
  `DateCreated` datetime default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ServiceOptionId`),
  KEY `FK_ServiceOption_Service_idx` (`ServiceId`),
  CONSTRAINT `FK_ServiceOption_Service` FOREIGN KEY (`ServiceId`) REFERENCES `Service` (`ServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceOption`
--

LOCK TABLES `ServiceOption` WRITE;
/*!40000 ALTER TABLE `ServiceOption` DISABLE KEYS */;
INSERT INTO `ServiceOption` VALUES (1,9,1,'Fire Pits','2016-01-25 10:16:56','');
INSERT INTO `ServiceOption` VALUES (2,1,1,'GMO Free','2016-01-29 11:31:23','');
INSERT INTO `ServiceOption` VALUES (3,10,1,'Red Brick','2016-02-04 12:43:03','');
INSERT INTO `ServiceOption` VALUES (4,10,2,'Flat Stones','2016-02-04 12:43:03','');
INSERT INTO `ServiceOption` VALUES (5,11,1,'Hot Tub','2016-02-09 11:05:51','');
/*!40000 ALTER TABLE `ServiceOption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceOptionItem`
--

DROP TABLE IF EXISTS `ServiceOptionItem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceOptionItem` (
  `ServiceOptionItemId` int(11) NOT NULL auto_increment,
  `ServiceOptionId` int(11) NOT NULL,
  `Order` int(11) default NULL,
  `Name` varchar(125) NOT NULL,
  `Price` decimal(16,2) NOT NULL,
  `DateCreated` datetime default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ServiceOptionItemId`),
  KEY `FK_OptionItem_Option_idx` (`ServiceOptionId`),
  CONSTRAINT `FK_ServiceOptionItem_ServiceOption` FOREIGN KEY (`ServiceOptionId`) REFERENCES `ServiceOption` (`ServiceOptionId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceOptionItem`
--

LOCK TABLES `ServiceOptionItem` WRITE;
/*!40000 ALTER TABLE `ServiceOptionItem` DISABLE KEYS */;
INSERT INTO `ServiceOptionItem` VALUES (3,1,1,'Small Stone Circle',65.00,'2016-01-29 11:34:23','');
INSERT INTO `ServiceOptionItem` VALUES (4,1,2,'Small Red Brick Circle',0.00,'2016-01-29 11:34:23','');
INSERT INTO `ServiceOptionItem` VALUES (5,1,3,'Medium Red Brick Circle',0.00,'2016-01-29 11:34:23','');
INSERT INTO `ServiceOptionItem` VALUES (6,3,1,'Marble Center Piece',10.00,'2016-02-08 09:08:29','');
INSERT INTO `ServiceOptionItem` VALUES (7,2,1,'Brand 1',50.00,'2016-02-10 11:19:25','');
INSERT INTO `ServiceOptionItem` VALUES (8,2,2,'Brand 2',60.00,'2016-02-10 11:19:25','');
INSERT INTO `ServiceOptionItem` VALUES (9,2,3,'Brand 3',20.00,'2016-02-10 11:19:25','');
/*!40000 ALTER TABLE `ServiceOptionItem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceOptionItemRequest`
--

DROP TABLE IF EXISTS `ServiceOptionItemRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceOptionItemRequest` (
  `ServiceOptionItemRequestId` int(11) NOT NULL auto_increment,
  `ServiceOptionRequestId` int(11) default NULL,
  `ContractorId` int(11) NOT NULL,
  `Order` int(11) default NULL,
  `ServiceName` varchar(50) default NULL,
  `OptionName` varchar(50) default NULL,
  `Name` varchar(50) NOT NULL,
  `Price` decimal(16,2) NOT NULL default '0.00',
  `DateRequested` datetime NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`ServiceOptionItemRequestId`),
  KEY `FK_ServiceOptionItemRequest_Contractor_idx` (`ContractorId`),
  KEY `FK_ServiceOptionItemRequest_ServiceOptionId_idx` (`ServiceOptionRequestId`),
  CONSTRAINT `FK_ServiceOptionItemRequest_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_ServiceOptionItemRequest_ServiceOptionId` FOREIGN KEY (`ServiceOptionRequestId`) REFERENCES `ServiceOptionRequest` (`ServiceOptionRequestId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceOptionItemRequest`
--

LOCK TABLES `ServiceOptionItemRequest` WRITE;
/*!40000 ALTER TABLE `ServiceOptionItemRequest` DISABLE KEYS */;
INSERT INTO `ServiceOptionItemRequest` VALUES (1,1,5,1,NULL,NULL,'Fire Pit',165.00,'2016-01-25 10:16:05','');
/*!40000 ALTER TABLE `ServiceOptionItemRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ServiceOptionRequest`
--

DROP TABLE IF EXISTS `ServiceOptionRequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ServiceOptionRequest` (
  `ServiceOptionRequestId` int(11) NOT NULL auto_increment,
  `ContractorId` int(11) NOT NULL,
  `ServiceId` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `DateRequested` datetime NOT NULL,
  `IsApproved` bit(1) NOT NULL default b'0',
  `IsActive` bit(1) NOT NULL,
  PRIMARY KEY  (`ServiceOptionRequestId`),
  KEY `FK_ServiceOptionRequest_Contractor_idx` (`ContractorId`),
  KEY `FK_ServiceOptionRequest_Service_idx` (`ServiceId`),
  CONSTRAINT `FK_ServiceOptionRequest_Contractor` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_ServiceOptionRequest_Service` FOREIGN KEY (`ServiceId`) REFERENCES `Service` (`ServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ServiceOptionRequest`
--

LOCK TABLES `ServiceOptionRequest` WRITE;
/*!40000 ALTER TABLE `ServiceOptionRequest` DISABLE KEYS */;
INSERT INTO `ServiceOptionRequest` VALUES (1,5,9,'Fire Pit','2016-01-25 10:16:05','\0','');
/*!40000 ALTER TABLE `ServiceOptionRequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SiteSetting`
--

DROP TABLE IF EXISTS `SiteSetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SiteSetting` (
  `SiteSettingId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) NOT NULL,
  `Value` varchar(100) NOT NULL,
  PRIMARY KEY  (`SiteSettingId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SiteSetting`
--

LOCK TABLES `SiteSetting` WRITE;
/*!40000 ALTER TABLE `SiteSetting` DISABLE KEYS */;
INSERT INTO `SiteSetting` VALUES (1,'Yard Lad Contact Email','test@yardlad.com');
INSERT INTO `SiteSetting` VALUES (2,'Yard Lad Phone Number','(913) 871-9327');
INSERT INTO `SiteSetting` VALUES (3,'Yard Lad Address','1600 Genessee St, Suite 822, Kansas City, Missouri 64102');
INSERT INTO `SiteSetting` VALUES (4,'Business Professional Yearly Fee','250.00');
INSERT INTO `SiteSetting` VALUES (5,'Freelance Contractor Yearly Fee','100.00');
INSERT INTO `SiteSetting` VALUES (6,'Student Contractor Yearly Fee','50.00');
INSERT INTO `SiteSetting` VALUES (7,'Yard Lad Contact Email (Login)','default');
INSERT INTO `SiteSetting` VALUES (8,'Yard Lad Contact Email (Password)','default');
INSERT INTO `SiteSetting` VALUES (9,'Yard Lad Fax Number','913-555-6789');
/*!40000 ALTER TABLE `SiteSetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `State`
--

DROP TABLE IF EXISTS `State`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `State` (
  `StateId` int(11) NOT NULL auto_increment,
  `Name` varchar(50) NOT NULL,
  `Abbreviation` varchar(5) NOT NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`StateId`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `State`
--

LOCK TABLES `State` WRITE;
/*!40000 ALTER TABLE `State` DISABLE KEYS */;
INSERT INTO `State` VALUES (102,'Alabama','AL','');
INSERT INTO `State` VALUES (103,'Alaska','AK','');
INSERT INTO `State` VALUES (104,'Arizona','AZ','');
INSERT INTO `State` VALUES (105,'Arkansas','AR','');
INSERT INTO `State` VALUES (106,'California','CA','');
INSERT INTO `State` VALUES (107,'Colorado','CO','');
INSERT INTO `State` VALUES (108,'Connecticut','CT','');
INSERT INTO `State` VALUES (109,'Delaware','DE','');
INSERT INTO `State` VALUES (110,'District of Columbia','DC','');
INSERT INTO `State` VALUES (111,'Florida','FL','');
INSERT INTO `State` VALUES (112,'Georgia','GA','');
INSERT INTO `State` VALUES (113,'Hawaii','HI','');
INSERT INTO `State` VALUES (114,'Idaho','ID','');
INSERT INTO `State` VALUES (115,'Illinois','IL','');
INSERT INTO `State` VALUES (116,'Indiana','IN','');
INSERT INTO `State` VALUES (117,'Iowa','IA','');
INSERT INTO `State` VALUES (118,'Kansas','KS','');
INSERT INTO `State` VALUES (119,'Kentucky','KY','');
INSERT INTO `State` VALUES (120,'Louisiana','LA','');
INSERT INTO `State` VALUES (121,'Maine','ME','');
INSERT INTO `State` VALUES (122,'Maryland','MD','');
INSERT INTO `State` VALUES (123,'Massachusetts','MA','');
INSERT INTO `State` VALUES (124,'Michigan','MI','');
INSERT INTO `State` VALUES (125,'Minnesota','MN','');
INSERT INTO `State` VALUES (126,'Mississippi','MS','');
INSERT INTO `State` VALUES (127,'Missouri','MO','');
INSERT INTO `State` VALUES (128,'Montana','MT','');
INSERT INTO `State` VALUES (129,'Nebraska','NE','');
INSERT INTO `State` VALUES (130,'Nevada','NV','');
INSERT INTO `State` VALUES (131,'New Hampshire','NH','');
INSERT INTO `State` VALUES (132,'New Jersey','NJ','');
INSERT INTO `State` VALUES (133,'New Mexico','NM','');
INSERT INTO `State` VALUES (134,'New York','NY','');
INSERT INTO `State` VALUES (135,'North Carolina','NC','');
INSERT INTO `State` VALUES (136,'North Dakota','ND','');
INSERT INTO `State` VALUES (137,'Ohio','OH','');
INSERT INTO `State` VALUES (138,'Oklahoma','OK','');
INSERT INTO `State` VALUES (139,'Oregon','OR','');
INSERT INTO `State` VALUES (140,'Pennsylvania','PA','');
INSERT INTO `State` VALUES (141,'Rhode Island','RI','');
INSERT INTO `State` VALUES (142,'South Carolina','SC','');
INSERT INTO `State` VALUES (143,'South Dakota','SD','');
INSERT INTO `State` VALUES (144,'Tennessee','TN','');
INSERT INTO `State` VALUES (145,'Texas','TX','');
INSERT INTO `State` VALUES (146,'Utah','UT','');
INSERT INTO `State` VALUES (147,'Vermont','VT','');
INSERT INTO `State` VALUES (148,'Virginia','VA','');
INSERT INTO `State` VALUES (149,'Washington','WA','');
INSERT INTO `State` VALUES (150,'West Virginia','WV','');
INSERT INTO `State` VALUES (151,'Wisconsin','WI','');
INSERT INTO `State` VALUES (152,'Wyoming','WY','');
INSERT INTO `State` VALUES (153,'Istanbul','IBUL','\0');
/*!40000 ALTER TABLE `State` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Survey`
--

DROP TABLE IF EXISTS `Survey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Survey` (
  `SurveyId` int(11) NOT NULL auto_increment,
  `UserId` int(11) NOT NULL,
  `RequestedServiceId` int(11) NOT NULL,
  `IsCompleted` bit(1) NOT NULL default b'0',
  `DateCompleted` datetime default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`SurveyId`),
  KEY `FK_Survey_my_aspnet_users_idx` (`UserId`),
  KEY `FK_Survey_RequestedService_idx` (`RequestedServiceId`),
  CONSTRAINT `FK_Survey_my_aspnet_users` FOREIGN KEY (`UserId`) REFERENCES `my_aspnet_users` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_Survey_RequestedService` FOREIGN KEY (`RequestedServiceId`) REFERENCES `RequestedService` (`RequestedServiceId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Survey`
--

LOCK TABLES `Survey` WRITE;
/*!40000 ALTER TABLE `Survey` DISABLE KEYS */;
/*!40000 ALTER TABLE `Survey` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SurveyQuestion`
--

DROP TABLE IF EXISTS `SurveyQuestion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SurveyQuestion` (
  `SurveyQuestionId` int(11) NOT NULL auto_increment,
  `SurveyId` int(11) NOT NULL,
  `QuestionId` int(11) NOT NULL,
  `Answer` varchar(250) default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`SurveyQuestionId`),
  KEY `FK_SurveyQuestion_Survey_idx` (`SurveyId`),
  KEY `FK_SurveyQuestion_Question_idx` (`QuestionId`),
  CONSTRAINT `FK_SurveyQuestion_Question` FOREIGN KEY (`QuestionId`) REFERENCES `Question` (`QuestionId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_SurveyQuestion_Survey` FOREIGN KEY (`SurveyId`) REFERENCES `Survey` (`SurveyId`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SurveyQuestion`
--

LOCK TABLES `SurveyQuestion` WRITE;
/*!40000 ALTER TABLE `SurveyQuestion` DISABLE KEYS */;
/*!40000 ALTER TABLE `SurveyQuestion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tax`
--

DROP TABLE IF EXISTS `Tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tax` (
  `TaxId` int(11) NOT NULL auto_increment,
  `AddressId` int(11) NOT NULL,
  `ContractorId` int(11) NOT NULL,
  `Rate` decimal(16,2) default '0.00',
  `SetByContractor` bit(1) default b'0',
  PRIMARY KEY  (`TaxId`),
  KEY `FK_Tax_Address_idx` (`AddressId`),
  KEY `FK_Tax_ContractorId_idx` (`ContractorId`),
  CONSTRAINT `FK_Tax_Address` FOREIGN KEY (`AddressId`) REFERENCES `Address` (`AddressId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_Tax_ContractorId` FOREIGN KEY (`ContractorId`) REFERENCES `Contractor` (`ContractorId`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tax`
--

LOCK TABLES `Tax` WRITE;
/*!40000 ALTER TABLE `Tax` DISABLE KEYS */;
INSERT INTO `Tax` VALUES (3,8,3,8.75,NULL);
INSERT INTO `Tax` VALUES (4,9,4,NULL,'');
INSERT INTO `Tax` VALUES (5,11,5,10.00,'');
INSERT INTO `Tax` VALUES (7,16,8,8.75,NULL);
INSERT INTO `Tax` VALUES (8,19,9,8.75,NULL);
INSERT INTO `Tax` VALUES (9,29,10,8.75,NULL);
INSERT INTO `Tax` VALUES (10,31,11,8.75,'');
INSERT INTO `Tax` VALUES (11,32,12,8.75,NULL);
INSERT INTO `Tax` VALUES (12,33,13,8.75,NULL);
INSERT INTO `Tax` VALUES (13,34,14,8.75,NULL);
/*!40000 ALTER TABLE `Tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserProfiles`
--

DROP TABLE IF EXISTS `UserProfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserProfiles` (
  `UserId` int(11) NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `DateOfBirth` datetime default NULL,
  `Phone` varchar(20) NOT NULL,
  `Mobile` varchar(20) default NULL,
  `Gender` varchar(6) NOT NULL,
  `AddressId` int(11) default NULL,
  `AcceptSMS` bit(1) NOT NULL,
  `LastLoginDate` datetime default NULL,
  `IsActive` bit(1) NOT NULL default b'1',
  PRIMARY KEY  (`UserId`),
  KEY `FK_User_Address_idx` (`AddressId`),
  CONSTRAINT `FK_UserProfiles_Address` FOREIGN KEY (`AddressId`) REFERENCES `Address` (`AddressId`) ON UPDATE NO ACTION,
  CONSTRAINT `FK_UserProfiles_my_aspnet_users` FOREIGN KEY (`UserId`) REFERENCES `my_aspnet_users` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserProfiles`
--

LOCK TABLES `UserProfiles` WRITE;
/*!40000 ALTER TABLE `UserProfiles` DISABLE KEYS */;
INSERT INTO `UserProfiles` VALUES (1,'Yard','Lad','0001-01-01 00:00:00','(913) 555-6789','(913) 555-6789','male',NULL,'','2016-02-10 13:15:13','');
INSERT INTO `UserProfiles` VALUES (2,'John','Smith','0001-01-01 00:00:00','(913) 555-6789','(913) 555-6789','male',8,'','2016-01-22 14:31:14','');
INSERT INTO `UserProfiles` VALUES (3,'Scott','Adams','1991-10-12 00:00:00','(816) 752-1525','(816) 752-1525','male',9,'','2015-07-20 15:46:39','');
INSERT INTO `UserProfiles` VALUES (4,'David','Thomas','1945-01-20 00:00:00','(816) 830-8407','(555) 555-5555','male',11,'','2016-02-10 13:12:01','');
INSERT INTO `UserProfiles` VALUES (6,'Logan','McAlister','1991-05-23 00:00:00','(816) 914-1596','(816) 914-1596','male',20,'','2016-02-02 15:48:21','');
INSERT INTO `UserProfiles` VALUES (11,'Mr.','Test','1953-01-22 00:00:00','(777) 777-7777',NULL,'male',12,'\0','2016-01-13 14:02:08','');
INSERT INTO `UserProfiles` VALUES (16,'Testing','Tester','1993-01-20 00:00:00','(987) 654-3210',NULL,'male',NULL,'\0','2016-01-15 11:17:18','');
INSERT INTO `UserProfiles` VALUES (17,'Strapford','Williams','1982-01-13 00:00:00','(913) 321-2277',NULL,'male',27,'\0','2016-01-18 09:18:19','');
INSERT INTO `UserProfiles` VALUES (18,'Ryu','Kayo','1989-01-01 00:00:00','(888) 888-8888',NULL,'male',15,'\0','2016-02-04 10:43:31','');
INSERT INTO `UserProfiles` VALUES (19,'winton','kane','1989-01-30 00:00:00','(913) 200-6106','(913) 200-6106','male',NULL,'','2016-01-19 12:30:06','');
INSERT INTO `UserProfiles` VALUES (20,'winton','kane','1989-01-30 00:00:00','(913) 200-6106','(913) 200-6106','male',NULL,'','2016-01-19 12:31:42','');
INSERT INTO `UserProfiles` VALUES (21,'testing','tester','1989-01-01 00:00:00','(000) 000-0000','(000) 000-0000','male',NULL,'','2016-01-22 12:26:55','');
INSERT INTO `UserProfiles` VALUES (22,'testing','tester','1989-01-04 00:00:00','(000) 000-0000','(000) 000-0000','male',NULL,'','2016-01-22 13:32:12','');
INSERT INTO `UserProfiles` VALUES (23,'Testing','Tester','1989-02-01 00:00:00','(000) 000-0000','(000) 000-0000','male',NULL,'','2016-01-22 14:30:23','');
INSERT INTO `UserProfiles` VALUES (24,'Scott','Adams','1991-10-12 00:00:00','(816) 752-1525','(816) 752-1525','male',16,'','2016-01-22 14:34:53','');
INSERT INTO `UserProfiles` VALUES (25,'Cory','Webb','1976-01-18 00:00:00','(913) 940-1411','(913) 940-1411','female',NULL,'','2016-01-22 14:28:56','');
INSERT INTO `UserProfiles` VALUES (26,'Tim','Timothy','1951-01-17 00:00:00','(000) 000-0000','(000) 000-0000','male',NULL,'','2016-01-29 10:42:07','');
INSERT INTO `UserProfiles` VALUES (27,'billy','bob','1932-01-13 00:00:00','(000) 000-0000','(000) 000-0000','female',NULL,'','2016-01-29 10:46:15','');
INSERT INTO `UserProfiles` VALUES (28,'mona','lisa','1926-01-13 00:00:00','(000) 000-0000','(000) 000-0000','female',NULL,'','2016-01-29 10:47:50','');
INSERT INTO `UserProfiles` VALUES (29,'Cool','Student','1998-01-29 00:00:00','(000) 000-0000','(000) 000-0000','male',19,'','2016-01-29 12:32:55','');
INSERT INTO `UserProfiles` VALUES (30,'ManageUser','EmailRedirect','1952-02-01 00:00:00','(000) 000-0000','(000) 000-0000','female',NULL,'','2016-02-01 14:05:01','');
INSERT INTO `UserProfiles` VALUES (34,'Testing','Tester','1918-02-01 00:00:00','(000) 000-0000',NULL,'female',22,'\0','2016-02-02 12:38:49','');
INSERT INTO `UserProfiles` VALUES (35,'Spencer','Davis','1993-09-01 00:00:00','(123) 456-7890','(098) 765-4321','male',21,'\0','2016-02-01 16:47:04','');
INSERT INTO `UserProfiles` VALUES (36,'Cool','Man','1989-02-01 00:00:00','(123) 456-7899','(122) 345-6789','male',23,'\0','2016-02-02 16:05:43','');
INSERT INTO `UserProfiles` VALUES (37,'Edit','Address Check Null','1986-02-01 00:00:00','(000) 000-0000',NULL,'female',25,'\0','2016-02-02 12:55:32','');
INSERT INTO `UserProfiles` VALUES (38,'Add Address','Address Check Null','1981-02-05 00:00:00','(000) 000-0000',NULL,'female',28,'\0','2016-02-03 14:24:57','');
INSERT INTO `UserProfiles` VALUES (39,'Spencer','Davis','1972-02-02 00:00:00','(765) 098-1234',NULL,'male',26,'\0','2016-02-02 16:47:02','');
INSERT INTO `UserProfiles` VALUES (40,'Finn','McAlister','1991-05-08 00:00:00','(816) 914-1596','(816) 914-1596','male',NULL,'','2016-02-02 15:54:49','');
INSERT INTO `UserProfiles` VALUES (41,'email','test','1916-02-03 00:00:00','(111) 111-1111',NULL,'male',NULL,'\0','2016-02-03 10:21:53','');
INSERT INTO `UserProfiles` VALUES (53,'american','dad','1916-02-08 00:00:00','(000) 000-0000',NULL,'male',NULL,'\0','2016-02-08 13:36:26','');
INSERT INTO `UserProfiles` VALUES (55,'cowboy','bebop','1916-02-08 00:00:00','(000) 000-0000',NULL,'female',NULL,'\0','2016-02-08 13:43:21','');
INSERT INTO `UserProfiles` VALUES (56,'cowboy','bebop','1916-02-09 00:00:00','(000) 000-0000',NULL,'male',NULL,'\0','2016-02-08 12:51:07','');
INSERT INTO `UserProfiles` VALUES (57,'winton','kane','1998-02-08 00:00:00','(000) 000-0000',NULL,'male',29,'\0','2016-02-10 11:16:07','');
INSERT INTO `UserProfiles` VALUES (58,'contractor','test','1917-02-01 00:00:00','(000) 000-0000','(000) 000-0000','male',31,'','2016-02-08 14:55:53','');
INSERT INTO `UserProfiles` VALUES (59,'email','test','1961-02-09 00:00:00','(000) 000-0000',NULL,'female',NULL,'\0','2016-02-08 14:40:04','');
INSERT INTO `UserProfiles` VALUES (60,'testing','tester','1980-02-07 00:00:00','(000) 000-0000',NULL,'male',32,'\0','2016-02-09 10:43:06','');
INSERT INTO `UserProfiles` VALUES (61,'Test','Tester','1947-02-13 00:00:00','(000) 000-0000',NULL,'male',33,'\0','2016-02-09 11:00:06','');
INSERT INTO `UserProfiles` VALUES (62,'testing','tester','1932-02-10 00:00:00','(000) 000-0000',NULL,'male',34,'\0','2016-02-09 10:42:57','');
INSERT INTO `UserProfiles` VALUES (63,'case','summaries','1950-02-22 00:00:00','(000) 000-0000','(000) 000-0000','male',NULL,'\0','2016-02-10 12:55:07','');
/*!40000 ALTER TABLE `UserProfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL auto_increment,
  `active` bit(1) NOT NULL,
  `first_name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `username` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `phone_number` varchar(255) default NULL,
  `sms_from_number` varchar(255) default NULL,
  `created_dt` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log4Net`
--

DROP TABLE IF EXISTS `log4Net`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log4Net` (
  `Id` int(11) NOT NULL auto_increment,
  `Date` datetime default NULL,
  `Thread` varchar(255) default NULL,
  `Level` varchar(50) default NULL,
  `Logger` varchar(255) default NULL,
  `Message` varchar(4000) default NULL,
  `Exception` varchar(2000) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log4Net`
--

LOCK TABLES `log4Net` WRITE;
/*!40000 ALTER TABLE `log4Net` DISABLE KEYS */;
/*!40000 ALTER TABLE `log4Net` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_applications`
--

DROP TABLE IF EXISTS `my_aspnet_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_applications` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(256) default NULL,
  `description` varchar(256) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_applications`
--

LOCK TABLES `my_aspnet_applications` WRITE;
/*!40000 ALTER TABLE `my_aspnet_applications` DISABLE KEYS */;
INSERT INTO `my_aspnet_applications` VALUES (1,'YardLad',NULL);
/*!40000 ALTER TABLE `my_aspnet_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_membership`
--

DROP TABLE IF EXISTS `my_aspnet_membership`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_membership` (
  `userId` int(11) NOT NULL default '0',
  `Email` varchar(128) default NULL,
  `Comment` varchar(255) default NULL,
  `Password` varchar(128) NOT NULL,
  `PasswordKey` char(32) default NULL,
  `PasswordFormat` tinyint(4) default NULL,
  `PasswordQuestion` varchar(255) default NULL,
  `PasswordAnswer` varchar(255) default NULL,
  `IsApproved` tinyint(1) default NULL,
  `LastActivityDate` datetime default NULL,
  `LastLoginDate` datetime default NULL,
  `LastPasswordChangedDate` datetime default NULL,
  `CreationDate` datetime default NULL,
  `IsLockedOut` tinyint(1) default NULL,
  `LastLockedOutDate` datetime default NULL,
  `FailedPasswordAttemptCount` int(10) unsigned default NULL,
  `FailedPasswordAttemptWindowStart` datetime default NULL,
  `FailedPasswordAnswerAttemptCount` int(10) unsigned default NULL,
  `FailedPasswordAnswerAttemptWindowStart` datetime default NULL,
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='2';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_membership`
--

LOCK TABLES `my_aspnet_membership` WRITE;
/*!40000 ALTER TABLE `my_aspnet_membership` DISABLE KEYS */;
INSERT INTO `my_aspnet_membership` VALUES (1,'admin@yardlad.com','','2LMtM8/ingGv5JEwMhfkROVadDYHuXnCJEwIpk+tJIo=','AL6vV9vQPfEeafwN1IF5iQ==',1,NULL,NULL,1,'2016-02-10 11:15:12','2016-02-10 11:13:54','2015-06-27 19:55:31','2015-06-27 19:55:31',0,'2015-06-27 19:55:31',1,'2016-02-05 11:35:56',0,'2015-06-27 19:55:31');
INSERT INTO `my_aspnet_membership` VALUES (2,'student@studentcontractor.com','','oxB3X5eEZqDxsG1cqUv2iSNS+IThiD2oQjLktYcJDjk=','Vq3b845z+EG4KjgB1tn/Fg==',1,NULL,NULL,1,'2016-01-22 12:31:14','2016-01-22 12:30:33','2015-07-05 18:59:29','2015-07-05 18:59:29',0,'2015-07-05 18:59:29',0,'2015-07-05 18:59:29',0,'2015-07-05 18:59:29');
INSERT INTO `my_aspnet_membership` VALUES (3,'scottadams.adamlawncare@yahoo.com','','mFRthUEevT6xe4zUAroANCiYnAFtNyId5lhYw/smzVM=','xp2FheYtn4wpDg7UYSoBqw==',1,NULL,NULL,1,'2015-07-21 15:42:10','2015-07-21 15:34:35','2015-07-14 07:45:48','2015-07-14 07:45:48',0,'2015-07-14 07:45:48',1,'2015-07-20 13:46:27',0,'2015-07-14 07:45:48');
INSERT INTO `my_aspnet_membership` VALUES (4,'1@2.com','','/gvDYm668O/ZiYwFuWl7Dt2cwBzBr1o7wRdKun77+7w=','l2ldgUTUOlw7sDzvm7GGVw==',1,NULL,NULL,1,'2016-02-10 11:12:00','2016-02-10 11:02:06','2016-01-12 06:59:46','2016-01-12 06:59:46',0,'2016-01-12 06:59:46',0,'2016-01-12 06:59:46',0,'2016-01-12 06:59:46');
INSERT INTO `my_aspnet_membership` VALUES (5,'1@5.com','','1hAUDAsm4W8qUjALVz+9Tcfbuo4OrSBd3/UpKVHwmdA=','3PGCisvwebAxQcHXNGixFw==',1,NULL,NULL,1,'2016-01-12 12:44:01','2016-01-12 12:44:01','2016-01-12 11:34:08','2016-01-12 11:34:08',0,'2016-01-12 11:34:08',0,'2016-01-12 11:34:08',0,'2016-01-12 11:34:08');
INSERT INTO `my_aspnet_membership` VALUES (6,'logan@wtg-llc.com','','xvChGCyaA6HMVHQ/Ly7iBMqcUkpLsx5WINFGl5LJit4=','sQjTeurwHHVC9e74i+vxqg==',1,NULL,NULL,1,'2016-02-02 13:48:21','2016-02-02 13:46:54','2016-01-12 10:28:16','2016-01-12 10:28:16',0,'2016-01-12 10:28:16',1,'2016-01-22 14:39:42',0,'2016-01-12 10:28:16');
INSERT INTO `my_aspnet_membership` VALUES (7,'winton@wtg-llc.com','','N3beB8jJhJvl8jbG2KBt4rOSW3N6dqByksZelIO8XVQ=','yCC5e3zETBli4hGn+is96w==',1,NULL,NULL,1,'2016-01-12 12:52:28','2016-01-12 12:44:15','2016-01-12 12:24:41','2016-01-12 12:24:41',0,'2016-01-12 12:24:41',0,'2016-01-12 12:24:41',0,'2016-01-12 12:24:41');
INSERT INTO `my_aspnet_membership` VALUES (8,'tester@testing.com','','0nXd3wZAFo56NR9tI48A7uMku/YlcJMEFIMxYzbAYzc=','2kN8/fat3uKEjY1xybjglA==',1,NULL,NULL,1,'2016-01-12 12:54:27','2016-01-12 12:54:27','2016-01-12 12:54:27','2016-01-12 12:54:27',0,'2016-01-12 12:54:27',0,'2016-01-12 12:54:27',0,'2016-01-12 12:54:27');
INSERT INTO `my_aspnet_membership` VALUES (9,'wintonkane@gmail.com','','ngKlhWkWMvCqbkvFgw0bPf1EuPsbWi1AYMnqvRdiwJc=','vhQ+X4fdIBh1PcY+6Hwmyw==',1,NULL,NULL,1,'2016-01-13 07:06:48','2016-01-13 06:56:41','2016-01-13 06:56:41','2016-01-13 06:56:41',0,'2016-01-13 06:56:41',0,'2016-01-13 06:56:41',0,'2016-01-13 06:56:41');
INSERT INTO `my_aspnet_membership` VALUES (10,'wintonkane@outlook.com','','oqvCfmVLU/1nTyoAMC90GfIK7CWFtyPLN1w8sUGp8pc=','Z1904Ki2CHGvfJmhy0hndw==',1,NULL,NULL,1,'2016-01-13 07:17:49','2016-01-13 07:10:52','2016-01-13 07:10:52','2016-01-13 07:10:52',0,'2016-01-13 07:10:52',0,'2016-01-13 07:10:52',0,'2016-01-13 07:10:52');
INSERT INTO `my_aspnet_membership` VALUES (11,'123@abc.com','','kz8+jsE9VqkwyHZDCIK73loadoDXgC9Ooo4q8uX0r58=','NdQieL6bpw/uyj+0o/14ng==',1,NULL,NULL,1,'2016-01-13 12:02:08','2016-01-13 11:59:53','2016-01-13 11:34:44','2016-01-13 11:34:44',0,'2016-01-13 11:34:44',0,'2016-01-13 11:34:44',0,'2016-01-13 11:34:44');
INSERT INTO `my_aspnet_membership` VALUES (12,'mac@mackattack.com','','x/GnHvjFiT75g5mATen+co3Jpb5Y3JVaqfNok2t9q88=','MYt6jjjJ6J5ygrx6QZO49Q==',1,NULL,NULL,1,'2016-01-13 12:07:19','2016-01-13 12:05:29','2016-01-13 12:05:29','2016-01-13 12:05:29',0,'2016-01-13 12:05:29',0,'2016-01-13 12:05:29',0,'2016-01-13 12:05:29');
INSERT INTO `my_aspnet_membership` VALUES (13,'dejdaniel12@gmail.com','','wUzcoopIlb2MxCQXyQl/34jDLpleEK/S/uu6ZohAaX8=','+94aTbPpv0eQhHjfdcppIA==',1,NULL,NULL,1,'2016-01-14 12:31:52','2016-01-14 12:31:52','2016-01-14 12:31:52','2016-01-14 12:31:52',0,'2016-01-14 12:31:52',0,'2016-01-14 12:31:52',0,'2016-01-14 12:31:52');
INSERT INTO `my_aspnet_membership` VALUES (14,'8@9.com','','yCth8DW6NCl4Drvw0cE3mE78/szmWsxSfweLliudZBI=','zzhnDt0uBIPG/jdLcyE10g==',1,NULL,NULL,1,'2016-01-15 10:00:19','2016-01-15 08:30:33','2016-01-15 08:30:33','2016-01-15 08:30:33',0,'2016-01-15 08:30:33',0,'2016-01-15 08:30:33',0,'2016-01-15 08:30:33');
INSERT INTO `my_aspnet_membership` VALUES (15,'1@6.com','','vNJGZax0e37PT4AP3jdUwqTK9aX0/NyxwhGoEGGNUrA=','12hoNqqbLdyvVpff3Ii5uA==',1,NULL,NULL,1,'2016-01-15 10:38:06','2016-01-15 09:09:09','2016-01-15 08:40:56','2016-01-15 08:40:56',0,'2016-01-15 08:40:56',0,'2016-01-15 08:40:56',0,'2016-01-15 08:40:56');
INSERT INTO `my_aspnet_membership` VALUES (16,'3@5.com','','3vtZPAJsQNZKdnhqq+9VxtH7+l76Ojh2Zx+ZeCaRsM4=','gJrlclFEMrEAuoU9RbX2jw==',1,NULL,NULL,1,'2016-01-15 16:05:59','2016-01-15 16:05:59','2016-01-15 09:15:03','2016-01-15 09:15:03',0,'2016-01-15 09:15:03',1,'2016-01-15 12:18:46',0,'2016-01-15 09:15:03');
INSERT INTO `my_aspnet_membership` VALUES (17,'onehoggiedown@bhd.com','','RySZGp9wpxo2GNNQQ3vKdnq0QDF0l7G+DXjya9S0ELY=','LMorZhbihCRVRHt1A4RJgg==',1,NULL,NULL,1,'2016-01-18 07:18:23','2016-01-18 07:18:19','2016-01-18 07:18:19','2016-01-18 07:18:19',0,'2016-01-18 07:18:19',0,'2016-01-18 07:18:19',0,'2016-01-18 07:18:19');
INSERT INTO `my_aspnet_membership` VALUES (18,'ryukao@2.com','','ulfo78KO1523d5pwS+Er1xv1LoP7O5UAO859xQTI6ug=','oCF9yXfWO6bsGgJ2wMYcSw==',1,NULL,NULL,1,'2016-02-04 08:43:31','2016-02-04 08:42:29','2016-01-18 07:48:47','2016-01-18 07:48:47',0,'2016-01-18 07:48:47',1,'2016-01-25 08:20:48',0,'2016-01-18 07:48:47');
INSERT INTO `my_aspnet_membership` VALUES (19,'wintonkane@wtg-llc.com','','EM3WzzfKgAddSJSxxVsAXdYVYTKbj6zyCK3B+iW9QgA=','QjXhNYvk0aMR63j0ipsrrw==',1,NULL,NULL,1,'2016-01-19 10:30:06','2016-01-19 10:29:08','2016-01-19 10:29:08','2016-01-19 10:29:08',0,'2016-01-19 10:29:08',0,'2016-01-19 10:29:08',0,'2016-01-19 10:29:08');
INSERT INTO `my_aspnet_membership` VALUES (20,'2rbcf5+8e3prrpeslzf8@sharklasers.com','','4FefkoC4WyaaB7hMx6WdF+etLTb3ggWIXT6fKBZzghU=','YZlOd2P7fDHQ5S1Jqbfnpg==',1,NULL,NULL,1,'2016-01-19 10:31:42','2016-01-19 10:31:42','2016-01-19 10:31:42','2016-01-19 10:31:42',0,'2016-01-19 10:31:42',0,'2016-01-19 10:31:42',0,'2016-01-19 10:31:42');
INSERT INTO `my_aspnet_membership` VALUES (21,'2s3pqn+z3yjb2t36ea4@sharklasers.com','','LjHMuUc8QmH3Z0NdJI3wvcp5giv2pPiEbWbBXFBDBV0=','ozdFkLZ97tAaGqGgv0Ii4A==',1,NULL,NULL,1,'2016-01-22 10:26:55','2016-01-22 10:26:15','2016-01-21 11:01:41','2016-01-21 11:01:41',0,'2016-01-21 11:01:41',1,'2016-01-22 08:03:11',0,'2016-01-21 11:01:41');
INSERT INTO `my_aspnet_membership` VALUES (22,'2s3py5+9lhnjfpq98m6c@sharklasers.com','','dAy5Mp/V3BUFBahQ+3tkGyPrxhzP2YXlntB9yyc24Lg=','wl5p4cTgEeDONvV+jlhfYw==',1,NULL,NULL,1,'2016-01-22 11:32:12','2016-01-22 11:30:05','2016-01-21 10:04:09','2016-01-21 10:04:09',0,'2016-01-21 10:04:09',1,'2016-01-22 08:03:19',0,'2016-01-21 10:04:09');
INSERT INTO `my_aspnet_membership` VALUES (23,'testingtester3@4.com','','TTAJwbShCnNKapZDg0CXVaabpAFbD3ycYED4YQxLZTQ=','1QjzRkMChamhdCmDt964zA==',1,NULL,NULL,1,'2016-01-22 12:30:23','2016-01-22 11:37:20','2016-01-22 11:34:11','2016-01-22 11:34:11',0,'2016-01-22 11:34:11',0,'2016-01-22 11:34:11',0,'2016-01-22 11:34:11');
INSERT INTO `my_aspnet_membership` VALUES (24,'scott@adams-lawncare.com','','IY/L/uxPr0q3u9pZPxQ97Q5quP0wzrKhOnTxnlwKFWQ=','w4Y8NeiDKAKIpTv7jexpmA==',1,NULL,NULL,1,'2016-01-23 12:11:48','2016-01-23 12:11:48','2016-01-22 11:52:43','2016-01-22 11:52:43',0,'2016-01-22 11:52:43',0,'2016-01-22 11:52:43',0,'2016-01-22 11:52:43');
INSERT INTO `my_aspnet_membership` VALUES (25,'cory@webb-family.com','','GX6QnmCE3ywb/ys6mG1amFSYjtMkJOKghXmYixWI/QM=','/KtsWf64BWbY6G5F4hUw/w==',1,NULL,NULL,1,'2016-01-22 12:28:56','2016-01-22 12:24:41','2016-01-22 12:24:41','2016-01-22 12:24:41',0,'2016-01-22 12:24:41',0,'2016-01-22 12:24:41',0,'2016-01-22 12:24:41');
INSERT INTO `my_aspnet_membership` VALUES (26,'2twhpd+aachqane6l6l0@sharklasers.com','','cBgcM+cKFSlvQakCWGqsA6yuy13wS1GCSv2CplhoyZY=','U9Dl0jPr74IF6jsUWLC9TA==',1,NULL,NULL,1,'2016-01-29 08:42:07','2016-01-29 08:40:28','2016-01-29 08:40:28','2016-01-29 08:40:28',0,'2016-01-29 08:40:28',0,'2016-01-29 08:40:28',0,'2016-01-29 08:40:28');
INSERT INTO `my_aspnet_membership` VALUES (27,'2twhwk+3hd3rnbwbcc50@sharklasers.com','','VSs9A+Sc5oPlI5C10C4icY6/lkFzj4qS5g801gSjDOk=','UedYgzWghhfLhJ3jYt/UQQ==',1,NULL,NULL,1,'2016-01-29 08:46:15','2016-01-29 08:43:47','2016-01-29 08:43:47','2016-01-29 08:43:47',0,'2016-01-29 08:43:47',0,'2016-01-29 08:43:47',0,'2016-01-29 08:43:47');
INSERT INTO `my_aspnet_membership` VALUES (28,'2twi5l+icic27iptbfs@sharklasers.com','','vgqCSBTorPMiTBiMT290Z0TiiMC9yiXt5Sy7vOFoPCI=','9mtN4g7NSDHRGe9Koo4kZg==',1,NULL,NULL,1,'2016-01-29 08:47:50','2016-01-29 08:47:50','2016-01-29 08:47:50','2016-01-29 08:47:50',0,'2016-01-29 08:47:50',0,'2016-01-29 08:47:50',0,'2016-01-29 08:47:50');
INSERT INTO `my_aspnet_membership` VALUES (29,'2txbhp+6gf7ovacao7pg@sharklasers.com','','0TVGwYqs23yFCp1oc+jYMqkI4L4/p50JBGd1PVzHBMo=','bSZn6eVfOdPBiTavM0Nxsg==',1,NULL,NULL,1,'2016-01-29 10:33:12','2016-01-29 10:32:54','2016-01-29 10:32:54','2016-01-29 10:32:54',0,'2016-01-29 10:32:54',0,'2016-01-29 10:32:54',0,'2016-01-29 10:32:54');
INSERT INTO `my_aspnet_membership` VALUES (30,'2vpe6q+9p3pe2k5wsyrk@sharklasers.com','','0YcQTypOUeNDur+dtQ/4AdXtaui6f9+rNFjghf4Hpo4=','WBFiFwxqaCV3eXonenC5VA==',1,NULL,NULL,1,'2016-02-01 12:05:01','2016-02-01 12:03:55','2016-02-01 10:23:35','2016-02-01 10:23:35',0,'2016-02-01 10:23:35',0,'2016-02-01 10:23:35',0,'2016-02-01 10:23:35');
INSERT INTO `my_aspnet_membership` VALUES (31,'2vqm5b+3uvhgeodu747g@sharklasers.com','','EN+DK0aiLJm5L4bYCym8JFBQYnbcFt/ZNOegB7uhEgw=','v1kFlCPC3snopR1o8OJPxQ==',1,NULL,NULL,1,'2016-02-02 10:09:53','2016-02-02 07:42:53','2016-02-01 12:42:42','2016-02-01 12:42:42',0,'2016-02-01 12:42:42',0,'2016-02-01 12:42:42',0,'2016-02-01 12:42:42');
INSERT INTO `my_aspnet_membership` VALUES (32,'2vqn81+5ne5zibr2wzzk@sharklasers.com','','tVPDCDjA/ksHf4AUGt3+hPKiBY+4vB4lT1oihcCwrOE=','ktimgRg9V/FhnDM6Y0nILg==',1,NULL,NULL,1,'2016-02-01 12:45:44','2016-02-01 12:45:44','2016-02-01 12:45:44','2016-02-01 12:45:44',0,'2016-02-01 12:45:44',0,'2016-02-01 12:45:44',0,'2016-02-01 12:45:44');
INSERT INTO `my_aspnet_membership` VALUES (33,'2vqpwn+neapaqtfy5qxaj@sharklasers.com','','IxctvEqkBi8fmjmvRrD+TG/SgA6Nwn6k5dy9lqq3vPM=','4ae1kgmLKjLskZ1cfLequA==',1,NULL,NULL,1,'2016-02-02 07:08:31','2016-02-02 07:08:25','2016-02-01 12:50:08','2016-02-01 12:50:08',0,'2016-02-01 12:50:08',0,'2016-02-01 12:50:08',0,'2016-02-01 12:50:08');
INSERT INTO `my_aspnet_membership` VALUES (34,'2vqr51+unhaaow@sharklasers.com','','AjG3H2dgxXiZCoQDXfgwOZhhSOKmk0qDgbgxyGvfq98=','sZZguk4/tuqbtW5RSJpdqg==',1,NULL,NULL,1,'2016-02-02 10:38:49','2016-02-02 10:37:25','2016-02-01 11:53:36','2016-02-01 11:53:36',0,'2016-02-01 11:53:36',0,'2016-02-01 11:53:36',0,'2016-02-01 11:53:36');
INSERT INTO `my_aspnet_membership` VALUES (35,'tst@test.com','','/khAK3ZKFFGRxcFDkPIq7Hw7zhq4JtLSGsaSLjN9JSw=','9qlNfdIHvQhSKVuEMPAJlw==',1,NULL,NULL,1,'2016-02-01 14:47:04','2016-02-01 14:33:02','2016-02-01 14:33:02','2016-02-01 14:33:02',0,'2016-02-01 14:33:02',0,'2016-02-01 14:33:02',0,'2016-02-01 14:33:02');
INSERT INTO `my_aspnet_membership` VALUES (36,'test@newyear','','mTpT09X3zn981RIc0rgm+Uf907ENc8djLibDcOblCUw=','oh+xtf6ya/gQg+YOMSs0EQ==',1,NULL,NULL,1,'2016-02-02 14:05:43','2016-02-01 15:27:03','2016-02-01 15:27:03','2016-02-01 15:27:03',0,'2016-02-01 15:27:03',0,'2016-02-01 15:27:03',0,'2016-02-01 15:27:03');
INSERT INTO `my_aspnet_membership` VALUES (37,'2w9h74+6kx1sk6xfk2c0@sharklasers.com','','j9+XwOtOfvecYq96/ltrTwU18by8ccdW03Lyvc7wohc=','vxXMGKiP5jqdUOq0JmHmXw==',1,NULL,NULL,1,'2016-02-02 10:55:31','2016-02-02 10:40:09','2016-02-02 10:40:09','2016-02-02 10:40:09',0,'2016-02-02 10:40:09',0,'2016-02-02 10:40:09',0,'2016-02-02 10:40:09');
INSERT INTO `my_aspnet_membership` VALUES (38,'2w9qe4+4wkjpzg940cw@sharklasers.com','','wdsgtvwSzFo8rWJ/JOXfl9uep2U8BUVv2tKyL83C48U=','s4fhw84KP9VXpPCzaf5CEw==',1,NULL,NULL,1,'2016-02-03 12:24:57','2016-02-03 12:24:27','2016-02-02 10:57:16','2016-02-02 10:57:16',0,'2016-02-02 10:57:16',1,'2016-02-03 07:59:16',0,'2016-02-02 10:57:16');
INSERT INTO `my_aspnet_membership` VALUES (39,'Test@2016.now','','YBhjbOyPvfsHgYpJeAuBystDr65zns7rdYFle+oikfE=','H1OfWlRrmcXE4ci4cHKxRw==',1,NULL,NULL,1,'2016-02-02 14:47:01','2016-02-02 14:11:48','2016-02-02 14:11:48','2016-02-02 14:11:48',0,'2016-02-02 14:11:48',0,'2016-02-02 14:11:48',0,'2016-02-02 14:11:48');
INSERT INTO `my_aspnet_membership` VALUES (40,'lmcalister13@gmail.com','','M1gpyUvy/elrFLf/5AboPvj5kAf5ZdfDEMW5g8jWCrc=','ZRYeKqkVKX3QucD06eqcJg==',1,NULL,NULL,1,'2016-02-02 13:55:07','2016-02-02 13:54:49','2016-02-02 13:54:49','2016-02-02 13:54:49',0,'2016-02-02 13:54:49',0,'2016-02-02 13:54:49',0,'2016-02-02 13:54:49');
INSERT INTO `my_aspnet_membership` VALUES (41,'2wu6x0+87ifwtfey90v4@sharklasers.com','','KhU24n/BU2Pry61HpJEzqtgx5G+QPbpMWCgkDE5gAcE=','FeqRgfuwF9PsEGFZIPntYQ==',1,NULL,NULL,1,'2016-02-08 07:16:53','2016-02-08 07:09:31','2016-02-03 08:19:38','2016-02-03 08:19:38',0,'2016-02-03 08:19:38',1,'2016-02-04 07:08:52',0,'2016-02-03 08:19:38');
INSERT INTO `my_aspnet_membership` VALUES (42,'info@wtg-llc.com','','EgEnfTR+dmH5z8oCXDHPkcIIT0C63V/4xzL2/l9JUcA=','DqfvvyiyQ3jNA6tYiSyzhw==',1,NULL,NULL,1,'2016-02-03 09:56:45','2016-02-03 09:56:45','2016-02-03 09:56:45','2016-02-03 09:56:45',0,'2016-02-03 09:56:45',0,'2016-02-03 09:56:45',0,'2016-02-03 09:56:45');
INSERT INTO `my_aspnet_membership` VALUES (43,'wintonkane@gmail.com','','yhmsRKa2cHk4nt/an/7lKmfP9OCjLj0f3AZHnmA50Vk=','43+861KGVn+K4+j5xgOQsQ==',1,NULL,NULL,1,'2016-02-04 10:47:06','2016-02-04 10:47:06','2016-02-04 10:47:06','2016-02-04 10:47:06',0,'2016-02-04 10:47:06',0,'2016-02-04 10:47:06',0,'2016-02-04 10:47:06');
INSERT INTO `my_aspnet_membership` VALUES (44,'2xixgs+pgmbbxbuyjng@sharklasers.com','','wTexplRJWFyPuUrfBTqPza3Cf6TI21VaMNBfxdqfB7k=','tzrNYcEb4qzYMICHt2vAqg==',1,NULL,NULL,1,'2016-02-05 10:49:51','2016-02-05 10:49:51','2016-02-05 10:49:51','2016-02-05 10:49:51',0,'2016-02-05 10:49:51',0,'2016-02-05 10:49:51',0,'2016-02-05 10:49:51');
INSERT INTO `my_aspnet_membership` VALUES (45,'2xixqm+etpvjspvef2lrt@sharklasers.com','','H9UfiJKRwV4s0WcUI11Qe04YKMozA9Wb/4IPAfHFRso=','vBIPGOE8F60fq5MOA6VWLw==',1,NULL,NULL,1,'2016-02-05 10:52:38','2016-02-05 10:52:38','2016-02-05 10:52:38','2016-02-05 10:52:38',0,'2016-02-05 10:52:38',0,'2016-02-05 10:52:38',0,'2016-02-05 10:52:38');
INSERT INTO `my_aspnet_membership` VALUES (46,'2xj3zk+tp4jmoq45ym00j8k@sharklasers.com','','HWg/EJyEGBFK5QQDnM+iw+n2N905o/K7g6L9Hccgw/A=','H4SZEh2BN3haUyi2kX2OBA==',1,NULL,NULL,1,'2016-02-05 11:45:50','2016-02-05 11:45:50','2016-02-05 11:45:50','2016-02-05 11:45:50',0,'2016-02-05 11:45:50',0,'2016-02-05 11:45:50',0,'2016-02-05 11:45:50');
INSERT INTO `my_aspnet_membership` VALUES (47,'2xk1ee+8xvbxc3iipie0@sharklasers.com','','GBD5+FzJurhFauUTrZL0zDV3FqDqWjCRvEKsggs8daA=','UKm75C8K/Mbrit8QxczTgw==',1,NULL,NULL,1,'2016-02-05 16:46:28','2016-02-05 16:46:28','2016-02-05 16:46:28','2016-02-05 16:46:28',0,'2016-02-05 16:46:28',0,'2016-02-05 16:46:28',0,'2016-02-05 16:46:28');
INSERT INTO `my_aspnet_membership` VALUES (48,'2xk3jm+bo6di3vtwzdt8@sharklasers.com','','ChgMjrIl9lLoskiCAs/d1SjsloqeuTId7mUrNWwgaGo=','5xCHplqxuug29VCrN7jh8Q==',1,NULL,NULL,1,'2016-02-05 17:02:08','2016-02-05 17:02:08','2016-02-05 17:02:08','2016-02-05 17:02:08',0,'2016-02-05 17:02:08',0,'2016-02-05 17:02:08',0,'2016-02-05 17:02:08');
INSERT INTO `my_aspnet_membership` VALUES (49,'1@02.com','','ZR9gUJWrC1yLFS9LSwEh09t1JhpwIJiG2oWeN86GgwM=','3xXtyeRoRfFL8VdNRdK9fw==',1,NULL,NULL,1,'2016-02-08 08:49:08','2016-02-08 08:49:08','2016-02-08 08:49:08','2016-02-08 08:49:08',0,'2016-02-08 08:49:08',0,'2016-02-08 08:49:08',0,'2016-02-08 08:49:08');
INSERT INTO `my_aspnet_membership` VALUES (50,'1@03.com','','QukXxAVlLrFmxh0cYiFnWKce8h/saSkZV69kZNJvScU=','FQGf2iSi2vUyOazb6Ma3EA==',1,NULL,NULL,1,'2016-02-08 09:55:50','2016-02-08 09:55:50','2016-02-08 09:55:50','2016-02-08 09:55:50',0,'2016-02-08 09:55:50',0,'2016-02-08 09:55:50',0,'2016-02-08 09:55:50');
INSERT INTO `my_aspnet_membership` VALUES (51,'1@04.com','','ZA7kRcN+FmrG39//ykojrRUtNVz4FqCVDKPbiGIaPBE=','qb7wyApSb2FFb98tZA7Awg==',1,NULL,NULL,1,'2016-02-08 10:24:41','2016-02-08 10:24:41','2016-02-08 10:24:41','2016-02-08 10:24:41',0,'2016-02-08 10:24:41',0,'2016-02-08 10:24:41',0,'2016-02-08 10:24:41');
INSERT INTO `my_aspnet_membership` VALUES (52,'1@05.com','','LCw2/FNZAJJoRLK6tFehse/uy0mmXpaEWY0G6Nuy/0Y=','d1iyd+V5Ogalvsx361XRWQ==',1,NULL,NULL,1,'2016-02-08 10:27:18','2016-02-08 10:27:18','2016-02-08 10:27:18','2016-02-08 10:27:18',0,'2016-02-08 10:27:18',0,'2016-02-08 10:27:18',0,'2016-02-08 10:27:18');
INSERT INTO `my_aspnet_membership` VALUES (53,'1@06.com','','UpIqA7QJGz1ku08bMyrwEg3+v2CWrc76JMuRKJg/33c=','0L2gZL3ud/3M+Uy0d/eCDQ==',1,NULL,NULL,1,'2016-02-08 11:36:25','2016-02-08 10:36:16','2016-02-08 10:36:16','2016-02-08 10:36:16',0,'2016-02-08 10:36:16',0,'2016-02-08 10:36:16',0,'2016-02-08 10:36:16');
INSERT INTO `my_aspnet_membership` VALUES (54,'1@07.com','','0Xyaoy8A6JLvlS4I3c98zQAkUC2RTJRMVFLGJM/JwNo=','Q03xBm58tznvJLMUFZzzgA==',1,NULL,NULL,1,'2016-02-08 09:39:57','2016-02-08 09:39:57','2016-02-08 09:39:57','2016-02-08 09:39:57',0,'2016-02-08 09:39:57',0,'2016-02-08 09:39:57',0,'2016-02-08 09:39:57');
INSERT INTO `my_aspnet_membership` VALUES (55,'1@08.com','','7MSr5/hcadGyWspymyWIug+lMAaU5JQcheqwASQZQg0=','fzSM4Jm9hhSnaOouPe+a3Q==',1,NULL,NULL,1,'2016-02-08 11:43:20','2016-02-08 11:37:37','2016-02-08 11:37:37','2016-02-08 11:37:37',0,'2016-02-08 11:37:37',0,'2016-02-08 11:37:37',0,'2016-02-08 11:37:37');
INSERT INTO `my_aspnet_membership` VALUES (56,'1@09.com','','SZfoC+PMkwIalS0bX7sZ+jt4o9XowiU68vAshLoG/RY=','iMr+iQRrpRBHWTZdC0pP9w==',1,NULL,NULL,1,'2016-02-08 10:52:29','2016-02-08 10:52:12','2016-02-08 10:45:21','2016-02-08 10:45:21',0,'2016-02-08 10:45:21',0,'2016-02-08 10:45:21',0,'2016-02-08 10:45:21');
INSERT INTO `my_aspnet_membership` VALUES (57,'2z0ele+3ykcyz3oyymtc@sharklasers.com','','rXaRCbkgXmjZg9n2HZ85ZIjeTh6CsMjR00+rFgGQQbA=','HFgCF5DBRSmkBXzGmNXlGQ==',1,NULL,NULL,1,'2016-02-10 09:16:07','2016-02-10 09:14:21','2016-02-08 11:29:07','2016-02-08 11:29:07',0,'2016-02-08 11:29:07',0,'2016-02-08 11:29:07',0,'2016-02-08 11:29:07');
INSERT INTO `my_aspnet_membership` VALUES (58,'1@11. com','','CxXVC9NGMpWyYmEDWFrM5MBTURPQTNXS5lRiGoGMAXA=','icv7Psetc6OjYftgdcBzrg==',1,NULL,NULL,1,'2016-02-08 12:55:51','2016-02-08 12:55:51','2016-02-08 12:55:51','2016-02-08 12:55:51',0,'2016-02-08 12:55:51',0,'2016-02-08 12:55:51',0,'2016-02-08 12:55:51');
INSERT INTO `my_aspnet_membership` VALUES (59,'2z20tw+5kdur5o7hvzf0@sharklasers.com','','eBIsmaacjx+/AjdpkjKmICvSTT1QEHFF/wX0cTmEps4=','7p0WsSF64p99HQVXAlrN0Q==',1,NULL,NULL,1,'2016-02-08 12:40:04','2016-02-08 12:40:04','2016-02-08 12:40:04','2016-02-08 12:40:04',0,'2016-02-08 12:40:04',0,'2016-02-08 12:40:04',0,'2016-02-08 12:40:04');
INSERT INTO `my_aspnet_membership` VALUES (60,'1@14.com','','/j2MGLOojgNtKnawd3qBvJE34tW1kfbqjDckDYPfKK8=','iXaOVf1C5iUm3xvh5d8Sdw==',1,NULL,NULL,1,'2016-02-09 08:43:05','2016-02-09 08:43:05','2016-02-09 08:43:05','2016-02-09 08:43:05',0,'2016-02-09 08:43:05',0,'2016-02-09 08:43:05',0,'2016-02-09 08:43:05');
INSERT INTO `my_aspnet_membership` VALUES (61,'1@15.com','','N0+vRvGcmoxt13kpGlheyYqY6GefSylDLpPDsVe1RZk=','Z/p/blnhmyIS6wTQPZTqmg==',1,NULL,NULL,1,'2016-02-09 09:00:04','2016-02-09 09:00:04','2016-02-09 09:00:04','2016-02-09 09:00:04',0,'2016-02-09 09:00:04',0,'2016-02-09 09:00:04',0,'2016-02-09 09:00:04');
INSERT INTO `my_aspnet_membership` VALUES (62,'1@17.com','','3+O4aTPLvjZoEPTZblfUlR6MfnCoytjBcHBOazObBdo=','14flKyM+prL2Nu4wHOMAOA==',1,NULL,NULL,1,'2016-02-09 08:42:57','2016-02-09 08:42:57','2016-02-09 08:42:57','2016-02-09 08:42:57',0,'2016-02-09 08:42:57',0,'2016-02-09 08:42:57',0,'2016-02-09 08:42:57');
INSERT INTO `my_aspnet_membership` VALUES (63,'1@18.com','','rbeD4OcCgYCEvaWTls3+tf6kq84B2hxp5c2vCRoTadY=','ft86mTjHPuUr5n0Qwpo74w==',1,NULL,NULL,1,'2016-02-10 10:55:06','2016-02-09 11:03:04','2016-02-09 09:19:48','2016-02-09 09:19:48',0,'2016-02-09 09:19:48',0,'2016-02-09 09:19:48',0,'2016-02-09 09:19:48');
/*!40000 ALTER TABLE `my_aspnet_membership` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_paths`
--

DROP TABLE IF EXISTS `my_aspnet_paths`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_paths` (
  `applicationId` int(11) NOT NULL,
  `pathId` varchar(36) NOT NULL,
  `path` varchar(256) NOT NULL,
  `loweredPath` varchar(256) NOT NULL,
  PRIMARY KEY  (`pathId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_paths`
--

LOCK TABLES `my_aspnet_paths` WRITE;
/*!40000 ALTER TABLE `my_aspnet_paths` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_paths` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_personalizationallusers`
--

DROP TABLE IF EXISTS `my_aspnet_personalizationallusers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_personalizationallusers` (
  `pathId` varchar(36) NOT NULL,
  `pageSettings` blob NOT NULL,
  `lastUpdatedDate` datetime NOT NULL,
  PRIMARY KEY  (`pathId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_personalizationallusers`
--

LOCK TABLES `my_aspnet_personalizationallusers` WRITE;
/*!40000 ALTER TABLE `my_aspnet_personalizationallusers` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_personalizationallusers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_personalizationperuser`
--

DROP TABLE IF EXISTS `my_aspnet_personalizationperuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_personalizationperuser` (
  `id` int(11) NOT NULL auto_increment,
  `applicationId` int(11) NOT NULL,
  `pathId` varchar(36) default NULL,
  `userId` int(11) default NULL,
  `pageSettings` blob NOT NULL,
  `lastUpdatedDate` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_personalizationperuser`
--

LOCK TABLES `my_aspnet_personalizationperuser` WRITE;
/*!40000 ALTER TABLE `my_aspnet_personalizationperuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_personalizationperuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_profiles`
--

DROP TABLE IF EXISTS `my_aspnet_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_profiles` (
  `userId` int(11) NOT NULL,
  `valueindex` longtext,
  `stringdata` longtext,
  `binarydata` longblob,
  `lastUpdatedDate` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_profiles`
--

LOCK TABLES `my_aspnet_profiles` WRITE;
/*!40000 ALTER TABLE `my_aspnet_profiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_roles`
--

DROP TABLE IF EXISTS `my_aspnet_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_roles` (
  `id` int(11) NOT NULL auto_increment,
  `applicationId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_roles`
--

LOCK TABLES `my_aspnet_roles` WRITE;
/*!40000 ALTER TABLE `my_aspnet_roles` DISABLE KEYS */;
INSERT INTO `my_aspnet_roles` VALUES (1,1,'admin');
INSERT INTO `my_aspnet_roles` VALUES (2,1,'contractor');
INSERT INTO `my_aspnet_roles` VALUES (3,1,'contractor2');
/*!40000 ALTER TABLE `my_aspnet_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_schemaversion`
--

DROP TABLE IF EXISTS `my_aspnet_schemaversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_schemaversion` (
  `version` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_schemaversion`
--

LOCK TABLES `my_aspnet_schemaversion` WRITE;
/*!40000 ALTER TABLE `my_aspnet_schemaversion` DISABLE KEYS */;
INSERT INTO `my_aspnet_schemaversion` VALUES (10);
/*!40000 ALTER TABLE `my_aspnet_schemaversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_sessioncleanup`
--

DROP TABLE IF EXISTS `my_aspnet_sessioncleanup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_sessioncleanup` (
  `LastRun` datetime NOT NULL,
  `IntervalMinutes` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  PRIMARY KEY  (`ApplicationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_sessioncleanup`
--

LOCK TABLES `my_aspnet_sessioncleanup` WRITE;
/*!40000 ALTER TABLE `my_aspnet_sessioncleanup` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_sessioncleanup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_sessions`
--

DROP TABLE IF EXISTS `my_aspnet_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_sessions` (
  `SessionId` varchar(191) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `Created` datetime NOT NULL,
  `Expires` datetime NOT NULL,
  `LockDate` datetime NOT NULL,
  `LockId` int(11) NOT NULL,
  `Timeout` int(11) NOT NULL,
  `Locked` tinyint(1) NOT NULL,
  `SessionItems` longblob,
  `Flags` int(11) NOT NULL,
  PRIMARY KEY  (`SessionId`,`ApplicationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_sessions`
--

LOCK TABLES `my_aspnet_sessions` WRITE;
/*!40000 ALTER TABLE `my_aspnet_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_sitemap`
--

DROP TABLE IF EXISTS `my_aspnet_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_sitemap` (
  `Id` int(11) NOT NULL auto_increment,
  `Title` varchar(50) default NULL,
  `Description` varchar(512) default NULL,
  `Url` varchar(512) default NULL,
  `Roles` varchar(1000) default NULL,
  `ParentId` int(11) default NULL,
  PRIMARY KEY  (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_sitemap`
--

LOCK TABLES `my_aspnet_sitemap` WRITE;
/*!40000 ALTER TABLE `my_aspnet_sitemap` DISABLE KEYS */;
/*!40000 ALTER TABLE `my_aspnet_sitemap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_users`
--

DROP TABLE IF EXISTS `my_aspnet_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_users` (
  `id` int(11) NOT NULL auto_increment,
  `applicationId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `isAnonymous` tinyint(1) NOT NULL default '1',
  `lastActivityDate` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_users`
--

LOCK TABLES `my_aspnet_users` WRITE;
/*!40000 ALTER TABLE `my_aspnet_users` DISABLE KEYS */;
INSERT INTO `my_aspnet_users` VALUES (1,1,'admin',0,'2016-02-10 11:15:12');
INSERT INTO `my_aspnet_users` VALUES (2,1,'studentcontractor',0,'2016-01-22 12:31:14');
INSERT INTO `my_aspnet_users` VALUES (3,1,'adamslawncare',0,'2015-07-21 15:42:10');
INSERT INTO `my_aspnet_users` VALUES (4,1,'daveslawn',0,'2016-02-10 11:12:00');
INSERT INTO `my_aspnet_users` VALUES (5,1,'billybob',0,'2016-01-12 12:44:01');
INSERT INTO `my_aspnet_users` VALUES (6,1,'lmcalister',0,'2016-02-02 13:48:21');
INSERT INTO `my_aspnet_users` VALUES (11,1,'Test2015',0,'2016-01-13 12:02:08');
INSERT INTO `my_aspnet_users` VALUES (16,1,'TestAddress1_15_16',0,'2016-01-15 16:05:59');
INSERT INTO `my_aspnet_users` VALUES (17,1,'Hoggie',0,'2016-01-18 07:18:23');
INSERT INTO `my_aspnet_users` VALUES (18,1,'coolguyryu',0,'2016-02-04 08:43:31');
INSERT INTO `my_aspnet_users` VALUES (19,1,'wintonkane',0,'2016-01-19 10:30:06');
INSERT INTO `my_aspnet_users` VALUES (20,1,'wintonk',0,'2016-01-19 10:31:42');
INSERT INTO `my_aspnet_users` VALUES (21,1,'testingtester',0,'2016-01-22 10:26:55');
INSERT INTO `my_aspnet_users` VALUES (22,1,'testingtester2',0,'2016-01-22 11:32:12');
INSERT INTO `my_aspnet_users` VALUES (23,1,'testingtester3',0,'2016-01-22 12:30:23');
INSERT INTO `my_aspnet_users` VALUES (24,1,'adamsoutdoor',0,'2016-01-23 12:11:48');
INSERT INTO `my_aspnet_users` VALUES (25,1,'CWebb',0,'2016-01-22 12:28:56');
INSERT INTO `my_aspnet_users` VALUES (26,1,'tinytim',0,'2016-01-29 08:42:07');
INSERT INTO `my_aspnet_users` VALUES (27,1,'bigbilly',0,'2016-01-29 08:46:15');
INSERT INTO `my_aspnet_users` VALUES (28,1,'monalisa',0,'2016-01-29 08:47:50');
INSERT INTO `my_aspnet_users` VALUES (29,1,'coolstudent',0,'2016-01-29 10:33:12');
INSERT INTO `my_aspnet_users` VALUES (30,1,'test2-1-16',0,'2016-02-01 12:05:01');
INSERT INTO `my_aspnet_users` VALUES (34,1,'test2-1-16--4',0,'2016-02-02 10:38:49');
INSERT INTO `my_aspnet_users` VALUES (35,1,'Not-real 2-1-16',0,'2016-02-01 14:47:04');
INSERT INTO `my_aspnet_users` VALUES (36,1,'coolGuy2',0,'2016-02-02 14:05:43');
INSERT INTO `my_aspnet_users` VALUES (37,1,'Test2-2-16--1',0,'2016-02-02 10:55:31');
INSERT INTO `my_aspnet_users` VALUES (38,1,'Test2-2-16--2',0,'2016-02-03 12:24:57');
INSERT INTO `my_aspnet_users` VALUES (39,1,'coolGuy3',0,'2016-02-02 14:47:01');
INSERT INTO `my_aspnet_users` VALUES (40,1,'finnywabbit',0,'2016-02-02 13:55:07');
INSERT INTO `my_aspnet_users` VALUES (41,1,'emailtest',0,'2016-02-08 07:16:53');
INSERT INTO `my_aspnet_users` VALUES (42,1,'loganlee',0,'2016-02-03 09:56:45');
INSERT INTO `my_aspnet_users` VALUES (43,1,'daveslawncare',0,'2016-02-04 10:47:06');
INSERT INTO `my_aspnet_users` VALUES (44,1,'newuser',0,'2016-02-05 10:49:51');
INSERT INTO `my_aspnet_users` VALUES (45,1,'thenewguy',0,'2016-02-05 10:52:38');
INSERT INTO `my_aspnet_users` VALUES (46,1,'thenewguy2',0,'2016-02-05 11:45:50');
INSERT INTO `my_aspnet_users` VALUES (47,1,'mysqlconnection',0,'2016-02-05 15:46:29');
INSERT INTO `my_aspnet_users` VALUES (48,1,'mysqlconnection2',0,'2016-02-05 16:02:09');
INSERT INTO `my_aspnet_users` VALUES (49,1,'coolguystan',0,'2016-02-08 08:49:08');
INSERT INTO `my_aspnet_users` VALUES (50,1,'coolguybo',0,'2016-02-08 08:55:48');
INSERT INTO `my_aspnet_users` VALUES (51,1,'coolguybebop',0,'2016-02-08 09:24:39');
INSERT INTO `my_aspnet_users` VALUES (52,1,'coolguybebop2',0,'2016-02-08 09:27:17');
INSERT INTO `my_aspnet_users` VALUES (53,1,'coolguystan2',0,'2016-02-08 11:36:25');
INSERT INTO `my_aspnet_users` VALUES (54,1,'coolguystan3',0,'2016-02-08 09:39:58');
INSERT INTO `my_aspnet_users` VALUES (55,1,'coolguybebop3',0,'2016-02-08 11:43:20');
INSERT INTO `my_aspnet_users` VALUES (56,1,'coolguybebop4',0,'2016-02-08 10:52:29');
INSERT INTO `my_aspnet_users` VALUES (57,1,'contractor1',0,'2016-02-10 09:16:07');
INSERT INTO `my_aspnet_users` VALUES (58,1,'createcontractor2',0,'2016-02-08 11:55:50');
INSERT INTO `my_aspnet_users` VALUES (59,1,'emailtest2',0,'2016-02-08 12:40:04');
INSERT INTO `my_aspnet_users` VALUES (60,1,'createcontractor3',0,'2016-02-09 07:43:03');
INSERT INTO `my_aspnet_users` VALUES (61,1,'createcontractor5',0,'2016-02-09 08:00:03');
INSERT INTO `my_aspnet_users` VALUES (62,1,'createcontractor6',0,'2016-02-09 08:42:57');
INSERT INTO `my_aspnet_users` VALUES (63,1,'createusere1',0,'2016-02-10 10:55:06');
/*!40000 ALTER TABLE `my_aspnet_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `my_aspnet_usersinroles`
--

DROP TABLE IF EXISTS `my_aspnet_usersinroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `my_aspnet_usersinroles` (
  `userId` int(11) NOT NULL default '0',
  `roleId` int(11) NOT NULL default '0',
  PRIMARY KEY  (`userId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `my_aspnet_usersinroles`
--

LOCK TABLES `my_aspnet_usersinroles` WRITE;
/*!40000 ALTER TABLE `my_aspnet_usersinroles` DISABLE KEYS */;
INSERT INTO `my_aspnet_usersinroles` VALUES (1,1);
INSERT INTO `my_aspnet_usersinroles` VALUES (2,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (3,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (4,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (5,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (13,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (24,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (29,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (57,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (58,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (60,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (61,2);
INSERT INTO `my_aspnet_usersinroles` VALUES (62,2);
/*!40000 ALTER TABLE `my_aspnet_usersinroles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue` (
  `id` bigint(20) NOT NULL auto_increment,
  `number` varchar(255) NOT NULL,
  `wait_time` bigint(20) default NULL,
  `status` varchar(255) NOT NULL,
  `created_dt` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue`
--

LOCK TABLES `queue` WRITE;
/*!40000 ALTER TABLE `queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_version`
--

DROP TABLE IF EXISTS `schema_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) default NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY  (`version`),
  KEY `schema_version_vr_idx` (`version_rank`),
  KEY `schema_version_ir_idx` (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_version`
--

LOCK TABLES `schema_version` WRITE;
/*!40000 ALTER TABLE `schema_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `schema_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_answers`
--

DROP TABLE IF EXISTS `survey_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_answers` (
  `id` bigint(20) NOT NULL auto_increment,
  `answer` varchar(255) default NULL,
  `participant_id` bigint(20) default NULL,
  `question_id` bigint(20) default NULL,
  `created_dt` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_answers`
--

LOCK TABLES `survey_answers` WRITE;
/*!40000 ALTER TABLE `survey_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `survey_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_participant`
--

DROP TABLE IF EXISTS `survey_participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_participant` (
  `id` bigint(20) NOT NULL auto_increment,
  `first_name` varchar(255) default NULL,
  `last_name` varchar(255) default NULL,
  `survey_number` varchar(255) default NULL,
  `question_id` bigint(20) default NULL,
  `status` varchar(255) default 'NEW',
  `created_dt` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `survey_id` bigint(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_participant`
--

LOCK TABLES `survey_participant` WRITE;
/*!40000 ALTER TABLE `survey_participant` DISABLE KEYS */;
/*!40000 ALTER TABLE `survey_participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `survey_questions`
--

DROP TABLE IF EXISTS `survey_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `survey_questions` (
  `id` bigint(20) NOT NULL auto_increment,
  `question` varchar(255) default NULL,
  `survey_id` bigint(20) default NULL,
  `question_order` bigint(20) default NULL,
  `next_question_id` bigint(20) default NULL,
  `created_dt` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `survey_questions`
--

LOCK TABLES `survey_questions` WRITE;
/*!40000 ALTER TABLE `survey_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `survey_questions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-10 11:36:00
