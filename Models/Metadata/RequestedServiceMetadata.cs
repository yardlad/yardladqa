﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class RequestedServiceMetadata
    {
        [Display(Name = "Service Id")]
        public int ServiceId { get; set; }

        [Required(ErrorMessage = "please select a State")]
        [Display(Name = "State")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "please select an area")]
        [Display(Name = "Service Area")]
        public int RequestServiceAreaId { get; set; }
    }
}