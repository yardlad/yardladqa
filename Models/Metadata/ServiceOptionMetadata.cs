﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class ServiceOptionMetadata
    {
        [Display(Name = "Service Option Id")]
        public int ServiceOptionId { get; set; }

        [Display(Name = "Service")]
        [Required(ErrorMessage = "please select a service")]
        public int ServiceId { get; set; }

        public Nullable<int> Order { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ServiceOptionMetadata))]
    public partial class ServiceOption
    {
    
    }
}