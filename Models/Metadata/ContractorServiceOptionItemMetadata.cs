﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContractorServiceOptionItemMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Contractor Service Option Item Id")]
        public int ContractorServiceOptionItemId { get; set; }

        [Required(ErrorMessage = "please select a contractor service option")]
        [Display(Name = "Contractor Service Option")]
        public int ContractorServiceOptionId { get; set; }

        public Nullable<int> Order { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{0:c}")]
        [Required(ErrorMessage = "please enter a price")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContractorServiceOptionItemMetadata))]
    public partial class ContractorServiceOptionItem
    {

    }
}