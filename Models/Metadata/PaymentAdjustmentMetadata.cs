﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class PaymentAdjustmentMetadata
    {
        [Display(Name = "Payment Adjustment Id")]
        public int PaymentAdjustmentId { get; set; }

        [Display(Name = "Requested Service")]
        [Required(ErrorMessage = "please indicated the requested service this adjustment is being made for")]
        public int RequestedServiceId { get; set; }

        [Display(Name = "Transaction Id")]
        public Nullable<int> TransactionId { get; set; }

        [Required(ErrorMessage = "please enter a positive or negative amount for the adjustment")]
        public decimal Amount { get; set; }
        
        [Required(ErrorMessage = "please describe why the adjustment needs to be made")]
        public string Description { get; set; }

        [Display(Name = "Is Completed")]
        public bool IsCompleted { get; set; }
    }

    [MetadataType(typeof(PaymentAdjustmentMetadata))]
    public partial class PaymentAdjustment
    {
        
    }
}