﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YardLad.Models.Domain
{
    public class RatingMetadata
    {
        [Required(ErrorMessage = "please enter comments about the service")]
        public string Comments { get; set; }
    }

    [MetadataType(typeof(RatingMetadata))]
    public partial class Rating
    {
        
    }
}