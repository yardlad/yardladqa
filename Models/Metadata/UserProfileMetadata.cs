﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class UserProfileMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "User Id")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "please enter your first name")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "please enter your last name")]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "please enter a phone number")]
        public string Phone { get; set; }

        //[Required(ErrorMessage = "please enter a mobile number")]
        public string Mobile { get; set; }

        [Required(ErrorMessage = "please select a gender")]
        public string Gender { get; set; }

        //[Required(ErrorMessage = "please enter in a billing address")]
        [Display(Name = "Address")]
        public Nullable<int> AddressId { get; set; }

        [Display(Name = "SMS")]
        public Nullable<bool> AcceptSMS { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(UserProfileMetadata))]
    public partial class UserProfile
    {
        
    }
}