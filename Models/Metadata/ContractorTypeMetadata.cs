﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContractorTypeMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Contractor Type Id")]
        public int ContractorTypeId { get; set; }

        [Required(ErrorMessage = "please select a contractor type")]
        [Display(Name = "Contractor Type")]
        public string Type { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContractorTypeMetadata))]
    public partial class ContractorType
    {
        
    }
}