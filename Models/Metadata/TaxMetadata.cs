﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class TaxMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Tax Id")]
        public int TaxId { get; set; }

        [Required(ErrorMessage = "please select an address")]
        [Display(Name = "Address")]
        public int AddressId { get; set; }

        [Required(ErrorMessage = "please select a contractor")]
        [Display(Name = "Contractor")]
        public int ContractorId { get; set; }

        public Nullable<decimal> Rate { get; set; }
    }

    [MetadataType(typeof(TaxMetadata))]
    public partial class Tax
    {

    }
}