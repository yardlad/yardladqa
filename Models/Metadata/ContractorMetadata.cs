﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContractorMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Contractor Id")]
        public int ContractorId { get; set; }

        [Required(ErrorMessage = "please enter a contractor name")]
        [Display(Name = "Contractor Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please select a contractor type")]
        [Display(Name = "Contractor Type")]
        public int ContractorTypeId { get; set; }

        [Required(ErrorMessage = "please enter a business phone")]
        [Display(Name = "Business Phone")]
        public string BusinessPhone { get; set; }

        [Display(Name = "Service Area")]
        public Nullable<int> ServiceAreaId { get; set; }

        [Display(Name = "Qualification")]
        public Nullable<int> QualificationId { get; set; }

        [Required(ErrorMessage = "please indicate if this contractor is approved")]
        [Display(Name = "Is Approved?")]
        public bool IsApproved { get; set; }
        
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [Display(Name = "Approved On")]
        public Nullable<System.DateTime> ApprovedOn { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContractorMetadata))]
    public partial class Contractor
    {
        
    }
}