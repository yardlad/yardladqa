﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace YardLad.Models.Domain
{
    public class ContentMetadata
    {
        [HiddenInput(DisplayValue = false)]
        [Display(Name = "Content Id")]
        public int ContentId { get; set; }

        [Required(ErrorMessage = "please enter a section name")]
        [Display(Name = "Section Name")]
        public string SectionName { get; set; }

        [AllowHtml]
        [UIHint("tinymce_jquery_full")]
        [Display(Name = "Content Body")]
        public string ContentBody { get; set; }

        public string Source { get; set; }

        [Display(Name = "Image")]
        public Nullable<int> ImageId { get; set; }

        [Display(Name = "Yard Owner")]
        public string YardOwner { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ContentMetadata))]
    public partial class Content
    {
        
    }
}