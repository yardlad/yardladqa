﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class ServiceMetadata
    {
        [Display(Name = "Service Id")]
        public int ServiceId { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "please select a category")]
        [Display(Name = "Service Category")]
        public int ServiceCategoryId { get; set; }

        [Required(ErrorMessage = "please enter a base price")]
        [DisplayFormat(DataFormatString = "{0:c}")]
        [Display(Name = "Base Price")]
        public decimal BasePrice { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ServiceMetadata))]
    public partial class Service
    {
        
    }

}