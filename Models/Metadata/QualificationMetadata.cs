﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class QualificationMetadata
    {
        [Display(Name = "Qualification Id")]
        public int QualificationId { get; set; }

        [Required(ErrorMessage = "please make a selection")]
        public string Citizenship { get; set; }

        [Display(Name = "Filing Status")]
        [Required(ErrorMessage = "please make a selection")]
        public string FilingStatus { get; set; }

        [Display(Name = "Business Owner")]
        [Required(ErrorMessage = "please enter the name of the business owner")]
        public string BusinessOwner { get; set; }

        public string EIN { get; set; }

        [Display(Name = "Tax ID")]
        public string TaxId { get; set; }

        [Display(Name = "Address")]
        public Nullable<int> AddressId { get; set; }

        [Display(Name = "Tax Exemption")]
        public string TaxExemption { get; set; }

        [Display(Name = "Source")]
        [Required(ErrorMessage = "please upload your qualification")]
        public string Source { get; set; }

        [Display(Name = "File name")]
        public string FileName { get; set; }

        [Display(Name = "Extension")]
        public string MimeType { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        
    }

    [MetadataType(typeof(QualificationMetadata))]
    public partial class Qualification
    {
        
    }
}