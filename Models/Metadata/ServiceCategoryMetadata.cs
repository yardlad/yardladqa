﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace YardLad.Models.Domain
{
    public class ServiceCategoryMetadata
    {
        [Display(Name = "Service Category Id")]
        public int ServiceCategoryId { get; set; }

        [Required(ErrorMessage = "please enter a category name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please select an activation status")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }

    [MetadataType(typeof(ServiceCategoryMetadata))]
    public partial class ServiceCategory
    {
        
    }
}