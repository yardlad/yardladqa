﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Mail;

namespace YardLad.Models.Interfaces
{
    public interface IEmailSender
    {
        void SendEmail(string subject, string body, string from, string recipient);
    }
}
