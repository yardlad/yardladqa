﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class UserProfileDetailsViewModel
    {
        public UserProfile UserProfile { get; set; }
        public Address AddressId { get; set; }
    }

    public class UserProfileViewModel
    {
        public UserProfile UserProfile { get; set; }

        [Required]
        public string Area { get; set; }

        [Required]
        public string Prefix { get; set; }

        [Required]
        public string Suffix { get; set; }

        [Required]
        public string MobileArea { get; set; }

        [Required]
        public string MobilePrefix { get; set; }

        [Required]
        public string MobileSuffix { get; set; }
    }

    
}