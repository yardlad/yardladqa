﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ServiceViewModel
    {
        [Display(Name = "Service Id")]
        public int ServiceId { get; set; }

        [Required(ErrorMessage = "please enter a name")]
        public string Name { get; set; }

        [UIHint("multilinetext")]
        public string Description { get; set; }

        [Display(Name = "Base Price")]
        public decimal BasePrice { get; set; }

        [Display(Name = "Service Category")]
        [Required(ErrorMessage = "please select a category")]
        public int ServiceCategoryId { get; set; }

        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }

        [Display(Name = "Service options")]
        public List<ServiceOption> ServiceOptions { get; set; }
    }

    public class RequestServiceViewModel
    {
        [Required(ErrorMessage = "please select a state")]
        public int StateId { get; set; }

        [Required(ErrorMessage = "please select a service area")]
        public int ServiceAreaId { get; set; }

        [Required(ErrorMessage = "please select a service")]
        public int ServiceId { get; set; }

        [Required(ErrorMessage = "please indicate the items selected")]
        public string[] SelectedServiceOptionItemIds { get; set; }

        [Required(ErrorMessage = "please indicate the contractors available for the request")]
        public string[] AvailableContractorIds { get; set; }

        public State SelectedState { get; set; }
        public ServiceArea SelectedServiceArea { get; set; }
        public Service SelectedService { get; set; }
        public List<ServiceOption> SelectedServiceOptions { get; set; }
        public List<ServiceOptionItem> SelectedServiceOptionItems { get; set; }
        public List<Contractor> AvailableContractors { get; set; }

        public int SelectedContractorId { get; set; }
        public Contractor SelectedContractor { get; set; }

        public int SelectedContractorServiceId { get; set; }
        public ContractorService SelectedContractorService { get; set; }

        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal SelectedContractorTaxRate { get; set; }
        public decimal Total { get; set; }

        public bool UserIsLoggedIn { get; set; }
        public int UserAddressId { get; set; }
        public Address UserAddress { get; set; }
        public bool CreateCustomAddress { get; set; }
        public Address CustomAddress { get; set; }
    }

    public class SelectContractorViewModel
    {
        public int ServiceAreaId { get; set; }
        public ServiceArea SelectedServiceArea { get; set; }
        public int ServiceId { get; set; }
        public Service SelectedService { get; set; }
        public List<ServiceOption> ServiceOptions { get; set; }
        public List<ServiceOptionItem> ServiceOptionItems { get; set; }

        public List<Contractor> AvailableContractors { get; set; }

        public Contractor SelectedContractor { get; set; }
        public int ContractorTypeId { get; set; }
        public int ContractorServiceId { get; set; }
        public ContractorService SelectedContractorService { get; set; }
        public List<ContractorServiceOption> ContractorServiceOptions { get; set; }
        public List<ContractorServiceOptionItem> ContractorServiceOptionItems { get; set; }

        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal SelectedContractorTaxRate { get; set; }
        public decimal Total { get; set; }
    }
}