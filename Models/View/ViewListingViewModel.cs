﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ViewListingViewModel
    {
        public int ContractorId { get; set; }
        public Contractor Contractor { get; set; }
        public List<ContractorService> ContractorServices { get; set; }
        public List<ContractorServiceOption> ContractorServiceOptions { get; set; }
        public List<ContractorServiceOptionItem> ContractorServiceOptionItems { get; set; }
        public List<Service> Services { get; set; }
        public List<ServiceArea> ServiceAreas { get; set; }

        public decimal TaxRate { get; set; }

        public int StateId { get; set; }
        public int ServiceAreaId { get; set; }
        public int ServiceId { get; set; }
    }
}