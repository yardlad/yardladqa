﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ServiceOptionCreateViewModel
    {
        public int ServiceId { get; set; }
        public Service Service { get; set; }
        public List<ServiceOption> ServiceOptions { get; set; }
    }
}