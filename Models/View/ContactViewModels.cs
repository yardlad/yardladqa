﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YardLad.Models.View
{
    public class ContactViewModel
    {
        [Required(ErrorMessage = "please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "please enter your email address")]
        public string Email { get; set; }

        [Required]
        public string Area { get; set; }

        [Required]
        public string Prefix { get; set; }

        [Required]
        public string Suffix { get; set; }

        public string Phone { get; set; }

        [Required(ErrorMessage = "please enter a subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "please enter a message")]
        [UIHint("MultilineText")]
        public string Message { get; set; }
    }

    public class ServiceProviderContactViewModel
    {
        [Display(Name = "Your name")]
        [Required(ErrorMessage = "please enter your name")]
        public string Name { get; set; }

        [Display(Name = "Your email address")]
        [Required(ErrorMessage = "please enter your email")]
        public string Email { get; set; }

        [Display(Name = "Type of business")]
        [Required(ErrorMessage = "please make a selection")]
        [Range(1, 5, ErrorMessage = "please make a selection")]
        public string TypeOfBusiness { get; set; }

        [Display(Name = "Business name")]
        [Required(ErrorMessage = "please enter your business name")]
        public string BusinessName { get; set; }

        [Display(Name = "Business phone")]
        [Required(ErrorMessage = "please enter your business phone")]
        public string BusinessPhone { get; set; }

        [Required(ErrorMessage = "please enter a subject")]
        public string Subject { get; set; }

        [UIHint("MultilineText")]
        [Required(ErrorMessage = "please enter a message")]
        public string Message { get; set; }

        [Required(ErrorMessage = "you must accept the terms before submitting")]
        public bool TermsAccepted { get; set; }
    }

    public class ContactAdminViewModel
    {
        [Display(Name = "Your name")]
        [Required(ErrorMessage = "please enter your name")]
        public string Name { get; set; }

        [Display(Name = "Your email address")]
        [Required(ErrorMessage = "please enter your email")]
        public string Email { get; set; }

        [Display(Name = "Type of business")]
        public string TypeOfBusiness { get; set; }

        [Display(Name = "Business name")]
        public string BusinessName { get; set; }

        [Display(Name = "Business phone")]
        public string BusinessPhone { get; set; }

        [Required(ErrorMessage = "please enter a subject")]
        public string Subject { get; set; }

        [UIHint("MultilineText")]
        [Required(ErrorMessage = "please enter a message")]
        public string Message { get; set; }

        public bool IsContractor { get; set; }

        public string[] Roles { get; set; }

        public string ReturnUrl { get; set; }

    }
}