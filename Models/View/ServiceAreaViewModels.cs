﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YardLad.Models.Domain;

namespace YardLad.Models.View
{
    public class ServiceAreaEditViewModel
    {
        public Contractor Contractor { get; set; }
        public int ContractorId { get; set; }
        public int StateId { get; set; }
        public System.Nullable<int> ServiceAreaId { get; set; }
    }
}