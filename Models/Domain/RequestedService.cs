//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YardLad.Models.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class RequestedService
    {
        public RequestedService()
        {
            this.Payments = new HashSet<Payment>();
            this.Payments1 = new HashSet<Payment>();
            this.PaymentAdjustments = new HashSet<PaymentAdjustment>();
            this.PaymentAdjustments1 = new HashSet<PaymentAdjustment>();
            this.RequestedServiceOptions = new HashSet<RequestedServiceOption>();
            this.Surveys = new HashSet<Survey>();
            this.RequestedServiceOptions1 = new HashSet<RequestedServiceOption>();
            this.Surveys1 = new HashSet<Survey>();
        }
    
        public int RequestedServiceId { get; set; }
        public int UserId { get; set; }
        public int ContractorId { get; set; }
        public int ContractorServiceId { get; set; }
        public System.DateTime DateRequestMade { get; set; }
        public Nullable<System.DateTime> RequestedStartDate { get; set; }
        public Nullable<System.DateTime> RequestedEndDate { get; set; }
        public int AddressId { get; set; }
        public int Recurring { get; set; }
        public string ContractorComment { get; set; }
        public string AdminComment { get; set; }
        public bool IsCompleted { get; set; }
        public Nullable<System.DateTime> DateCompleted { get; set; }
        public Nullable<int> RatingId { get; set; }
        public bool IsActive { get; set; }
    
        public virtual Contractor Contractor { get; set; }
        public virtual Contractor Contractor1 { get; set; }
        public virtual ContractorService ContractorService { get; set; }
        public virtual ContractorService ContractorService1 { get; set; }
        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Payment> Payments1 { get; set; }
        public virtual ICollection<PaymentAdjustment> PaymentAdjustments { get; set; }
        public virtual ICollection<PaymentAdjustment> PaymentAdjustments1 { get; set; }
        public virtual Rating Rating { get; set; }
        public virtual Rating Rating1 { get; set; }
        public virtual ICollection<RequestedServiceOption> RequestedServiceOptions { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual ICollection<Survey> Surveys { get; set; }
        public virtual UserProfile UserProfile1 { get; set; }
        public virtual ICollection<RequestedServiceOption> RequestedServiceOptions1 { get; set; }
        public virtual ICollection<Survey> Surveys1 { get; set; }
    }
}
