//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace YardLad.Models.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class ServiceOptionItemRequest
    {
        public int ServiceOptionItemRequestId { get; set; }
        public Nullable<int> ServiceOptionRequestId { get; set; }
        public int ContractorId { get; set; }
        public Nullable<int> Order { get; set; }
        public string ServiceName { get; set; }
        public string OptionName { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public System.DateTime DateRequested { get; set; }
        public bool IsActive { get; set; }
    
        public virtual Contractor Contractor { get; set; }
        public virtual Contractor Contractor1 { get; set; }
        public virtual ServiceOptionRequest ServiceOptionRequest { get; set; }
        public virtual ServiceOptionRequest ServiceOptionRequest1 { get; set; }
    }
}
